﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QMDAdminActions.ascx.cs" Inherits="QMDDashboard_TPL.QMDAdminActions.QMDAdminActions" %>
<table style="width:45%">
    <tr>
        <td>
            <asp:Label ID="lblError" runat="server" ></asp:Label>
        </td>
        
    </tr>
    <tr>
        <td>
            <div>
                <table style="width:100%">
                    <tr>
                        <td>
                            <asp:RadioButton ID="rdbNew" Text="Add project" OnCheckedChanged="rdbNew_CheckedChanged" AutoPostBack="true" GroupName="FQE" runat="server" />
                            <asp:RadioButton ID="rdbChangeFQE" Text="Change FQE" OnCheckedChanged="rdbChangeFQE_CheckedChanged" AutoPostBack="true" GroupName="FQE" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="dvNewProject" runat="server">
                <table style="width:100%">
                    <tr id="trSBU" runat="server">
                        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
                            <asp:Label ID="Label1" ForeColor="White" runat="server" Text="SBU"></asp:Label>
                        </td>
                        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
                            <asp:DropDownList ID="ddlSBU" runat="server" OnSelectedIndexChanged="ddlSBU_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trProject" runat="server">
                        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
                            <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Project"></asp:Label>
                        </td>
                        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
                            <asp:DropDownList ID="ddlProjects" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProjects_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                     <tr id="trTemplate" runat="server">
                        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
                            <asp:Label ID="lbltemplate" ForeColor="White" runat="server" Text="Template"></asp:Label>
                        </td>
                        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
                            <asp:DropDownList ID="ddltemplate" runat="server" AutoPostBack="true" ></asp:DropDownList>
                        </td>
                    </tr>                   
                    <tr id="trUser" runat="server">
                        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
                            <asp:Label ID="Label2" ForeColor="White" runat="server" Text="Pick User"></asp:Label>
                        </td>
                        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
                        <%--   <SharePoint:PeopleEditor runat="server" ID="FQE" MultiSelect="false" />--%>
                          <%--  <SharePoint:PeopleEditor ID="ppeEmpName" runat="server" MultiSelect="false" SelectionSet="User" />--%>

                             <SharePoint:PeopleEditor ID="ppeEmpName" runat="server" AllowEmpty="true" MultiSelect="false" SelectionSet="User" ValidatorEnabled="true" MaximumEntities="1" />    
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <div  id="dvModifyFQE" runat="server">
                <table style="width:100%">
                    <tr id="trSBUFQE" runat="server">
                        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
                            <asp:Label ID="Label4" ForeColor="White" runat="server" Text="SBU"></asp:Label>
                        </td>
                        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
                            <asp:DropDownList ID="ddlSBUEdit" runat="server" OnSelectedIndexChanged="ddlSBUEdit_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trProjectFQE" runat="server">
                        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
                            <asp:Label ID="Label5" ForeColor="White" runat="server" Text="Project"></asp:Label>
                        </td>
                        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
                            <asp:DropDownList ID="ddlProjectFQEEdit" OnSelectedIndexChanged="ddlProjectFQEEdit_SelectedIndexChanged" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trEditTemplate" runat="server">
                         <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
                            <asp:Label ID="lblEditTemplate" ForeColor="White" runat="server" Text="Template"></asp:Label>
                        </td>
                        <td  style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
                            <asp:DropDownList ID="ddlEditTemplate" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEditTemplate_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trNameFQE" runat="server">
                        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
                            <asp:Label ID="Label7" ForeColor="White" runat="server" Text="Existing FQE"></asp:Label>
                        </td>
                        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
                            <asp:Label ID="lblExistingFQE" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trFQEModify" runat="server">
                        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
                            <asp:Label ID="Label6" ForeColor="White" runat="server" Text="Pick User"></asp:Label>
                        </td>
                        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
                           <SharePoint:PeopleEditor runat="server" ID="peFQE" MultiSelect="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnEdit" runat="server" Text="Update" OnClick="btnEdit_Click" />
                        </td>
                        <td>
                            <asp:Button ID="Button2" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
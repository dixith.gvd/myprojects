﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HousekeepingActions.ascx.cs" Inherits="SafetyAutomation.HousekeepingActions.HousekeepingActions" %>

<style type="text/css">
    .required {
        color: Red;
    }
    .auto-style1 {
        height: 30px;
        width: 27%;
    }
    .auto-style2 {
        width: 245px;
    }
    .auto-style3 {
        width: 246px;
    }
    .auto-style4 {
        width: 412px;
    }
</style>
<script>
    function ReloadFun() {
        _spFormOnSubmitCalled = false; _spSuppressFormOnSubmitWrapper = true;
    }
</script>

<div id="dvFilledForm" runat="server" visible="false">
    <table border="1" cellpadding="1" cellspacing="1" width="100%">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label12" runat="server" ForeColor="White" Text="Safety Compliance" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label11" ForeColor="White" runat="server" Text="Current Stage" CssClass="auto-style2"></asp:Label>
                </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblcurstage" runat="server" CssClass="auto-style2"></asp:Label>
                </td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="lblTID" ForeColor="White" runat="server" Text="Frequency" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblfrequency" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label4" ForeColor="White" runat="server" Text="Checklist" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblchecklist" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label17" ForeColor="White" runat="server" Text="BU"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblBU" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label18" ForeColor="White" runat="server" Text="Project"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblProject" runat="server"></asp:Label>

            </td>

        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label6" ForeColor="White" runat="server" Text="Inspection Date" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblinsdate" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label14" ForeColor="White" runat="server" Text="Compliance"></asp:Label>
            </td>

        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="Both" Visible="false">
                    <Columns>
                        <%--<asp:BoundField DataField="SNo" HeaderText="S No" HeaderStyle-HorizontalAlign="Left" />--%>
                        <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%"> <HeaderStyle Width="10%" /> </asp:BoundField>
                        <asp:BoundField DataField="isCompliant" HeaderText="Is Housekeeping Proper?" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%"> <HeaderStyle Width="10%" /> </asp:BoundField>
                        <asp:BoundField DataField="SORemarks" HeaderText="SO Remarks" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="20%"> <HeaderStyle Width="20%" /> </asp:BoundField>
                        <asp:TemplateField HeaderText="Photo evidence Uploaded" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Width="95%" CausesValidation="false" CommandName="DownLoad NCR" OnClientClick="ReloadFun()" ID="lnkView" Text='<%# Eval("PicEvidence_DocName") %>' OnClick="lnkView_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Corrective Action Photo evidence Uploaded" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Width="95%" CausesValidation="false" CommandName="DownLoad NCR" OnClientClick="ReloadFun()" ID="lnkView1" Text='<%# Eval("CorrEvidence_DocName") %>' OnClick="lnkView1_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:BoundField DataField="Correction_Details" HeaderText="Corrective Action Details" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="20%"> <HeaderStyle Width="20%" /> </asp:BoundField>
                    </Columns>
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </td>
            </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="grdCompliances" runat="server" AutoGenerateColumns="false" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="Both">
                    <Columns>
                        <%--<asp:BoundField DataField="SNo" HeaderText="S No" HeaderStyle-HorizontalAlign="Left" />--%>
                        <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:BoundField DataField="isCompliant" HeaderText="Is Housekeeping Proper?" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:BoundField DataField="SORemarks" HeaderText="SO Remarks" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="20%" />
                        <asp:TemplateField HeaderText="Photo evidence Uploaded" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Width="95%" CausesValidation="false" CommandName="DownLoad NCR" OnClientClick="ReloadFun()" ID="lnkView" Text='<%# Eval("PicEvidence_DocName") %>' OnClick="lnkView_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Corrective Action photo evidence(with date and time)" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:FileUpload ID="uploadCorrectiveEvidence" runat="server" Width="95%" Enabled="false" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.png|.jpg|.jpeg|.gif|.xls|.xlsx)$"
                                    ControlToValidate="uploadCorrectiveEvidence" runat="server" ForeColor="Red" ErrorMessage="Please select a valid word,excel,jpg,jpeg,png,gif or pdf file format."
                                    Display="Dynamic" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Corrective Action Details" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:TextBox ID="txtActionDetails" TextMode="MultiLine" runat="server" Width="95%" Enabled="false"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </td>
        </tr>
        <tr>

            <td colspan="2" style="text-align: center;">
                <%-- <asp:LinkButton ID="lnkSWI" runat="server" Font-Bold="true" onclick="lnkSWI_Click" >View SWI Form</asp:LinkButton>--%>
                <%--<asp:HyperLink ID="hplRWC" runat="server" Visible="false" Target="_blank">View RWC Form</asp:HyperLink>--%>
            </td>
        </tr>
    </table>
</div>
</br>
<div id="dvRCM" runat="server" visible="false">
    <table style="width: 80%" border="1">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label5" runat="server" ForeColor="White" Text="RCM Actions" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
    </table>
    <table runat="server" id="petblRemarks" style="width: 80%" border="1">
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label2" ForeColor="White" runat="server" ToolTip="Enter TPLID" Text="Assignee Name"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                <%--<SharePoint:PeopleEditor ID="ppAssigneeName" runat="server" AllowEmpty="true" MultiSelect="false" SelectionSet="User" ValidatorEnabled="true" MaximumEntities="1" />--%>
                <asp:Label ID="lblselectAssignee" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label1" ForeColor="White" runat="server" Text="Target Closure Date"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                <SharePoint:DateTimeControl ID="dtTargetClosureDate" DateOnly="true" runat="server" />

                <asp:Label ID="lblTargetClosureDate" Text="Please select a date greater than or equal to today." runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Remarks"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                <asp:TextBox ID="txtRCMRemarks" runat="server" TextMode="MultiLine" Width="90%"></asp:TextBox>
            </td>
        </tr>
    </table>

</div>
<div id="RCMSubmit" runat="server" visible="false">
    <table style="width: 80%">
        <tr>
            <td style="text-align: right">
                <asp:Button ID="btnRCMActions" Font-Bold="true" runat="server" Text="Submit" OnClick="btnRCMActions_Click" />
            </td>
        </tr>
    </table>
    <br />
</div>
<div id="SOSubmit" runat="server" visible="false">
    <table style="width: 80%">
        <tr>
            <td style="text-align:right" class="auto-style4">
                <asp:Button ID="btnSOActions" Font-Size="Medium" runat="server" Text="Save" OnClick="btnSOActions_Click" Width="140px" />
            </td>
            <td style="text-align:left">
                <asp:Button ID="btnSOSubmit" Font-Size="Medium" runat="server" Text="Send for Closure" OnClick="btnSOSubmit_Click" Width="140px" />
            </td>
        </tr>
    </table>
    <br />
</div>
<div id="dvRCMClose" runat="server" visible="false">
    <table style="width: 80%" border="1">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label10" runat="server" ForeColor="White" Text="RCM Closure Actions" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label111" ForeColor="White" runat="server" Text="Close Checklist"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <%--<asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="txtverifi_Remarks" ErrorMessage="Enter Remarks." CssClass="required" Display="Dynamic" />--%>
                <asp:CheckBox ID="cbClose" runat="server" />
            </td>
        </tr>

        <tr>
            <td colspan="2" style="text-align: right">
                <asp:Button ID="btnClosure" Font-Bold="true" runat="server" Text="Submit" OnClick="btnClosure_Click" />
            </td>
        </tr>
    </table>
</div>
<div id="dvUnAuth" runat="server" visible="false">
    <asp:Label ID="Label7" runat="server" ForeColor="Red" Font-Size="Large" Text="You are not authorized to access" CssClass="auto-style2"></asp:Label>
</div>
    <table>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblSOSubNotification" runat="server" CssClass="auto-style2" Font-Bold="true" Font-Size="Medium" ForeColor="Blue"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblRCMSubNotification" runat="server" CssClass="auto-style2" Font-Bold="true" Font-Size="Medium" ForeColor="Blue"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblAsigneeSubNotification" runat="server" CssClass="auto-style2" Font-Bold="true" Font-Size="Medium" ForeColor="Blue"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblLinkClosureNotification" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>
    </table>
<asp:Label ID="lblProjectCode" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblTransactionID" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblRCMID" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblRCMName" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblRCMEmail" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblPMID" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblPMName" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblPMEmail" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblAssigneID" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblAssigneName" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblAssigneEmail" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblbuheadid" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblbuheadName" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblbuheadEmail" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblHead" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblHeadID" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblHeadEmail" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblSOName" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblSOID" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblSOEmail" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblPE" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblPEID" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblPEEmail" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblNewSBU" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
        <asp:Label ID="lblInduCoE" runat="server" Visible="false"></asp:Label>
﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.SharePoint;
using System.IO;
using System.Collections.Specialized;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
namespace SiteSafetyUploads.QualityUploadsPendingItems
{
    [ToolboxItemAttribute(false)]
    public partial class QualityUploadsPendingItems : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public QualityUploadsPendingItems()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        [WebBrowsable(true), Category("Custom Properties"), Personalizable(PersonalizationScope.Shared), WebDisplayName("Role"), WebDescription("Role")]
        public string Role
        {
            get;
            set;
        }
        [WebBrowsable(true), Category("Custom Properties"), Personalizable(PersonalizationScope.Shared), WebDisplayName("Stage"), WebDescription("Stage")]
        public string Stage
        {
            get;
            set;
        }
        Uploads objup = new Uploads();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                if (IsDataValidAtCustomProperties())
                {
                    DataTable dt = new DataTable();
                    dt = GetValuesAndSetToControls();
                    if (dt != null)
                    {
                        DataView dv = new DataView();
                        dv = dt.DefaultView;
                        dv.Sort = "Transaction_ID" + " DESC";
                        grdPendingsQuality.DataSource = dv;
                        grdPendingsQuality.DataBind();
                    }
                    else
                    {
                        DataTable dtdata = new DataTable();
                        grdPendingsQuality.DataSource = dtdata;
                        grdPendingsQuality.DataBind();
                    }
                }
                else
                {
                    lblError.Text = "Please set the custom properties";
                }

            }
        }
        private DataTable GetValuesAndSetToControls()
        {
            string projectCode = string.Empty;
            if (this.Role == "FQE")
            {
                DataTable projectCodeItem = objup.GetProjectCodeByLoggedInUserFQE();
                if (projectCodeItem != null)
                {
                    foreach (DataRow row in projectCodeItem.Rows)
                    {
                        projectCode = projectCode + "," + row["Project_x0020_Code"].ToString();
                    }
                    DataTable dt = new DataTable();
                    string Email = string.Empty;
                    if (this.Stage == "Active")
                    {
                        DataTable dtdataSO = objup.GetQualityUploadsPendingItems(projectCode.TrimStart(','), "Active");
                        if (dtdataSO.Rows.Count > 0)
                        {
                            grdPendingsQuality.DataSource = dtdataSO;
                            grdPendingsQuality.DataBind();
                        }
                    }
                    else if (this.Stage == "Closed")
                    {
                        DataTable dtdataSO = objup.GetQualityUploadsPendingItems(projectCode.TrimStart(','), "Closed");
                        if (dtdataSO.Rows.Count > 0)
                        {
                            grdPendingsQuality.DataSource = dtdataSO;
                            grdPendingsQuality.DataBind();
                        }
                    }
                }
            }
            else if (this.Role == "RCM")
            {
                DataTable projectCodeItem = objup.GetProjectCodeByLoggedInUserRCM();
                if (projectCodeItem != null)
                {
                    foreach (DataRow row in projectCodeItem.Rows)
                    {
                        projectCode = projectCode + "," + row["Project_x0020_Code"].ToString();
                    }
                    if (this.Stage == "Active")
                    {
                        DataTable dtdataSO = objup.GetQualityUploadsPendingItems(projectCode.TrimStart(','), "Active");
                        if (dtdataSO.Rows.Count > 0)
                        {
                            grdPendingsQuality.DataSource = dtdataSO;
                            grdPendingsQuality.DataBind();
                        }
                    }
                    else if (this.Stage == "Closed")
                    {
                        DataTable dtdataSO = objup.GetQualityUploadsPendingItems(projectCode.TrimStart(','), "Closed");
                        if (dtdataSO.Rows.Count > 0)
                        {
                            grdPendingsQuality.DataSource = dtdataSO;
                            grdPendingsQuality.DataBind();
                        }
                    }
                }
            }
            return grdPendingsQuality.DataSource as DataTable;
        }
        bool IsDataValidAtCustomProperties()
        {
            bool IsValidData = false;
            try
            {
                string ln = this.Role;
                string st = this.Stage;
                if (!string.IsNullOrEmpty(ln) && !string.IsNullOrEmpty(st))
                {
                    IsValidData = true;
                }
            }
            catch (Exception exp)
            {
                this.Page.Response.Write("Please set the custom properties");
            }
            return IsValidData;
        }
        protected void grdPendingsQuality_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            grdPendingsQuality.PageIndex = e.NewPageIndex;
            DataTable dt = new DataTable();
            dt = GetValuesAndSetToControls();
            DataView dv = new DataView();
            dv = dt.DefaultView;
            dv.Sort = "Transaction_ID" + " DESC";
            grdPendingsQuality.DataSource = dv;
            grdPendingsQuality.DataBind();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security;
namespace TPLAssetInfoServiceInstaller
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var pass = new SecureString();
            pass.AppendChar('l');
            pass.AppendChar('p');
            pass.AppendChar('t');
            pass.AppendChar('$');
            pass.AppendChar('3');
            pass.AppendChar('2');
            pass.AppendChar('1');
            //pass.AppendChar('3');
                        
            string jobName = "TPLAssetInfo";
            //string exeuninstall = @"C:\Windows\AssetInfo\WindowsService1.exe";
            
            
            var psi = new System.Diagnostics.ProcessStartInfo();
            
            psi.RedirectStandardOutput = true;
            psi.UseShellExecute = false;
            //psi.UserName = "spuatadmin";
            //psi.Password = "lpt$321";
            psi.Password = pass;
            //psi.Domain = "TPL";
            psi.CreateNoWindow = true; //This hides the dos-style black window that the command prompt usually shows
            psi.FileName = @"cmd.exe";
            psi.Verb = "runas"; //This is what actually runs the command as administrator
            psi.Arguments = "/C " + @"C:\AssetInfo\InstallService.bat";
                       
                //C:\Windows\System32\net.exe
            
            try
            {
                var process = new System.Diagnostics.Process();
                process.StartInfo = psi;
                process.Start();
                string result = process.StandardOutput.ReadToEnd();
                Console.WriteLine(result);
                Console.Read();
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                //If you are here the user clicked decline to grant admin privileges (or he's not administrator)
                Console.WriteLine("error is :" + ex.Message.ToString());
                Console.Read();
            }
            
        }
    }
}

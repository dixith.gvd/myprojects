﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.SharePoint;
using System.IO;
using System.Collections.Specialized;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
namespace QMDDashboard_TPL.ChecklistsModule.QualitySelfCertPendingItems
{
    [ToolboxItemAttribute(false)]
    public partial class QualitySelfCertPendingItems : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public QualitySelfCertPendingItems()
        {
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        QualityChecklists objQuality = new QualityChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                DataTable dt = new DataTable();
                dt = GetValuesAndSetToControls();
                if (dt != null)
                {
                    DataView dv = new DataView();
                    dv = dt.DefaultView;
                    dv.Sort = "ID" + " DESC";
                    grdPendingsSafety.DataSource = dv;
                    grdPendingsSafety.DataBind();
                }
                else
                {
                    DataTable dtdata = new DataTable();
                    grdPendingsSafety.DataSource = dtdata;
                    grdPendingsSafety.DataBind();
                }
            }
        }
        private DataTable GetValuesAndSetToControls()
        {
            string projectCode = string.Empty;
            DataTable projectCodeItem = objQuality.GetProjectCodeByLoggedInUserRCM();
            if (projectCodeItem != null)
            {
                foreach (DataRow row in projectCodeItem.Rows)
                {
                    projectCode = projectCode + "," + row["Project_x0020_Code"].ToString();
                }
                DataTable dtdata = objQuality.GetQualitySelfCertPendingItemsRCM(projectCode.TrimStart(','));
                if (dtdata.Rows.Count > 0)
                {
                    grdPendingsSafety.DataSource = dtdata;
                    grdPendingsSafety.DataBind();
                }
            }
            return grdPendingsSafety.DataSource as DataTable;
        }
        protected void grdPendingsSafety_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPendingsSafety.PageIndex = e.NewPageIndex;
            DataTable dt = new DataTable();
            dt = GetValuesAndSetToControls();
            DataView dv = new DataView();
            dv = dt.DefaultView;
            dv.Sort = "ID" + " DESC";
            grdPendingsSafety.DataSource = dv;
            grdPendingsSafety.DataBind();
        }
    }
}

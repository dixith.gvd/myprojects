﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Xml;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Net.Mail;
namespace EmpDataWebAPICall
{
    class Program
    {
        static string connectionString = ConfigurationManager.AppSettings["ConnectionStringSql"].ToString();
        public static string firstname = string.Empty; public static string lastname = string.Empty; public static string middlename = string.Empty;
        public static string title = string.Empty; public static string gender = string.Empty; public static string doj = string.Empty;
        public static string dob = string.Empty; public static string mobile = string.Empty; public static string email = string.Empty; public static string empid = string.Empty;
        public static string lastday = string.Empty; public static string sbg = string.Empty; public static string sbu = string.Empty; public static string bu = string.Empty;
        public static string location = string.Empty; public static string state = string.Empty; public static string grade = string.Empty; public static string designation = string.Empty;
        public static string parentdept = string.Empty; public static string managername = string.Empty; public static string managerempid = string.Empty;
        public static string lastdate = string.Empty; public static string createdDate = string.Empty; public static string updtionDate = string.Empty;
        public static string movementtype = string.Empty; public static string activestatus = string.Empty; public static string dept = string.Empty;
        static void Main(string[] args)
        {
            XmlReader xmlFile = null; string xmlPath = string.Empty;
            try
            {

                string todate = DateTime.Today.ToString("dd-MMM-yyyy");
                string fromdate = DateTime.Today.AddDays(-1).ToString("dd-MMM-yyyy");
                Uri url = new Uri("https://www.tplhronline.com/api/fhremp/GetAllMovement");
                //string url = ""; // Just a sample url
                WebClient wc = new WebClient();
                //wc.QueryString.Add("AppID", "FHRADMIN");
                //wc.QueryString.Add("Username", "FHRUSER");
                //wc.QueryString.Add("Password", "NdUsWmiEPsLgdMcZsr");

                //wc.QueryString.Add("Empid", null);
                //wc.QueryString.Add("FromDate", "01-Jan-2000");
                //wc.QueryString.Add("ToDate", ydate);
                //wc.QueryString.Add("Basic", "VGVwTmRGaUpUS2JzWTpCZkZLYmVoRVlzS1NsTkQ=");
                wc.Headers.Add(HttpRequestHeader.Authorization, "Basic " + "R2hZVFVqaGdmVFlVSDpBSFVzaGR5dWFzamtqc0hZ");
                wc.Headers[HttpRequestHeader.ContentType] = "application/json; charset=utf-8";
                //wc.Encoding = Encoding.UTF8;
                //wc.Headers.Add("ContentType", "text/json");

                //var data = wc.UploadValues(url, "POST", wc.QueryString);
                //var json = "{\"AppId\":\"FHRADMIN\",\"Username\":\"FHRUSER\",\"Password\":\"NdUsWmiEPsLgdMcZsr\",\"FromDate\":\"" + ydate + "\",\"ToDate\":\"" + ydate + "\"}";

                var json = "{\"AppID\":\"FHRADMIN\",\"Username\":\"FHRUSER\",\"Password\":\"get90All98mov10ment\",\"FromDate\":\"" + fromdate + "\",\"ToDate\":\"" + todate + "\"}";

                var responseString = wc.UploadString(url, json);

                DataTable dt = (DataTable)JsonConvert.DeserializeObject(responseString, (typeof(DataTable)));

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        InsertIntoSqlTable(row);
                    }
                    SendMail("Record inserted", "Employee data added to the table - TPLEmployeeMaster_08032021. Number of records inserted : " + dt.Rows.Count);
                }
                else
                {
                    SendMail("No records found", "There are no records found to insert in the table - TPLEmployeeMaster_08032021 for the date : " + todate);
                }

            }
            catch (Exception ex)
            {
                ReminderBackupLog(ex.Message + " : Error while connecting to web service");
                SendMail("Error while connecting to web service", ex.Message.ToString());
            }
        }

        public static void InsertIntoSqlTable(DataRow row)
        {
            firstname = string.Empty; lastname = string.Empty; middlename = string.Empty; title = string.Empty; gender = string.Empty;
            doj = string.Empty; dob = string.Empty; mobile = string.Empty; email = string.Empty; empid = string.Empty;
            lastday = string.Empty; sbg = string.Empty; sbu = string.Empty; bu = string.Empty;
            location = string.Empty; state = string.Empty; grade = string.Empty; designation = string.Empty;
            parentdept = string.Empty; managername = string.Empty; managerempid = string.Empty;
            lastdate = string.Empty; createdDate = string.Empty; updtionDate = string.Empty;
            movementtype = string.Empty; activestatus = string.Empty; dept = string.Empty;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        conn.Open();
                        //for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        //{

                        empid = row["TPLEmpID"].ToString();

                        firstname = row["FirstName"].ToString();

                        DateTime dttoday = DateTime.Now;
                        createdDate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dttoday);
                        middlename = row["MiddleName"].ToString();
                        //if (middlename != "")
                        //{
                        //    firstname = firstname + " " + middlename;
                        //}
                        lastname = row["LastName"].ToString();
                        //doj = ds.ItemArray[4].ToString();
                        //if (!string.IsNullOrEmpty(doj))
                        //{
                        //    DateTime dtdoj = Convert.ToDateTime(doj);
                        //    doj = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dtdoj);
                        //}
                        lastday = row["LastWorkingDay"].ToString();
                        if (!string.IsNullOrEmpty(lastday))
                        {
                            DateTime dtlastday = Convert.ToDateTime(lastday);
                            lastday = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dtlastday);
                        }
                        title = row["Title"].ToString();
                        //if (title.Trim() == "Ms")
                        //{
                        //    title = "Miss";
                        //}
                        gender = row["Gender"].ToString();
                        if (gender == "M")
                        {
                            gender = "Male";
                        }
                        else
                        {
                            gender = "Female";
                        }
                        dob = row["DOB"].ToString();
                        if (!string.IsNullOrEmpty(dob))
                        {
                            DateTime dtdob = Convert.ToDateTime(dob);
                            dob = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dtdob);
                        }
                        doj = row["DOJ"].ToString();
                        if (!string.IsNullOrEmpty(doj))
                        {
                            DateTime dtdoj = Convert.ToDateTime(doj);
                            doj = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dtdoj);
                        }
                        mobile = row["MobileNumber"].ToString();
                        email = row["Emailid"].ToString();
                        sbg = row["SBG"].ToString();
                        sbu = row["SBU"].ToString();
                        //sbu = dob;
                        bu = row["BU"].ToString();
                        location = row["Location"].ToString();
                        state = row["State"].ToString();
                        grade = row["Grade"].ToString();
                        designation = row["Designation"].ToString();
                        parentdept = row["ParentDepartment"].ToString();
                        dept = row["Department"].ToString();
                        managerempid = row["ManagerEmpid"].ToString();
                        //managername = ds.ItemArray[21].ToString();
                        //role = "Manager";
                        activestatus = row["ActiveStatus"].ToString();
                        movementtype = row["MovementType"].ToString();
                        lastdate = row["LastupdatedDate"].ToString();
                        if (!string.IsNullOrEmpty(lastdate))
                        {
                            DateTime dtlastdate = Convert.ToDateTime(lastdate);
                            lastdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dtlastdate);
                        }
                        DataTable dt = CheckIfExists(empid);
                        if (dt.Rows.Count > 0)
                        {
                            UpdateEmpData(empid);
                        }
                        else
                        {
                            cmd.CommandText = "INSERT INTO TPLEmployeeMaster_08032021 (FirstName,MiddleName,LastName,Title,Gender,DOB,DOJ,MobileNumber,Emailid,TPLEmpID,LastWorkingDay,SBG,SBU,BU,Location,State,Grade,Designation,ParentDepartment,Department,ManagerEmpid,ActiveStatus,MovementType,LastupdatedDate,CreatedDate,ModifiedDate) VALUES (@FirstName,@MiddleName,@LastName,@Title,@Gender,@DOB,@DOJ,@MobileNumber,@Emailid,@TplEmpID,@LastWorkingDay,@SBG,@SBU,@BU,@Location,@State,@Grade,@Designation,@ParentDepartment,@Department,@ManagerEmpid,@ActiveStatus,@MovementType,@LastUpdatedDate,@Created_Date,@Modified_Date)";
                            cmd.Parameters.Add("@DOB", SqlDbType.DateTime);
                            cmd.Parameters.Add("@LastWorkingDay", SqlDbType.DateTime);
                            cmd.Parameters.Add("@DOJ", SqlDbType.DateTime);
                            cmd.Parameters.Add("@LastUpdatedDate", SqlDbType.DateTime);

                            cmd.Parameters.AddWithValue("@TplEmpID", empid);
                            cmd.Parameters.AddWithValue("@FirstName", firstname);
                            cmd.Parameters.AddWithValue("@MiddleName", middlename);
                            //cmd.Parameters.AddWithValue("@DOB", dob);
                            cmd.Parameters.AddWithValue("@LastName", lastname);
                            cmd.Parameters.AddWithValue("@Title", title);
                            cmd.Parameters.AddWithValue("@Gender", gender);
                            //cmd.Parameters.AddWithValue("@DateofGroupJoin", datejoin);
                            //cmd.Parameters.AddWithValue("@DOJ", doj);
                            cmd.Parameters.AddWithValue("@MobileNumber", mobile);
                            cmd.Parameters.AddWithValue("@Emailid", email);
                            cmd.Parameters.AddWithValue("@SBG", sbg);
                            cmd.Parameters.AddWithValue("@SBU", sbu);
                            cmd.Parameters.AddWithValue("@BU", bu);
                            cmd.Parameters.AddWithValue("@Location", location);

                            cmd.Parameters.AddWithValue("@State", state);
                            cmd.Parameters.AddWithValue("@Grade", grade);
                            cmd.Parameters.AddWithValue("@Designation", designation);
                            cmd.Parameters.AddWithValue("@ParentDepartment", parentdept);
                            //cmd.Parameters.AddWithValue("@EffectiveFrom", effective);
                            cmd.Parameters.AddWithValue("@Department", dept);
                            cmd.Parameters.AddWithValue("@ManagerEmpid", managerempid);
                            //cmd.Parameters.AddWithValue("@ManagerName", managername);
                            //cmd.Parameters.AddWithValue("@RoleName", role);
                            cmd.Parameters.AddWithValue("@ActiveStatus", activestatus);
                            cmd.Parameters.AddWithValue("@MovementType", movementtype);

                            cmd.Parameters.AddWithValue("@Created_Date", createdDate);
                            cmd.Parameters.AddWithValue("@Modified_Date", createdDate);
                            if (dob == "")
                            {
                                cmd.Parameters["@DOB"].Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters["@DOB"].Value = dob;
                            }
                            if (lastday == "")
                            {
                                cmd.Parameters["@LastWorkingDay"].Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters["@LastWorkingDay"].Value = lastday;
                            }
                            if (doj == "")
                            {
                                cmd.Parameters["@DOJ"].Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters["@DOJ"].Value = doj;
                            }
                            if (lastdate == "")
                            {
                                cmd.Parameters["@LastUpdatedDate"].Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters["@LastUpdatedDate"].Value = lastdate;
                            }
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                ReminderBackupLog(exp.Message + ", while inserting to the table - TPLEmployeeMaster_08032021 with values : " + empid + "," + firstname + "," + lastname + "," + lastday + "," + title + "," + gender + "," + dob + "," + mobile + "," + email + "," + sbg + "," + sbu + "," + bu + "," + location + "," + state + "," + grade + "," + designation + "," + parentdept + "," + managerempid + "," + managername + "," + activestatus + "," + movementtype + "," + createdDate);
                SendMail("Error while inserting to the table - TPLEmployeeMaster_08032021 for EmployeeID : " + empid, exp.Message.ToString());
            }
        }

        public static void ReminderBackupLog(string message)
        {
            try
            {
                string logsPath = ConfigurationManager.AppSettings["logsPath"].ToString();
                string PathName = logsPath;
                PathName = PathName + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                StreamWriter sw = new StreamWriter(PathName, true);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception exp)
            {
            }
        }
        public static DataTable CheckIfExists(string empid)
        {
            SqlConnection con;
            DataTable dt = new DataTable();
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "select * from TPLEmployeeMaster_08032021 where TPLEmpID='" + empid + "'";
                    using (SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con))
                    {
                        con.Open();
                        sda.Fill(dt);
                        con.Close();
                    }
                }
            }
            catch (Exception exp)
            {
                ReminderBackupLog("error while getting EmployeeID('" + empid + "')" + exp.ToString());
                SendMail("Error while getting the EmployeeID : " + empid, exp.Message.ToString());
            }
            return dt;
        }
        public static void UpdateEmpData(string empid)
        {
            string modDate = string.Empty;
            DateTime dttoday = DateTime.Now;
            modDate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dttoday);
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        conn.Open();
                        cmd.CommandText = "Update TPLEmployeeMaster_08032021 set FirstName=@FirstName,MiddleName=@MiddleName,LastName=@LastName,Title=@Title,Gender=@Gender,DOB=@DOB,MobileNumber=@MobileNumber,Emailid=@Emailid,LastWorkingDay=@LastWorkingDay,DOJ=@DOJ,SBG=@SBG,SBU=@SBU,BU=@BU,Location=@Location,State=@State,Grade=@Grade,Designation=@Designation,ParentDepartment=@ParentDepartment,Department=@Department,ManagerEmpid=@ManagerEmpid,ActiveStatus=@ActiveStatus,MovementType=@MovementType,LastupdatedDate=@LastUpdatedDate,ModifiedDate=@Modified_Date where TPLEmpID='" + empid + "'";

                        cmd.Parameters.Add("@DOB", SqlDbType.DateTime);
                        cmd.Parameters.Add("@LastWorkingDay", SqlDbType.DateTime);
                        cmd.Parameters.Add("@DOJ", SqlDbType.DateTime);
                        cmd.Parameters.Add("@LastUpdatedDate", SqlDbType.DateTime);

                        cmd.Parameters.AddWithValue("@TplEmpID", empid);
                        cmd.Parameters.AddWithValue("@FirstName", firstname);
                        cmd.Parameters.AddWithValue("@MiddleName", middlename);
                        //cmd.Parameters.AddWithValue("@DOB", dob);
                        cmd.Parameters.AddWithValue("@LastName", lastname);
                        cmd.Parameters.AddWithValue("@Title", title);
                        cmd.Parameters.AddWithValue("@Gender", gender);
                        //cmd.Parameters.AddWithValue("@DateofGroupJoin", datejoin);
                        //cmd.Parameters.AddWithValue("@DOJ", doj);
                        cmd.Parameters.AddWithValue("@MobileNumber", mobile);
                        cmd.Parameters.AddWithValue("@Emailid", email);
                        cmd.Parameters.AddWithValue("@SBG", sbg);
                        cmd.Parameters.AddWithValue("@SBU", sbu);
                        cmd.Parameters.AddWithValue("@BU", bu);
                        cmd.Parameters.AddWithValue("@Location", location);

                        cmd.Parameters.AddWithValue("@State", state);
                        cmd.Parameters.AddWithValue("@Grade", grade);
                        cmd.Parameters.AddWithValue("@Designation", designation);
                        cmd.Parameters.AddWithValue("@ParentDepartment", parentdept);
                        //cmd.Parameters.AddWithValue("@EffectiveFrom", effective);
                        cmd.Parameters.AddWithValue("@Department", dept);
                        cmd.Parameters.AddWithValue("@ManagerEmpid", managerempid);
                        //cmd.Parameters.AddWithValue("@ManagerName", managername);
                        //cmd.Parameters.AddWithValue("@RoleName", role);
                        cmd.Parameters.AddWithValue("@ActiveStatus", activestatus);
                        cmd.Parameters.AddWithValue("@MovementType", movementtype);

                        //cmd.Parameters.AddWithValue("@Created_Date", createdDate);
                        cmd.Parameters.AddWithValue("@Modified_Date", modDate);

                        if (dob == "")
                        {
                            cmd.Parameters["@DOB"].Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters["@DOB"].Value = dob;
                        }
                        if (lastday == "")
                        {
                            cmd.Parameters["@LastWorkingDay"].Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters["@LastWorkingDay"].Value = lastday;
                        }
                        if (doj == "")
                        {
                            cmd.Parameters["@DOJ"].Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters["@DOJ"].Value = doj;
                        }
                        if (lastdate == "")
                        {
                            cmd.Parameters["@LastUpdatedDate"].Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters["@LastUpdatedDate"].Value = lastdate;
                        }
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                ReminderBackupLog("error while updating EmployeeID('" + empid + "')" + ex.ToString());
                SendMail("Error while updating the table - AllEmployeeData for EmployeeID : " + empid, ex.Message.ToString());
            }
        }
        public static void SendMail(string text, string message)
        {
            try
            {
                // SenderMail = Exceldata.SenderMailID.ToString();

                MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
                //MailAddress SendTo = new MailAddress("BG Desk");

                MailMessage MyMessage = new MailMessage();
                string ToMail = "deekshithgvd-t@tataprojects.com;keerthi-t@tataprojects.com";
                if (!string.IsNullOrEmpty(ToMail))
                {
                    string strTo = ToMail.Trim();
                    string[] ToEmailids = strTo.Split(';');
                    foreach (string ToEmail in ToEmailids)
                    {
                        if (!MyMessage.To.Contains(new MailAddress(ToEmail)))
                        {
                            MyMessage.To.Add(new MailAddress(ToEmail));
                        }
                    }
                }
                MyMessage.Subject = text;
                MyMessage.Body = TextFile(message);
                //MyMessage.Body = MailBody.ToString();
                MyMessage.From = SendFrom;

                //MyMessage.CC.Add(data.initiatorEmail);
                try
                {
                    //emailClient.Send(MyMessage);
                    if (MyMessage != null)
                    {
                        SmtpClient emailClient = new SmtpClient();
                        emailClient.UseDefaultCredentials = false;
                        emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                        emailClient.Host = "smtp.office365.com";
                        emailClient.Port = 25;
                        emailClient.EnableSsl = true;
                        emailClient.TargetName = "STARTTLS/smtp.office365.com";
                        emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        emailClient.Send(MyMessage);

                    }

                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {

            }

        }

        public static String TextFile(string msg)
        {
            StringBuilder strbody = new StringBuilder();
            strbody.Append("Hi,");
            strbody.Append("\n");
            strbody.Append("\n");
            strbody.Append("" + msg + "");
            strbody.Append("\n");
            strbody.Append("\n");
            strbody.Append("Thanks & Regards,");

            return strbody.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Collections;

namespace QualityReminders
{
    class Reminders
    {
        public void pendingWithRCM()
        {
            DataTable pendingrcm = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select RCM_Email, RCM_Name, count(*) from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with RCM' group by RCM_Email, RCM_Name";
                string sqlQuery = "Select RCM_Email, RCM_Name, count(*) from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with RCM' and B.[Quality Active]=1 group by A.RCM_Email, A.RCM_Name";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingrcm);
                DataTable dt = pendingrcm;
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingrcm, "RCM");
        }
        public void PendingWithAssignee()
        {
            DataTable pendingassignee = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select Assignee_Email, Assignee_Name, count(*) from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with Assignee' and Target_Closure_Date < GETDATE() group by Assignee_Email, Assignee_Name";
                string sqlQuery = "Select Assignee_Email, Assignee_Name, count(*) from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with Assignee' and A.Target_Closure_Date < GETDATE() and B.[Quality Active]=1 group by A.Assignee_Email, A.Assignee_Name";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingassignee);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingassignee, "Assignee");
        }
        public void PendingWithPE()
        {
            DataTable pendingpe = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select PE_Email, PE_Name, count(*) from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with PE' group by PE_Email, PE_Name";
                string sqlQuery = "Select PE_Email, PE_Name, count(*) from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with PE' and B.[Quality Active]=1 group by A.PE_Email, A.PE_Name";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingpe);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingpe, "PE");
        }
        public DataTable PendingRecordsRCM(string rcmemail)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select Transaction_ID,Stage,Compliance_Type,BUName,ProjectName,ProjectType,Discipline,Activity,[Sub Activity],isCompliant from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with RCM' and RCM_Email='" + rcmemail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        public DataTable PendingRecordsAssignee(string assigneeemail)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select Transaction_ID,Stage,Compliance_Type,BUName,ProjectName,ProjectType,Discipline,Activity,[Sub Activity],isCompliant from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with Assignee' and Target_Closure_Date < GETDATE() and Assignee_Email='" + assigneeemail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        public DataTable PendingRecordsPE(string peemail)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select Transaction_ID,Stage,Compliance_Type,BUName,ProjectName,ProjectType,Discipline,Activity,[Sub Activity],isCompliant from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with PE' and PE_Email='" + peemail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }
        public void StartProcess()
        {
            pendingWithRCM();
            PendingWithAssignee();
            PendingWithPE();
        }
        string CCMailQualityII()
        {
            string CCMailQuality = ConfigurationManager.AppSettings["CCQualityEmailListII"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(CCMailQuality))
            {
                string strTo = CCMailQuality.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(ccEmail));
                }
            }
            return MyMessage.CC.ToString();
        }
        string CCMailQualityUI()
        {
            string CCMailQuality = ConfigurationManager.AppSettings["CCQualityEmailListUI"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(CCMailQuality))
            {
                string strTo = CCMailQuality.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(ccEmail));
                }
            }
            return MyMessage.CC.ToString();
        }
        public void SendMailList(DataTable pending, string role)
        {

            MailAddress SendFrom = new MailAddress("coeqii@tataprojects.com");
            SmtpClient emailClient = new SmtpClient();
            emailClient.UseDefaultCredentials = false;
            emailClient.Credentials = new System.Net.NetworkCredential("coeqii@tataprojects.com", "tata@1237");
            emailClient.Host = "smtp.office365.com";
            emailClient.Port = 25;
            emailClient.EnableSsl = true;
            emailClient.TargetName = "STARTTLS/smtp.office365.com";
            emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            DataTable dt = pending;
            DataTable ds;
            MailMessage MyMessage = new MailMessage();

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row[0].ToString() != "")
                    {
                        if (role == "RCM")
                        {
                            ds = PendingRecordsRCM(row[0].ToString());
                            if (ds.Rows[0]["BUName"].ToString() == "Buildings & Airports" || ds.Rows[0]["BUName"].ToString() == "Metros, Tunnels & Waterways" || ds.Rows[0]["BUName"].ToString() == "Roads, Bridges & Ports" || ds.Rows[0]["BUName"].ToString() == "Smart Cities" || ds.Rows[0]["BUName"].ToString() == "Transportation & Hydro")
                            {
                                MyMessage.CC.Add(CCMailQualityUI());
                            }
                            else
                            {
                                MyMessage.CC.Add(CCMailQualityII());
                            }
                        }
                        else if (role == "Assignee")
                        {
                            ds = PendingRecordsAssignee(row[0].ToString());
                            if (ds.Rows[0]["BUName"].ToString() == "Buildings & Airports" || ds.Rows[0]["BUName"].ToString() == "Metros, Tunnels & Waterways" || ds.Rows[0]["BUName"].ToString() == "Roads, Bridges & Ports" || ds.Rows[0]["BUName"].ToString() == "Smart Cities" || ds.Rows[0]["BUName"].ToString() == "Transportation & Hydro")
                            {
                                MyMessage.CC.Add(CCMailQualityUI());
                            }
                            else
                            {
                                MyMessage.CC.Add(CCMailQualityII());
                            }
                        }
                        else
                        {
                            ds = PendingRecordsPE(row[0].ToString());
                            if (ds.Rows[0]["BUName"].ToString() == "Buildings & Airports" || ds.Rows[0]["BUName"].ToString() == "Metros, Tunnels & Waterways" || ds.Rows[0]["BUName"].ToString() == "Roads, Bridges & Ports" || ds.Rows[0]["BUName"].ToString() == "Smart Cities" || ds.Rows[0]["BUName"].ToString() == "Transportation & Hydro")
                            {
                                MyMessage.CC.Add(CCMailQualityUI());
                            }
                            else
                            {
                                MyMessage.CC.Add(CCMailQualityII());
                            }
                        }
                        MyMessage.To.Add(row[0].ToString());
                        //MyMessage.To.Add("deekshithgvd-t@tataprojects.com");
                        MyMessage.From = SendFrom;
                        MyMessage.Subject = "Pending Actions on QPRC / FQPC";
                        MyMessage.Body = FormMailBodyQuality(row[1].ToString(), row[2].ToString(), ds, role);
                        MyMessage.IsBodyHtml = true;
                        emailClient.Send(MyMessage);
                        MyMessage.To.Clear();
                        MyMessage.CC.Clear();
                    }
                }
            }
        }
        string FormMailBodyQuality(string user, string number, DataTable ds, string role)
        {

            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + user + ",");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("There are <font color='red'>" + number + "</font> no of records pending with you as " + role + ". We request you to login to TPLNET and take necessary action on priority.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details of the pending records are as follows. Click on the Transaction ID to view the details of the checklist.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            //sbBody.Append("<html><head></head><title></title>");
            //sbBody.Append("<body style='font-size:12px;font-family:Trebuchet MS;'>");
            sbBody.Append("<table width='100%' border='1' cellpadding='1' cellspacing='1';'");
            sbBody.Append("<table><tr><td width='10%'>Transaction ID</td><td width='10%'>Stage</td><td width='10%'>Compliance Type</td><td width='10%'>BU Name</td><td width='10%'>Project Name</td><td width='10%'>Project Type</td><td width='10%'>Discipline</td><td width='10%'>Activity</td><td width='10%'>Sub Activity</td><td width='10%'>Is Compliant</td></tr>");
            for (int i = 0; i < ds.Rows.Count; i++)
            {
                sbBody.Append("<tr>");
                //sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Transaction_ID"] + "</td>"));
                sbBody.Append("<a href='https://tplnet.tataprojects.com/Pages/ChecklistDetails.aspx?Tid=" + ds.Rows[i]["Transaction_ID"] + "'><td width='10%'>" + Convert.ToString(ds.Rows[i]["Transaction_ID"] + "</td></a>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Stage"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Compliance_Type"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["BUName"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["ProjectName"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["ProjectType"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Discipline"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Activity"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Sub Activity"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["isCompliant"] + "</td>"));
                sbBody.Append("</tr>");
            }
            sbBody.Append("</table>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            return sbBody.ToString();
        }
    }
}

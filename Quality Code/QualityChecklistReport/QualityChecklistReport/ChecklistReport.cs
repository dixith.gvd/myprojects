﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Utilities;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
using System.Configuration;

namespace QualityChecklistReport
{
    class ChecklistReport
    {
        public static string sdate = string.Empty;
        public static string edate = string.Empty;
        //SPListItemCollection GetProjectCodes(string siteurl)
        //{
        //    SPListItemCollection items = null;
        //    using (SPSite site = new SPSite(siteurl))
        //    {
        //        using (SPWeb web = site.OpenWeb())
        //        {
        //            SPSecurity.RunWithElevatedPrivileges(delegate()
        //            {
        //                SPList lst = null;
        //                if (web.Lists["Project PM RCM"] != null)
        //                {
        //                    lst = web.Lists["Project PM RCM"];
        //                    SPQuery query = new SPQuery();
        //                    //query.Query = "<Where><IsNotNull><FieldRef Name='Project_x0020_Code' /></IsNotNull></Where>";
        //                    //query.Query = "<Where><And><IsNotNull><FieldRef Name='Project_x0020_Code' /></IsNotNull><IsNotNull><FieldRef Name='Field_x0020_Quality_x0020_Engine' /></IsNotNull></And></Where>";
        //                    query.Query = "<Where><And><IsNotNull><FieldRef Name='Project_x0020_Code' /></IsNotNull><And><IsNotNull><FieldRef Name='Field_x0020_Quality_x0020_Engine' /></IsNotNull><Eq><FieldRef Name='Quality_x0020_Active' /><Value Type='Boolean'>1</Value></Eq></And></And></Where>";
        //                    query.ViewFields = "<FieldRef Name='Project_x0020_Code' />";
        //                    items = lst.GetItems(query);
        //                }
        //            });
        //        }
        //    }
        //    return items;
        //}
        public DataTable GetProjectCodesQuality()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                string sqlQuery = "select distinct [Project Code] from Monitored_Projects where [Field Quality Engineer]!='' and [Quality Active]=1";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dt);
                con.Open();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            return dt;
        }
        public string GetProjectName(string projectCode)
        {
            DataTable dt = new DataTable();
            string pname = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                string sqlQuery = "select [Project Name] from Monitored_Projects where [Project Code]='" + projectCode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dt);
                con.Open();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            if (dt.Rows.Count > 0)
            {
                pname = dt.Rows[0]["Project Name"].ToString();
            }
            return pname;
        }
        //string GetProjectNameByCode(string projectCode, SPWeb web)
        //{
        //    string projectName = string.Empty;

        //    SPSecurity.RunWithElevatedPrivileges(delegate()
        //    {
        //        SPList lst = null;
        //        if (web.Lists["TPL Projects List"] != null)
        //        {
        //            lst = web.Lists["TPL Projects List"];
        //            SPQuery query = new SPQuery();
        //            query.Query = "<Where><Eq><FieldRef Name='Title' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
        //            SPListItemCollection items = lst.GetItems(query);
        //            foreach (SPListItem item in items)
        //            {
        //                if (item["Final_x0020_Project_x0020_Name"] != null)
        //                {
        //                    SPFieldCalculated calculatedField = lst.Fields.GetFieldByInternalName("Final_x0020_Project_x0020_Name") as SPFieldCalculated;
        //                    projectName = calculatedField.GetFieldValueAsText(item["Final_x0020_Project_x0020_Name"]);
        //                }
        //            }
        //        }
        //    });

        //    return projectName;
        //}
        public int GetTotalUplodedProjectCount(string ctype, string projectCode)
        {
            int count = 0;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                string sqlQuery = "Select count(*) from [dbo].[QChecklist_TnxMain] where Compliance_Type= '" + ctype + "' and ProjectCode='" + projectCode + "'";
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                count = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            return count;
        }

        public static int GetTotalUplodedProjectCountLastweek(string ctype, string projectCode)
        {
            DateTime date = DateTime.Now.AddDays(-7);
            while (date.DayOfWeek != DayOfWeek.Monday)
            {
                date = date.AddDays(-1);
            }
            DateTime dtPrevWeekStartDate = date;
            DateTime dtPrevWeekStartEndDate = date.AddDays(6);
            //string startDate = (SPUtility.CreateISO8601DateTimeFromSystemDateTime(dtPrevWeekStartDate));
            //string endDate = (SPUtility.CreateISO8601DateTimeFromSystemDateTime(dtPrevWeekStartEndDate));
            string startDate = dtPrevWeekStartDate.ToString("yyyy-MM-ddTHH:mm:ssZ");
            string endDate = dtPrevWeekStartEndDate.ToString("yyyy-MM-ddTHH:mm:ssZ");
            sdate = dtPrevWeekStartDate.ToShortDateString();
            edate = dtPrevWeekStartEndDate.ToShortDateString();
            int count = 0;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                string sqlQuery = "Select count(*) from [dbo].[QChecklist_TnxMain] where Compliance_Type='" + ctype + "' and FQE_Submitted_Date >= '" + startDate + "' and FQE_Submitted_Date <= '" + endDate + "' and ProjectCode='" + projectCode + "'";
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                count = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            return count;
        }
        public void StartProcess()
        {
            Hashtable ht = new Hashtable();
            ht.Add("QPR", "QPR");
            ht.Add("FQP", "FQP");
            DataTable dt = new DataTable();

            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            string fPath = ConfigurationManager.AppSettings["fPath"].ToString();
            string dtFormat = String.Format("{0:dddd_MMMM_d_yyyy}", DateTime.Today);
            List<string> fileNamesFormail = new List<string>();
            foreach (DictionaryEntry de in ht)
            {
                string fileNamePart = string.Concat(de.Value.ToString(), "_", dtFormat, ".txt");
                string fileName = string.Concat(fPath, fileNamePart);
                if (de.Key.ToString() == "QPR")
                {

                    //SPListItemCollection projectCodes = GetProjectCodes(siteurl);
                    DataTable dtpcodes = GetProjectCodesQuality();

                    using (StreamWriter _testData = new StreamWriter(fileName, true))
                    {
                        StringBuilder sbtitle = new StringBuilder();
                        sbtitle.Append("Statistics from " + sdate + " to " + edate);
                        sbtitle.Append(",");
                        sbtitle.Append(Environment.NewLine);
                        sbtitle.Append(Environment.NewLine);
                        sbtitle.Append("Project Code");
                        sbtitle.Append(",");
                        sbtitle.Append("Project Name");
                        sbtitle.Append(",");
                        sbtitle.Append("Uploaded Total");
                        sbtitle.Append(",");
                        sbtitle.Append("Uploaded Last Week");
                        _testData.WriteLine(sbtitle.ToString());
                        //foreach (SPListItem eachProject in projectCodes)
                        //{
                        foreach (DataRow dr in dtpcodes.Rows)
                        {
                            if (dr["Project Code"] != null)
                            {
                                StringBuilder sb = new StringBuilder();
                                string projectCode = dr["Project Code"].ToString();
                                if (!string.IsNullOrEmpty(projectCode))
                                {
                                    //SPWeb web = eachProject.ParentList.ParentWeb;
                                    sb.Append(projectCode);
                                    sb.Append(",");
                                    //string projectName = GetProjectNameByCode(projectCode, web);
                                    string projectName = GetProjectName(projectCode);
                                    sb.Append(projectName);
                                    sb.Append(",");
                                    string countTotal = GetTotalUplodedProjectCount(de.Key.ToString(), projectCode).ToString();
                                    sb.Append(countTotal);
                                    sb.Append(",");
                                    string countLastweek = GetTotalUplodedProjectCountLastweek(de.Key.ToString(), projectCode).ToString();
                                    sb.Append(countLastweek);
                                    //sb.Append(",");
                                    _testData.WriteLine(sb.ToString());

                                }
                            }
                        }
                    }
                }
                else if (de.Key.ToString() == "FQP")
                {
                    //SPListItemCollection projectCodes = GetProjectCodes(siteurl);
                    DataTable dtpcodes = GetProjectCodesQuality();

                    using (StreamWriter _testData = new StreamWriter(fileName, true))
                    {
                        GetTotalUplodedProjectCountLastweek(de.Key.ToString(), string.Empty).ToString();
                        StringBuilder sbtitle = new StringBuilder();
                        sbtitle.Append("Statistics from " + sdate + " to " + edate);
                        sbtitle.Append(",");
                        sbtitle.Append(Environment.NewLine);
                        sbtitle.Append(Environment.NewLine);
                        sbtitle.Append("Project Code");
                        sbtitle.Append(",");
                        sbtitle.Append("Project Name");
                        sbtitle.Append(",");
                        sbtitle.Append("Uploaded Total");
                        sbtitle.Append(",");
                        sbtitle.Append("Uploaded Last Week");
                        _testData.WriteLine(sbtitle.ToString());
                        //foreach (SPListItem eachProject in projectCodes)
                        //{
                        foreach (DataRow dr in dtpcodes.Rows)
                        {
                            if (dr["Project Code"] != null)
                            {
                                StringBuilder sb = new StringBuilder();
                                string projectCode = dr["Project Code"].ToString();
                                if (!string.IsNullOrEmpty(projectCode))
                                {
                                    //SPWeb web = eachProject.ParentList.ParentWeb;
                                    sb.Append(projectCode);
                                    sb.Append(",");
                                    //string projectName = GetProjectNameByCode(projectCode, web);
                                    string projectName = GetProjectName(projectCode);
                                    sb.Append(projectName);
                                    sb.Append(",");
                                    string countTotal = GetTotalUplodedProjectCount(de.Key.ToString(), projectCode).ToString();
                                    sb.Append(countTotal);
                                    sb.Append(",");
                                    string countLastweek = GetTotalUplodedProjectCountLastweek(de.Key.ToString(), projectCode).ToString();
                                    sb.Append(countLastweek);
                                    //sb.Append(",");
                                    _testData.WriteLine(sb.ToString());

                                }
                            }
                        }
                    }
                }
                string csvPath = Path.ChangeExtension(fileName, ".csv");
                System.IO.File.Move(fileName, csvPath);
                fileNamesFormail.Add(csvPath);
            }
            //SendMail(concatenatedString.ToString());
            List<string> Attachments = new List<string>();

            foreach (string each in fileNamesFormail)
            {
                if (each.ToUpper().Contains("FQP"))
                {
                    Attachments.Add(each);
                }
                if (each.ToUpper().Contains("QPR"))
                {
                    Attachments.Add(each);
                }
            }


            MailAddress SendFrom = new MailAddress("coeqii@tataprojects.com");
            SmtpClient emailClient = new SmtpClient();
            emailClient.UseDefaultCredentials = false;
            emailClient.Credentials = new System.Net.NetworkCredential("coeqii@tataprojects.com", "tata@1237");
            emailClient.Host = "smtp.office365.com";
            emailClient.Port = 25;
            emailClient.EnableSsl = true;
            emailClient.TargetName = "STARTTLS/smtp.office365.com";
            emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

            //Send mails For Quality uplaods with attachments
            string toMailQuality = ConfigurationManager.AppSettings["ToChecklistEmailList"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMailQuality))
            {
                string strTo = toMailQuality.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Quality Checklist Upload Details";
            MyMessage.Body = FormMailBodyQuality();
            MyMessage.IsBodyHtml = true;
            foreach (string each in Attachments)
            {
                if (System.IO.File.Exists(each))
                {
                    try
                    {
                        System.Net.Mail.Attachment attachFile = new System.Net.Mail.Attachment(each);
                        MyMessage.Attachments.Add(attachFile);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

            emailClient.Send(MyMessage);
            UploadDocuments(Attachments);

        }
        string FormMailBodyQuality()
        {

            StringBuilder sbBody = new StringBuilder();
            string openHtml = @"<html>
        <head>
            <style>
                body{
                    font-family:""Trebuchet MS"", Arial, Helvetica, sans-serif;
                    font-size:0.9em;
                    text-align:left;
                }
            table th{border: 1px solid #424242; color: #FFFFFF;text-align: center; padding: 0 10px;background-color: #5675BA;}
            table td{border: 1px solid #D1D1D1;background-color: #F3F3F3; padding: 0 10px;}
            </style>
        </head>
            <body>";

            sbBody.Append(openHtml);
            sbBody.Append("<table>");
            //sbBody.Append("<tr>");
            ////sbBody.Append("<th>");
            ////sbBody.Append("Quality Upload Details");
            ////sbBody.Append("</th>");
            //sbBody.Append("</tr>");
            sbBody.Append("<tr>");
            sbBody.Append("<td>");
            sbBody.Append("Dear All");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Please find attached the statistics for the Quality Checklist Uploads for the last week.");
            //sbBody.Append("<br>");
            //sbBody.Append("<br>");
            //sbBody.Append("To get complete details of the uploads, please click on the following links :");
            //sbBody.Append("<br>");
            //sbBody.Append("<br>");
            //sbBody.Append("<b>Click on the Refresh All button in the Data Tab in excel to get the latest updated date.</b>");
            //sbBody.Append("<br>");
            //sbBody.Append("<br>");
            //sbBody.Append("Good \\ Bad Practices - <a href='https://tplnet.tataprojects.com/Documents/Quality%20Site%20Uploads.xlsx'>https://tplnet.tataprojects.com/Documents/Quality%20Site%20Uploads.xlsx</a>");
            //sbBody.Append("<br>");
            //sbBody.Append("<br>");
            //sbBody.Append("Self-Certification  -  <a href='https://tplnet.tataprojects.com/Documents/Quality%20Self%20Certification%20Results.xlsx'>https://tplnet.tataprojects.com/Documents/Quality%20Self%20Certification%20Results.xlsx</a>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards");
            sbBody.Append("</td>");
            sbBody.Append("</tr>");
            string closeHtml = @"</body></html>";
            sbBody.Append(closeHtml);
            return sbBody.ToString();
        }

        private void UploadDocuments(List<string> attachments)
        {
            string library = ConfigurationManager.AppSettings["library"].ToString();
            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            ClientContext clientContext = new ClientContext(siteurl);
            Web web = clientContext.Web;
            List list = web.Lists.GetByTitle(library);
            if (attachments != null)
            {
                string fileupload1 = attachments[0];
                string fileupload2 = attachments[1];
                string f1 = System.IO.Path.GetFileName(fileupload1.Replace(fileupload1, "FQP_Upload_Statistics.csv"));
                string f2 = System.IO.Path.GetFileName(fileupload2.Replace(fileupload2, "QPR_Upload_Statistics.csv"));
                FileCreationInformation newFile = new FileCreationInformation();
                FileCreationInformation newFile2 = new FileCreationInformation();
                // Assign to content byte[] i.e. documentStream
                newFile.Content = System.IO.File.ReadAllBytes(fileupload1);
                newFile2.Content = System.IO.File.ReadAllBytes(fileupload2);
                // Allow owerwrite of document
                newFile.Overwrite = true;
                newFile2.Overwrite = true;
                // Upload URL
                newFile.Url = siteurl + library + "/" + f1;
                newFile2.Url = siteurl + library + "/" + f2;
                Microsoft.SharePoint.Client.File uploadFile = list.RootFolder.Files.Add(newFile);
                Microsoft.SharePoint.Client.File uploadFile2 = list.RootFolder.Files.Add(newFile2);

                clientContext.Load(uploadFile);
                clientContext.Load(uploadFile2);
                clientContext.ExecuteQuery();
            }
        }
    }
}

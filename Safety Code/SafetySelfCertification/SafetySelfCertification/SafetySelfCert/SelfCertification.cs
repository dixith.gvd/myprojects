﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Microsoft.SharePoint;
namespace SafetySelfCertification.SafetySelfCert
{
    class SelfCertification
    {
        public DataTable GetProjectCodeByLoggedInUserRCM()
        {
            DataTable dt = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='RCM' /><Value Type='Integer'><UserID /></Value></Eq></Where>";
                            dt = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dt;
        }

        public DataTable GetProjectdetailsByProjectCode(string projectCode)
        {
            DataTable dtProjectDetails = new DataTable();
            // SPListItem projectCodeItem = null;
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["TPL Projects List"] != null)
                    {
                        SPList list = web.Lists["TPL Projects List"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='Title' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";

                            dtProjectDetails = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }

                }
            }
            return dtProjectDetails;
        }

        public void SaveSelfCertification(int id,string buname,string projectname,string comp1,string comp2,string comp3,string comp4,string comp5,string certification,string created,string modified,string repmonth,string repweek,string projectcode,string createdby,string modifiedby)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into Safety_SelfCertification(ID,[BU Name],[Project Name],[Comp 1],[Comp 2],[Comp 3],[Comp 4],[Comp 5],Certification,Created,Modified,[Reporting Month],[Rep Week],[Project Code],[Created By],[Modified By])");
            insertQuery.Append(" values ("); insertQuery.Append("" + id + ","); insertQuery.Append("'" + buname + "',"); insertQuery.Append("'" + projectname + "',");
            insertQuery.Append("'" + comp1 + "',"); insertQuery.Append("'" + comp2 + "',"); insertQuery.Append("'" + comp3 + "',"); insertQuery.Append("'" + comp4 + "',"); insertQuery.Append("'" + comp5 + "',");
            insertQuery.Append("'" + certification + "',"); insertQuery.Append("'" + created + "',"); insertQuery.Append("'" + modified + "',"); insertQuery.Append("'" + repmonth + "',");
            insertQuery.Append("'" + repweek + "',"); insertQuery.Append("'" + projectcode + "',"); insertQuery.Append("'" + createdby + "',"); insertQuery.Append("'" + modifiedby + "')");
            try
            {
                SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {

            }
        }

        public string GetLatestIDSafetySelfCert()
        {
            string ID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 ID FROM [EQHSAutomation].[dbo].[Safety_SelfCertification] ORDER BY ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["ID"] != null)
                        {
                            ID = dt.Rows[0]["ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {

            }
            return ID;
        }
        public DataTable GetSafetySelfCertificationDatabyID(string ID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from Safety_SelfCertification where ID='" + ID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {

            }
            return dtChecklistData;
        }

    }
}

﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Text;
using System.Web;
using System.Web.UI;
namespace SafetyAutomation.HiratalkChecklist
{
    [ToolboxItemAttribute(false)]
    public partial class HiratalkChecklist : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public HiratalkChecklist()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        SafetyChecklists objSafety = new SafetyChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            dvMainFormChecklist.Visible = true;
            if (!this.Page.IsPostBack)
            {
                if (this.Page.Request.QueryString["Tid"] != null)
                {
                    lblTransactionID.Text = this.Page.Request.QueryString["Tid"].ToString();
                    DataTable dtdata = objSafety.GetChecklistDatabyTransactionIDSafety(lblTransactionID.Text);
                    if (dtdata.Rows.Count > 0)
                    {
                        if (dtdata.Rows[0]["BUName"] != null)
                        {
                            lblBU.Text = dtdata.Rows[0]["BUName"].ToString();
                        }
                        if (dtdata.Rows[0]["ProjectName"] != null)
                        {
                            lblProject.Text = dtdata.Rows[0]["ProjectName"].ToString();
                            ddlProjectCode.Visible = false;
                            lblProject.Visible = true;
                        }
                        if (dtdata.Rows[0]["Frequency"] != null)
                        {
                            lblfrequency.Text = dtdata.Rows[0]["Frequency"].ToString();
                        }
                        if (dtdata.Rows[0]["Checklist Name"] != null)
                        {
                            lblchecklist.Text = dtdata.Rows[0]["Checklist Name"].ToString();
                        }
                        if (dtdata.Rows[0]["Inspection Date"] != null)
                        {
                            lbldtinitiation.Text = Convert.ToDateTime(dtdata.Rows[0]["Inspection Date"]).ToShortDateString();
                            lbldtinitiation.Visible = true;
                            DateTimeControl1.Visible = false;
                        }
                        DataTable dtChildData = objSafety.GetLocationComplianceDatabyTransactionID(lblTransactionID.Text);
                        if (dtChildData != null)
                        {
                            GridView1.DataSource = dtChildData;
                            GridView1.DataBind();
                            GridView1.Visible = true;
                            grdCompliances.Visible = false;
                            btnsubmit.Visible = false;
                            btnhome.Visible = false;
                        }
                    }
                }
                else
                {
                    DataTable projectCodeItem = objSafety.GetProjectCodeByLoggedInUserSafety();
                    if (projectCodeItem != null)
                    {
                        dvUnAuth.Visible = false;
                        dvMainFormChecklist.Visible = true;
                        for (int i = 0; i < projectCodeItem.Rows.Count; i++)
                        {
                            string projectCode = projectCodeItem.Rows[i][0].ToString();

                            DataTable projectItem = objSafety.GetProjectdetailsByProjectCode(projectCode);

                            for (int j = 0; j < projectItem.Rows.Count; j++)
                            {
                                string projectName = projectItem.Rows[j]["Title"].ToString();
                                if (!ddlProjectCode.Items.Contains(new ListItem(projectName)))
                                {
                                    ddlProjectCode.Items.Add(projectName);
                                }
                            }
                            lblfrequency.Text = "Daily";
                            lblchecklist.Text = "HIRA talk";
                            lblBU.Text = projectItem.Rows[0]["BU_x0020_Name"].ToString();
                            lblNewSBU.Text = projectItem.Rows[0]["SBU_x0020_Name"].ToString();
                            if (lblBU.Text == "Industrial Infrastructure")
                            {
                                lblInduCoE.Text = "test@tataprojects.com";
                            }
                            else if (lblBU.Text == "Urban Infrastructure")
                            {
                                lblInduCoE.Text = "test@tataprojects.com";
                            }
                            else
                            {
                                lblInduCoE.Text = "test@tataprojects.com";
                            }
                        }
                        if (!ddlProjectCode.Items.Contains(new ListItem("--Select--")))
                        {
                            ddlProjectCode.Items.Insert(0, "--Select--");
                        }
                        if (ddlProjectCode.SelectedIndex > 0)
                        {
                            string projectNameForRCM = ddlProjectCode.SelectedItem.Text;
                            DataTable projectCodeFromList = objSafety.GetProjectCodeByProjectName(projectNameForRCM);

                            for (int i = 0; i < projectCodeFromList.Rows.Count; i++)
                            {
                                string projectCode = projectCodeFromList.Rows[i][0].ToString();
                                ViewState["ProjectCode"] = projectCode;
                                grdCompliances.DataSource = objSafety.GetEmptyRowsPPE();
                                grdCompliances.DataBind();
                                //grdworkplace.RowDataBound += new GridViewRowEventHandler(grdworkplace_RowDataBound);
                                //grdworkplace.DataBind();
                                SPListItem projectCodeFromPMRCM = objSafety.GetProjectCodeByProjectNameForRCM(projectCode);
                                if (projectCodeFromPMRCM["RCM"] != null)
                                {
                                    string strRCM = projectCodeFromPMRCM["RCM"].ToString();
                                    if (!string.IsNullOrEmpty(strRCM))
                                    {
                                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strRCM);
                                        lblRCMEmail.Text = userValue.User.Email.ToString();
                                        lblRCMName.Text = userValue.User.Name;
                                        lblRCMID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                                    }

                                }
                                if (projectCodeFromPMRCM["BU_x0020_Head"] != null)
                                {
                                    string strbuHead = projectCodeFromPMRCM["BU_x0020_Head"].ToString();
                                    if (!string.IsNullOrEmpty(strbuHead))
                                    {
                                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strbuHead);
                                        lblbuheadid.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                                        lblbuheadName.Text = userValue.User.Name;
                                        lblbuheadEmail.Text = userValue.User.Email.ToString();
                                    }
                                }
                                if (projectCodeFromPMRCM["PM"] != null)
                                {
                                    string strPM = projectCodeFromPMRCM["PM"].ToString();
                                    if (!string.IsNullOrEmpty(strPM))
                                    {
                                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPM);
                                        lblPMEmail.Text = userValue.User.Email.ToString();
                                        lblPMName.Text = userValue.User.Name;
                                        lblPMID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                                    }

                                }

                                if (projectCodeFromPMRCM["Head_x0020_Operations"] != null)
                                {
                                    string strHead = projectCodeFromPMRCM["Head_x0020_Operations"].ToString();
                                    if (!string.IsNullOrEmpty(strHead))
                                    {
                                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strHead);
                                        lblHeadEmail.Text = userValue.User.Email.ToString();
                                        lblHead.Text = userValue.User.Name;
                                        lblHeadID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        dvUnAuth.Visible = true;
                        dvMainFormChecklist.Visible = false;
                    }
                }
            }
        }

        protected void ddlProjectCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string projectName = ddlProjectCode.SelectedItem.Text;
            lblProjectCode.Text = ddlProjectCode.SelectedItem.Text;
            //grdworkplace.Visible = true;
            //DataTable dtProjectCode = objSafety.GetProjectCodeByProjectName(projectName);
            if (!string.IsNullOrEmpty(ddlProjectCode.SelectedItem.Text) && ddlProjectCode.SelectedIndex > 0)
            {

                //for (int i = 0; i < dtProjectCode.Rows.Count; i++)
                //{
                //string projectCode = dtProjectCode.Rows[i][1].ToString();
                string projectCode = ddlProjectCode.SelectedItem.Text.Split('-')[1];
                lblProjectCode.Text = projectCode;
                ViewState["ProjectCode"] = projectCode;
                grdCompliances.DataSource = objSafety.GetEmptyRowsPPE();
                grdCompliances.DataBind();
                SPListItem projectCodeFromPMRCM = objSafety.GetProjectCodeByProjectNameForRCM(projectCode);

                if (projectCodeFromPMRCM["RCM"] != null)
                {
                    string strRCM = projectCodeFromPMRCM["RCM"].ToString();
                    if (!string.IsNullOrEmpty(strRCM))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strRCM);
                        lblRCMEmail.Text = userValue.User.Email.ToString();
                        lblRCMName.Text = userValue.User.Name;
                        lblRCMID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                    }

                }
                if (projectCodeFromPMRCM["PM"] != null)
                {
                    string strPM = projectCodeFromPMRCM["PM"].ToString();
                    if (!string.IsNullOrEmpty(strPM))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPM);
                        lblPMEmail.Text = userValue.User.Email.ToString();
                        lblPMName.Text = userValue.User.Name;
                        lblPMID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                    }

                }
                if (projectCodeFromPMRCM["BU_x0020_Head"] != null)
                {
                    string strbuHead = projectCodeFromPMRCM["BU_x0020_Head"].ToString();
                    if (!string.IsNullOrEmpty(strbuHead))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strbuHead);
                        lblbuheadid.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                        lblbuheadName.Text = userValue.User.Name;
                        lblbuheadEmail.Text = userValue.User.Email.ToString();
                    }
                }
                if (projectCodeFromPMRCM["Head_x0020_Operations"] != null)
                {
                    string strHead = projectCodeFromPMRCM["Head_x0020_Operations"].ToString();
                    if (!string.IsNullOrEmpty(strHead))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strHead);
                        lblHeadEmail.Text = userValue.User.Email.ToString();
                        lblHead.Text = userValue.User.Name;
                        lblHeadID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                    }

                }
                //}
            }
        }

        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlProjectCode.SelectedIndex > 0)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlLocations = (e.Row.FindControl("ddllocationhtalk") as DropDownList);
                    ddlLocations.DataSource = objSafety.GetLocationsByProjectCodeSafety(ViewState["ProjectCode"].ToString());
                    ddlLocations.DataTextField = "Location";
                    ddlLocations.DataValueField = "Location";
                    ddlLocations.DataBind();

                    //Add Default Item in the DropDownList
                    ddlLocations.Items.Insert(0, new ListItem("--select--"));

                }
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            int count = 0;
            int countNo = 0;
            int filecnt = 0;
            foreach (GridViewRow gvr in grdCompliances.Rows)
            {
                RadioButton rdbYes = (RadioButton)gvr.FindControl("rdbYes");
                RadioButton rdbNo = (RadioButton)gvr.FindControl("rdbNo");
                DropDownList ddlLocatoin = (DropDownList)gvr.FindControl("ddllocationhtalk");
                FileUpload fu = (FileUpload)gvr.FindControl("uploadEvidence");
                string filePath = fu.PostedFile.FileName;
                if (rdbYes.Checked && ddlLocatoin.SelectedIndex != 0)
                {
                    count += 1;
                }
                if (rdbNo.Checked && ddlLocatoin.SelectedIndex != 0)
                {
                    count += 1;
                    countNo += 1;
                }
                if (ddlLocatoin.SelectedIndex != 0)
                {
                    if (!string.IsNullOrEmpty(filePath))
                    {
                        filecnt += 1;
                    }
                    else
                    {
                        lblError.Text = "file upload for selected location is mandatory";
                        return;
                    }
                }
                //if ((!(rdbYes.Checked) && ddlLocatoin.SelectedIndex != 0) || (!(rdbNo.Checked) && ddlLocatoin.SelectedIndex != 0))
                //{
                //    lblError.Text = "Please check the Compliance(Yes/No) for selected location";
                //    return;
                //}
            }
            if (ddlProjectCode.SelectedIndex != 0)
            {
                if (count > 0 && filecnt > 0)
                {
                    if (rbyes.Checked)
                    {
                        lblError.Text = string.Empty;
                        string stage = string.Empty;

                        string isComplaint = "Yes";
                        if (countNo > 0)
                        {
                            isComplaint = "No";
                            stage = "Closed";
                        }
                        else
                        {
                            stage = "Closed";
                        }
                        string frequency = lblfrequency.Text;
                        string checklist = lblchecklist.Text;
                        string bu = lblBU.Text;
                        string project = ddlProjectCode.SelectedItem.Text;
                        string projectCode = lblProjectCode.Text;
                        string selfcert = rbyes.Text;
                        lblSOEmail.Text = SPContext.Current.Web.CurrentUser.Email;
                        lblSOName.Text = SPContext.Current.Web.CurrentUser.Name;
                        SPUser user = SPContext.Current.Web.EnsureUser(lblSOEmail.Text);
                        SPFieldUserValue user2 = new SPFieldUserValue(SPContext.Current.Web, user.ID, user.Email.ToString());
                        lblSOID.Text = objSafety.GetOnlyEmployeeID(user2.User.LoginName);
                        DateTime date = DateTimeControl1.SelectedDate;
                        string inspdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", date);
                        string transID = string.Empty;
                        DateTime today = DateTime.Now;
                        string LastStageUpdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                        int TransactionID = Convert.ToInt32(objSafety.GetLatestTransactionIDSafety()) + 1;
                        bool isSaved = objSafety.SaveHousekeepingChecklist(TransactionID, frequency, checklist, isComplaint, bu, project, projectCode, inspdate, stage, lblSOID.Text, lblSOName.Text, lblSOEmail.Text, lblRCMID.Text, lblRCMName.Text, lblRCMEmail.Text, lblPMID.Text, lblPMName.Text, lblPMEmail.Text, lblHeadID.Text, lblHead.Text, lblHeadEmail.Text, lblbuheadid.Text, lblbuheadName.Text, lblbuheadEmail.Text, LastStageUpdate, lblNewSBU.Text, selfcert);
                        if (isSaved)
                        {
                            //transID = objSafety.GetLatestTransactionIDSafety();
                            transID = TransactionID.ToString();
                            lblTransactionID.Text = transID;
                            if (!string.IsNullOrEmpty(transID))
                            {
                                // InsertEvidenceDocument(transID);
                                foreach (GridViewRow gvr in grdCompliances.Rows)
                                {
                                    DropDownList ddlLocatoin = (DropDownList)gvr.FindControl("ddllocationhtalk");
                                    if (ddlLocatoin.SelectedIndex != 0)
                                    {
                                        string locationText = ddlLocatoin.Text;
                                        RadioButton rdbYes = (RadioButton)gvr.FindControl("rdbYes");
                                        RadioButton rdbNo = (RadioButton)gvr.FindControl("rdbNo");
                                        TextBox txtRemarks = (TextBox)gvr.FindControl("txtSORemarks");
                                        string isComplaintEach = string.Empty;
                                        if (rdbYes.Checked)
                                        {
                                            isComplaintEach = rdbYes.Text;
                                        }
                                        if (rdbNo.Checked)
                                        {
                                            isComplaintEach = rdbNo.Text;
                                        }
                                        FileUpload fu = (FileUpload)gvr.FindControl("uploadEvidence");
                                        string filePath = fu.PostedFile.FileName;
                                        string filename = Path.GetFileName(filePath);
                                        string ext = Path.GetExtension(filename);
                                        string contenttype = String.Empty;
                                        switch (ext)
                                        {
                                            case ".doc":
                                                contenttype = "application/vnd.ms-word";
                                                break;
                                            case ".docx":
                                                contenttype = "application/vnd.ms-word";
                                                break;
                                            case ".xls":
                                                contenttype = "application/vnd.ms-excel";
                                                break;
                                            case ".xlsx":
                                                contenttype = "application/vnd.ms-excel";
                                                break;
                                            case ".jpg":
                                                contenttype = "image/jpg";
                                                break;
                                            case ".jpeg":
                                                contenttype = "image/jpeg";
                                                break;
                                            case ".png":
                                                contenttype = "image/png";
                                                break;
                                            case ".gif":
                                                contenttype = "image/gif";
                                                break;
                                            case ".pdf":
                                                contenttype = "application/pdf";
                                                break;
                                        }
                                        Stream fs = fu.PostedFile.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                        String strQuery = "insert into EHSChecklist_Details(PicEvidence_DocName,PicEvidence_DocContentType,PicEvidence,Transaction_ID,Location,isCompliant,SORemarks) values (@PicEvidence_DocName, @PicEvidence_DocContentType, @PicEvidence,@Transaction_ID, @Location,@isCompliant,@SORemarks)";
                                        try
                                        {
                                            SqlCommand cmd = new SqlCommand(strQuery);
                                            cmd.Parameters.AddWithValue("@PicEvidence_DocName", filename);
                                            cmd.Parameters.AddWithValue("@PicEvidence_DocContentType", contenttype);
                                            cmd.Parameters.AddWithValue("@PicEvidence", bytes);
                                            cmd.Parameters.AddWithValue("@Transaction_ID", lblTransactionID.Text);
                                            cmd.Parameters.AddWithValue("@Location", locationText);
                                            cmd.Parameters.AddWithValue("@isCompliant", isComplaintEach);
                                            cmd.Parameters.AddWithValue("@SORemarks", txtRemarks.Text);

                                            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                                            cmd.CommandType = CommandType.Text;
                                            cmd.Connection = con;
                                            con.Open();
                                            cmd.ExecuteNonQuery();
                                            con.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                    }
                                }
                            }
                        }
                        if (isComplaint == "Yes")
                        {
                            objSafety.SendClosureMailHiraTalk(lblRCMName.Text, lblRCMEmail.Text, lblBU.Text, project, checklist, SPContext.Current.Web.CurrentUser.Name, lblPMEmail.Text, lblInduCoE.Text, lblTransactionID.Text);
                        }
                        else
                        {
                            objSafety.SendCompRCMForClosureMailHiratalk(lblRCMName.Text, lblRCMEmail.Text, lblBU.Text, project, checklist, SPContext.Current.Web.CurrentUser.Name, lblPMEmail.Text, lblInduCoE.Text, lblTransactionID.Text);
                        }
                        btnsubmit.Enabled = false;
                        lblSOSubNotification.Text = "You have submitted successfully";
                        string navigateUrl = Constants.link + "Pages/EHSCheckList.aspx";
                        lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to submit another Safety Checklist", navigateUrl);
                        lblError.Text = "";
                    }
                    else
                    {
                        lblError.Text = "Please certify that above information is accurate.";
                    }
                }
                else
                {
                    lblError.Text = "Please select atleast one location from dropdown";
                }
            }
            else
            {
                lblError.Text = "Please select Project";
            }
        }

        protected void lnkView_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            LinkButton btn = sender as LinkButton;
            string docName = btn.Text;

            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select PicEvidence_DocName,PicEvidence_DocContentType,PicEvidence from EHSChecklist_Details where Transaction_ID='" + lblTransactionID.Text + "'and PicEvidence_DocName='" + docName + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["PicEvidence_DocName"].ToString();
                    contentType = sdr["PicEvidence_DocContentType"].ToString();
                    bytes = (byte[])sdr["PicEvidence"];
                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
            }


            catch (Exception ex)
            {

            }

        }

        protected void btnhome_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("https://tplnet.tataprojects.com/Pages/EHSCheckList.aspx");
        }

    }
}

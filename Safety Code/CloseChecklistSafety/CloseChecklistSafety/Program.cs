﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloseChecklistSafety
{
    class Program
    {
        static void Main(string[] args)
        {
            CloseChecklist objNC = new CloseChecklist();
            Console.WriteLine("Started");
            objNC.ProcessLogic();
            Console.WriteLine("Completed...");
        }
    }
}

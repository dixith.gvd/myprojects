﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SafetySelfCert.ascx.cs" Inherits="SafetySelfCertification.SafetySelfCert.SafetySelfCert" %>

<style type="text/css">
    .table {
        padding: 0;
        border-spacing: 0;
        text-align: center;
        font-family: 'Trebuchet MS';
        font-size: 10pt;
        vertical-align: middle;
        border-collapse: collapse;
    }

    .required {
        color: Red;
    }
</style>

<table border="1" cellpadding="1" cellspacing="1" width="100%">
    <tr>
        <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
            <br />
            <asp:Label ID="Label5" runat="server" ForeColor="White" Text="Safety Self Certification" Font-Size="X-Large" Font-Bold="true"></asp:Label>
            <br />
            <br />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="left">
            <font color="#000000" cssclass="auto-style3" face="Trebuchet MS">
            <br />
            Note:<br />
            # Note that all the fields are mandatory<br />
            # Select the points that you \ your site comply with<br />
            # Select the final declaration in order to submit the form<br />
           </font>
        </td>
    </tr>

    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label1" ForeColor="White" runat="server" Text="BU" CssClass="auto-style3"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <asp:Label ID="lblBU" runat="server" CssClass="auto-style3"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
            <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Project" CssClass="auto-style3"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
            <asp:DropDownList ID="ddlProjectCode" runat="server" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlProjectCode_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="lblProject" runat="server" Visible="false"></asp:Label></td>
            <%--<asp:RequiredFieldValidator ID="RFProject" runat="server" ErrorMessage="Please Select Project" InitialValue="--Select--" ControlToValidate="ddlProjectCode" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>--%>
            <%-- <asp:Label ID="lblProject" runat="server"></asp:Label>--%></td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label21" ForeColor="White" runat="server" Text="Reporting Month" CssClass="auto-style3"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <asp:Label ID="lblrepmonth" CssClass="auto-style3" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
            <asp:Label ID="Label23" ForeColor="White" runat="server" Text="Reporting Week" CssClass="auto-style3"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
            <asp:Label ID="lblrepweek" runat="server" Text="" CssClass="auto-style3"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label22" ForeColor="White" runat="server" Text="Compliance To" CssClass="auto-style3"></asp:Label>
        </td>
        <td>

            <asp:CheckBox ID="chkcomp1" Text="HIRA reviewed daily and conducted for every new activity & all control measures complied" ValidationGroup="Compliance" runat="server" />
            <br />
            <asp:CheckBox ID="chkcomp2" Text="Dynamic HIRA Talk conducted for every activity daily" ValidationGroup="Compliance" runat="server" />
            <br />
            <asp:CheckBox ID="chkcomp3" Text="Permit to Work signed off with all control measures complied before commencement of work" ValidationGroup="Compliance" runat="server" />
            <br />
            <asp:CheckBox ID="chkcomp4" Text="All mandatory checklists complied as relevant to activities daily" ValidationGroup="Compliance" runat="server" />
            <br />
            <asp:CheckBox ID="chkcomp5" Text="Weekly Cross Functional Team audit & Weekly Safety Review Meeting conducted" ValidationGroup="Compliance" runat="server" />

        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2" align="Center">
            <asp:CheckBox ID="chkCertify" Text="As the process owner, I take full accountability for the above furnished details & am self-certifying for the reporting period" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="Center">
            <asp:Button ID="btnsubmit" runat="server" Text="Submit" Font-Size="Medium" OnClick="btnsubmit_Click" />
    </tr>
</table>
<p>
    &nbsp;
</p>

<asp:Label ID="lblProjectCode" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
<asp:Label ID="lblNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" CssClass="auto-style2" runat="server" ></asp:Label>
<asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
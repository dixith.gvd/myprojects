﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TemplateReports.ascx.cs" Inherits="QMDDashboard_TPL.TemplateReports.TemplateReports" %>
<table>
    <tr>
        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
            <asp:Label ID="lblYear" ForeColor="White" runat="server" Text="Year"></asp:Label>
        </td>
        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
            <asp:DropDownList ID="ddlyear" runat="server" OnSelectedIndexChanged="ddlyear_SelectedIndexChanged" AutoPostBack="True" Width="200px"></asp:DropDownList></td>
    </tr>
    <tr>
        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
               <asp:Label ID="lblMonth" ForeColor="White" runat="server" Text="Month"></asp:Label>
        </td>
        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
            <asp:DropDownList ID="ddlMonth" runat="server" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged" AutoPostBack="True" Width="200px"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
               <asp:Label ID="lblTemplates" ForeColor="White" runat="server" Text="Templates"></asp:Label>
        </td>
        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
            <asp:DropDownList ID="ddlTemplates" runat="server" OnSelectedIndexChanged="ddlTemplates_SelectedIndexChanged" AutoPostBack="True" Width="200px"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="background-color:#507CD1; width:40%; text-align:left; height:30px; padding-right:10px;">
               <asp:Label ID="lblPeriod" ForeColor="White" runat="server" Text="Period"></asp:Label>
        </td>
        <td style="background-color:#fff; padding-left:10px; width:60%; text-align:left; height:30px;">
            <asp:DropDownList ID="ddlPeriod" runat="server" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align:center;">
            <asp:Button ID="btnSubmit" runat="server" Text="Show Report" OnClick="btnSubmit_Click" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
             <asp:Label ID="lblNote" ForeColor="Black" runat="server" Text="Note :- Approved reports for the selected period"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
              <asp:Label ID="lblErrorMessage" ForeColor="Red" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
           <%-- <asp:GridView ID="grdtemplateReport" runat="server"></asp:GridView>--%>
            <asp:GridView ID="grdtemplateReport" runat="server" AutoGenerateColumns="false" ForeColor="#333333" BorderWidth="1">
                <AlternatingRowStyle BackColor="White" />
                <Columns>         
                    <asp:BoundField DataField="Project Code" HeaderText="Project Code"/>                    
                    <asp:BoundField DataField="Project Name" HeaderText="Project Name"/>
                    <asp:BoundField DataField="FQE_Name" HeaderText="FQE Name"/>                    
                    <asp:BoundField DataField="Template Name" HeaderText="Template Name" />                    
                    <asp:BoundField DataField="SBU" HeaderText="SBU" />             
                </Columns>  
                  <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />            
            </asp:GridView>
        </td>
    </tr>
</table>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using System.Xml;
using System.IO;
using System.Xml.XPath;
using System.Data;
using System.Collections;
using Microsoft.Office;
using Microsoft.SharePoint.WorkflowActions;
using Microsoft.SharePoint.Workflow;
using System.Configuration;
using Microsoft.Office.InfoPath;
using System.Data.SqlClient;
namespace MoveInfopathDoc
{
    class Program
    {
        static void Main(string[] args)
        {
            string fpath = ConfigurationManager.AppSettings["fPath"].ToString();
            DateTime startime = DateTime.Now;
            MoveDoc objChecklist = new MoveDoc();
            Console.WriteLine("Started");
            Console.WriteLine("start time is:" + startime);
            StringBuilder sb = new StringBuilder();
            sb.Append(startime);
            objChecklist.StartProcess();
            Console.WriteLine("Completed...");
            DateTime endtime = DateTime.Now;
            Console.WriteLine("end time is:" + endtime);
            //sb.Append("");
            sb.Append(endtime);
            File.AppendAllText(fpath + "log.txt", sb.ToString());
            sb.Clear();
            Console.Read();
        }
    }
}

﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
namespace QMDDashboard_TPL.ChecklistsModule
{
    public class QualityChecklists
    {
        public DataTable GetProjectCodeByLoggedInUser()
        {
            //SPListItem projectCode = null;
            DataTable dt = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><And><Eq><FieldRef Name='Field_x0020_Quality_x0020_Engine' /><Value Type='Integer'><UserID Type='Integer'/></Value></Eq><Eq><FieldRef Name='Quality_x0020_Active' /><Value Type='Boolean'>1</Value></Eq></And></Where>";
                            //query.Query = "<Where><Eq><FieldRef Name='Field_x0020_Quality_x0020_Engine' /><Value Type='Integer'><UserID Type='Integer'/></Value></Eq></Where>";
                            //query.ViewFields = "<FieldRef Name='Project_x0020_Code' />";
                            //SPListItemCollection items = list.GetItems(query);
                            //foreach (SPListItem item in items)
                            //{
                            //    if (item["Project_x0020_Code"] != null)
                            //    {
                            //        projectCode = item;
                            //        // break;
                            //    }
                            //}
                            dt = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dt;
        }
        public DataTable GetProjectCodeByLoggedInUserSafety()
        {
            //SPListItem projectCode = null;
            DataTable dt = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            //query.Query = "<Where><Eq><FieldRef Name='Field_x0020_Quality_x0020_Engine' /><Value Type='Integer'><UserID /></Value></Eq></Where>";
                            query.Query = "<Where><Eq><FieldRef Name='Safety_x0020_Officer' /><Value Type='Integer'><UserID /></Value></Eq></Where>";
                            //query.Query = "<Where><IsNotNull><FieldRef Name='Safety_x0020_Officer' /></IsNotNull></Where>";
                            //query.ViewFields = "<FieldRef Name='Project_x0020_Code' />";
                            //SPListItemCollection items = list.GetItems(query);
                            //foreach (SPListItem item in items)
                            //{
                            //    if (item["Project_x0020_Code"] != null)
                            //    {
                            //        projectCode = item;
                            //        // break;
                            //    }
                            //}
                            dt = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dt;
        }
        public DataTable GetProjectCodeByLoggedInUserRCM()
        {
            DataTable dt = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='RCM' /><Value Type='Integer'><UserID /></Value></Eq></Where>";
                            dt = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dt;
        }
        public SPListItem GetProjectCodeFromList()
        {
            SPListItem projectCode = null;
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            //query.Query = "<Where><Eq><FieldRef Name='Field_x0020_Quality_x0020_Engine' /><Value Type='Integer'><UserID /></Value></Eq></Where>";
                            query.Query = "<Where><Eq><FieldRef Name='Field_x0020_Quality_x0020_Engine' /><Value Type='Integer'><UserID Type='Integer'/></Value></Eq></Where>";
                            //query.ViewFields = "<FieldRef Name='Project_x0020_Code' />";
                            SPListItemCollection items = list.GetItems(query);
                            foreach (SPListItem item in items)
                            {
                                if (item["Project_x0020_Code"] != null)
                                {
                                    projectCode = item;
                                    // break;
                                }
                            }
                            //dt = list.GetItems(query).GetDataTable();
                            //web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return projectCode;
        }
        public DataTable GetProjectdetailsByProjectCode(string projectCode)
        {
            DataTable dtProjectDetails = new DataTable();
            // SPListItem projectCodeItem = null;
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            //query.Query = "<Where><Eq><FieldRef Name='Title' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                            query.Query = "<Where><Eq><FieldRef Name='Project_x0020_Code' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                            // SPListItemCollection items = list.GetItems(query);
                            //foreach (SPListItem item in items)
                            //{
                            //    projectCodeItem = item;
                            //    break;
                            //}
                            dtProjectDetails = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dtProjectDetails;
        }
        public DataTable GetProjectTypesByProcess()
        {
            DataTable dtProjectTypes = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                //con.Open();
                string sqlQuery = "select distinct [Project type] from [ComplianceList] where Type ='QPR'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtProjectTypes);
                //con.Close();
            }
            catch (Exception exp)
            {
            }
            return dtProjectTypes;
        }
        public DataTable GetProjectTypesByProcessFQP()
        {
            DataTable dtProjectTypes = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                //con.Open();
                string sqlQuery = "select distinct [Project type] from [ComplianceList] where Type ='FQP'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtProjectTypes);
                //con.Close();
            }
            catch (Exception exp)
            {
            }
            return dtProjectTypes;
        }
        public DataTable GetDesciplinesByProcess()
        {
            DataTable dtDiscipline = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();
                string sqlQuery = "select distinct [Discipline] from [ComplianceList] where Type ='QPR'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return dtDiscipline;
        }
        public DataTable GetDesciplinesByProcessFQP()
        {
            DataTable dtDiscipline = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();
                string sqlQuery = "select distinct [Discipline] from [ComplianceList] where Type ='FQP'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return dtDiscipline;
        }
        public DataTable GetLocationsByProjectCode(string projectCode)
        {
            DataTable dtDiscipline = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();
                string sqlQuery = "select distinct Location from [Loc_SubLoc] where Project='" + projectCode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return dtDiscipline;
        }
        public DataTable GetProjectCodeByProjectName(string projectCode)
        {
            DataTable dtProjectDetails = new DataTable();
            // SPListItem projectCodeItem = null;
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["TPL Projects List"] != null)
                    {
                        SPList list = web.Lists["TPL Projects List"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Contains><FieldRef Name='Final_x0020_Project_x0020_Name' /><Value Type='Text'>" + projectCode + "</Value></Contains></Where>";
                            //query.ViewFields = "<FieldRef Name='Final_x0020_Project_x0020_Name' /><FieldRef Name='SBUName' />";
                            // SPListItemCollection items = list.GetItems(query);
                            //foreach (SPListItem item in items)
                            //{
                            //    projectCodeItem = item;
                            //    break;
                            //}
                            dtProjectDetails = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dtProjectDetails;
        }
        public SPListItem GetProjectCodeByProjectNameForRCM(string projectCode)
        {
            SPListItem projectCodeForRCM = null;
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='Project_x0020_Code' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                            //query.ViewFields = "<FieldRef Name='Final_x0020_Project_x0020_Name' /><FieldRef Name='SBUName' />";
                            SPListItemCollection items = list.GetItems(query);
                            foreach (SPListItem item in items)
                            {
                                projectCodeForRCM = item;
                                break;
                            }
                            web.AllowUnsafeUpdates = false;
                            //dtProjectDetails = list.GetItems(query).GetDataTable();
                            //web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return projectCodeForRCM;
        }
        public DataTable GetProjectDetailsByProjectName(string projectName)
        {
            DataTable dtProjectDetails = new DataTable();
            // SPListItem projectCodeItem = null;
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["TPL Projects List"] != null)
                    {
                        SPList list = web.Lists["TPL Projects List"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = " <Where><Eq><FieldRef Name='Final_x0020_Project_x0020_Name' /><Value Type='Calculated'>" + projectName + "</Value></Eq></Where>";
                            //query.ViewFields = "<FieldRef Name='Final_x0020_Project_x0020_Name' /><FieldRef Name='SBUName' />";
                            // SPListItemCollection items = list.GetItems(query);
                            //foreach (SPListItem item in items)
                            //{
                            //    projectCodeItem = item;
                            //    break;
                            //}
                            dtProjectDetails = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dtProjectDetails;
        }
        public DataTable GetSubLocationsBylocations(string location, string pcode)
        {
            DataTable dtSubLocations = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();
                string sqlQuery = "select distinct [Sub Location] from [Loc_SubLoc] where Location='" + location + "' and project='" + pcode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtSubLocations);
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return dtSubLocations;
        }
        public DataTable GetMainActivity(string ProjectType, string discipline)
        {
            DataTable dtDiscipline = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct MainActivity from ComplianceList where Type ='QPR' and [Project type]='" + ProjectType.Trim() + "' and Discipline='" + discipline.Trim() + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtDiscipline);
            }
            catch (Exception exp)
            {
            }
            return dtDiscipline;
        }
        public DataTable GetActivities(string ProjectType, string discipline)
        {
            DataTable dtDiscipline = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct Activity from ComplianceList where Type ='QPR' and [Project type]='" + ProjectType.Trim() + "' and Discipline='" + discipline.Trim() + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtDiscipline);
            }
            catch (Exception exp)
            {
            }
            return dtDiscipline;
        }
        public DataTable GetMainActivityforFQP(string ProjectType, string discipline)
        {
            DataTable dtDiscipline = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct MainActivity from ComplianceList where Type ='FQP' and [Project type]='" + ProjectType.Trim() + "' and Discipline='" + discipline.Trim() + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtDiscipline);
            }
            catch (Exception exp)
            {
            }
            return dtDiscipline;
        }
        public DataTable GetActivitiesforFQP(string ProjectType, string discipline)
        {
            DataTable dtDiscipline = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct Activity from ComplianceList where Type ='FQP' and [Project type]='" + ProjectType.Trim() + "' and Discipline='" + discipline.Trim() + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtDiscipline);
            }
            catch (Exception exp)
            {
            }
            return dtDiscipline;
        }
        public int GetRowsSpanOfActvity(string ProjectType, string discipline, string activity)
        {
            int count = 0;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from [dbo].[ComplianceList] where Type='QPR' and [Project type]='" + ProjectType + "' and Discipline='" + discipline + "' and Activity='" + activity + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dtDiscipline = new DataTable();
                sda.Fill(dtDiscipline);
                if (dtDiscipline != null)
                {
                    count = dtDiscipline.Rows.Count;
                }
            }
            catch (Exception exp)
            {
            }
            return count;
        }
        public int GetRowsSpanOfSubActvity(string ProjectType, string discipline, string activity, string subActivity)
        {
            int count = 0;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from [dbo].[ComplianceList] where Type='QPR' and [Project type]='" + ProjectType + "' and Discipline='" + discipline + "' and Activity='" + activity + "'  and [Sub Activity]='" + subActivity + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dtDiscipline = new DataTable();
                sda.Fill(dtDiscipline);
                if (dtDiscipline != null)
                {
                    count = dtDiscipline.Rows.Count;
                }
            }
            catch (Exception exp)
            {
            }
            return count;
        }
        public DataTable GetSubactivities(string actvity, string projectType, string discipline)
        {
            DataTable dtSubActivities = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct [Sub Activity] from ComplianceList where Type ='QPR' and [Project type]='" + projectType.Trim() + "' and Discipline='" + discipline.Trim() + "' and Activity='" + actvity.Trim() + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtSubActivities);
            }
            catch (Exception exp)
            {
            }
            return dtSubActivities;
        }
        public DataTable GetSubactivitiesforFQP(string actvity, string projectType, string discipline)
        {
            DataTable dtSubActivities = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct [Sub Activity] from ComplianceList where Type ='FQP' and [Project type]='" + projectType.Trim() + "' and Discipline='" + discipline.Trim() + "' and Activity='" + actvity.Trim() + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtSubActivities);
            }
            catch (Exception exp)
            {
            }
            return dtSubActivities;
        }
        public DataTable GetDefectCategoryforFQP(string mainactivity)
        {
            DataTable dtdefectcat = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct Main_Defect_Category from DefectCategoryList where MainActivity ='" + mainactivity + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtdefectcat);
            }
            catch (Exception exp)
            {
            }
            return dtdefectcat;
        }
        public DataTable GetRootCauseCategoryforFQP()
        {
            DataTable dtdefectcat = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct RootCauseCategory from RootCauseList";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtdefectcat);
            }
            catch (Exception exp)
            {
            }
            return dtdefectcat;
        }
        public DataTable GetRootCauseforFQP(string rootcause)
        {
            DataTable dtdefectcat = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct RootCause from RootCauseList where RootCauseCategory='" + rootcause + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtdefectcat);
            }
            catch (Exception exp)
            {
            }
            return dtdefectcat;
        }
        public DataTable GetMainActivityforFQP()
        {
            DataTable dtdefectcat = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct MainActivity from DefectCategoryList";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtdefectcat);
            }
            catch (Exception exp)
            {
            }
            return dtdefectcat;
        }
        public DataTable GetSubDefectCategoryforFQP(string mainactivity, string defectcategory)
        {
            DataTable dtdefectcat = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct Sub_Defect_Category from DefectCategoryList where MainActivity ='" + mainactivity + "' and Main_Defect_Category='" + defectcategory + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtdefectcat);
            }
            catch (Exception exp)
            {
            }
            return dtdefectcat;
        }
        public DataTable GetCompliances(string actvity, string subactivity, string projectType, string discipline)
        {
            DataTable dtSubActivities = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct Compliance from ComplianceList where Type ='QPR' and [Project type]='" + projectType.Trim() + "' and Discipline='" + discipline.Trim() + "' and Activity='" + actvity.Trim() + "' and [Sub Activity]='" + subactivity + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtSubActivities);
            }
            catch (Exception exp)
            {
            }
            return dtSubActivities;
        }
        public DataTable GetCompliancesforFQP(string actvity, string subactivity, string projectType, string discipline)
        {
            DataTable dtSubActivities = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select Compliance from ComplianceList where Type ='FQP' and [Project type]='" + projectType.Trim() + "' and Discipline='" + discipline.Trim() + "' and Activity='" + actvity.Trim() + "' and [Sub Activity]='" + subactivity + "' order by RowID ASC";
                // string sqlQuery = "select distinct Compliance from ComplianceList where Type ='FQP' and [Project type]='" + projectType.Trim() + "' and Discipline='" + discipline.Trim() + "' and Activity='" + actvity.Trim() + "' and [Sub Activity]='" + subactivity + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtSubActivities);
            }
            catch (Exception exp)
            {
            }
            return dtSubActivities;
        }
        public string GetOnlyEmployeeID(string loggedInName)
        {
            string empID = string.Empty;
            string loggedNameLowerCase = loggedInName.ToLower().ToString();
            if (loggedNameLowerCase.Contains("tpl"))
            {
                string replaced = loggedNameLowerCase.Replace('\\', '@');
                if (replaced.Contains("@"))
                {
                    string[] seperators = { "@" };
                    string[] nameArray = replaced.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
                    if (nameArray.Length == 2)
                    {
                        if (nameArray[1] != null)
                        {
                            empID = nameArray[1];
                        }
                    }
                }
                else
                {
                    string replacedTest = loggedNameLowerCase.Replace('\\', '@');
                }
            }
            return empID.ToUpper();
        }
        public bool SaveChecklistByMaster(string stage, string bu, string projectName, string projectCode, string location, string subLocation, string complianceType, string projectType, string discipline, string activity, string subActivity, string isComplaint, string FQEid, string FQEName, string FQEEmail, string FQESubmittedDate, string rcmID, string rcmName, string rcmEmail, string pmID, string pmName, string pmEmail, string newSBUName, string hop_Name, string hop_Email, string pe_Id, string pe_Name, string pe_Email, string buhead_Id, string buhead_Name, string buhead_Email, string identification, string sbgname, string mainactivity)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into QChecklist_TnxMain(Stage,BUName,ProjectName,ProjectCode,Location,SubLocation,Compliance_Type,ProjectType,Discipline,Activity,[Sub Activity],isCompliant,FQE_ID,FQE_Name,FQE_Email,FQE_Submitted_Date,RCM_ID,RCM_Name,RCM_Email,PM_ID,PM_Name,PM_Email,SBUName,HOP_Name,HOP_Email,PE_ID,PE_Name,PE_Email,BUHead_ID,BUHead_Name,BUHead_Email,Identification,SBGName,MainActivity)");
            insertQuery.Append(" values ("); insertQuery.Append("'" + stage + "',"); insertQuery.Append("'" + bu + "',"); insertQuery.Append("'" + projectName + "',"); insertQuery.Append("'" + projectCode + "',"); insertQuery.Append("'" + location + "',"); insertQuery.Append("'" + subLocation + "',");
            insertQuery.Append("'" + complianceType + "',"); insertQuery.Append("'" + projectType + "',"); insertQuery.Append("'" + discipline + "',"); insertQuery.Append("'" + activity + "',"); insertQuery.Append("'" + subActivity + "',"); insertQuery.Append("'" + isComplaint + "',"); insertQuery.Append("'" + FQEid + "',");
            insertQuery.Append("'" + FQEName + "',"); insertQuery.Append("'" + FQEEmail + "',"); insertQuery.Append("'" + FQESubmittedDate + "',"); insertQuery.Append("'" + rcmID + "',"); insertQuery.Append("'" + rcmName + "',"); insertQuery.Append("'" + rcmEmail + "',"); insertQuery.Append("'" + pmID + "',"); insertQuery.Append("'" + pmName + "',");
            insertQuery.Append("'" + pmEmail + "',"); insertQuery.Append("'" + newSBUName + "',"); insertQuery.Append("'" + hop_Name + "',"); insertQuery.Append("'" + hop_Email + "',"); insertQuery.Append("'" + pe_Id + "',"); insertQuery.Append("'" + pe_Name + "',"); insertQuery.Append("'" + pe_Email + "',");
            insertQuery.Append("'" + buhead_Id + "',"); insertQuery.Append("'" + buhead_Name + "',"); insertQuery.Append("'" + buhead_Email + "',"); insertQuery.Append("'" + identification + "',"); insertQuery.Append("'" + sbgname + "',"); insertQuery.Append("'" + mainactivity + "')");
            try
            {
                SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public bool SaveChecklist(int Tid ,string frequency, string checklist, string bu, string project, string projectcode, string inspdate, string compliance, string stage, string location, string observations, string SOID, string SOName, string SOEmail, string RCMID, string RCMName, string RCMEmail, string PMID, string PMName, string PMEmail, string hop_id, string hop_Name, string hop_Email, string buheadid, string buheadName, string buheadEmail, string LastStageUpdate, string selfcert, string newsbu)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into EHSChecklist(Transaction_ID,Frequency,[Checklist Name],BUName,ProjectName,ProjectCode,[Inspection Date],isCompliant,Stage,Location,[SO Remarks],SO_ID,SO_Name,SO_Email,RCM_ID,RCM_Name,RCM_Email,PM_ID,PM_Name,PM_Email,HOP_ID,HOP_Name,HOP_Email,BUHead_ID,BUHead_Name,BUHead_Email,[SO Submission Date],LastStageUpdate,[Self Certification],SBUName)");
            insertQuery.Append(" values ("); insertQuery.Append("" + Tid + ","); insertQuery.Append("'" + frequency + "',"); insertQuery.Append("'" + checklist + "',"); insertQuery.Append("'" + bu + "',"); insertQuery.Append("'" + project + "',"); insertQuery.Append("'" + projectcode + "',"); insertQuery.Append("'" + inspdate + "',"); insertQuery.Append("'" + compliance + "',");
            insertQuery.Append("'" + stage + "',"); insertQuery.Append("'" + location + "',"); insertQuery.Append("'" + observations + "',"); insertQuery.Append("'" + SOID + "',"); insertQuery.Append("'" + SOName + "',"); insertQuery.Append("'" + SOEmail + "',");
            insertQuery.Append("'" + RCMID + "',"); insertQuery.Append("'" + RCMName + "',"); insertQuery.Append("'" + RCMEmail + "',"); insertQuery.Append("'" + PMID + "',"); insertQuery.Append("'" + PMName + "',");
            insertQuery.Append("'" + PMEmail + "',"); insertQuery.Append("'" + hop_id + "',"); insertQuery.Append("'" + hop_Name + "',"); insertQuery.Append("'" + hop_Email + "',");
            insertQuery.Append("'" + buheadid + "',"); insertQuery.Append("'" + buheadName + "',"); insertQuery.Append("'" + buheadEmail + "',"); insertQuery.Append("'" + LastStageUpdate + "',"); insertQuery.Append("'" + LastStageUpdate + "',"); insertQuery.Append("'" + selfcert + "',"); insertQuery.Append("'" + newsbu + "')");
            try
            {
                SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public bool SaveRCMData(string rcmremarks, string targetdate, string tranasactionID)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update EHSChecklist set [RCM Comments]='" + rcmremarks + "',[Target Closure Date]='" + targetdate + "' where Transaction_ID='" + tranasactionID + "'";
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public bool UpdateSWIInfoInParent(string tranasactionID, string swiID, string package, string subContractor, string swidate, string isSWirequired, string swiissuedby)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            //string sqlQuery = "UPDATE QChecklist_TnxMain (SWIID,SWIDate, Package,Subcontractor,isSWIRequired) VALUES ('" + swiID + "','" + swidate + "','" + package + "','" + subContractor + "','" + isSWirequired + "') WHERE Transaction_ID='" + tranasactionID + "'";
            string sqlQuery = "update QChecklist_TnxMain set SWIID='" + swiID + "',SWIDate='" + swidate + "',Package='" + package + "',Subcontractor='" + subContractor + "',isSWIRequired='" + isSWirequired + "',SWIIssuedBy='" + swiissuedby + "'  where Transaction_ID='" + tranasactionID + "'";
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public bool InsertRWCInfoInChild(string tranasactionID, string rwcNumber, string rwcdate, string Package, string subContractor, string powoNo, string NCRFINCCR, string NC_Details, string NC_RootCause)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into FQP_Rework(Transaction_ID,RWCID,RWC_Date,Package,Subcontractor,PO_No,NCRFINCCR,NC_Details,NC_RootCause)");
            insertQuery.Append(" values ("); insertQuery.Append("'" + tranasactionID + "',"); insertQuery.Append("'" + rwcNumber + "',"); insertQuery.Append("'" + rwcdate + "',");
            insertQuery.Append("'" + Package + "',"); insertQuery.Append("'" + subContractor + "',"); insertQuery.Append("'" + powoNo + "',");
            insertQuery.Append("'" + NCRFINCCR + "',"); insertQuery.Append("'" + NC_Details + "',"); insertQuery.Append("'" + NC_RootCause + "')");
            try
            {
                SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public bool UpdateRWCInParentforPE(string tranasactionID, string stage, string isSWIRequired, string isRWCrequired, string peID, string peName, string peEmail, string ncissuedby, string maindefect, string subdefect, string rootcausecategory, string rootcause, string why1, string why2, string why3, string why4, string why5)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            //string sqlQuery = "UPDATE QChecklist_TnxMain (SWIID,SWIDate, Package,Subcontractor,isSWIRequired) VALUES ('" + swiID + "','" + swidate + "','" + package + "','" + subContractor + "','" + isSWirequired + "') WHERE Transaction_ID='" + tranasactionID + "'";
            string sqlQuery = "update QChecklist_TnxMain set Stage='" + stage + "',isRWCRequired='" + isRWCrequired + "',PE_ID='" + peID + "',PE_Name='" + peName + "',PE_Email='" + peEmail + "', isSWIRequired='" + isSWIRequired + "',SWIIssuedBy='" + ncissuedby + "',MaindefectCategory='" + maindefect + "',SubDefectCategory='" + subdefect + "',RootCauseCategory='" + rootcausecategory + "',RootCause='" + rootcause + "',Why1='" + why1 + "',Why2='" + why2 + "',Why3='" + why3 + "',Why4='" + why4 + "',Why5='" + why5 + "' where Transaction_ID='" + tranasactionID + "'";
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public bool UpdateRWCInParent(string tranasactionID, string stage, string isSWIRequired, string isRWCrequired, string peID, string peName, string peEmail)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            //string sqlQuery = "UPDATE QChecklist_TnxMain (SWIID,SWIDate, Package,Subcontractor,isSWIRequired) VALUES ('" + swiID + "','" + swidate + "','" + package + "','" + subContractor + "','" + isSWirequired + "') WHERE Transaction_ID='" + tranasactionID + "'";
            string sqlQuery = "update QChecklist_TnxMain set Stage='" + stage + "',isRWCRequired='" + isRWCrequired + "',RCM_ID='" + peID + "',RCM_Name='" + peName + "',RCM_Email='" + peEmail + "', isSWIRequired='" + isSWIRequired + "' where Transaction_ID='" + tranasactionID + "'";
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public bool SaveChecklistChild(string transactionID, string complaincetext, string isComplaint, string swiRemarks)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into QChecklist_TnxChild(Transaction_ID,Compliance_Text,isCompliant,SWIRemarks)");
            insertQuery.Append(" values ("); insertQuery.Append("'" + transactionID + "',"); insertQuery.Append("'" + complaincetext + "',"); insertQuery.Append("'" + isComplaint + "',"); insertQuery.Append("'" + swiRemarks + "')");
            try
            {
                SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public bool UpdateSWIInfoInChild(string tranasactionID, string swiRemarks, string compliancetext)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update QChecklist_TnxChild set SWIRemarks='" + swiRemarks + "' where Transaction_ID='" + tranasactionID + "' and Compliance_Text='" + compliancetext.Trim() + "'";
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public DataTable GetChecklistDatabyTransactionID(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QChecklist_TnxMain where Transaction_ID='" + transactionID + "' and  Compliance_Type ='QPR'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetChecklistDatabyTransactionIDSafety(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetChecklistDataRCM(string uname)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QChecklist_TnxMain where RCM_Email='" + uname + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetChecklistDataAssignee(string uname)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QChecklist_TnxMain where Assignee_Email='" + uname + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetChecklistDataPE(string uname)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QChecklist_TnxMain where PE_Email='" + uname + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetChecklistDataFQE(string uname)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QChecklist_TnxMain where FQE_Email='" + uname + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetChecklistDatabyTransactionIDForFQP(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QChecklist_TnxMain where Transaction_ID='" + transactionID + "' and  Compliance_Type ='FQP'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetDatabyTransactionID(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QChecklist_TnxMain where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public string GetLatestTransactionID()
        {
            string transactionID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 Transaction_ID FROM [EQHSAutomation].[dbo].[QChecklist_TnxMain] ORDER BY Transaction_ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Transaction_ID"] != null)
                        {
                            transactionID = dt.Rows[0]["Transaction_ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return transactionID;
        }
        public string GetLatestTransactionIDSafety()
        {
            string transactionID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 Transaction_ID FROM [EQHSAutomation].[dbo].[EHSChecklist] ORDER BY Transaction_ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Transaction_ID"] != null)
                        {
                            transactionID = dt.Rows[0]["Transaction_ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return transactionID;
        }
        public DataTable GetComplaincesDatabyTransactionID(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QChecklist_TnxChild where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
                if (dtChecklistData != null)
                {
                    dtChecklistData.Columns.Add("SNo", typeof(string));
                    for (int i = 0; i < dtChecklistData.Rows.Count; i++)
                    {
                        int sno = i + 1;
                        dtChecklistData.Rows[i]["SNo"] = sno.ToString();
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetComplaincesDatabyTransactionIDforFQP(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QChecklist_TnxChild where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
                if (dtChecklistData != null)
                {
                    dtChecklistData.Columns.Add("SNo", typeof(string));
                    for (int i = 0; i < dtChecklistData.Rows.Count; i++)
                    {
                        int sno = i + 1;
                        dtChecklistData.Rows[i]["SNo"] = sno.ToString();
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetNCRUploadDatabyTransactionIDforFQP(string transactionID)
        {
            DataTable dtNCRData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from Qchecklist_NCRdoc where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtNCRData);
            }
            catch (Exception exp)
            {
            }
            return dtNCRData;
        }
        public bool SendAssigneeMail(string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail;
            //  string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(pmEmail, ";", CCMails);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "QPR-Assigned to you";
            string Link = Constants.link + "Pages/QMDChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear Sir");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The Non-compliance in QPRC for the below mentioned sub activity is assigned to you by the RCM.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            //sbBody.Append("Non Complience");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendAssigneeMailSafety(string rcmname, string rcmEmail, string EmailToSent,string soname, string project, string checklist, string assigneename, string observation, string targetclosuredate, string remarks, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail;
            string ccMail2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string ccMail2 = "deekshithgvd-t@tataprojects.com";
            string ccMail = string.Concat(pmEmail, ";", rcmEmail, ";", ccMail2, ";", CCMails);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist is assigned to you";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + assigneename);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(soname + " has uploaded a checklist that does not meet compliance standards so it is assigned to you by " + rcmname + " for your action.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by -" + soname);
            sbBody.Append("<br>");
            sbBody.Append("Non compliance observations - " + observation);
            sbBody.Append("<br>");
            sbBody.Append("Target Date for closure - " + Convert.ToDateTime(targetclosuredate).ToShortDateString());
            sbBody.Append("<br>");
            sbBody.Append("RCM comments - " + remarks);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking link  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            //sbBody.Append("Non Complience");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendRCMMail(string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            // string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(pmEmail, ";", CCMails);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "QPR - Checklist submitted";
            string nonCompli = "has uploaded a check list for QPRC where in a Non-compliance is observed for the below mentioned sub activity.";
            string Link = Constants.link + "Pages/QMDChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(fqeName + " " + nonCompli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendRCMMailforFQP(string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string fqemail = fqeEmail;
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", fqemail);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQPC-Checklist submitted, Transaction ID - " + transactionId + " Not Complied";
            string nonCompli = "has uploaded a check list for FQPC, where in a Non - Compliance is observed for the mentioned sub activity..";
            string Link = Constants.link + "Pages/FQPChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(fqeName + " " + nonCompli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendPEtoRCMMailforFQP(string rcmEmail, string peName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string fqemail = fqeEmail;
            string rcmmail = fqeEmail;
            string ccMail = string.Concat(rcmmail, ";", pmEmail, ";", CCMails, ";", fqemail);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQPC-Checklist submitted, Transaction ID - " + transactionId + " Not Complied";
            string nonCompli = "has uploaded a check list for FQPC along with ReworkCard, where in a Non - Compliance is observed for the below mentioned sub activity.";
            string Link = Constants.link + "Pages/FQPChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + peName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(fqeName + " " + nonCompli);
            sbBody.Append("<br>");
            sbBody.Append("Please indicate details of proposed Rework and estimated Rework Cost.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendPEtoRCMMail(string rcmEmail, string EmailToSent, string peEmail, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = rcmEmail;
            string fqeMail = fqeEmail;
            string peMail = peEmail;
            string CCMails = CoEEmail;
            //  string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", fqeEmail, ";", peMail);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQPC - Proposed Rework details updated - Transaction ID " + transactionId + " ";
            string Link = Constants.link + "Pages/FQPChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear Sir");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Updated the details of proposed Rework & estimated Rework cost by the Planning Engineer for the Non-compliance in FQPC for the below mentioned sub activity");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            //sbBody.Append("Non Complience");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendPEtoRCMMailMoreThanFive(string hopemail, string rcmEmail, string EmailToSent, string peEmail, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = rcmEmail;
            string fqeMail = fqeEmail;
            string peMail = peEmail;
            string hopmail = hopemail;
            string CCMails = CoEEmail;
            //  string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", hopmail, ";", fqeEmail, ";", peMail);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQPC - Proposed Rework details updated - Transaction ID " + transactionId + " ";
            string Link = Constants.link + "Pages/FQPChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear Sir");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Updated the details of proposed Rework procedure and estimated Rework cost by the Planning Engineer for the Non-compliance in FQPC for the below mentioned sub activity");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            //sbBody.Append("Non Complience");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendAssigneeMailforFQP(string rcmEmail, string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string fqeMail = fqeEmail;
            string rcmMail = rcmEmail;
            string CCMails = CoEEmail;
            //  string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", fqeMail, ";", rcmMail);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQPC - Assigned to you, Transaction ID -" + transactionId + "";
            string Link = Constants.link + "Pages/FQPChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear Sir");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The Non-compliance in FQPC for the below mentioned sub activity is assigned to you by the RCM/CPM.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            //sbBody.Append("Non Complience");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendAssigneeMailforFQPMoreThanFive(string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail, string hopEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail;
            string hopMail = hopEmail;
            //  string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", hopMail);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQP-Assigned to you";
            string Link = Constants.link + "Pages/FQPChecklistActions.aspx?Tid= " + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear Sir");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The Non-compliance in FQPC for the below mentioned sub activity is assigned to you by the PE.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            //sbBody.Append("Non Complience");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendCompRCMForClosureMail(string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            // string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(pmEmail, ";", CCMails);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "QPR - Verification & Close";
            string Link = Constants.link + "Pages/QMDChecklistActions.aspx?Tid= " + transactionId;
            string compli = "has uploaded a check list for QPRC with no Non-compliance for the below mentioned sub activity.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(fqeName + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("To view the item click <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendClosureMailSafety(string rcmName, string EmailToSent, string project, string checklist, string createdby, string observation, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string CCMails2 = "deekshithgvd-t@tataprojects.com";
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist submitted";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that meet compliance standards.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            sbBody.Append("Observation - " + observation);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendCompRCMForClosureMailSafety(string rcmName, string EmailToSent, string project, string checklist, string createdby, string observation, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string CCMails2 = "deekshithgvd-t@tataprojects.com";
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist submitted";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that does not meet compliance standards.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            sbBody.Append("Non Compliance observation - " + observation);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendCompRCMForClosureMailforFQP(string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string fqeMail = fqeEmail;
            string CCMails = CoEEmail.Trim();
            // string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(fqeMail, ";", CCMails);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQPC-Checklist submitted, Transaction ID - " + transactionId + "- Complied";
            string Link = Constants.link + "Pages/FQPChecklistActions.aspx?Tid=" + transactionId;
            string compli = "has uploaded a check list for FQPC with no Non-compliance for the below mentioned sub activity.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(fqeName + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("To view the item click <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendRCMForClosureMail(string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            // string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(pmEmail, ";", CCMails);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "QPR - Verification & Close";
            string Link = Constants.link + "Pages/QMDChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The assignee has taken necessary actions and closed the Non-compliance in QPRC.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendRCMForClosureMailsafety(string rcmName, string EmailToSent, string project, string checklist, string soname,string assigneename, string observation, string targetdate, string rcmremarks, string assigneeremarks,string closuredate, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string CCMails2 = "deekshithgvd-t@tataprojects.com";
            string ccMail = string.Concat(pmEmail, ";", EmailToSent, ";", CCMails2, ";", CCMails);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist closed";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Safety Officer " + soname + " had uploaded a checklist that does not meet compliance standards, which was assigned by You to " + assigneename + "who has taken the actions to address the gaps.");
            sbBody.Append("<br>");
            sbBody.Append("You are requested to verify and validate the actions towards closure.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + soname);
            sbBody.Append("<br>");
            sbBody.Append("Non compliance observations - " + observation);
            sbBody.Append("<br>");
            sbBody.Append("Target Date for closure - " + Convert.ToDateTime(targetdate).ToShortDateString());
            sbBody.Append("<br>");
            sbBody.Append("RCM comments -" + rcmremarks);
            sbBody.Append("<br>");
            sbBody.Append("Assignee action taken details - " + assigneeremarks);
            sbBody.Append("<br>");
            sbBody.Append("Closure Date - " + Convert.ToDateTime(closuredate).ToShortDateString());
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("You can also view the item by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendRCMForClosureMailforFQP(string AssigneEMail, string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string AssigneeMail = AssigneEMail;
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", AssigneeMail, ";", fqeEmail);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQPC - Verification & Closure, Transaction ID -" + transactionId + "";
            string Link = Constants.link + "Pages/FQPChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The assignee has taken necessary actions and closed the Non-compliance in FQPC. Details of the Rework done and actual Rework Cost are updated.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendRCMForClosureMailforFQPNoRWC(string AssigneEMail, string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string AssigneeMail = AssigneEMail;
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", AssigneeMail, ";", fqeEmail);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQPC - Verification & Closure, Transaction ID -" + transactionId + "";
            string Link = Constants.link + "Pages/FQPChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The assignee has taken necessary actions and closed the Non-compliance in FQPC.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendRCMForClosureMailforFQPMoreThanFive(string hopEmail, string AssigneEMail, string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string AssigneeMail = AssigneEMail;
            string hopMail = hopEmail;
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", hopMail, ";", AssigneeMail, ";", fqeEmail);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQPC - Verification & Closure, Transaction ID " + transactionId + "";
            string Link = Constants.link + "Pages/FQPChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The assignee has taken necessary actions and closed the Non-compliance in FQPC and updated the details of Rework done and actual Rework Cost.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendFinalClosureMail(string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            // string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(pmEmail, ";", CCMails);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "QPR Closure Email ";
            string Link = Constants.link + "Pages/ChecklistDetails.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + fqeName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The Non-compliance in QPRC for the below mentioned sub activity has been closed.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }

        public bool SendBacktoRCM(string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            // string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(pmEmail, ";", CCMails);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQPC send back for re-verification by FQE ";
            string Link = Constants.link + "Pages/FQPChecklistActions.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The Non-compliance in FQPC for the below mentioned sub activity has been send back for re-verification by Field Quality Engineer.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendFinalClosureMailforFQP(string AssigneeEmail, string rcmEmail, string rcmName, string EmailToSent, string fqeName, string fqeEmail, string project, string activity, string subActivity, string location, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string AssignEmail = AssigneeEmail;
            string rcmMail = rcmEmail;
            string CCMails = CoEEmail.Trim();
            // string CCMails = Constants.CoEMails;
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", AssignEmail, ";", rcmMail);
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "FQPC - Non Complaince Closed, Transaction ID -" + transactionId + "";
            string Link = Constants.link + "Pages/ChecklistDetails.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + fqeName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The Non-Compliance in FQPC for the below mentioned sub activity has been closed.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Activity - " + activity);
            sbBody.Append("<br>");
            sbBody.Append("Sub Activity -" + subActivity);
            sbBody.Append("<br>");
            sbBody.Append("Non-compliance Location - " + location);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public DataTable GetRWCbyTransactionID(string transactionID)
        {
            DataTable dtRWCData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from FQP_Rework where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtRWCData);
            }
            catch (Exception exp)
            {
            }
            return dtRWCData;
        }
        public DataTable GetRCMPendingItems(string rcmEmail, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from QChecklist_TnxMain where RCM_Email='" + rcmEmail + "' and stage='" + stage + "' and Compliance_Type='QPR'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        public DataTable GetPEPendingItemsforFQP(string peEmail, string stage)
        {
            DataTable dtPE = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from QChecklist_TnxMain where PE_Email='" + peEmail + "' and stage='" + stage + "' and Compliance_Type='FQP'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtPE);
            }
            catch (Exception exp)
            {
            }
            return dtPE;
        }
        public DataTable GetSafetyPendingItemsforRCM(string rcmEmail, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from EHSChecklist where RCM_Email='" + rcmEmail + "' and stage='" + stage + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        public DataTable GetSafetyPendingItemsforAssignee(string assigneeEmail, string stage)
        {
            DataTable dtAssignee = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from EHSChecklist where Assignee_Email='" + assigneeEmail + "' and stage='" + stage + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtAssignee);
            }
            catch (Exception exp)
            {
            }
            return dtAssignee;
        }
        public DataTable GetSafetyPendingItemsforSO(string SOEmail)
        {
            DataTable dtSO = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from EHSChecklist where (Stage='Pending with RCM' or Stage='Pending with Assignee') and SO_Email='" + SOEmail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtSO);
            }
            catch (Exception exp)
            {
            }
            return dtSO;
        }
        public DataTable GetSafetyPendingItemsforSOClosed(string SOEmail)
        {
            DataTable dtSO = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = " select * from EHSChecklist where Stage='Closed' and SO_Email='" + SOEmail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtSO);
            }
            catch (Exception exp)
            {
            }
            return dtSO;
        }
        public DataTable GetSafetyPendingItemsforHO()
        {
            DataTable dtSO = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist where Stage='Pending with RCM' or Stage='Pending with Assignee'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtSO);
            }
            catch (Exception exp)
            {
            }
            return dtSO;
        }
        public DataTable GetSafetyPendingItemsforHOClosed()
        {
            DataTable dtSO = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist where Stage='Closed'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtSO);
            }
            catch (Exception exp)
            {
            }
            return dtSO;
        }
        public DataTable GetRCMPendingItemsforFQP(string rcmEmail, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from QChecklist_TnxMain where RCM_Email='" + rcmEmail + "' and stage='" + stage + "' and Compliance_Type='FQP'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        public DataTable GetAssigneePendingItemsforFQP(string assigneeEmail, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from QChecklist_TnxMain where Assignee_Email='" + assigneeEmail + "' and stage='" + stage + "' and Compliance_Type='FQP'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        public DataTable GetAssigneePendingItems(string assigneeEmail, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from QChecklist_TnxMain where Assignee_Email='" + assigneeEmail + "' and stage='" + stage + "' and Compliance_Type='QPR'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        private Boolean InsertEvidenceDoc(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public bool SaveQualityChecklist(int Tid, string bu, string projectname, string projectcode, string reportingon, string evidencefor, string category, string comments, string rdate, string tdate, string itemstatus, string submissiondate, string lastupdatedate, string SOID, string SOName, string SOEmail, string RCMID, string RCMName, string RCMEmail, string PMID, string PMName, string PMEmail, string PEID, string PEName, string PEEmail, string hop_id, string hop_Name, string hop_Email, string buheadid, string buheadName, string buheadEmail)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into QualityUploads(Transaction_ID,BUName,ProjectName,ProjectCode,Reporting_On,Evidence_For,Category,Comments,Reporting_Date,Target_Date,ItemStatus,FQE_Submission_Date,Last_Update_Date,FQE_ID,FQE_Name,FQE_Email,RCM_ID,RCM_Name,RCM_Email,PM_ID,PM_Name,PM_Email,PE_ID,PE_Name,PE_Email,HOP_ID,HOP_Name,HOP_Email,BUHead_ID,BUHead_Name,BUHead_Email)");
            insertQuery.Append(" values ("); insertQuery.Append("" + Tid + ","); insertQuery.Append("'" + bu + "',"); insertQuery.Append("'" + projectname + "',"); insertQuery.Append("'" + projectcode + "',"); insertQuery.Append("'" + reportingon + "',");
            insertQuery.Append("'" + evidencefor + "',"); insertQuery.Append("'" + category + "',"); insertQuery.Append("'" + comments + "',"); insertQuery.Append("'" + rdate + "',"); insertQuery.Append("@targetdate,"); insertQuery.Append("'" + itemstatus + "',");
            insertQuery.Append("'" + submissiondate + "',"); insertQuery.Append("'" + lastupdatedate + "',"); insertQuery.Append("'" + SOID + "',"); insertQuery.Append("'" + SOName + "',"); insertQuery.Append("'" + SOEmail + "',");
            insertQuery.Append("'" + RCMID + "',"); insertQuery.Append("'" + RCMName + "',"); insertQuery.Append("'" + RCMEmail + "',"); insertQuery.Append("'" + PMID + "',"); insertQuery.Append("'" + PMName + "',");
            insertQuery.Append("'" + PMEmail + "',"); insertQuery.Append("'" + PEID + "',"); insertQuery.Append("'" + PEName + "',");
            insertQuery.Append("'" + PEEmail + "',"); insertQuery.Append("'" + hop_id + "',"); insertQuery.Append("'" + hop_Name + "',"); insertQuery.Append("'" + hop_Email + "',");
            insertQuery.Append("'" + buheadid + "',"); insertQuery.Append("'" + buheadName + "',"); insertQuery.Append("'" + buheadEmail + "')");
            SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
            cmd.Parameters.Add("@targetdate", SqlDbType.DateTime);
            if (reportingon == "Good Practice")
            {
                cmd.Parameters["@targetdate"].Value = DBNull.Value;
            }
            else
            {
                cmd.Parameters["@targetdate"].Value = DateTime.Parse(tdate);
            }
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public string GetLatestTransactionIDQuality()
        {
            string transactionID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 Transaction_ID FROM [EQHSAutomation].[dbo].[QualityUploads] ORDER BY Transaction_ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Transaction_ID"] != null)
                        {
                            transactionID = dt.Rows[0]["Transaction_ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return transactionID;
        }
        public DataTable GetCategoryList(string Evidence)
        {
            DataTable dtData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct Category from CategoryList where EvidenceFor='" + Evidence + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtData);
            }
            catch (Exception exp)
            {
            }
            return dtData;
        }
        public DataTable GetEvidenceData()
        {
            DataTable dtEvidence = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();
                string sqlQuery = "select distinct EvidenceFor from CategoryList";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtEvidence);
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return dtEvidence;
        }
        public void UpdateQualityUploadsEditDetails(string Tid, string corraction, string rfclosure, string fqecorrectiondate)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update QualityUploads set Corrective_Action='" + corraction + "',ReadyForClosure='" + rfclosure + "',FQE_Correction_Date='" + fqecorrectiondate + "',Last_Update_Date='" + fqecorrectiondate + "' where Transaction_ID='" + Tid + "'";
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {
            }
        }
        public DataTable GetAttachmentDatabyTransactionID(string transactionID, string listname)
        {
            DataTable dtData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from UploadAttachments where Transaction_ID='" + transactionID + "' and List='" + listname + "' and DocumentType='Supporting'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtData);
            }
            catch (Exception exp)
            {
            }
            return dtData;
        }
        public DataTable GetQualityUploadsDatabyTransactionID(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QualityUploads where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public void UpdateQualityStatus(string Tid, string status, string rcmsubdate)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update QualityUploads set ItemStatus='" + status + "',RCM_Submission_Date='" + rcmsubdate + "',Last_Update_Date='" + rcmsubdate + "' where Transaction_ID='" + Tid + "'";
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {
            }
        }
        public DataTable GetEvidenceAttachmentDatabyTransactionID(string transactionID, string listname)
        {
            DataTable dtData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from UploadAttachments where Transaction_ID='" + transactionID + "' and List='" + listname + "' and DocumentType='Evidence'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtData);
            }
            catch (Exception exp)
            {
            }
            return dtData;
        }
        public DataTable GetQualityUploadsPendingItems(string pcode, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from QualityUploads where ProjectCode in (" + pcode + ") and ItemStatus='" + stage + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        public DataTable GetQualityUploadsPendingItemsRCM(string pcode, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from QualityUploads where ProjectCode in (" + pcode + ") and ItemStatus='" + stage + "' and ReadyForClosure='True'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        public void SaveSelfCertification(int id, string buname, string projectname, string comp1, string comp2, string comp3, string comp4, string comp5, string certification, string created, string modified, string repmonth, string repweek, string projectcode, string createdby, string modifiedby,string repyear)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into Quality_SelfCertification(ID,[BU Name],[Project Name],[Comp 1],[Comp 2],[Comp 3],[Comp 4],[Comp 5],Certification,Created,Modified,[Reporting Month],[Rep Week],ProjectCode,[Created By],[Modified By],[Rep Year])");
            insertQuery.Append(" values ("); insertQuery.Append("" + id + ","); insertQuery.Append("'" + buname + "',"); insertQuery.Append("'" + projectname + "',");
            insertQuery.Append("'" + comp1 + "',"); insertQuery.Append("'" + comp2 + "',"); insertQuery.Append("'" + comp3 + "',"); insertQuery.Append("'" + comp4 + "',"); insertQuery.Append("'" + comp5 + "',");
            insertQuery.Append("'" + certification + "',"); insertQuery.Append("'" + created + "',"); insertQuery.Append("'" + modified + "',"); insertQuery.Append("'" + repmonth + "',");
            insertQuery.Append("'" + repweek + "',"); insertQuery.Append("'" + projectcode + "',"); insertQuery.Append("'" + createdby + "',"); insertQuery.Append("'" + modifiedby + "',"); insertQuery.Append("'" + repyear + "')");
            try
            {
                SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {
            }
        }
        public string GetLatestIDQualitySelfCert()
        {
            string ID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 ID FROM [EQHSAutomation].[dbo].[Quality_SelfCertification] ORDER BY ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["ID"] != null)
                        {
                            ID = dt.Rows[0]["ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return ID;
        }
        public DataTable GetQualitySelfCertificationDatabyID(string ID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from Quality_SelfCertification where ID='" + ID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetQualitySelfCertPendingItemsRCM(string pcode)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from Quality_SelfCertification where ProjectCode in (" + pcode + ")";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
    }
}

﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChecklistsActions.ascx.cs" Inherits="QMDDashboard_TPL.ChecklistsModule.ChecklistsActions.ChecklistsActions" %>



<style type="text/css">
    .required {
        color: Red;
    }
</style>
<script>
    function ReloadFun() {
        _spFormOnSubmitCalled = false; _spSuppressFormOnSubmitWrapper = true;
    }
</script>
<div id="dvFilledForm" runat="server">
    <table border="1" cellpadding="1" cellspacing="1" width="80%">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label12" runat="server" ForeColor="White" Text="QPR Compliance" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="lblTID" ForeColor="White" runat="server" Text="Transaction ID"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblTransID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label28" ForeColor="White" runat="server" Text="SBG"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblSBG" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label30" ForeColor="White" runat="server" Text="SBU"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblSBU" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label17" ForeColor="White" runat="server" Text="BU"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblBU" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label18" ForeColor="White" runat="server" Text="Project"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblProject" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label19" ForeColor="White" runat="server" Text="Location"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblLocation" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label20" ForeColor="White" runat="server" Text="SubLocation"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblSubLocation" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label21" ForeColor="White" runat="server" Text="Project Type"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblProjectType" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label22" ForeColor="White" runat="server" Text="Discipline"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblDiscipline" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label29" ForeColor="White" runat="server" Text="Main Activity"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblMainactivity" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label6" ForeColor="White" runat="server" Text="Activity"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblActivity" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label11" ForeColor="White" runat="server" Text="Sub Activity"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblSubActivity" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label34" ForeColor="White" runat="server" Text="Identification"></asp:Label></td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblidentify" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label25" ForeColor="White" runat="server" Text="FQE Evidence"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:LinkButton ID="fqeEvidence" runat="server" OnClick="fqeEvidence_Click" OnClientClick="ReloadFun()"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label27" ForeColor="White" runat="server" Text="SWI Issued By"></asp:Label>

            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblissuedby" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label14" ForeColor="White" runat="server" Text="Compliance"></asp:Label>
            </td>

        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="grdCompliances" runat="server" AutoGenerateColumns="false" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="Both" OnSelectedIndexChanged="grdCompliances_SelectedIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="SNo" HeaderText="S No" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Compliance_Text" HeaderText="Prerequisite" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="isCompliant" HeaderText="Complied" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="SWIRemarks" HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </td>
        </tr>
        <tr>

            <td colspan="2" style="text-align: center;">
                <%-- <asp:LinkButton ID="lnkSWI" runat="server" Font-Bold="true" onclick="lnkSWI_Click" >View SWI Form</asp:LinkButton>--%>
                <asp:HyperLink ID="hplSWI" runat="server" Visible="false" Target="_blank">View SWI Form</asp:HyperLink>
            </td>
        </tr>
    </table>
</div>
<br />
<div id="dvRCM" runat="server" visible="true">
    <table style="width: 80%" border="1">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label5" runat="server" ForeColor="White" Text="RCM Actions" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label2" ForeColor="White" runat="server" ToolTip="Enter TPLID" Text="Select Assignee"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                <SharePoint:PeopleEditor ID="ppAssigneeName" runat="server" AllowEmpty="true" MultiSelect="false" SelectionSet="User" ValidatorEnabled="true" MaximumEntities="1" />
                <asp:Label ID="lblselectAssignee" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label1" ForeColor="White" runat="server" Text="Target Closure Date"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                <SharePoint:DateTimeControl ID="dtTargetClosureDate" DateOnly="true" OnDateChanged="dateDateBorrowed_OnDateChanged" runat="server" />

                <asp:Label ID="lblTargetClosureDate" Text="Please select a date greater than or equal to today." runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Remarks"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                <asp:TextBox ID="txtRCMRemarks" runat="server" TextMode="MultiLine" Width="90%"></asp:TextBox>
            </td>
        </tr>
    </table>

</div>
<div id="RCMActions" runat="server" visible="false">
    <table style="width: 80%">
        <tr>
            <td style="text-align: right">
                <asp:Button ID="btnRCMActions" Font-Bold="true" runat="server" Text="Submit" OnClick="btnRCMActions_Click" />
            </td>
        </tr>
    </table>

</div>
<br />
<div id="dvAssignee" runat="server" visible="false">
    <table style="width: 80%" border="1">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label4" runat="server" ForeColor="White" Text="Assignee Actions" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label15" ForeColor="White" runat="server" Text="Target Closure Date"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:Label ID="lblTargetClosureDateAssRCM" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label16" ForeColor="White" runat="server" Text="RCM Remarks"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:Label ID="lblAssigneeRCMRemarks" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label9" ForeColor="White" runat="server" Text="Upload Evidence"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:FileUpload ID="assigneeEvidenceFUP" runat="server" Width="40%" />
                <asp:LinkButton ID="upLoadEvidenceDoc" OnClick="upLoadEvidenceDoc_Click" OnClientClick="ReloadFun()" runat="server" Visible="true"></asp:LinkButton>

            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label41" ForeColor="White" runat="server" Text="Actual cost of Rework in INR :"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:TextBox ID="txtRWCRCMCost" runat="server" Width="30%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFtxtRWCRCMCost" runat="server" ErrorMessage="Please Enter Actual Cost" ControlToValidate="txtRWCRCMCost" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegtxtRWCRCMCost" runat="server" ControlToValidate="txtRWCRCMCost" ErrorMessage="Please Enter Only Numbers" CssClass="required" ValidationExpression="^\d+$" Display="Dynamic"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
               <asp:Label ID="Label36" ForeColor="White" runat="server" Text="Cost Bourne By"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlborne" runat="server" Width="152px">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>Sub-Contractor</asp:ListItem>
                    <asp:ListItem>TPL</asp:ListItem>
                </asp:DropDownList>

            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label7" ForeColor="White" runat="server" Text="Action Taken Date"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 90%; text-align: left; height: 30px;">
                <SharePoint:DateTimeControl ID="assigneeClosureDateDATE" DateOnly="true" runat="server" />
                <asp:Label ID="lblassclosdate" runat="server" Text="Please select an Action Taken Date between the FQE Submission Date and Today."></asp:Label>
            </td>

        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label8" ForeColor="White" runat="server" Text="Remarks"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                <asp:TextBox ID="assigneeRemarksTXT" runat="server" TextMode="MultiLine" Width="97%"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td colspan="2" style="text-align: right">
                <asp:Button ID="btnAssigneeSubmit" OnClick="btnAssigneeSubmit_Click" Font-Bold="true" runat="server" Text="Submit" />
            </td>
        </tr>
    </table>
</div>
<br />
<div id="dvRCMClose" runat="server" visible="false">
    <table style="width: 80%" border="1">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label10" runat="server" ForeColor="White" Text="RCM Closure Actions" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label26" ForeColor="White" runat="server" Text="Assignee Action Taken Date"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:Label ID="lblAssigneeActiondate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label23" ForeColor="White" runat="server" Text="Assignee Action Taken Remarks"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:Label ID="lblRCMAssigneeRemarks" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label24" ForeColor="White" runat="server" Text="Uploaded Evidence"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:LinkButton ID="LinkButton1" OnClick="LinkButton1_Click" OnClientClick="ReloadFun()" runat="server"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label111" ForeColor="White" runat="server" Text="Verification remarks"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:TextBox ID="txtverifi_Remarks" runat="server" TextMode="MultiLine" Width="90%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="txtverifi_Remarks" ErrorMessage="Enter Remarks." CssClass="required" Display="Dynamic" />
            </td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label13" ForeColor="White" runat="server" Text="Closure Date"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 90%; text-align: left; height: 30px;">
                <SharePoint:DateTimeControl ID="dtClosuredate" DateOnly="true" runat="server" />
                <%--<asp:CompareValidator ID="ValidateEventDate" ControlToValidate="dtClosuredate" Operator="GreaterThanEqual" Type="Date" runat="server" ErrorMessage="Can't pick a date in the past"></asp:CompareValidator>--%>
            </td>

        </tr>
        <tr>
            <td style="text-align: center" colspan="2">
                <asp:Label ID="Label141" Font-Bold="true" runat="server" Text="I here by self certify above furnished information is accurate"></asp:Label>
                <asp:CheckBox ID="chkClosureYes" Font-Bold="true" Text="Yes" AutoPostBack="true" OnCheckedChanged="chkClosureYes_CheckedChanged" runat="server" />
            </td>


        </tr>
        <tr>
            <td colspan="2" style="text-align: right">
                <asp:Button ID="btnClosure" Font-Bold="true" runat="server" Enabled="false" Text="Submit" OnClick="btnClosure_Click" />
            </td>
        </tr>
    </table>
</div>

<div id="dvfqeclose" runat="server" visible="false">
    <table style="width: 80%" border="1">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label31" runat="server" ForeColor="White" Text="FQE Closure Actions" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label32" ForeColor="White" runat="server" Text="Assignee Action Taken Date"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:Label ID="lblAssigneeActiondatefqe" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label35" ForeColor="White" runat="server" Text="Assignee Action Taken Remarks"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:Label ID="lblRCMAssigneeRemarksfqe" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label37" ForeColor="White" runat="server" Text="Uploaded Evidence"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:LinkButton ID="LinkButton2" OnClick="LinkButton2_Click" OnClientClick="ReloadFun()" runat="server"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label38" ForeColor="White" runat="server" Text="Verification remarks"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:TextBox ID="txtverifi_Remarksfqe" runat="server" TextMode="MultiLine" Width="90%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtverifi_Remarksfqe" ErrorMessage="Enter Remarks." CssClass="required" Display="Dynamic" />
            </td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label39" ForeColor="White" runat="server" Text="Closure Date"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 90%; text-align: left; height: 30px;">
                <SharePoint:DateTimeControl ID="dtClosuredatefqe" DateOnly="true" runat="server" />
                <%--<asp:CompareValidator ID="ValidateEventDate" ControlToValidate="dtClosuredate" Operator="GreaterThanEqual" Type="Date" runat="server" ErrorMessage="Can't pick a date in the past"></asp:CompareValidator>--%>
            </td>

        </tr>

        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label33" ForeColor="White" runat="server" Text="Closure Verification"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 90%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlclosureverify" runat="server">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>Accept</asp:ListItem>
                    <asp:ListItem>Send for Re-Verificartion</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvclosureverify" runat="server" ErrorMessage="Please Select Closure Verification" InitialValue="--Select--" ControlToValidate="ddlclosureverify" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>

        </tr>

        <tr>
            <td colspan="2" style="text-align: right">
                <asp:Button ID="btnclosurefqe" runat="server" Text="Submit" OnClick="btnclosurefqe_Click" />
            </td>
        </tr>
    </table>
</div>





<asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
<asp:Label ID="lblTransactionID" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblRCMID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblRCMName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblRCMEmail" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblPMID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPMName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPMEmail" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblFQEID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblFQEName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblFQEEmail" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblUrbalCoE" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblInduCoE" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblAssigneID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="FQESubmittedDate" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblAssigneName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblAssigneEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblNewSBU" runat="server" Visible="false"></asp:Label>

<asp:Label ID="ltScriptLoader" runat="server"></asp:Label>
<asp:Label ID="lblAssigneeClosure" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" runat="server"></asp:Label>
<table>
    <tr>
        <td>
            <asp:Label ID="lblRCMSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblRCMClosureSubmitNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblAsigneeSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblLinkClosureNotification" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<div id="dvUnAuth" runat="server" visible="false">
    <asp:Label ID="lblUnAuth" runat="server" ForeColor="Red" Font-Size="Large" Text="You are not authorized to access"></asp:Label>
</div>

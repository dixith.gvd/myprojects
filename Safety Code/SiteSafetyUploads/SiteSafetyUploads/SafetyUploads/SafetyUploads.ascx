﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SafetyUploads.ascx.cs" Inherits="SiteSafetyUploads.SafetyUploads.SafetyUploads" %>

<style type="text/css">
    .table {
        padding: 0;
        border-spacing: 0;
        text-align: center;
        font-family: 'Trebuchet MS';
        font-size: 10pt;
        vertical-align: middle;
        border-collapse: collapse;
    }

    .auto-style1 {
        height: 30px;
        font-family: 'Trebuchet MS';
        font-size: 10pt;
    }

    .required {
        color: Red;
    }

    .auto-style2 {
        width: 60%;
        height: 30px;
        font-family: 'Trebuchet MS';
        font-size: 10pt;
    }
.ms-metadata,
.ms-descriptiontext
{
/* [ReplaceColor(themeColor:"SubtleBodyText")] */ color:#777;
}
.ms-textSmall,
.ms-textXSmall,
.ms-metadata,
.ms-descriptiontext,
.ms-secondaryCommandLink
{
font-size:.9em;
}
.ms-metadata,
.ms-descriptiontext,
.ms-secondaryCommandLink
{
/* [ReplaceFont(themeFont:"body")] */ font-family:"Segoe UI","Segoe",Tahoma,Helvetica,Arial,sans-serif;
}
</style>

<div id="dvMainForm" runat="server" visible="false">

    <table border="1" cellpadding="1" cellspacing="1" width="100%">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label5" runat="server" ForeColor="White" Text="Site Safety Uploads" CssClass="auto-style2" Font-Size="Large"></asp:Label>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align="right">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label21" ForeColor="White" runat="server" Text="BU Name"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:Label ID="lblBU" runat="server"></asp:Label>&nbsp;</td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label23" ForeColor="White" runat="server" Text="Project Name"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlplist" runat="server" Width="634px" AutoPostBack="True" >
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select TPL Projects List" InitialValue="--Select--" ControlToValidate="ddlplist" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator></td>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label1" ForeColor="White" runat="server" Text="Reporting On"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:RadioButton ID="rbgoodpractice" Text="Good Practice" runat="server" GroupName="Reporting" ValidationGroup="Reporting" Checked="True" />
                <br />
                <asp:RadioButton ID="rbareas" Text="Areas for Improvement" runat="server" GroupName="Reporting" ValidationGroup="Reporting" />
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Evidence For"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <%--<asp:RequiredFieldValidator ID="RFProject" runat="server" ErrorMessage="Please Select Project" InitialValue="--Select--" ControlToValidate="ddlProjectCode" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                <%-- <asp:Label ID="lblProject" runat="server"></asp:Label>--%>
                <asp:DropDownList ID="ddlevidence" runat="server" Width="634px">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>Fall Protection</asp:ListItem>
                    <asp:ListItem>Electrical Safety</asp:ListItem>
                    <asp:ListItem>Erection</asp:ListItem>
                    <asp:ListItem>Housekeeping</asp:ListItem>
                    <asp:ListItem>Scaffolding</asp:ListItem>
                    <asp:ListItem>Excavation</asp:ListItem>
                    <asp:ListItem>Permit To Work</asp:ListItem>
                    <asp:ListItem>Tools &amp; Plants \ Vehicle</asp:ListItem>
                    <asp:ListItem>Emergency Resource Planning</asp:ListItem>
                    <asp:ListItem>PPE</asp:ListItem>
                    <asp:ListItem>Others</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Evidence For" InitialValue="--Select--" ControlToValidate="ddlevidence" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator></td>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label22" ForeColor="White" runat="server" Text="Comments"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:TextBox ID="txtcomments" runat="server" TextMode="MultiLine" Height="80px" Width="512px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="txtcomments" ErrorMessage="Enter Comments." CssClass="required" Display="Dynamic" />
                <br />
                <span id="part1"><span class="ms-metadata">This is Mandatory. Include evidence description, location information, mitigation steps implemented</span> </span></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label6" ForeColor="White" runat="server" Text="Reporting Date"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <SharePoint:DateTimeControl ID="dtreporting" DateOnly="true" runat="server" SelectedDate="" />
                <span id="Span1" class="auto-style2"><span class="ms-metadata">Date for which this practice is being uploaded </span> </span>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label25" ForeColor="White" runat="server" Text="Target Date"></asp:Label>
                &nbsp;</td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <SharePoint:DateTimeControl ID="dttarget" DateOnly="true" Visible="true" runat="server" />
                <span id="Span2" class="auto-style2"><span class="ms-metadata">Date by which the current issue will be rectified if it is an "Area Of Improvement"  </span> </span>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label32" ForeColor="White" runat="server" Text="Upload Supporting Document"></asp:Label>&nbsp;</td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:FileUpload ID="FileUpload1" AllowMultiple="true" runat="server" />
                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.png|.jpg|.gif|.xls|.xlsx|.DOC|.DOCX|.PDF|.PNG|.JPG|.GIF|.XLS|.XLSX)$"
                    ControlToValidate="FileUpload1" runat="server" ForeColor="Red" ErrorMessage="Please select a valid word,excel,jpg,png,gif or pdf file format."
                    Display="Dynamic" />--%>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Button Font-Size="Medium" ID="btnsubmit" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
            </td>
        </tr>
    </table>

</div>

<div id="dvUnAuth" runat="server" visible="false">
    <asp:Label ID="Label13" runat="server" ForeColor="Red" Font-Size="Large" CssClass="auto-style2" Text="You are not authorized to access"></asp:Label>
    <br />
    <br />
    <br />
    <asp:Label ID="Label114" runat="server" ForeColor="Red" Font-Size="Large" CssClass="auto-style2" Text="[Can be filled only by safety officer]"></asp:Label>
</div>


<asp:Label ID="lblError" runat="server" CssClass="auto-style2" ForeColor="Red"></asp:Label>
<asp:Label ID="lblNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" runat="server"></asp:Label>
<asp:Label ID="lblProjectCode" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblTransactionID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblRCMID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblRCMName" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblRCMEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblPMID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblPMName" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblPMEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblHead" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblHeadID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblHeadEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblbuheadid" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblbuheadName" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblbuheadEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblSOName" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblSOID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblSOEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
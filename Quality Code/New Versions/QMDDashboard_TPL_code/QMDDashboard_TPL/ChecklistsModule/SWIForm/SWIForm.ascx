﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SWIForm.ascx.cs" Inherits="QMDDashboard_TPL.ChecklistsModule.SWIForm.SWIForm" %>
<style type="text/css">
    .table {
        padding: 0;
        border-spacing: 0;
        text-align: center;
        font-family: 'Trebuchet MS';
        vertical-align: middle;
        border-collapse: collapse;
    }
    .auto-style2 {
        width: 450px;       
    }
</style>
<div id="dvSWIForm" runat="server">
<table border="1" style="width: 95%; font-family: 'Trebuchet MS'; font-size: 14px">
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td>
                        <asp:Image ID="imgTataLogo" ImageUrl="../../_layouts/15/images/TataLogo.jpg" runat="server" />
                    </td>
                    <td style="width: 100%;">
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: center; height: 40px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label5" runat="server" Text="TATA PROJECTS LTD" Font-Size="22px" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr style="text-align: center">
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label1" runat="server" Text="STOP WORK INSTRUCTION(SWI)" Font-Size="16px"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">
            <asp:Label ID="Label13" runat="server" Text="SWI No:" Style="font-weight: 600"></asp:Label>
            <asp:Label ID="lblSWINO" runat="server"></asp:Label>
        </td>
        <td>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblDateTime" runat="server" Text="Date & Time:" Style="font-weight: 600"></asp:Label></td>
                    <td>
                        <SharePoint:DateTimeControl ID="dtTargetClosureDate" runat="server" Enabled="false" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="height: 28px">
        <td colspan="2">
            <asp:Label ID="Label14" runat="server" Style="font-weight: 600" Text="Project/Location :"></asp:Label>
            <asp:Label ID="lblProject" runat="server" Text="Auto populated"></asp:Label>
        </td>
    </tr>
    <tr style="height: 28px">
        <td class="auto-style2">
            <asp:Label ID="Label9" Style="font-weight: 600" runat="server" Text="Package / Equipment :"></asp:Label>
            <asp:Label ID="txtPackage" Width="50%" runat="server"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label12" Style="font-weight: 600" runat="server" Text="Subcontractor:"></asp:Label>
            <asp:Label ID="txtSubcontractors" Width="70%" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 28px">
          <td class="auto-style2">
            <asp:Label ID="Label27" Font-Bold="true"  runat="server" Text="Project type :"></asp:Label><asp:Label ID="lblProjectType" runat="server" ></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label24"  Font-Bold="true" runat="server" Width="30%"  Text="Discipline :"></asp:Label><asp:Label ID="lblDiscipline" runat="server" ></asp:Label>
        </td>
    </tr>
    <tr style="height: 28px">
        <td class="auto-style2">
            <asp:Label ID="Label15"   Font-Bold="true" runat="server" Width="20%" Text="Activity: "></asp:Label><asp:Label ID="lblActivity" runat="server" ></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label26" Font-Bold="true" runat="server" Width="30%" Text="Sub-Activity: "></asp:Label><asp:Label ID="lblSubActivity" Width="65%" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 28px">
        <td class="auto-style2">
            <asp:Label ID="Label4" Style="font-weight: 600" runat="server" Width="20%" Text="Issued by:"></asp:Label><asp:Label ID="lblIssuedby" runat="server"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label3" Style="font-weight: 600" runat="server" Width="30%" Text="Email ID:"></asp:Label><asp:Label ID="lblIssueEmail" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 28px">
        <td class="auto-style2">
            <asp:Label ID="Label6" Style="font-weight: 600" runat="server" Text="Issued to :" Width="20%"></asp:Label><asp:Label ID="lblIssuedTo" runat="server"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label10" runat="server" Style="font-weight: 600" Width="30%" Text="Email ID:"></asp:Label><asp:Label ID="lblIssuetoEmail" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="height: 20px">You are requested to stop the work immediately due to the following deficiencies observed in QPR compliance.
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:GridView ID="grdSWIComplainces" AutoGenerateColumns="false" Width="100%" runat="server">
                <Columns>
                    <asp:BoundField DataField="SNo" HeaderText="S.No"  ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="Compliance_Text" HeaderText="Compliance" />
                    <asp:BoundField DataField="isCompliant" HeaderText="Complied" ItemStyle-HorizontalAlign="Center" />
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr style="display:none">
        <td colspan="2" style="height: 30px">&nbsp;<asp:Label ID="Label2" runat="server" Text="Re-inspection details after correction"></asp:Label>
        </td>
    </tr>
    <tr style="display:none">
        <td colspan="2">
            <asp:TextBox ID="txtReInspection" TextMode="MultiLine" Rows="7" Width="98%" runat="server"></asp:TextBox>
        </td>
    </tr>
</table>
    </div>
<%--<div id="dvAssignSubmi" runat="server" visible="false">
    <table style="width:75%"  border="1"><tr><td style="text-align:right" class="auto-style1"><asp:Button ID="btnassignsub" runat="server" Font-Bold="true" Text="SUBMIT" OnClick="btnassignsub_Click" /></td></tr> </table>
</div>
<div id="dvSWIRCMForm" runat="server" visible="false">
    <table  border="1" style="width: 75%; font-family: 'Trebuchet MS'; font-size: 14px">
        <tr>
            <td>&nbsp;<asp:Label ID="Date_Time" Style="font-weight: 600" runat="server" Text="Date & Time"></asp:Label>
            </td>
            <td>
                <SharePoint:DateTimeControl ID="dtRCM" DateOnly="true" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="rdAccepted" ValidationGroup="ac" Style="font-weight: 600" Text="Accepted" runat="server" /><asp:RadioButton ID="rdNotAccepted" Text="Not Accepted" Style="font-weight: 600" ValidationGroup="ac" runat="server" />
            </td>
            <td style="height: 30px">The SWI is closed & released for further work
            </td>
        </tr>
        <tr style="height: 67px">
            <td colspan="2"></td>
        </tr>
        <tr style="height: 28px">
            <td style="text-align: right" colspan="2">QA / QC incharge</td>
        </tr>
        <tr style="height: 28px">
            <td colspan="2">
                Distribution to : RCM, PM, Concerned Execution Engineer, Subcontractor, Inspection Coordinator 
            </td>
        </tr>
    </table>
</div>
<div id="dvMain" runat="server" visible="false">
    <table style="width:75%"  border="1"><tr><td style="text-align:right"><asp:Button ID="btnRCMSWI" runat="server" Font-Bold="true" Text="SUBMIT" OnClick="btnRCMSWI_Click" /></td></tr> </table>
</div>--%>
<asp:Label ID="lblTransactionID" runat="server" Visible="false"></asp:Label>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPLITAssetInfoCapture
{
    class SoftwareInfo
    {
        public class AssetDataSoftware
        {

            public string HostName { get; set; }
            public string SerialNumber { get; set; }
            public string Name { get; set; }
            public string SoftwareVersion { get; set; }
            public string Vendor { get; set; }
            public string InstallDate { get; set; }
            public string EmailID { get; set; }
            public string CreatedDate { get; set; }
            public string CodeVersion { get; set; }
            //public string LoggedDate2 { get; set; }

            //public string[] AssetData { get; set; }
        }
        public class SoftwareDetails
        {
            public List<AssetDataSoftware> softwaredt { get; set; }
        }
    }
}

﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DowloadTemplate.ascx.cs" Inherits="QMDDashboard_TPL.DowloadTemplate.DowloadTemplate" %>
<style type="text/css">
    #Download_page {
        /*width: 60%;*/
         width: 80%;
        margin: 0px auto;
    }
        #Download_page tbody tr td select {
            width: 300px;
            margin-left: 10px;
            margin-bottom: 5px;
            margin-top: 5px;
        }
        #Download_page tbody tr td:first-child {
            text-align: right;
        }
        #Download_page tbody tr td:last-child {
            text-align: left;
        }
        #Download_page tbody tr td input[id$="_btnDownload"] {
            background: #4778d5;
            color: #fff;
            border-color: #4778d5;
            font-weight: bold;
        }
</style>
<asp:UpdatePanel ID="updatePanelDownload" runat="server" EnableViewState="true">
    <ContentTemplate>
        <table class="style1" id="Download_page" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="lblTemplate" runat="server" Text="Template:"></asp:Label>
                </td>
                <td>
                    <%-- <asp:DropDownList ID="ddlTemplate" runat="server" AutoPostBack="True" EnableViewState='true' AppendDataBoundItems='true' OnSelectedIndexChanged="ddlTemplate_SelectedIndexChanged">
            </asp:DropDownList>--%>
                    <asp:DropDownList ID="ddlTemplate" runat="server" AutoPostBack="True" AppendDataBoundItems='true' OnSelectedIndexChanged="ddlTemplate_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <%--<td>
                    <asp:RequiredFieldValidator ID="rvTemplate" runat="server" ControlToValidate="ddlTemplate" ErrorMessage="Please select a Template!" ForeColor="#CC0000" InitialValue="0"></asp:RequiredFieldValidator>
                </td>--%>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbNewExsiting" runat="server" Text="New/Existing:" Style="display: none;"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlNewExsiting" runat="server" Style="display: none;" AutoPostBack="True">
                        <asp:ListItem Value="0">--Select-- </asp:ListItem>
                        <asp:ListItem Value="1">New</asp:ListItem>
                        <asp:ListItem Value="2">Existing</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <%--<td>
                    <asp:RequiredFieldValidator ID="rvNewExisting" runat="server" Style="display: none;" Display="Dynamic" ControlToValidate="ddlNewExsiting" ErrorMessage="Please select either New or Existing!" ForeColor="#CC0000" InitialValue="0"></asp:RequiredFieldValidator>
                </td>--%>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblProjectName" runat="server" Text="Project Name:" Style="display: none;"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlProjectName" runat="server" Style="display: none;" AutoPostBack="True" OnSelectedIndexChanged="ddlProjectName_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <%--<td>
                    <asp:RequiredFieldValidator ID="rvProjectName" runat="server" Style="display: none;" Display="Dynamic" ControlToValidate="ddlProjectName" ErrorMessage="Please select a Project!" ForeColor="#CC0000" InitialValue="0"></asp:RequiredFieldValidator>
                </td>--%>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblForeNight" runat="server" Text="Fore Night:" Style="display: none;"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlForeNight" runat="server" Style="display: none;">
                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPeriod" runat="server" Text="Period:" Style="display: none;"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlPeriod" runat="server" Style="display: none;" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
               <%-- <td>
                    <asp:RequiredFieldValidator ID="rvPeriod" runat="server" ControlToValidate="ddlPeriod" Display="Dynamic" ErrorMessage="Please select a Period!" ForeColor="#CC0000" InitialValue="0"></asp:RequiredFieldValidator>
                </td>--%>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPeriodofRating" runat="server" Text="Period of Rating:" Style="display: none;"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlPeriodofRating" runat="server" Style="display: none;">
                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDateofRating" runat="server" Text="Date of Rating:" Style="display: none;"></asp:Label>
                </td>
                <td>
                    <%--<asp:DropDownList ID="ddlDateofRating" runat="server" style="display:none;">
                <asp:ListItem Value="0">--Select--</asp:ListItem>
            </asp:DropDownList>--%>
                    <%-- <asp:Calendar ID="calDateofRating" runat="server" style="display:none;"></asp:Calendar>--%>
                    <%-- <input type="text" class="datepicker" runat="server" id="calDateofRating">--%>
                    <div class="calDateofRating" style="margin: 5px 0 5px 15px">
                        <%--  <SharePoint:DateTimeControl DateOnly="true"  runat="server" Visible="false" ID="calDateofRating"/>--%>
                    </div>
                </td>
                <td></td>
            </tr>
            <%--<tr>
        <td>
            <asp:Label ID="lblCustmer" runat="server" Text="Customer:" style="display:none;"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="ddlCustmer" runat="server" style="display:none;">
                <asp:ListItem Value="0">--Select--</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td></td>
    </tr>--%>
            <tr>
                <asp:UpdatePanel ID="udpbtnDownload" runat="server">
                    <ContentTemplate>
                        <td></td>
                        <td>
                            <asp:Button ID="btnDownload" runat="server" Text="Download" Style="display: none;" CausesValidation="false"
                                OnClick="btnDownload_Click"  />
                        </td>
                        <td></td>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnDownload" />
                    </Triggers>
                </asp:UpdatePanel>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="lblError" runat="server" Style="display: none;"></asp:Label></td>
                <td></td>
            </tr>
        </table>
<script type="text/javascript">
    function waitMessage() {
        try {
            if (Page_IsValid) {
                SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showWaitScreenWithNoClose', "Please wait .....");
            }
            else {
                return Page_IsValid;
            }
        }
        catch (err) {
            //alert(err);
            //SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog', options);
        }
        return true;
    }
    function closeWaitMessage() {
        try {
            SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose', 1);
        } catch (e) {
        }
    }
</script>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnDownload" />
    </Triggers>
</asp:UpdatePanel>
<asp:UpdateProgress AssociatedUpdatePanelID="udpbtnDownload" ID="upProgressBar" runat="server">
    <ProgressTemplate>Download is in Progress</ProgressTemplate>
</asp:UpdateProgress>
﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QualityUploadsPendingItems.ascx.cs" Inherits="SiteSafetyUploads.QualityUploadsPendingItems.QualityUploadsPendingItems" %>

<table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                        <tr>
                            <td>
                                <asp:GridView ID="grdPendingsQuality" runat="server" AllowPaging="True" AllowSorting="true"
                                     CellPadding="4" ForeColor="#333333" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" 
                                    GridLines="None" Width="100%" OnPageIndexChanging="grdPendingsQuality_PageIndexChanging">
                                    <Columns>
                                       <%--<asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" HeaderStyle-HorizontalAlign="Left" HeaderText="Sub Activity" DataTextField="Sub Activity" DataNavigateUrlFormatString="~/Pages/QMDChecklistActions.aspx?Tid={0}" Target="_blank" />--%>
                                        <%--<asp:BoundField DataField="Activity" HeaderStyle-HorizontalAlign="Left" HeaderText="Activity" />--%>
                                        <%--<asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" DataNavigateUrlFormatString="https://tplnet.tataprojects.com/Pages/TestingSafetyAction.aspx?Tid={0}" DataTextField="Checklist Name" HeaderText="Checklist Name" Target="_blank" />--%>
                                        <asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" DataNavigateUrlFormatString="http://tplhydhospuat/SitePages/Quality%20Uploads%20Edit%20Form.aspx?Tid={0}" DataTextField="Transaction_ID" HeaderText="Transaction_ID" Target="_blank" />
                                        <%--<asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" DataNavigateUrlFormatString="CCMails2/Pages/EHSChecklistDetails.aspx?Tid={0}" DataTextField="Transaction_ID" HeaderText="Transaction_ID" Target="_blank" />--%>
                                        <asp:BoundField DataField="FQE_Name" HeaderText="Created By" />
                                        <asp:BoundField DataField="BUName" HeaderText="TPL BU"/>
                                        <asp:BoundField DataField="ProjectName" HeaderText="TPL Projects List" />
                                        <asp:BoundField DataField="Reporting_On" HeaderText="Reporting On"  />
                                        <asp:BoundField DataField="Evidence_For" HeaderText="Evidence For"  />
                                        <asp:BoundField DataField="Comments" HeaderText="Comments"  />
                                        <asp:BoundField DataField="Reporting_Date" HeaderText="Reporting Date" dataformatstring="{0:dd-MM-yyyy}" />
                                        <asp:BoundField DataField="Target_Date" HeaderText="Target Date" dataformatstring="{0:dd-MM-yyyy}" />
                                        <asp:BoundField DataField="Corrective_Action" HeaderText="Corrective Action" />
                                        <asp:BoundField DataField="ItemStatus" HeaderText="Item Status" />
                                        <asp:BoundField DataField="FQE_Submission_Date" HeaderText="Created Date" dataformatstring="{0:dd-MM-yyyy}"  />
                                    </Columns>
                                     <EmptyDataTemplate>No Records Available</EmptyDataTemplate>
                                    <AlternatingRowStyle BackColor="White" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />

                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
<asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
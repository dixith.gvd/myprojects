﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
namespace SafetyDailyRCMMail
{
    class RCMMail
    {

        public DataTable getDistinctRCMs()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                string sqlQuery = "select distinct RCM from Monitored_Projects where [Safety Officer]!='' and [Safety Active]=1";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dt);
                con.Open();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            return dt;
        }

        public string GetProjectName(string projectCode)
        {
            DataTable dt = new DataTable();
            string pname = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                string sqlQuery = "select [Project Name] from Monitored_Projects where [Project Code]='" + projectCode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dt);
                con.Open();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            if (dt.Rows.Count > 0)
            {
                pname = dt.Rows[0]["Project Name"].ToString();
            }
            return pname;
        }

        public DataTable getProjectDetails(string rcmemail)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                string sqlQuery = "select * from Monitored_Projects where RCM='" + rcmemail + "' and [Safety Active]=1";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dt);
                con.Open();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            return dt;
        }

        string ChecklistUploaded(string projectCode)
        {
            string records = string.Empty;
            DataTable ckname = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "select distinct [Checklist Name] from EHSChecklist where Frequency='Daily' and ProjectCode='" + projectCode + "' and [SO Submission Date]>GETDATE()-1";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(ckname);
                con.Close();
                if (ckname.Rows.Count > 0)
                {
                    foreach (DataRow row in ckname.Rows)
                    {
                        records = records + "," + row["Checklist Name"].ToString();
                    }

                }
                else
                {
                    records = string.Empty;
                }
            }

            catch (Exception exp)
            {

            }
            return records.TrimStart(',');
        }

        public void ProcessLogic()
        {
            string[] MandatoryChecklists = { "Housekeeping", "Personal Protective Equipment (PPE)", "HIRA talk", "Permit to Work", "Daily site safety inspection" };
            List<string> chkupload = new List<string>();
            DataTable dtrcms = getDistinctRCMs();
            foreach (DataRow rcmrow in dtrcms.Rows)
            {
                string projectCode = string.Empty;
                string projectName = string.Empty;
                string checklist = string.Empty;
                string buname = string.Empty;
                DataTable dt = new DataTable();
                dt.Columns.Add("projectCode", typeof(string));
                dt.Columns.Add("projectName", typeof(string));
                dt.Columns.Add("Checklist Uploaded", typeof(string));
                dt.Columns.Add("Checklist Not Uploaded", typeof(string));
                string cknewline = string.Empty;
                string ckuploadnewline = string.Empty;
                DataTable dtproject = getProjectDetails(rcmrow["RCM"].ToString());
                foreach (DataRow dtrow in dtproject.Rows)
                {
                    if (dtrow["Project Code"] != null)
                    {
                        projectCode = dtrow["Project Code"].ToString();
                        buname = dtrow["BU Name"].ToString();
                        projectName = GetProjectName(projectCode);
                        checklist = ChecklistUploaded(projectCode);
                        foreach (string chk in MandatoryChecklists)
                        {
                            if (!checklist.Contains(chk))
                            {
                                chkupload.Add(chk);
                            }
                        }
                    }
                    if (chkupload.Count > 0)
                    {
                        string[] cklist = checklist.Split(',');
                        foreach (string ck in cklist)
                        {
                            cknewline += ck + System.Environment.NewLine;
                        }
                        string[] cklist2 = string.Join(",", chkupload).Split(',');
                        foreach (string ck in cklist2)
                        {
                            ckuploadnewline += ck + System.Environment.NewLine;
                        }
                        dt.Rows.Add(projectCode, projectName, cknewline, ckuploadnewline);
                        chkupload.Clear();
                        cknewline = string.Empty;
                        ckuploadnewline = string.Empty;
                    }
                }
                if (dt.Rows.Count == 0)
                {
                    continue;
                }
                DateTime todaysdate = DateTime.Today;
                string todaydate = String.Format("{0:dd MMMM yyyy}", todaysdate);
                string rcmname = GetRCMName(rcmrow["RCM"].ToString());
                string mailBody = FormMailBody(rcmname, dt, todaydate);
                MailAddress SendFrom = new MailAddress("coesheii@tataprojects.com");

                MailMessage MyMessage = new MailMessage();
                MyMessage.From = SendFrom;
                //MyMessage.To.Add("deekshithgvd-t@tataprojects.com");
                MyMessage.To.Add(rcmrow["RCM"].ToString());
                if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
                {
                    MyMessage.CC.Add(CCMailSafetyUI());
                }
                else
                {
                    MyMessage.CC.Add(CCMailSafetyII());
                }
                MyMessage.Subject = "Safety Escalation";
                MyMessage.Body = mailBody;
                MyMessage.IsBodyHtml = true;
                if (MyMessage != null)
                {
                    SmtpClient emailClient = new SmtpClient();
                    emailClient.UseDefaultCredentials = false;
                    emailClient.Credentials = new System.Net.NetworkCredential("coesheii@tataprojects.com", "tata@123");
                    emailClient.Host = "smtp.office365.com";
                    emailClient.Port = 25;
                    emailClient.EnableSsl = true;
                    emailClient.TargetName = "STARTTLS/smtp.office365.com";
                    emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    emailClient.Send(MyMessage);
                }
                MyMessage.To.Clear();
                MyMessage.CC.Clear();
            }
        }

        private string GetRCMName(string rcmemail)
        {
            DataTable dt = new DataTable();
            string pmname = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = ConfigurationManager.AppSettings["ConnStringSrc"];
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                string sqlQuery = "select * from TPL_EMP_MASTER where EmailID='" + rcmemail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dt);
                con.Open();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            if (dt.Rows.Count > 0)
            {
                pmname = dt.Rows[0]["Name"].ToString();
            }
            return pmname;
        }

        string CCMailSafetyII()
        {
            string CCMailSafety = ConfigurationManager.AppSettings["CCSafetyEmailListII"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(CCMailSafety))
            {
                string strTo = CCMailSafety.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(ccEmail));
                }
            }
            return MyMessage.CC.ToString();
        }
        string CCMailSafetyUI()
        {
            string CCMailSafety = ConfigurationManager.AppSettings["CCSafetyEmailListUI"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(CCMailSafety))
            {
                string strTo = CCMailSafety.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(ccEmail));
                }
            }
            return MyMessage.CC.ToString();
        }
        private string FormMailBody(string rcmname, DataTable ds, string dateString)
        {
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmname + ",");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The following is the list of Checklists uploaded and Mandatory checklists Not uploaded for today - ");
            sbBody.Append(dateString);
            //sbBody.Append(" - for the site(s) mentioned below.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("<table width='100%' border='1' cellpadding='1' cellspacing='1';'");
            sbBody.Append("<table><tr><td width='10%'>Project Code</td><td width='20%'>Project Name</td><td width='35%'>Checklist Uploaded</td><td width='35%'>Checklist Not Uploaded</td></tr>");
            for (int i = 0; i < ds.Rows.Count; i++)
            {
                sbBody.Append("<tr>");
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["projectCode"] + "</td>"));
                sbBody.Append("<td width='20%'>" + Convert.ToString(ds.Rows[i]["projectName"] + "</td>"));
                sbBody.Append("<td width='35%'>" + Convert.ToString(ds.Rows[i]["Checklist Uploaded"] + "</td>").Replace(System.Environment.NewLine, "<br />"));
                sbBody.Append("<td width='35%'>" + Convert.ToString(ds.Rows[i]["Checklist Not Uploaded"] + "</td>").Replace(System.Environment.NewLine, "<br />"));
                sbBody.Append("</tr>");
            }
            sbBody.Append("</table>");
            sbBody.Append("<br>");

            sbBody.Append("<br>");
            sbBody.Append("<br>");

            sbBody.Append("Thanks & Regards");
            sbBody.Append("<br>");
            sbBody.Append("Centre of Excellence Team - SHE");
            return sbBody.ToString();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
namespace QMDDashboard_TPL
{
    public class SPOperations
    {
        public Hashtable GetSBUCodes()
        {
            Hashtable hTable = new Hashtable();
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                //ReminderBackupLog("Run With");
                string rootSiteUrl = SPContext.Current.Site.WebApplication.Sites[0].Url;
                using (SPSite site = new SPSite(rootSiteUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        try
                        {
                            web.AllowUnsafeUpdates = true;
                            if (web.Lists["TPL Projects List"] != null)
                            {
                                SPList list = web.Lists["TPL Projects List"];
                                DataTable dt; //= list.Items.GetDataTable();
                                SPQuery query = new SPQuery();
                                query.Query = "<Where><IsNotNull><FieldRef Name='SBUCode' /></IsNotNull></Where><OrderBy><FieldRef Name='SBUName' /></OrderBy>";
                                query.ViewFields = "<FieldRef Name='SBUCode' /> <FieldRef Name='SBUName' />";
                                dt = list.GetItems(query).GetDataTable();
                                if (dt != null)
                                {
                                    foreach (DataRow drow in dt.Rows)
                                    {
                                        if (drow["SBUCode"] != null && drow["SBUName"] != null)
                                        {
                                            if (!hTable.Contains(drow["SBUCode"]))
                                                hTable.Add(drow["SBUCode"], drow["SBUName"]);
                                        }
                                    }
                                }
                            }
                            web.AllowUnsafeUpdates = false;
                        }
                        catch (Exception exp)
                        {
                            throw;
                        }
                    }
                }
            });
            return hTable;
        }
        public DataTable GetProjectCode(string SBUCode)
        {
            string Code = SBUCode;
            DataTable dt = new DataTable();
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                string rootSiteUrl = SPContext.Current.Site.WebApplication.Sites[0].Url;
                using (SPSite site = new SPSite(rootSiteUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        web.AllowUnsafeUpdates = true;
                        SPList list = web.Lists["TPL Projects List"];
                        SPQuery query = new SPQuery();
                        query.Query = "<Where><Eq><FieldRef Name='SBUCode' /><Value Type='Text'>" + Code + "</Value></Eq></Where><OrderBy><FieldRef Name='Title' /></OrderBy>";
                        query.ViewFields = "<FieldRef Name='Title' /> <FieldRef Name='ProjectName' />";
                        dt = list.GetItems(query).GetDataTable();
                        web.AllowUnsafeUpdates = false;
                    }
                }
            });
            return dt;
        }
        public void AddProjectInfoInQMD(string SBU, string projectCode, string projectName, SPUser fqeUser, string Template, int TemplateNo, string Frequency)
        {
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    if (web.Lists["ProjectCodeProjectName"] != null)
                    {
                        SPList list = web.Lists["ProjectCodeProjectName"];
                        SPListItem newItem = list.Items.Add();
                        newItem["SBU"] = SBU;
                        newItem["ProjectCode"] = projectCode;
                        newItem["ProjectName"] = projectName;
                        newItem["EmployeeName"] = fqeUser;
                        newItem["Template"] = list.Fields["Template"].GetFieldValue(Template);
                        newItem["TemplateNo"] = TemplateNo;
                        newItem["Frequency"] = Frequency;
                        newItem.Update();
                    }
                }
            }
        }
        public void UpdateFQEInQMD(string ProjectName, SPUser fqeUser, string Template, string SBUName)
        {
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    if (web.Lists["ProjectCodeProjectName"] != null)
                    {
                        SPList list = web.Lists["ProjectCodeProjectName"];
                        SPQuery query = new SPQuery();
                        query.Query = "<Where><And><And><Eq><FieldRef Name='ProjectName' /><Value Type='Text'>" + ProjectName + "</Value></Eq><Eq><FieldRef Name='SBU' /><Value Type='Text'>" + SBUName + "</Value></Eq></And><Eq><FieldRef Name='Template' /><Value Type='Choice'>" + Template + "</Value></Eq></And></Where>";
                        SPListItemCollection queryItems = list.GetItems(query);
                        foreach (SPListItem item in queryItems)
                        {
                            item["EmployeeName"] = fqeUser;
                            item.Update();
                            break;
                        }
                    }
                }
            }
        }
        public Hashtable GetSBUCodesFromQMD()
        {
            Hashtable hTable = new Hashtable();
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                //ReminderBackupLog("Run With");
                string siteUrl = SPContext.Current.Web.Url;
                using (SPSite site = new SPSite(siteUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        try
                        {
                            if (web.Lists["ProjectCodeProjectName"] != null)
                            {
                                SPList list = web.Lists["ProjectCodeProjectName"];
                                DataTable dt; //= list.Items.GetDataTable();
                                SPQuery query = new SPQuery();
                                //query.Query = "<Where><IsNotNull><FieldRef Name='SBU' /></IsNotNull></Where><OrderBy><FieldRef Name='SBU' /></OrderBy>";
                                query.Query = "<Where><And><IsNotNull><FieldRef Name='SBU' /></IsNotNull><Eq><FieldRef Name='Active' /><Value Type='Boolean'>1</Value></Eq></And></Where><OrderBy><FieldRef Name='SBU' /></OrderBy>";
                                query.ViewFields = "<FieldRef Name='SBU' />";
                                dt = list.GetItems(query).GetDataTable();
                                if (dt != null)
                                {
                                    foreach (DataRow drow in dt.Rows)
                                    {
                                        if (drow["SBU"] != null)
                                        {
                                            if (!hTable.Contains(drow["SBU"]))
                                                hTable.Add(drow["SBU"], drow["SBU"]);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception exp)
                        {
                            throw;
                        }
                    }
                }
            });
            return hTable;
        }
        public DataTable GetProjectsFromQMD(string sbuCode)
        {
            string Code = sbuCode;
            DataTable dt = new DataTable();
            DataTable dtdistinct = new DataTable();
            if (SPContext.Current.Web.Lists["ProjectCodeProjectName"] != null)
            {
                SPList list = SPContext.Current.Web.Lists["ProjectCodeProjectName"];
                SPQuery query = new SPQuery();
                // query.Query = "<Where><Eq><FieldRef Name='SBU' /><Value Type='Text'>" + Code + "</Value></Eq></Where><OrderBy><FieldRef Name='ProjectName' /></OrderBy>";
                query.Query = "<Where><And><Eq><FieldRef Name='SBU' /><Value Type='Text'>" + Code + "</Value></Eq><Eq><FieldRef Name='Active' /><Value Type='Boolean'>1</Value></Eq></And></Where><OrderBy><FieldRef Name='ProjectName' /></OrderBy>";
                query.ViewFields = "<FieldRef Name='ProjectCode' /> <FieldRef Name='ProjectName' />";
                dt = list.GetItems(query).GetDataTable();
                DataView dtview = new DataView(dt);
                dtdistinct = dtview.ToTable(true, "ProjectName");
            }
            return dtdistinct;
        }
        public string GetFQEFromQMDByProjectCode(string ProjectName, string SBUName, string Template)
        {
            string fqe = string.Empty;
            string LoginName = string.Empty;
            SPUser Username = null;
            if (SPContext.Current.Web.Lists["ProjectCodeProjectName"] != null)
            {
                SPList list = SPContext.Current.Web.Lists["ProjectCodeProjectName"];
                SPQuery query = new SPQuery();
                //query.Query = "<Where><And><And><Eq><FieldRef Name='ProjectName' /><Value Type='Text'>"+ProjectName+"</Value></Eq><Eq><FieldRef Name='SBU' /><Value Type='Text'>"+SBUName+"</Value></Eq></And><Eq><FieldRef Name='Template' /><Value Type='Choice'>"+Template+"</Value></Eq></And></Where>";
                query.Query = "<Where><And><And><And><Eq><FieldRef Name='ProjectName' /><Value Type='Text'>" + ProjectName + "</Value></Eq><Eq><FieldRef Name='SBU' /><Value Type='Text'>" + SBUName + "</Value></Eq></And><Eq><FieldRef Name='Template' /><Value Type='Choice'>" + Template + "</Value></Eq></And><Eq><FieldRef Name='Active' /><Value Type='Boolean'>1</Value></Eq></And></Where>";
                query.ViewFields = "<FieldRef Name='EmployeeName' />";
                SPListItemCollection queryItems = list.GetItems(query);
                foreach (SPListItem item in queryItems)
                {
                    if (item["EmployeeName"] != null)
                    {
                        fqe = item["EmployeeName"].ToString();
                        if (!string.IsNullOrEmpty(fqe))
                        {
                            SPFieldUserValue userValue = new SPFieldUserValue(SPContext.Current.Web, fqe);
                            Username = userValue.User;
                            LoginName = Username.Name;
                        }
                    }
                }
            }
            return LoginName;
        }
        public DataTable GetTemplateFromQMDByProjectCode(string ProjectName, string SBUName)
        {
            DataTable dtTemplates = new DataTable();
            if (SPContext.Current.Web.Lists["ProjectCodeProjectName"] != null)
            {
                SPList list = SPContext.Current.Web.Lists["ProjectCodeProjectName"];
                SPQuery query = new SPQuery();
                // query.Query = "<Where><And><Eq><FieldRef Name='ProjectName' /><Value Type='Text'>"+ProjectName+"</Value></Eq><Eq><FieldRef Name='SBU' /><Value Type='Text'>"+SBUName+"</Value></Eq></And></Where>";
                query.Query = "<Where><And><And><Eq><FieldRef Name='ProjectName' /><Value Type='Text'>" + ProjectName + "</Value></Eq><Eq><FieldRef Name='SBU' /><Value Type='Text'>" + SBUName + "</Value></Eq></And><Eq><FieldRef Name='Active' /><Value Type='Boolean'>1</Value></Eq></And></Where>";
                query.ViewFields = "<FieldRef Name='Template' />";
                dtTemplates = list.GetItems(query).GetDataTable();
            }
            return dtTemplates;
        }
        public int GetTemplateNo(string Template)
        {
            int templateno = 0;
            if (SPContext.Current.Web.Lists["Templates"] != null)
            {
                SPList list = SPContext.Current.Web.Lists["Templates"];
                SPQuery query = new SPQuery();
                query.Query = "<Where><Eq><FieldRef Name='Template' /><Value Type='Text'>" + Template + "</Value></Eq></Where>";
                query.ViewFields = "<FieldRef Name='TemplateNo' />";
                SPListItemCollection queryItems = list.GetItems(query);
                foreach (SPListItem item in queryItems)
                {
                    if (item["TemplateNo"] != null)
                    {
                        templateno = Convert.ToInt32(item["TemplateNo"]);
                    }
                }
            }
            return templateno;
        }
        public string GetFrequency(string Template)
        {
            string Frequency = string.Empty;
            if (SPContext.Current.Web.Lists["Templates"] != null)
            {
                SPList list = SPContext.Current.Web.Lists["Templates"];
                SPQuery query = new SPQuery();
                query.Query = "<Where><Eq><FieldRef Name='Template' /><Value Type='Text'>" + Template + "</Value></Eq></Where>";
                query.ViewFields = "<FieldRef Name='Frequency' />";
                SPListItemCollection queryItems = list.GetItems(query);
                foreach (SPListItem item in queryItems)
                {
                    if (item["Frequency"] != null)
                    {
                        Frequency = item["Frequency"].ToString();
                    }
                }
            }
            return Frequency;
        }
        public DataTable Year()
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionString);
            DataTable dtYear = new DataTable();
            string sqlQuery = "select DISTINCT  Year from Transactions";
            SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
            con.Open();
            sda.Fill(dtYear);
            con.Close();
            return dtYear;
        }
        public DataTable Month(string Year)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionString);
            DataTable dtMonth = new DataTable();
            string sqlQuery = "select DISTINCT  Month from Transactions where Year='" + Year + "'";
            SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
            con.Open();
            sda.Fill(dtMonth);
            con.Close();
            return dtMonth;
        }
        public DataTable GetTemplates()
        {
            DataTable dtTemplates = new DataTable();
            if (SPContext.Current.Web.Lists["Templates"] != null)
            {
                SPList list = SPContext.Current.Web.Lists["Templates"];
                SPQuery query = new SPQuery();
                query.Query = "<Where><Eq><FieldRef Name='Status' /><Value Type='Choice'>Active</Value></Eq></Where>";
                query.ViewFields = "<FieldRef Name='Template' /><FieldRef Name='TemplateNo' />";
                SPListItemCollection queryItems = list.GetItems(query);
                dtTemplates = list.GetItems(query).GetDataTable();
            }
            return dtTemplates;
        }
        public DataTable GetPeriod(string Year, string Month, string Template)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionString);
            DataTable dtPeriods = new DataTable();
            string sqlQuery = "select distinct Period from Transactions where Year='" + Year + "' and Month='" + Month + "' and [Template Name]='" + Template + "'";
            SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
            con.Open();
            sda.Fill(dtPeriods);
            con.Close();
            return dtPeriods;
        }
        public DataTable GetData(string Year, string Month, string Template, string Period)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionString);
            DataTable dtData = new DataTable();
            string sqlQuery = "select [Project Code],[Project Name],FQE_Name,[Template Name],SBU from Transactions where Year='" + Year + "' and Month='" + Month + "' and [Template Name]='" + Template + "' and period='" + Period + "' and Approved='Y'";
            SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
            con.Open();
            sda.Fill(dtData);
            con.Close();
            return dtData;
        }
    }
}

﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
namespace QMDDashboard_TPL.UploadTemplate
{
    [ToolboxItemAttribute(false)]
    public partial class UploadTemplate : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public UploadTemplate()
        {
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        DataTable dtTemp = new DataTable();
        QMDBAL objBal = new QMDBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlTemplate.Attributes.Add("onchange", "waitMessage();");
            ddlProjectName.Attributes.Add("onchange", "waitMessage();");
            ddlPeriod.Attributes.Add("onchange", "waitMessage();");
            //btnDownload.Attributes.Add("onclick", "waitMessage();");
            btnUpload.UseSubmitBehavior = false;
            btnUpload.OnClientClick = "_spFormOnSubmitCalled = false;_spSuppressFormOnSubmitWrapper=true";
            if (!Page.IsPostBack)
            {
                BindTemplateDataList();
            }
            //btnDownload.Visible = false;
            lblTemplateUpload.Attributes.Add("style", "display: none;");
            fuTemplateUpload.Attributes.Add("style", "display: none;");
            btnUpload.Attributes.Add("style", "display: none;");
            lblError.Text = string.Empty;
            //lblError.Visible = false;
            lblError.Attributes.Add("style", "display: none;");
        }
        private void BindTemplateDataList()
        {
            try
            {
                SPUserToken adminToken = SPContext.Current.Site.SystemAccount.UserToken;
                using (SPSite site = new SPSite(SPContext.Current.Site.Url, adminToken))
                {
                    using (SPWeb objSPWeb = site.OpenWeb())
                    {
                        SPList oList = SPContext.Current.Web.Lists[Constants.TemplateList];
                        SPQuery query = new SPQuery();//Template
                        query.Query = "<OrderBy><FieldRef Name='Template' /></OrderBy><Where><And><Eq><FieldRef Name='Status' /><Value Type='Choice'>Active</Value></Eq><IsNotNull><FieldRef Name='Template' /></IsNotNull></And></Where>";
                        DataTable dtcamltest = oList.GetItems(query).GetDataTable();
                        DataView dtview = new DataView(dtcamltest);
                        DataTable dtdistinct = dtview.ToTable(true, "Template", "TemplateNo");
                        if (dtdistinct != null && dtdistinct.Rows.Count > 0)
                        {
                            ddlTemplate.DataSource = dtdistinct;
                            ddlTemplate.DataTextField = "Template";
                            ddlTemplate.DataValueField = "TemplateNo";
                            ddlTemplate.DataBind();
                            ddlTemplate.Items.Insert(0, new ListItem("--Select--", "0"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void ddlTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblSuccess.Attributes.Add("style", "display: none;");
                if (ddlTemplate.SelectedIndex > 0)
                {
                    BindProjectsData(ddlTemplate.SelectedItem.Value.Trim());
                }
                else
                {
                    ddlProjectName.Attributes.Add("style", "display: none;");
                    lblProjectName.Attributes.Add("style", "display: none;");
                    lblPeriod.Attributes.Add("style", "display: none;");
                    ddlPeriod.Attributes.Add("style", "display: none;");
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "msg", "closeWaitMessage();", true);
            }
        }
        private void BindProjectsData(string TemplateName)
        {
            try
            {
                string userName = SPContext.Current.Web.CurrentUser.Name;
                SPUserToken adminToken = SPContext.Current.Site.SystemAccount.UserToken;
                using (SPSite site = new SPSite(SPContext.Current.Site.Url, adminToken))
                {
                    using (SPWeb objSPWeb = site.OpenWeb())
                    {
                        SPList projectList = SPContext.Current.Web.Lists[Constants.ListProjectCodProjectName];
                        SPQuery queryProjectList = new SPQuery();
                        queryProjectList.Query = @"<Where><And><And><Eq><FieldRef Name='TemplateNo' /><Value Type='Number'>" + ddlTemplate.SelectedValue.ToString() + "</Value></Eq><Eq><FieldRef Name='EmployeeName' /><Value Type='User'>" + userName + "</Value></Eq></And><Eq><FieldRef Name='Active' /><Value Type='Boolean'>1</Value></Eq></And></Where>";
                        DataTable dtProjectList = projectList.GetItems(queryProjectList).GetDataTable();
                        if (dtProjectList != null && dtProjectList.Rows.Count > 0)
                        {
                            ddlProjectName.DataSource = dtProjectList;
                            ddlProjectName.DataTextField = "ProjectName";
                            ddlProjectName.DataValueField = "ProjectCode";
                            ddlProjectName.DataBind();
                            ddlProjectName.Items.Insert(0, new ListItem("--Select--", "0"));
                            ddlProjectName.Attributes.Add("style", "display: block;");
                            lblProjectName.Attributes.Add("style", "display: block;");
                            lblPeriod.Attributes.Add("style", "display: none;");
                            ddlPeriod.Attributes.Add("style", "display: none;");
                        }
                        else
                        {
                            lblError.Text = "There are no projects assigned for the user!";
                            lblError.Attributes.Add("style", "display: block;");
                            lblError.ForeColor = Color.Red;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void ddlProjectName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlProjectName.SelectedIndex > 0)
                {
                    BindPeriodsData(ddlTemplate.SelectedItem.Value.Trim(), ddlProjectName.SelectedValue.Trim());
                }
                else
                {
                    ddlPeriod.Attributes.Add("style", "display: none;");
                    lblPeriod.Attributes.Add("style", "display: none;");
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "msg", "closeWaitMessage();", true);
            }
        }
        private void BindPeriodsData(string TemplateNo, string ProjectName)
        {
            try
            {
                SPUserToken adminToken = SPContext.Current.Site.SystemAccount.UserToken;
                using (SPSite site = new SPSite(SPContext.Current.Site.Url, adminToken))
                {
                    using (SPWeb objSPWeb = site.OpenWeb())
                    {
                        DataTable dt = new DataTable();
                        string ConnectionString = Constants.ConnectionString;
                        SqlConnection con = new SqlConnection(ConnectionString);
                        SqlDataAdapter da = new SqlDataAdapter("select [Template Name],[Period],[Template No],[Project Code],[Project Name],[Approved] from Transactions where [Approved] ='N' and [Project Code]='" + ProjectName + "' and [Template No]='" + TemplateNo + "' ", con);
                        DataSet ds = new DataSet();
                        con.Open();
                        da.Fill(ds);
                        if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        {
                            dt = ds.Tables[0];
                            ddlPeriod.DataSource = dt;
                            ddlPeriod.DataTextField = "Period";
                            ddlPeriod.DataValueField = "Period";
                            ddlPeriod.DataBind();
                            ddlPeriod.Items.Insert(0, new ListItem("--Select--", "0"));
                            ddlPeriod.Attributes.Add("style", "display: block;");
                            lblPeriod.Attributes.Add("style", "display: block;");
                            btnUpload.Attributes.Add("style", "display: none;");
                            lblTemplateUpload.Attributes.Add("style", "display: none;");
                            fuTemplateUpload.Attributes.Add("style", "display: none;");
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlPeriod.SelectedItem.Text != "--Select--")
                {
                    btnUpload.Attributes.Add("style", "display: block;");
                    fuTemplateUpload.Attributes.Add("style", "display: block;");
                    lblTemplateUpload.Attributes.Add("style", "display: block;");
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "msg", "closeWaitMessage();", true);
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (fuTemplateUpload.HasFile)
                {
                    if (fuTemplateUpload.PostedFile.ContentLength > 0)
                    {
                        string fileName = string.Empty;
                        string extension = ".xlsx";
                        fileName = fuTemplateUpload.PostedFile.FileName;
                        System.IO.Stream strm = fuTemplateUpload.PostedFile.InputStream;
                        byte[] byt = new byte[Convert.ToInt32(fuTemplateUpload.PostedFile.ContentLength)];
                        strm.Read(byt, 0, Convert.ToInt32(fuTemplateUpload.PostedFile.ContentLength));
                        strm.Close();
                       // SPSecurity.RunWithElevatedPrivileges(delegate()
                       // {
                            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
                            {
                                using (SPWeb objWeb = site.OpenWeb())
                                {
                                    if (objWeb.Lists[Constants.DocLibUpload] != null)
                                    {
                                        SPFolder mylibrary = objWeb.Folders[Constants.DocLibUpload];
                                        objWeb.AllowUnsafeUpdates = true;
                                        string fileNameWithoutExtension = ddlProjectName.SelectedValue + "_" + ddlTemplate.SelectedItem.Text + "_" + ddlPeriod.SelectedItem.Text;
                                        string fileNameToSave = string.Concat(fileNameWithoutExtension, extension);
                                        SPFile spFile = mylibrary.Files.Add(fileNameToSave, byt, true);
                                        string str = ddlTemplate.SelectedItem.Text.ToString() + "--" + ddlTemplate.SelectedItem.Value.ToString() + "--" + ddlProjectName.SelectedItem.Text.ToString() + "--" + ddlProjectName.SelectedItem.Value.ToString() + "--" + ddlPeriod.SelectedValue.ToString();
                                        spFile.Item.Properties["Template Name"] = ddlTemplate.SelectedItem.Text.ToString();
                                        spFile.Item.Properties["TemplateNo"] = ddlTemplate.SelectedItem.Value.ToString();
                                        spFile.Item.Properties["Project Name"] = ddlProjectName.SelectedItem.Text.ToString();
                                        spFile.Item.Properties["Project Code"] = ddlProjectName.SelectedItem.Value.ToString();
                                        spFile.Item.Properties["Approval Status"] = "Pending";
                                        spFile.Item.Properties["SBU"] = GetSBU(ddlProjectName.SelectedItem.Value);
                                        spFile.Item.Properties["Period"] = ddlPeriod.SelectedValue.ToString();
                                        SPUser CreatedUser = SPContext.Current.Web.CurrentUser;
                                        spFile.Item.Properties["FQEName"] = CreatedUser.Name.ToString();
                                        spFile.Item.Properties["FQEID"] = GetOnlyEmployeeID(CreatedUser.LoginName).ToString();
                                        spFile.Item.SystemUpdate();
                                        spFile.Update();
                                        mylibrary.Update();
                                        string workflowName = Constants.Workflow;                                       
                                        SPWorkflowAssociation wfAssoc = spFile.Item.ParentList.WorkflowAssociations.GetAssociationByName(workflowName, System.Globalization.CultureInfo.CurrentCulture);
                                        SPWorkflow wf = spFile.Item.Web.Site.WorkflowManager.StartWorkflow(spFile.Item, wfAssoc, wfAssoc.AssociationData, true);
                                        //result = true;
                                        objWeb.AllowUnsafeUpdates = false;
                                    }
                                }
                            }
                       // });
                    }
                    ddlTemplate.SelectedIndex = 0;
                    ddlProjectName.SelectedIndex = 0;
                    ddlPeriod.SelectedIndex = 0;
                    ddlTemplate.Attributes.Add("style", "display: block;");
                    lblTemplate.Attributes.Add("style", "display: block;");
                    ddlProjectName.Attributes.Add("style", "display: none;");
                    lblProjectName.Attributes.Add("style", "display: none;");
                    ddlPeriod.Attributes.Add("style", "display: none;");
                    lblPeriod.Attributes.Add("style", "display: none;");
                    btnUpload.Attributes.Add("style", "display: none;");
                    lblTemplateUpload.Attributes.Add("style", "display: none;");
                    fuTemplateUpload.Attributes.Add("style", "display: none;");
                    lblSuccess.Text = "Uploaded succesfully";
                    lblSuccess.Attributes.Add("style", "display: block;");
                }
                else
                {
                    lblError.Text = "Please select an attachment before clicking the upload button.";
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
        public string GetOnlyEmployeeID(string loggedInName)
        {
            string empID = string.Empty;
            string loggedNameLowerCase = loggedInName.ToLower().ToString();
            if (loggedNameLowerCase.Contains("tpl"))
            {
                string replaced = loggedNameLowerCase.Replace('\\', '@');
                if (replaced.Contains("@"))
                {
                    string[] seperators = { "@" };
                    string[] nameArray = replaced.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
                    if (nameArray.Length == 2)
                    {
                        if (nameArray[1] != null)
                        {
                            empID = nameArray[1];
                        }
                    }
                }
                else
                {
                    string replacedTest = loggedNameLowerCase.Replace('\\', '@');
                }
            }
            return empID.ToUpper();
        }
        public string GetSBU(string projectCode)
        {
            string code = projectCode;
            string SBUName = string.Empty;
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite site = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        try
                        {
                            SPList list = null;
                            if (web.Lists[Constants.ListProjectCodProjectName] != null)
                            {
                                web.AllowUnsafeUpdates = true;
                                list = web.Lists[Constants.ListProjectCodProjectName];
                                SPQuery query = new SPQuery();
                                query.Query = "<Where><And><Eq><FieldRef Name='TemplateNo' /><Value Type='Number'>" + ddlTemplate.SelectedValue.ToString() + "</Value></Eq><Eq><FieldRef Name='ProjectCode' /><Value Type='Text'>" + code + "</Value></Eq></And></Where>";
                                query.ViewFields = "<FieldRef Name='SBU' />";
                                SPListItemCollection queryItems = list.GetItems(query);
                                foreach (SPListItem item in queryItems)
                                {
                                    if (item["SBU"] != null)
                                    {
                                        SBUName = item["SBU"].ToString();
                                    }
                                }
                                web.AllowUnsafeUpdates = false;
                            }
                        }
                        catch (Exception exp)
                        {
                            throw;
                        }
                    }
                }
            });
            return SBUName;
        }
    }
}

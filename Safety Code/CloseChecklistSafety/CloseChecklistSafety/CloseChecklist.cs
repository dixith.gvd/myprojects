﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using System.Configuration;
using System.Data;
using System.Xml;
using System.IO;
using System.Xml.XPath;
namespace CloseChecklistSafety
{
    class CloseChecklist
    {

        public void ProcessLogic()
        {
            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            string data = GetProjectCodes();
            string[] pcodes = data.Split(',');
            foreach (string code in pcodes)
            {
                UpdateForm(code);
            }

        }

        public string GetProjectCodes()
        {
            DataTable dt = new DataTable();
            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            List<string> pcode = new List<string>();
            string pcodes = string.Empty;
            using (SPSite site = new SPSite(siteurl))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        SPList _list = web.Lists["Project PM RCM"];
                        SPQuery query = new SPQuery();
                        query.Query = "<Where><IsNull><FieldRef Name='Safety_x0020_Officer' /></IsNull></Where>";
                        SPListItemCollection items = _list.GetItems(query);
                        foreach (SPListItem item in items)
                        {
                            if (!pcode.Contains(item["Project_x0020_Code"].ToString()))
                            {
                                pcode.Add(item["Project_x0020_Code"].ToString());
                            }
                        }
                        if (pcode.Count > 0)
                        {
                            pcodes = string.Join(",", pcode);
                        }
                    });
                }
            }
            return pcodes;
        }

        public void UpdateForm(string pcode)
        {
            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            string fpath = ConfigurationManager.AppSettings["fPath"].ToString();
            using (SPSite site = new SPSite(siteurl))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPList lst = null;
                    if (web.Lists["Checklists_EHS"] != null)
                    {
                        lst = web.Lists["Checklists_EHS"];
                        SPQuery query = new SPQuery();
                        query.Query = "<Where><And><Eq><FieldRef Name='PCode' /><Value Type='Text'>"+ pcode +"</Value></Eq><Neq><FieldRef Name='Stage' /><Value Type='Text'>Closed</Value></Neq></And></Where>";
                        SPListItemCollection items = lst.GetItems(query);
                        foreach (SPListItem item in items)
                        {
                            //SPListItem lstitem = lst.GetItemById(660001);
                            DateTime todaysdate = DateTime.Today;
                            string todaydate = String.Format("{0:dd MMMM yyyy}", todaysdate);
                            MemoryStream oMemoryStream = new MemoryStream(item.File.OpenBinary());
                            XmlTextReader oReader = new XmlTextReader(oMemoryStream);
                            XmlDocument oDoc = new XmlDocument();
                            oDoc.Load(oReader);
                            oReader.Close();
                            oMemoryStream.Close();
                            XPathNavigator ipFormNav = oDoc.DocumentElement.CreateNavigator();
                            XmlNamespaceManager NamespaceManager = new XmlNamespaceManager(ipFormNav.NameTable);
                            NamespaceManager.AddNamespace("my", "http://schemas.microsoft.com/office/infopath/2003/myXSD/2016-12-21T04:42:18");
                            //XPathNavigator docname = ipFormNav.SelectSingleNode("my:AttachUploadSelfCert", NamespaceManager);
                            oDoc.DocumentElement.SelectSingleNode("my:Remarks", NamespaceManager).InnerText = "Closed on " + todaydate + " because the project was deactivated";
                            oDoc.DocumentElement.SelectSingleNode("my:Stage", NamespaceManager).InnerText = "Closed";
                            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                            SPFile oSPFile = web.Folders["Checklists_EHS"].Files.Add(item.File.Name.ToString(), (encoding.GetBytes(oDoc.OuterXml)), true);
                            item.File.Update();
                            StringBuilder sb = new StringBuilder();
                            sb.Append((int)item["ID"]+",");
                            File.AppendAllText(fpath + "logclose.txt", sb.ToString());
                            sb.Clear();
                        }
                    }
                }
            }
        }
    }
}

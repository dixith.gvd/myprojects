﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using Renci.SshNet;
namespace CSV_SFTP
{
    class Program
    {
        static string connectionString = ConfigurationManager.AppSettings["ConnectionStringSql"].ToString();
        static string logsPath = ConfigurationManager.AppSettings["logsPath"].ToString();
        static void Main(string[] args)
        {

            string tdate = DateTime.Today.ToString("yyyy-MM-dd");
            DataTable dt = new DataTable();
            string sqlQuery = "select * from HappayNewJoinee_Master where LastUpdatedDate='" + tdate + "'";
            //string sqlQuery = "select * from [dbo].['TPL Employee Data$'] order by [Request ID] asc";
            StringBuilder sb = new StringBuilder();
            string strdelim = "|";
            
            sb.Append("RequestID|First Name|Middle Name|Last Name|Title|Gender|Date of Birth|Mobile Number|Email Id|TPL Emp ID|Last Working Day|SBG Name|SBU Name|BU Name|Location|State|Grade|Designation|Parent_Dept_Name|Manager Name|Role Name ID1|Role Name 1|Vendor_Code|Vendor_Name");
            //sb.Append("Name,EmpID,Gender,DOB,Emailid,FatherHusband,FatherHusbandName,DateofGroupJoin,DOJ,Jobreason,Unit,Department,Band,Grade,Position,Designation,Zone,Region,Location,CostCenter,EmpCategory,Appraiser,ReportingTo,EffectiveFrom ,LocationType ,ParentDepartment,LineOfBusiness,Vendor_ID,Vendor_Name,Created_Date,Modified_Date");
            //sb.Append("Name|EmpID|Gender|DOB|Emailid|FatherHusband|FatherHusbandName|DateofGroupJoin|DOJ|Jobreason|Unit|Department|Band|Grade|Position|Designation|Zone|Region|Location|CostCenter|EmpCategory|Appraiser|ReportingTo|EffectiveFrom |LocationType |ParentDepartment|LineOfBusiness|Vendor_ID|Vendor_Name|Created_Date|Modified_Date");
            //sb.Append("RequestID|First Name|Last Name|Title|Gender|Date of Birth|Mobile Number|Email Id|TPL Emp ID|Last Working Day|SBG Name|SBU Name|SBU Code|BU Name|Location|State|Grade|Designation|Parent_Dept_Name|Manager Name|Role Name ID1|Role Name 1|Vendor_Code|Vendor_Name");
            sb.Append("\r\n");
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, conn))
                    {
                        conn.Open();
                        daAdap.Fill(dt);
                        //foreach (DataRow row in dt.Rows)
                        DataRowCollection row = dt.Rows;
                        for (int i = 0; i < row.Count; i++)
                        {
                            bool sentcsv = false;
                            //sb.Append(row["Name"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["EmpID"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Gender"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["DOB"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Emailid"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["FatherHusband"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["FatherHusbandName"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["DateofGroupJoin"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["DOJ"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Jobreason"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Unit"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Department"].ToString().Replace(",", " ") + strdelim);

                            //sb.Append(row["Band"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Grade"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Position"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Designation"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Zone"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Region"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Location"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["CostCenter"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["EmpCategory"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Appraiser"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["ReportingTo"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["EffectiveFrom"].ToString().Replace(",", " ") + strdelim);

                            //sb.Append(row["LocationType"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["ParentDepartment"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["LineOfBusiness"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Vendor_ID"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Vendor_Name"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Created_Date"].ToString().Replace(",", " ") + strdelim);
                            //sb.Append(row["Modified_Date"].ToString().Replace(",", " "));
                            if (row[i][0].ToString() != "" && row[i][1].ToString() != "" && row[i][3].ToString() != "" && row[i][4].ToString() != "" && row[i][5].ToString() != "" && row[i][6].ToString() != "" && row[i][7].ToString() != "" && row[i][8].ToString() != "" && row[i][9].ToString() != "")
                            {
                                sentcsv = true;
                                string dob = string.Empty; string lastday = string.Empty;

                                sb.Append(row[i][0].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][1].ToString().Replace(",", " ") + strdelim);

                                sb.Append(row[i][2].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][3].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][4].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][5].ToString().Replace(",", " ") + strdelim);
                                if (row[i][6].ToString() != "")
                                {
                                    dob = row[i][6].ToString();
                                    DateTime dtdob = Convert.ToDateTime(dob);
                                    dob = String.Format("{0:yyyy-MM-dd}", dtdob);
                                }
                                sb.Append(dob.Replace(",", " ") + strdelim);
                                sb.Append(row[i][7].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][8].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][9].ToString().Replace(",", " ") + strdelim);
                                if (row[i][10].ToString() != "")
                                {
                                    lastday = row[i][10].ToString();
                                    DateTime dtlast = Convert.ToDateTime(lastday);
                                    lastday = String.Format("{0:yyyy-MM-dd}", dtlast);
                                }
                                sb.Append(lastday.Replace(",", " ") + strdelim);
                                sb.Append(row[i][11].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][12].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][13].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][14].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][15].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][16].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][17].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][18].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][19].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][20].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][21].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][22].ToString().Replace(",", " ") + strdelim);
                                sb.Append(row[i][23].ToString().Replace(",", " "));
                                //sb.Append(row[i][23].ToString().Replace(",", " ") + strdelim);
                                //sb.Append(row[i][24].ToString().Replace(",", " ") + strdelim);
                                //sb.Append(row[i][25].ToString().Replace(",", " ") + strdelim);
                                //sb.Append(row[i][26].ToString().Replace(",", " ") + strdelim);
                                //sb.Append(row[i][27].ToString().Replace(",", " ") + strdelim);
                                //sb.Append(row[i][28].ToString().Replace(",", " ") + strdelim);
                                //sb.Append(row[i][29].ToString().Replace(",", " "));

                                sb.Append("\r\n");
                            }
                            UpdateCSVStatus(sentcsv, row[i][0].ToString());
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                ReminderBackupLog(exp.Message + ", while getting data from Vendor Table");
            }
            string dtFormat = String.Format("{0:ddMMMMyyyy}", DateTime.Today);
            string path = logsPath + "HRMSData_" + dtFormat + ".csv";
            StreamWriter file = new StreamWriter(path);
            file.WriteLine(sb.ToString());
            file.Close();
            file.Dispose();
            MoveFiletoSFTP(path);
        }
        public static void UpdateCSVStatus(bool status, string id)
        {

            string sqlQuery = "Update HappayNewJoinee_Master set SentToCsv='" + status + "' where [Request ID]='" + id + "'";
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Connection = conn;
                        conn.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception exp)
            {
                ReminderBackupLog(exp.Message + "while updating the data");
            }
        }
        public static void MoveFiletoSFTP(string path)
        {
            var host = "192.168.10.44";
            var port = 22;
            var username = "OTON-TCS";
            var password = "Welc0me#1234";

            // path for file you want to upload
            
            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {
                    Console.WriteLine("I'm connected to the client");

                    using (var fileStream = new FileStream(path, FileMode.Open))
                    {

                        client.BufferSize = 4 * 1024; // bypass Payload error large files
                        client.ChangeDirectory("/users/OTON-TCS/HRMS Sample");
                        client.UploadFile(fileStream, Path.GetFileName(path));
                        
                    }
                }
                else
                {
                    Console.WriteLine("I couldn't connect");
                }
            }
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
        }
        public static void ReminderBackupLog(string message)
        {
            try
            {
                string PathName = logsPath;
                PathName = PathName + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                StreamWriter sw = new StreamWriter(PathName, true);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception exp)
            {
            }
        }
    }
}

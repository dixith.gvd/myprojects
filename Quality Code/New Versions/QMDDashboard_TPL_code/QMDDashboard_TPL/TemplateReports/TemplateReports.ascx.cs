﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
namespace QMDDashboard_TPL.TemplateReports
{
    [ToolboxItemAttribute(false)]
    public partial class TemplateReports : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public TemplateReports()
        {
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        SPOperations objSPO = new SPOperations();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    btnSubmit.Enabled = false;
                    lblNote.Visible = false;
                    DataTable dtYear = new DataTable();
                    dtYear = objSPO.Year();
                    if (dtYear.Rows.Count > 0)
                    {
                        ddlyear.DataSource = dtYear;
                        ddlyear.DataTextField = "Year";
                        ddlyear.DataValueField = "Year";
                        ddlyear.DataBind();
                        ddlyear.Items.Insert(0, new ListItem("--Select--", "0"));
                    }
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.Message;
            }
        }
        protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtMonth = new DataTable();
                dtMonth = objSPO.Month(ddlyear.SelectedItem.Text);
                if (dtMonth.Rows.Count > 0)
                {
                    ddlMonth.DataSource = dtMonth;
                    ddlMonth.DataTextField = "Month";
                    ddlMonth.DataValueField = "Month";
                    ddlMonth.DataBind();
                    ddlMonth.Items.Insert(0, new ListItem("--Select--", "0"));
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.Message;
            }
        }
        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtTemplate = new DataTable();
            try
            {
                dtTemplate = objSPO.GetTemplates();
                if (dtTemplate.Rows.Count > 0)
                {
                    ddlTemplates.DataSource = dtTemplate;
                    ddlTemplates.DataTextField = "Template";
                    ddlTemplates.DataValueField = "TemplateNo";
                    ddlTemplates.DataBind();
                    ddlTemplates.Items.Insert(0, new ListItem("--Select--", "0"));
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.Message;
            }
        }
        protected void ddlTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtPeriods = new DataTable();
                dtPeriods = objSPO.GetPeriod(ddlyear.SelectedItem.Text, ddlMonth.SelectedItem.Text, ddlTemplates.SelectedItem.Text);
                if (dtPeriods.Rows.Count > 0)
                {
                    ddlPeriod.DataSource = dtPeriods;
                    ddlPeriod.DataTextField = "Period";
                    ddlPeriod.DataValueField = "Period";
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Insert(0, new ListItem("--Select--", "0"));
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.Message;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtData = objSPO.GetData(ddlyear.SelectedItem.Text, ddlMonth.SelectedItem.Text, ddlTemplates.SelectedItem.Text, ddlPeriod.SelectedItem.Text);
                if (dtData.Rows.Count > 0)
                {                    
                    lblNote.Visible = true;
                    grdtemplateReport.DataSource = dtData;
                    grdtemplateReport.DataBind();
                    grdtemplateReport.Visible = true;
                    string Year = ddlyear.SelectedItem.Text;                    
                    string Month = ddlMonth.SelectedItem.Text;
                    string Template = ddlTemplates.SelectedItem.Text;
                    Template=Template.Replace(' ','@');
                    string Period=ddlPeriod.SelectedItem.Text;
                    Period = Period.Replace(' ', '@');
                    string rediUrl = SPContext.Current.Web.Url+ "/Pages/DownloadReports.aspx?Year=" + Year + "&Month=" + Month + "&Template=" + Template + "&Period=" + Period;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "window.open('" + rediUrl + "','','width=500,height=320,left=200,top=200');", true);
                }
                else
                {
                    lblErrorMessage.Text = "No Records Found";
                    grdtemplateReport.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.Message;
            }
        }
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSubmit.Enabled = true;
        }
    }
}

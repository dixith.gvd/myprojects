﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Drawing;

namespace QMDDashboard_TPL.ChecklistsModule.ChecklistView1
{
    [ToolboxItemAttribute(false)]
    public partial class ChecklistView1 : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public ChecklistView1()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();

        }
        QualityChecklists objQuality = new QualityChecklists();
        private bool IsLoggedInAuthorisedUser(string email)
        {
            bool isValid = SPContext.Current.Web.CurrentUser.Email.Equals(email);
            return isValid;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["pending"] != null)
            {
                gvchecklist.DataSource = ViewState["pname"];
            }
            btnfirst.Visible = true;
            btnprevious.Visible = true;
            btnnext.Visible = true;
            btnlast.Visible = true;
            //btnfirst.Enabled = false;
            //btnprevious.Enabled = false;
            if (!this.Page.IsPostBack)
            {
                DataTable dt = new DataTable();
                //gvchecklist.PageIndex = 0;
                gvchecklist.DataSource = dt;
                gvchecklist.DataBind();
                btnexport.Enabled = false;
                btnfirst.Visible = false;
                btnprevious.Visible = false;
                btnnext.Visible = false;
                btnlast.Visible = false;
            }
        }
        private void ExportGridToExcel()
        {
            DataTable dtData = (DataTable)ViewState["pending"];
            //DataTable dtDataPE = objQuality.GetChecklistDataPE();
            //DataTable dtDataAssignee = objQuality.GetChecklistDataAssignee();
            //DataTable dtDataFQE = objQuality.GetChecklistDataFQE();

            if (dtData.Rows.Count > 0)
            {
                try
                {
                    GridView gv = new GridView();

                    SPBoundField boundField = new SPBoundField();

                    //boundField.HeaderText = "Transaction ID";
                    //boundField.DataField = "Transaction_ID";
                    //gv.Columns.Add(boundField);

                    HyperLinkField hfield = new HyperLinkField();
                    hfield.DataNavigateUrlFields = new string[] { "Transaction_ID" };
                    hfield.DataNavigateUrlFormatString = "https://tplnet.tataprojects.com/Pages/ChecklistDetails.aspx?Tid={0}";
                    hfield.DataTextField = "Transaction_ID";
                    hfield.HeaderText = "Transaction_ID";
                    hfield.Target = "_blank";
                    //hfield.NavigateUrl = "https://tplnet.tataprojects.com/Pages/ChecklistDetails.aspx?Tid=" + boundField.DataField;
                    gv.Columns.Add(hfield);

                    //SPBoundField boundField = new SPBoundField();

                    boundField = new SPBoundField();
                    boundField.HeaderText = "Stage";
                    boundField.DataField = "Stage";
                    boundField.ControlStyle.Width = new Unit(120);
                    gv.Columns.Add(boundField);

                    boundField = new SPBoundField();
                    boundField.HeaderText = "Type";
                    boundField.DataField = "Compliance_Type";
                    boundField.ControlStyle.Width = new Unit(120);
                    gv.Columns.Add(boundField);

                    boundField = new SPBoundField();
                    boundField.HeaderText = "BU Name";
                    boundField.DataField = "BUName";
                    boundField.ControlStyle.Width = new Unit(120);
                    gv.Columns.Add(boundField);

                    boundField = new SPBoundField();
                    boundField.HeaderText = "Project Name";
                    boundField.DataField = "ProjectName";
                    boundField.ControlStyle.Width = new Unit(120);
                    gv.Columns.Add(boundField);


                    boundField = new SPBoundField();
                    boundField.HeaderText = "Project Type";
                    boundField.DataField = "ProjectType";
                    boundField.ControlStyle.Width = new Unit(120);
                    gv.Columns.Add(boundField);

                    boundField = new SPBoundField();
                    boundField.HeaderText = "Discipline";
                    boundField.DataField = "Discipline";
                    boundField.ControlStyle.Width = new Unit(120);
                    gv.Columns.Add(boundField);

                    boundField = new SPBoundField();
                    boundField.HeaderText = "Activity";
                    boundField.DataField = "Activity";
                    boundField.ControlStyle.Width = new Unit(120);
                    gv.Columns.Add(boundField);


                    boundField = new SPBoundField();
                    boundField.HeaderText = "Sub Activity";
                    boundField.DataField = "Sub Activity";
                    boundField.ControlStyle.Width = new Unit(120);
                    gv.Columns.Add(boundField);

                    boundField = new SPBoundField();
                    boundField.HeaderText = "Is Compliant";
                    boundField.DataField = "isCompliant";
                    boundField.ControlStyle.Width = new Unit(120);
                    gv.Columns.Add(boundField);

                    gv.AutoGenerateColumns = false;
                    //if (dtDataRCM.Rows.Count > 0)
                    //    gv.DataSource = dtDataRCM;
                    //else if (dtDataPE.Rows.Count > 0)
                    //    gv.DataSource = dtDataPE;
                    //else if (dtDataFQE.Rows.Count > 0)
                    //    gv.DataSource = dtDataFQE;
                    //else if (dtDataAssignee.Rows.Count > 0)
                    gv.DataSource = dtData;
                    gv.DataBind();
                    gv.AllowSorting = false;
                    HttpContext.Current.Response.ClearContent();
                    HttpContext.Current.Response.ClearHeaders();
                    string attachment = "attachment; filename=" + SPContext.Current.Web.CurrentUser.Name + "_" + DateTime.Now.ToString() + ".xls";
                    HttpContext.Current.Response.AddHeader("content-disposition", attachment);
                    HttpContext.Current.Response.ContentType = "application/Excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    htw.AddAttribute("xmlns:x", "urn:schemas-microsoft-com:office:excel"); // ' optional'
                    htw.RenderBeginTag(HtmlTextWriterTag.Html);
                    htw.RenderBeginTag(HtmlTextWriterTag.Head);
                    htw.RenderBeginTag(HtmlTextWriterTag.Style);
                    htw.Write("br {mso-data-placement:same-cell;}");
                    htw.RenderEndTag(); // ' /Style'
                    htw.RenderEndTag(); // ' /Head'
                    htw.RenderBeginTag(HtmlTextWriterTag.Body);
                    gv.RenderControl(htw);
                    htw.RenderEndTag();// ' /Body'
                    htw.RenderEndTag(); ///Html'
                    HttpContext.Current.Response.Write(HttpUtility.HtmlDecode(sw.ToString()));
                    HttpContext.Current.Response.Write(sw.ToString());
                    //HttpContext.Current.Response.Flush();
                    //HttpContext.Current.Response.Close();
                    HttpContext.Current.Response.End();
                    //HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
                catch (System.Threading.ThreadAbortException)
                {
                    //lblError.Text = exp.Message;
                }
            }
        }

        private void BindGridRCM(string uname)
        {
            DataTable dtdataRCM = objQuality.GetChecklistDataRCM(uname);
            DataTable dt = new DataTable();
            string rcmEmail = string.Empty;
            string stage = string.Empty;
            if (dtdataRCM.Rows.Count > 0)
            {
                if (dtdataRCM.Rows[0]["RCM_Email"] != null)
                {
                    rcmEmail = dtdataRCM.Rows[0]["RCM_Email"].ToString();
                    if (IsLoggedInAuthorisedUser(rcmEmail))
                    {
                        gvchecklist.DataSource = dtdataRCM;
                        gvchecklist.DataBind();
                        btnexport.Enabled = true;
                        btnfirst.Enabled = false;
                        btnprevious.Enabled = false;
                        btnnext.Enabled = true;
                        btnlast.Enabled = true;
                    }
                }
            }
            else
            {
                gvchecklist.DataSource = dt;
                gvchecklist.DataBind();
                btnexport.Enabled = false;
                btnfirst.Visible = false;
                btnprevious.Visible = false;
                btnnext.Visible = false;
                btnlast.Visible = false;
            }
        }
        private DataTable BindGridPE(string uname)
        {
            DataTable dtdataPE = objQuality.GetChecklistDataPE(uname);
            DataTable dt = new DataTable();
            string peEmail = string.Empty;
            string stage = string.Empty;
            if (dtdataPE.Rows.Count > 0)
            {
                if (dtdataPE.Rows[0]["PE_Email"] != null)
                {
                    peEmail = dtdataPE.Rows[0]["PE_Email"].ToString();
                    if (IsLoggedInAuthorisedUser(peEmail))
                    {
                        gvchecklist.DataSource = dtdataPE;
                        gvchecklist.DataBind();
                        btnexport.Enabled = true;
                        btnfirst.Enabled = false;
                        btnprevious.Enabled = false;
                        btnnext.Enabled = true;
                        btnlast.Enabled = true;
                    }
                }
            }
            else
            {
                gvchecklist.DataSource = dt;
                gvchecklist.DataBind();
                btnexport.Enabled = false;
                btnfirst.Visible = false;
                btnprevious.Visible = false;
                btnnext.Visible = false;
                btnlast.Visible = false;
            }
            return (DataTable)gvchecklist.DataSource;
        }
        private DataTable BindGridFQE(string uname)
        {
            DataTable dtdataFQE = objQuality.GetChecklistDataFQE(uname);
            DataTable dt = new DataTable();
            string fqeEmail = string.Empty;
            string stage = string.Empty;
            if (dtdataFQE.Rows.Count > 0)
            {
                if (dtdataFQE.Rows[0]["FQE_Email"] != null)
                {
                    fqeEmail = dtdataFQE.Rows[0]["FQE_Email"].ToString();
                    if (IsLoggedInAuthorisedUser(fqeEmail))
                    {
                        gvchecklist.DataSource = dtdataFQE;
                        gvchecklist.DataBind();
                        btnexport.Enabled = true;
                        btnfirst.Enabled = false;
                        btnprevious.Enabled = false;
                        btnnext.Enabled = true;
                        btnlast.Enabled = true;
                    }
                }
            }
            else
            {
                gvchecklist.DataSource = dt;
                gvchecklist.DataBind();
                btnexport.Enabled = false;
                btnfirst.Visible = false;
                btnprevious.Visible = false;
                btnnext.Visible = false;
                btnlast.Visible = false;
            }
            return (DataTable)gvchecklist.DataSource;
        }
        private DataTable BindGridAssignee(string uname)
        {
            DataTable dtdataAssignee = objQuality.GetChecklistDataAssignee(uname);
            DataTable dt = new DataTable();
            string assigneeEmail = string.Empty;
            string stage = string.Empty;
            if (dtdataAssignee.Rows.Count > 0)
            {
                if (dtdataAssignee.Rows[0]["Assignee_Email"] != null)
                {
                    assigneeEmail = dtdataAssignee.Rows[0]["Assignee_Email"].ToString();
                    if (IsLoggedInAuthorisedUser(assigneeEmail))
                    {
                        gvchecklist.DataSource = dtdataAssignee;
                        gvchecklist.DataBind();
                        btnexport.Enabled = true;
                        btnfirst.Enabled = false;
                        btnprevious.Enabled = false;
                        btnnext.Enabled = true;
                        btnlast.Enabled = true;
                    }
                }
            }
            else
            {
                gvchecklist.DataSource = dt;
                gvchecklist.DataBind();
                btnexport.Enabled = false;
                btnfirst.Visible = false;
                btnprevious.Visible = false;
                btnnext.Visible = false;
                btnlast.Visible = false;
            }
            return (DataTable)gvchecklist.DataSource;
        }

        protected void btnrcm_Click(object sender, EventArgs e)
        {
            BindGridRCM(SPContext.Current.Web.CurrentUser.Email);
            ViewState["pending"] = gvchecklist.DataSource;
            //p = ViewState["pname"].ToString();
        }

        protected void btnassignee_Click(object sender, EventArgs e)
        {
            BindGridAssignee(SPContext.Current.Web.CurrentUser.Email);
            ViewState["pending"] = gvchecklist.DataSource;
            //p = ViewState["pname"].ToString();
        }

        protected void btnpe_Click(object sender, EventArgs e)
        {
            BindGridPE(SPContext.Current.Web.CurrentUser.Email);
            ViewState["pending"] = gvchecklist.DataSource;
            //p = ViewState["pname"].ToString();
        }

        protected void btnfqe_Click(object sender, EventArgs e)
        {
            BindGridFQE(SPContext.Current.Web.CurrentUser.Email);
            ViewState["pending"] = gvchecklist.DataSource;
            //p = ViewState["pname"].ToString();
        }



        protected void ChangePage(object sender, CommandEventArgs e)
        {
            try
            {

                switch (e.CommandName)
                {
                    case "Previous":
                        if (gvchecklist.PageIndex > 0)
                        {
                            gvchecklist.PageIndex = gvchecklist.PageIndex - 1;
                            btnlast.Enabled = true;
                            btnnext.Enabled = true;
                            btnfirst.Enabled = true;
                        }
                        if (gvchecklist.PageIndex == 0)
                        {
                            btnfirst.Enabled = false;
                            btnprevious.Enabled = false;
                        }

                        break;

                    case "Next":
                        int i = gvchecklist.PageIndex + 1;

                        if (i <= gvchecklist.PageCount)
                        {
                            gvchecklist.PageIndex = i;
                            btnlast.Enabled = true;
                            btnprevious.Enabled = true;
                            btnfirst.Enabled = true;
                        }
                        if (gvchecklist.PageCount - 1 == gvchecklist.PageIndex)
                        {
                            btnnext.Enabled = false;
                            btnlast.Enabled = false;
                        }
                        break;
                    case "First":
                        gvchecklist.PageIndex = 0;
                        btnfirst.Enabled = false;
                        btnprevious.Enabled = false;
                        btnlast.Enabled = true;
                        btnnext.Enabled = true;
                        break;
                    case "Last":
                        gvchecklist.PageIndex = gvchecklist.PageCount - 1;
                        btnlast.Enabled = false;
                        btnfirst.Enabled = true;
                        btnnext.Enabled = false;
                        btnprevious.Enabled = true;
                        break;
                    //case "ExcelExport":
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please reload the page after downloding excel file.');", true);
                    //    ExportGridToExcel();
                    //HttpContext.Current.Response.Buffer=true;

                }

            }
            catch (Exception)
            {

                throw;
            }

            gvchecklist.DataSource = ViewState["pending"];
            gvchecklist.DataBind();
        }

        protected void btnexport_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please reload the page after downloded excel file.');", true);
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showalert", "<script>alert('Please reload the page after downloding excel file.');</script>", false);
            ExportGridToExcel();
        }
    }
}

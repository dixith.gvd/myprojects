﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web;
using System.Web.UI.WebControls;
using System.Drawing;

namespace QMDDashboard_TPL.ExcelReport
{
    [ToolboxItemAttribute(false)]
    public partial class ExcelReport : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public ExcelReport()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        SPOperations objSPO = new SPOperations();
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Page.Request.QueryString["Year"] != null) && (Page.Request.QueryString["Month"] != null) && (Page.Request.QueryString["Template"] != null) && (Page.Request.QueryString["Period"] != null))
            {

                string Year = Page.Request.QueryString["Year"].ToString();
                string Month = Page.Request.QueryString["Month"].ToString();
                string Template = Page.Request.QueryString["Template"].ToString();
                Template = Template.Replace('@', ' ');
                string Period = Page.Request.QueryString["Period"].ToString();
                Period = Period.Replace('@', ' ');

                DataTable dtData = objSPO.GetData(Year, Month, Template, Period);
                if (dtData.Rows.Count > 0)
                {
                    this.Page.Response.Buffer = true;
                    this.Page.Response.ClearContent();
                    //this.Page.Response.ClearHeaders();
                    //this.Page.Response.Charset = "";
                    string FileName = "TemplateReport.xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    this.Page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    this.Page.Response.ContentType = "application/ms-excel";// "application/vnd.ms-excel";
                    this.Page.Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    GridView GridView1 = new GridView();
                    GridView1.DataSource = dtData;
                    GridView1.DataBind();
                    GridView1.GridLines = GridLines.Both;
                    GridView1.HeaderStyle.BackColor = Color.FromName("#507CD1");
                    GridView1.HeaderStyle.ForeColor = Color.White;
                    GridView1.HeaderStyle.Font.Bold = true;
                    GridView1.RenderControl(htmltextwrtter);
                    this.Page.Response.Write(strwritter.ToString());
                    this.Page.Response.End();

                }
            }

        }
    }
}

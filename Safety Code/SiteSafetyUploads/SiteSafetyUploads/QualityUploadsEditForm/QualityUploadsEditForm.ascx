﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QualityUploadsEditForm.ascx.cs" Inherits="SiteSafetyUploads.QualityUploadsEditForm.QualityUploadsEditForm" %>

<style type="text/css">
    .table {
        padding: 0;
        border-spacing: 0;
        text-align: center;
        font-family: 'Trebuchet MS';
        font-size: 10pt;
        vertical-align: middle;
        border-collapse: collapse;
    }

    .auto-style1 {
        height: 30px;
        font-family: 'Trebuchet MS';
        font-size: 10pt;
    }

    .required {
        color: Red;
    }

    .auto-style2 {
        width: 60%;
        height: 30px;
        font-family: 'Trebuchet MS';
        font-size: 10pt;
    }
</style>

<script>
    function ReloadFun() {
        _spFormOnSubmitCalled = false; _spSuppressFormOnSubmitWrapper = true;
    }
</script>

<table>
    <tr>
        <td>
            <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<table border="1" style="width: 100%" id="tblEditForm" runat="server" visible="true">
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label1" runat="server" ForeColor="White" Text="BU"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <asp:Label ID="lblBU" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label2" runat="server" ForeColor="White" Text="Projects"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <asp:Label ID="lblProject" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label3" runat="server" ForeColor="White" Text="Reporting on"></asp:Label>
        </td>
        <td>
            <table>
                <tr>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <asp:RadioButton ID="rdbGP" Text="Good Practice" GroupName="RO" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <asp:RadioButton ID="rdbAOI" Text="Areas for Improvement" GroupName="RO" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label4" runat="server" ForeColor="White" Text="Evidence For"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <asp:Label ID="lblEvidenceFor" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label5" runat="server" ForeColor="White" Text="Comments"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <asp:TextBox ID="txtComments" TextMode="MultiLine" Height="70px" Width="50%" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label9" runat="server" ForeColor="White" Text="Reporting Date"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <SharePoint:DateTimeControl ID="dtRepDate" DateOnly="true" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label10" runat="server" ForeColor="White" Text="Target Date"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <SharePoint:DateTimeControl ID="dtTargetDate" DateOnly="true" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label8" runat="server" ForeColor="White" Text="Corrective Action Taken"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <asp:TextBox ID="txtCorrectiveAction" TextMode="MultiLine" Height="70px" Width="50%" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtCorrectiveAction" runat="server" ValidationGroup="OnSub" ErrorMessage="Shouldn't be empty"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label11" runat="server" ForeColor="White" Text="Item Status"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <asp:DropDownList ID="ddlItemStatus" runat="server">
                <asp:ListItem>--Select--</asp:ListItem>
                <asp:ListItem>Active</asp:ListItem>
                <asp:ListItem>Closed</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label12" runat="server" ForeColor="White" Text="Ready for closure"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <asp:CheckBox ID="chkClosure" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label6" runat="server" ForeColor="White" Text="Uploaded Attachments"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <asp:GridView ID="GridView1" runat="server" ShowHeader="false" AutoGenerateColumns="false" CellPadding="4" ForeColor="#333333" GridLines="None" Width="606px">
                <Columns>
                    <asp:TemplateField HeaderText="" HeaderStyle-Width="0" HeaderStyle-BorderWidth="0" ShowHeader="false">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CausesValidation="false" CommandName="DownLoad Supporting Doc" OnClientClick="ReloadFun()" ID="lnkView" Text='<%# Eval("DocName") %>' OnClick="lnkView_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
            <asp:Label ID="Label7" runat="server" ForeColor="White" Text="Upload evidence of correction"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
            <asp:FileUpload ID="FileUpload1" AllowMultiple="true" runat="server" />
            &nbsp;<asp:GridView ID="GridView2" runat="server" Visible="false" ShowHeader="false" AutoGenerateColumns="false" CellPadding="4" ForeColor="#333333" GridLines="None" Width="606px">
                <Columns>
                    <asp:TemplateField HeaderText="" HeaderStyle-Width="0" HeaderStyle-BorderWidth="0" ShowHeader="false">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CausesValidation="false" CommandName="DownLoad Evidence Doc" OnClientClick="ReloadFun()" ID="lnkView1" Text='<%# Eval("DocName") %>' OnClick="lnkView1_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblError2" runat="server" ForeColor="Red"></asp:Label>

        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;">
            <asp:Button ID="btnSubmit" runat="server" Font-Bold="true" ValidationGroup="OnSub" Text="Submit" OnClick="btnSubmit_Click" />
            <asp:Button ID="btnCancel" runat="server" Font-Bold="true" Text="Cancel" OnClick="btnCancel_Click" />
        </td>
    </tr>

</table>
<asp:Label ID="lblItemID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblRCMEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblLoggedInUserIsCreated" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblLoggedInUserIsRCM" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblTransactionID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" runat="server"></asp:Label>
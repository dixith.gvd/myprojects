﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
using System.Configuration;

namespace QualityUploadDetails
{
    class UploadDetails
    {
        //public DataTable GetProjectCodesQPR()
        //{
        //    DataTable projectcode = new DataTable();
        //    try
        //    {
        //        SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
        //        con.Open();
        //        string sqlQuery = "select distinct ProjectCode from QChecklist_TnxMain where Compliance_Type='QPR'";
        //        SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
        //        daAdap.Fill(projectcode);
        //        con.Close();

        //    }
        //    catch (Exception exp)
        //    {

        //    }
        //    return projectcode;
        //}
        //public DataTable GetProjectCodesFQP()
        //{
        //    DataTable projectcode = new DataTable();
        //    try
        //    {
        //        SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
        //        con.Open();
        //        string sqlQuery = "select distinct ProjectCode from QChecklist_TnxMain where Compliance_Type='FQP'";
        //        SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
        //        daAdap.Fill(projectcode);
        //        con.Close();

        //    }
        //    catch (Exception exp)
        //    {

        //    }
        //    return projectcode;
        //}
        
        //public string GetProjectNameByCode(string projectCode)
        //{
        //    DataTable dtprojectName = new DataTable();
        //    string pname=string.Empty;
        //    try
        //    {
        //        SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
        //        con.Open();
        //        string sqlQuery = "select distinct projectname from QChecklist_TnxMain where ProjectCode='"+projectCode+"'";
        //        SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
        //        daAdap.Fill(dtprojectName);
        //        con.Close();
        //        //string pname = dtprojectName.ToString();
        //        pname=dtprojectName.Rows[0]["ProjectName"].ToString();
        //    }
        //    catch (Exception exp)
        //    {

        //    }
        //    return pname;
        //}

        SPListItemCollection GetProjectCodes(string siteurl)
        {
            SPListItemCollection items = null;
            using (SPSite site = new SPSite(siteurl))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        SPList lst = null;
                        if (web.Lists["Project PM RCM"] != null)
                        {
                            lst = web.Lists["Project PM RCM"];
                            SPQuery query = new SPQuery();
                            //query.Query = "<Where><IsNotNull><FieldRef Name='Project_x0020_Code' /></IsNotNull></Where>";
                            query.Query = "<Where><And><IsNotNull><FieldRef Name='Project_x0020_Code' /></IsNotNull><IsNotNull><FieldRef Name='Field_x0020_Quality_x0020_Engine' /></IsNotNull></And></Where>";
                            query.ViewFields = "<FieldRef Name='Project_x0020_Code' />";
                            items = lst.GetItems(query);
                        }
                    });
                }
            }
            return items;
        }
        string GetProjectNameByCode(string projectCode, SPWeb web)
        {
            string projectName = string.Empty;

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                SPList lst = null;
                if (web.Lists["TPL Projects List"] != null)
                {
                    lst = web.Lists["TPL Projects List"];
                    SPQuery query = new SPQuery();
                    query.Query = "<Where><Eq><FieldRef Name='Title' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                    SPListItemCollection items = lst.GetItems(query);
                    foreach (SPListItem item in items)
                    {
                        if (item["Final_x0020_Project_x0020_Name"] != null)
                        {
                            SPFieldCalculated calculatedField = lst.Fields.GetFieldByInternalName("Final_x0020_Project_x0020_Name") as SPFieldCalculated;
                            projectName = calculatedField.GetFieldValueAsText(item["Final_x0020_Project_x0020_Name"]);
                        }
                    }
                }
            });

            return projectName;
        }
        public int GetTotalUplodedProjectCount(string ctype, string projectCode)
        {
            int count = 0;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                string sqlQuery = "Select count(*) from [dbo].[QChecklist_TnxMain] where Compliance_Type= '"+ctype+"' and ProjectCode='"+projectCode+"'";
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                count = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            return count;
        }

        public int GetTotalUplodedProjectCountLastweek(string ctype, string projectCode)
        {
            DateTime date = DateTime.Now.AddDays(-7);
            while (date.DayOfWeek != DayOfWeek.Monday)
            {
                date = date.AddDays(-1);
            }

            DateTime dtPrevWeekStartDate = date;
            DateTime dtPrevWeekStartEndDate = date.AddDays(5);

            string startDate = (SPUtility.CreateISO8601DateTimeFromSystemDateTime(dtPrevWeekStartDate));
            string endDate = (SPUtility.CreateISO8601DateTimeFromSystemDateTime(dtPrevWeekStartEndDate));

            int count = 0;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                string sqlQuery = "Select count(*) from [dbo].[QChecklist_TnxMain] where Compliance_Type='"+ctype+"' and FQE_Submitted_Date >= '"+startDate+"' and FQE_Submitted_Date <= '"+endDate+"' and ProjectCode='"+projectCode+"'";
                SqlCommand cmd = new SqlCommand(sqlQuery,con);
                con.Open();
                count = (int)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            return count;

        }
        public void StartProcess()
        {
            Hashtable ht = new Hashtable();
            ht.Add("QPR", "QPR");
            ht.Add("FQP", "FQP");
            DataTable dt = new DataTable();

            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            string fPath = ConfigurationManager.AppSettings["fPath"].ToString();
            string dtFormat = String.Format("{0:dddd_MMMM_d_yyyy}", DateTime.Today);
            List<string> fileNamesFormail = new List<string>();
            foreach (DictionaryEntry de in ht)
            {
                string fileNamePart = string.Concat(de.Value.ToString(), "_", dtFormat, ".txt");
                string fileName = string.Concat(fPath, fileNamePart);
                if (de.Key.ToString() == "QPR")
                {

                    SPListItemCollection projectCodes = GetProjectCodes(siteurl);

                    using (StreamWriter _testData = new StreamWriter(fileName, true))
                    {
                        StringBuilder sbtitle = new StringBuilder();
                        sbtitle.Append("Project Code");
                        sbtitle.Append(",");
                        sbtitle.Append("Project Name");
                        sbtitle.Append(",");
                        sbtitle.Append("Uploaded Total");
                        sbtitle.Append(",");
                        sbtitle.Append("Uploaded Last Week");
                        _testData.WriteLine(sbtitle.ToString());
                        foreach (SPListItem eachProject in projectCodes)
                        {

                            if (eachProject["Project_x0020_Code"] != null)
                            {
                                StringBuilder sb = new StringBuilder();
                                string projectCode = eachProject["Project_x0020_Code"].ToString();
                                if (!string.IsNullOrEmpty(projectCode))
                                {
                                    SPWeb web = eachProject.ParentList.ParentWeb;
                                    sb.Append(projectCode);
                                    sb.Append(",");
                                    string projectName = GetProjectNameByCode(projectCode, web);
                                    sb.Append(projectName);
                                    sb.Append(",");
                                    string countTotal = GetTotalUplodedProjectCount(de.Key.ToString(), projectCode).ToString();
                                    sb.Append(countTotal);
                                    sb.Append(",");
                                    string countLastweek = GetTotalUplodedProjectCountLastweek(de.Key.ToString(), projectCode).ToString();
                                    sb.Append(countLastweek);
                                    //sb.Append(",");
                                    _testData.WriteLine(sb.ToString());

                                }
                            }
                        }
                    }
                }
                else if (de.Key.ToString() == "FQP")
                {
                    SPListItemCollection projectCodes = GetProjectCodes(siteurl);

                    using (StreamWriter _testData = new StreamWriter(fileName, true))
                    {
                        StringBuilder sbtitle = new StringBuilder();
                        sbtitle.Append("Project Code");
                        sbtitle.Append(",");
                        sbtitle.Append("Project Name");
                        sbtitle.Append(",");
                        sbtitle.Append("Uploaded Total");
                        sbtitle.Append(",");
                        sbtitle.Append("Uploaded Last Week");
                        _testData.WriteLine(sbtitle.ToString());
                        foreach (SPListItem eachProject in projectCodes)
                        {

                            if (eachProject["Project_x0020_Code"] != null)
                            {
                                StringBuilder sb = new StringBuilder();
                                string projectCode = eachProject["Project_x0020_Code"].ToString();
                                if (!string.IsNullOrEmpty(projectCode))
                                {
                                    SPWeb web = eachProject.ParentList.ParentWeb;
                                    sb.Append(projectCode);
                                    sb.Append(",");
                                    string projectName = GetProjectNameByCode(projectCode, web);
                                    sb.Append(projectName);
                                    sb.Append(",");
                                    string countTotal = GetTotalUplodedProjectCount(de.Key.ToString(), projectCode).ToString();
                                    sb.Append(countTotal);
                                    sb.Append(",");
                                    string countLastweek = GetTotalUplodedProjectCountLastweek(de.Key.ToString(), projectCode).ToString();
                                    sb.Append(countLastweek);
                                    //sb.Append(",");
                                    _testData.WriteLine(sb.ToString());

                                }
                            }
                        }
                    }
                }

                string csvPath = Path.ChangeExtension(fileName, ".csv");
                File.Move(fileName, csvPath);
                fileNamesFormail.Add(csvPath);   
            }
            //SendMail(concatenatedString.ToString());
            List<string> Attachments = new List<string>();

           foreach (string each in fileNamesFormail)
            {
                if (each.ToUpper().Contains("FQP"))
                {
                    Attachments.Add(each);
                }
                if (each.ToUpper().Contains("QPR"))
                {
                    Attachments.Add(each);
                }
            }

            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
            SmtpClient emailClient = new SmtpClient();
            emailClient.UseDefaultCredentials = false;
            emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
            emailClient.Host = "smtp.office365.com";
            emailClient.Port = 25;
            emailClient.EnableSsl = true;
            emailClient.TargetName = "STARTTLS/smtp.office365.com";
            emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            //Send mails For Quality uplaods with attachments
            string toMailQuality = ConfigurationManager.AppSettings["ToChecklistEmailList"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMailQuality))
            {
                string strTo = toMailQuality.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Quality Checklist Upload Details";
            MyMessage.Body = FormMailBodyQuality();
            MyMessage.IsBodyHtml = true;
            foreach (string each in Attachments)
            {
                if (File.Exists(each))
                {
                    try
                    {
                        Attachment attachFile = new Attachment(each);
                        MyMessage.Attachments.Add(attachFile);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

            emailClient.Send(MyMessage);

            
        }
        string FormMailBodyQuality()
        {

            StringBuilder sbBody = new StringBuilder();
            string openHtml = @"<html>
        <head>
            <style>
                body{
                    font-family:""Trebuchet MS"", Arial, Helvetica, sans-serif;
                    font-size:0.9em;
                    text-align:left;
                }
            table th{border: 1px solid #424242; color: #FFFFFF;text-align: center; padding: 0 10px;background-color: #5675BA;}
            table td{border: 1px solid #D1D1D1;background-color: #F3F3F3; padding: 0 10px;}
            </style>
        </head>
            <body>";

            sbBody.Append(openHtml);
            sbBody.Append("<table>");
            //sbBody.Append("<tr>");
            ////sbBody.Append("<th>");
            ////sbBody.Append("Quality Upload Details");
            ////sbBody.Append("</th>");
            //sbBody.Append("</tr>");
            sbBody.Append("<tr>");
            sbBody.Append("<td>");
            sbBody.Append("Dear All");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Please find attached the statistics for the Quality Checklist Uploads for the last week.");
            //sbBody.Append("<br>");
            //sbBody.Append("<br>");
            //sbBody.Append("To get complete details of the uploads, please click on the following links :");
            //sbBody.Append("<br>");
            //sbBody.Append("<br>");
            //sbBody.Append("<b>Click on the Refresh All button in the Data Tab in excel to get the latest updated date.</b>");
            //sbBody.Append("<br>");
            //sbBody.Append("<br>");
            //sbBody.Append("Good \\ Bad Practices - <a href='https://tplnet.tataprojects.com/Documents/Quality%20Site%20Uploads.xlsx'>https://tplnet.tataprojects.com/Documents/Quality%20Site%20Uploads.xlsx</a>");
            //sbBody.Append("<br>");
            //sbBody.Append("<br>");
            //sbBody.Append("Self-Certification  -  <a href='https://tplnet.tataprojects.com/Documents/Quality%20Self%20Certification%20Results.xlsx'>https://tplnet.tataprojects.com/Documents/Quality%20Self%20Certification%20Results.xlsx</a>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards");
            sbBody.Append("</td>");
            sbBody.Append("</tr>");
            string closeHtml = @"</body></html>";
            sbBody.Append(closeHtml);
            return sbBody.ToString();
        }
        
    }
}

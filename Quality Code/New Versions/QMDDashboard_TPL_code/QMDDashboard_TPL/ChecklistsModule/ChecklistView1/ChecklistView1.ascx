﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChecklistView1.ascx.cs" Inherits="QMDDashboard_TPL.ChecklistsModule.ChecklistView1.ChecklistView1" %>
<style type="text/css">
    .auto-style1 {
        width: 100%;
    }
    .auto-style2 {
        width: 412px;
    }
</style>
<script>
    function ReloadFun() {
        _spFormOnSubmitCalled = false; _spSuppressFormOnSubmitWrapper = true;
    }
</script>
<%--<asp:UpdatePanel ID="panel1" runat="server"><ContentTemplate>--%>
<p>
    <br />
    <table class="auto-style1">
        <tr>
            <td class="auto-style2">
                <asp:Button ID="btnfqe" runat="server" BackColor="#507CD1" ForeColor="White" Text="FQE" Width="82px" OnClick="btnfqe_Click" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnrcm" runat="server" BackColor="#507CD1" ForeColor="White" Text="RCM" Width="82px" OnClick="btnrcm_Click" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnassignee" runat="server" BackColor="#507CD1" ForeColor="White" Text="Assignee" OnClick="btnassignee_Click" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnpe" runat="server" BackColor="#507CD1" ForeColor="White" Text="PE" Width="82px" OnClick="btnpe_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td>
                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="<b>Please refresh the page after downloading excel</b>"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btnexport" runat="server" Text="Excel Export" Font-Overline="False" OnClick="btnexport_Click" OnClientClick="ReloadFun()" />
            </td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="gvchecklist" runat="server" AutoGenerateColumns="False" AllowPaging="true" AllowSorting="true"
                    EnableModelValidation="True" ForeColor="#333333" ShowFooter="false" PageSize="10" CellPadding="3" CellSpacing="3"
                    ShowHeaderWhenEmpty="true" BorderWidth="1px" Font-Overline="False" Height="81px" Width="1111px">
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#CCCCFF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <Columns>
                        <asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" DataNavigateUrlFormatString="https://tplnet.tataprojects.com/Pages/ChecklistDetails.aspx?Tid={0}" DataTextField="Transaction_ID" HeaderText="Transaction_ID" Target="_blank" />
                        <%--<asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" DataNavigateUrlFormatString="http://tplhydhospuat/Pages/ChecklistDetails.aspx?Tid={0}" DataTextField="Transaction_ID" HeaderText="Transaction_ID" Target="_blank" />--%>
                        <asp:BoundField DataField="Stage" HeaderText="Stage" />
                        <asp:BoundField DataField="Compliance_Type" HeaderText="Type" />
                        <asp:BoundField DataField="BUName" HeaderText="BU Name" />
                        <asp:BoundField DataField="ProjectName" HeaderText="Project Name" />
                        <asp:BoundField DataField="ProjectType" HeaderText="Project Type" />
                        <asp:BoundField DataField="Discipline" HeaderText="Discipline" />
                        <asp:BoundField DataField="Activity" HeaderText="Activity" />
                        <asp:BoundField DataField="Sub Activity" HeaderText="Sub Activity" />
                        <asp:BoundField DataField="isCompliant" HeaderText="Is Compliant" />
                    </Columns>
                    <EmptyDataTemplate>No Records Available</EmptyDataTemplate>
                    <HeaderStyle BackColor="#6699FF" BorderWidth="1px" />
                    <PagerSettings Visible="False" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnfirst" runat="server" CommandName="First" OnCommand="ChangePage" Text="First" Width="71px" />
                <asp:Button ID="btnprevious" runat="server" CommandName="Previous" OnCommand="ChangePage" Text="Previous" />
                <asp:Button ID="btnnext" runat="server" CommandName="Next" OnCommand="ChangePage" Text="Next" Width="71px" />
                <asp:Button ID="btnlast" runat="server" CommandName="Last" OnCommand="ChangePage" Text="Last" Width="71px" />
            </td>
        </tr>
    </table>
</p>
<%--</ContentTemplate></asp:UpdatePanel>--%>
<p>
    &nbsp;
</p>
<p style="margin-left: 240px">
    &nbsp;
</p>
<p>
    `&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</p>

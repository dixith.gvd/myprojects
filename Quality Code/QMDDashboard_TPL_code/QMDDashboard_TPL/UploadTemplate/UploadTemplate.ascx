﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadTemplate.ascx.cs" Inherits="QMDDashboard_TPL.UploadTemplate.UploadTemplate" %>
<style type="text/css">
    #Download_page {
        /*width: 60%;*/
         width: 80%;
        margin: 0px auto;
    }

        #Download_page tbody tr td select {
            width: 300px;
            margin-left: 10px;
            margin-bottom: 5px;
            margin-top: 5px;
        }

        #Download_page tbody tr td:first-child {
            text-align: right;
        }

        #Download_page tbody tr td:last-child {
            text-align: left;
        }

        #Download_page tbody tr td input[id$="_btnUpload"] {
            background: #4778d5;
            color: #fff;
            border-color: #4778d5;
            font-weight: bold;
        }
</style>




<asp:UpdatePanel ID="updatePanelDownload" runat="server" EnableViewState="true">
    <ContentTemplate>
        <table class="style1" id="Download_page" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="lblTemplate" runat="server" Text="Template:"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTemplate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTemplate_SelectedIndexChanged" AppendDataBoundItems='true'>
                    </asp:DropDownList>

                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblProjectName" runat="server" Text="Project Name:" Style="display: none;"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlProjectName" runat="server" Style="display: none;" OnSelectedIndexChanged="ddlProjectName_SelectedIndexChanged" AutoPostBack="True" >
                    </asp:DropDownList>

                </td>
              
            </tr>
           
            <tr>
                <td>
                    <asp:Label ID="lblPeriod" runat="server" Text="Period:" Style="display: none;"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlPeriod" runat="server" Style="display: none;" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>

                </td>
            </tr>
            
             <tr>
                <td>
                    <asp:Label ID="lblTemplateUpload" runat="server" Text="Upload Template:" Style="display: none;"></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="fuTemplateUpload" runat="server" Style="display: none;" CausesValidation="false" />
                </td>
            </tr>

            <tr>
                <asp:UpdatePanel ID="udpbtnDownload" runat="server">
                    <ContentTemplate>
                        <td></td>
                        <td>
                            <asp:Button ID="btnUpload" runat="server" Text="Upload" Style="display: none;" CausesValidation="false"
                                OnClick="btnUpload_Click"  />
                        </td>
                        <td></td>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUpload" />
                    </Triggers>
                </asp:UpdatePanel>

            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="lblError" runat="server" Style="display: none;"></asp:Label></td>
                <td>

                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="lblSuccess" runat="server" Style="display: none;"></asp:Label></td>
                <td>

                </td>
            </tr>
        </table>

<script type="text/javascript">
    function waitMessage() {
        try {
            if (Page_IsValid) {
                SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showWaitScreenWithNoClose', "Please wait .....");
            }
            else {
                return Page_IsValid;
            }
        }
        catch (err) {
            //alert(err);
            //SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog', options);
        }
        return true;
    }

    function closeWaitMessage() {
        try {
            SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose', 1);

        } catch (e) {
        }
    }
</script>

    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload" />
    </Triggers>
</asp:UpdatePanel>


<asp:UpdateProgress AssociatedUpdatePanelID="udpbtnDownload" ID="upProgressBar" runat="server">
    <ProgressTemplate>Upload is in Progress</ProgressTemplate>
</asp:UpdateProgress>
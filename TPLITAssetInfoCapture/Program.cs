﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;
using System.Security;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;
namespace TPLITAssetInfoCapture
{
    class Program
    {
        public static string loggingtxt = string.Empty;
        public static string loggingtxt2 = string.Empty;
        public static string logtype = "Logging";
        public static bool flag = true;
        public static DateTime LoggedDate = DateTime.Now;
        static void Main(string[] args)
        {
            
            
            string hostname = string.Empty; string serialnumber = string.Empty; string osname = string.Empty; string osversion = string.Empty;
            string osmanufacturer = string.Empty; string osconfig = string.Empty; string osbuildtype = string.Empty; string regowner = string.Empty;
            string regorg = string.Empty; string prodid = string.Empty; string sysman = string.Empty; string sysmodel = string.Empty;
            string systype = string.Empty; string processor = string.Empty; string codeversion = string.Empty; string loggeddate = string.Empty;
            string whoami = string.Empty; string ipconfig = string.Empty; string ipconfig2 = string.Empty; double dsize = 0; string disksize = string.Empty; string cpuname = string.Empty; string biosdate = string.Empty; string biosreldate = string.Empty; string biosversion = string.Empty; string emailid = string.Empty; string totpmem = string.Empty; string avlpmem = string.Empty; string fqdn = string.Empty;
            //string location = string.Empty;
            string result = string.Empty; string result2 = string.Empty; string result3 = string.Empty; string result4 = string.Empty;
            string result5 = string.Empty; string result6 = string.Empty; string result7 = string.Empty; string result8 = string.Empty;
            string result9 = string.Empty; string result10 = string.Empty; string result11 = string.Empty; //string result12 = string.Empty;
            DateTime? orginstalldate = null;
            DateTime? sysboottime = null;
            string filepath = @"C:\AssetInfo\ErrorLog.txt";
            
            try
            {
                if (System.IO.File.Exists(filepath))
                {
                    System.IO.File.Delete(filepath);
                }
                try
                {

                    Console.WriteLine("Please dont close this window..gathering critical data");
                    Console.WriteLine("............");
                    Console.WriteLine("............");
                    Console.WriteLine("............");
                    Console.WriteLine("............");
                    Console.WriteLine("..will take just a few minutes");
                    Console.SetWindowSize(100, 10);

                }
                catch (Exception)
                {

                    //throw;
                }
                string command = "systeminfo";
                string command1 = "whoami";
                string command2 = "wmic bios get serialnumber";
                string command3 = "wmic product get name, version, vendor, installdate";
                string command4 = "ipconfig | findstr IPv4";
                string command5 = "wmic diskdrive get size";
                string command6 = "wmic cpu get name";
                string command7 = "wmic bios get releasedate";
                string command8 = "wmic bios get biosversion";
                string command9 = "whoami /upn";
                string command10 = "whoami /fqdn";
                //string command11 = "curl ipinfo.io";
                ReminderBackupLog("Started executing the commands:");

                //string command9 = "curl ipinfo.io";
                //string command10 = "wmic cpu get systemname";
                System.Diagnostics.ProcessStartInfo procStartInfo =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo.CreateNoWindow = true;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();

                // Get the output into a string
                result = proc.StandardOutput.ReadToEnd();
                ReminderBackupLog("Result1 :" + result);

                System.Diagnostics.ProcessStartInfo procStartInfo2 =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command1);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo2.RedirectStandardOutput = true;
                procStartInfo2.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo2.CreateNoWindow = true;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc2 = new System.Diagnostics.Process();
                proc2.StartInfo = procStartInfo2;
                proc2.Start();

                // Get the output into a string
                result2 = proc2.StandardOutput.ReadToEnd();
                ReminderBackupLog("Result2 :" + result2);
                System.Diagnostics.ProcessStartInfo procStartInfo3 =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command2);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo3.RedirectStandardOutput = true;
                procStartInfo3.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo3.CreateNoWindow = true;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc3 = new System.Diagnostics.Process();
                proc3.StartInfo = procStartInfo3;
                proc3.Start();

                // Get the output into a string
                result3 = proc3.StandardOutput.ReadToEnd();
                ReminderBackupLog("Result3 :" + result3);
                System.Diagnostics.ProcessStartInfo procStartInfo4 =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command3);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo4.RedirectStandardOutput = true;
                procStartInfo4.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo4.CreateNoWindow = true;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc4 = new System.Diagnostics.Process();
                proc4.StartInfo = procStartInfo4;
                proc4.Start();

                // Get the output into a string
                result4 = proc4.StandardOutput.ReadToEnd();
                ReminderBackupLog("Result4 :" + result4);
                //To run the command4 = "ipconfig | findstr IPv4";
                System.Diagnostics.ProcessStartInfo procStartInfo5 =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command4);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo5.RedirectStandardOutput = true;
                procStartInfo5.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo5.CreateNoWindow = true;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc5 = new System.Diagnostics.Process();
                proc5.StartInfo = procStartInfo5;
                proc5.Start();

                // Get the output into a string
                result5 = proc5.StandardOutput.ReadToEnd();
                //END command4 = "ipconfig | findstr IPv4";
                ReminderBackupLog("Result5 :" + result5);
                //To run the command5 = "wmic diskdrive get size";
                System.Diagnostics.ProcessStartInfo procStartInfo6 =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command5);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo6.RedirectStandardOutput = true;
                procStartInfo6.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo6.CreateNoWindow = true;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc6 = new System.Diagnostics.Process();
                proc6.StartInfo = procStartInfo6;
                proc6.Start();

                // Get the output into a string
                result6 = proc6.StandardOutput.ReadToEnd();
                //END command5 = "wmic diskdrive get size";
                ReminderBackupLog("Result6 :" + result6);
                //To run the command6 = "wmic cpu get name";
                System.Diagnostics.ProcessStartInfo procStartInfo7 =
                   new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command6);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo7.RedirectStandardOutput = true;
                procStartInfo7.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo7.CreateNoWindow = true;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc7 = new System.Diagnostics.Process();
                proc7.StartInfo = procStartInfo7;
                proc7.Start();

                // Get the output into a string
                result7 = proc7.StandardOutput.ReadToEnd();
                //END command6 = "wmic cpu get name";
                ReminderBackupLog("Result7 :" + result7);
                //To run the command7 = "wmic bios get releasedate";
                System.Diagnostics.ProcessStartInfo procStartInfo8 =
                   new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command7);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo8.RedirectStandardOutput = true;
                procStartInfo8.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo8.CreateNoWindow = true;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc8 = new System.Diagnostics.Process();
                proc8.StartInfo = procStartInfo8;
                proc8.Start();

                // Get the output into a string
                result8 = proc8.StandardOutput.ReadToEnd();
                //END command7 = "wmic bios get releasedate";
                ReminderBackupLog("Result8 :" + result8);
                //To run the command8 = "wmic bios get biosversion";
                System.Diagnostics.ProcessStartInfo procStartInfo9 =
                   new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command8);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo9.RedirectStandardOutput = true;
                procStartInfo9.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo9.CreateNoWindow = true;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc9 = new System.Diagnostics.Process();
                proc9.StartInfo = procStartInfo9;
                proc9.Start();

                // Get the output into a string
                result9 = proc9.StandardOutput.ReadToEnd();
                //END command8 = "wmic bios get biosversion";
                ReminderBackupLog("Result9 :" + result9);
                //To run the command9 = "curl ipinfo.io";
                System.Diagnostics.ProcessStartInfo procStartInfo10 =
                   new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command9);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo10.RedirectStandardOutput = true;
                procStartInfo10.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo10.CreateNoWindow = true;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc10 = new System.Diagnostics.Process();
                proc10.StartInfo = procStartInfo10;
                proc10.Start();

                // Get the output into a string
                result10 = proc10.StandardOutput.ReadToEnd();
                //END command9 = "curl ipinfo.io";
                ReminderBackupLog("Result10 :" + result10);
                //To run the command10 = "whoami /fdn";
                System.Diagnostics.ProcessStartInfo procStartInfo11 =
                   new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command10);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo11.RedirectStandardOutput = true;
                procStartInfo11.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo11.CreateNoWindow = true;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc11 = new System.Diagnostics.Process();
                proc11.StartInfo = procStartInfo11;
                proc11.Start();

                // Get the output into a string
                result11 = proc11.StandardOutput.ReadToEnd();
                //END command10 = "whoami /fqdp";
                ReminderBackupLog("Result11 :" + result11);
                //To run the command10 = "whoami /fdn";
                //System.Diagnostics.ProcessStartInfo procStartInfo12 =
                //   new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command11);

                //// The following commands are needed to redirect the standard output.
                //// This means that it will be redirected to the Process.StandardOutput StreamReader.
                //procStartInfo12.RedirectStandardOutput = true;
                //procStartInfo12.UseShellExecute = false;
                //// Do not create the black window.
                //procStartInfo12.CreateNoWindow = true;

                //// Now we create a process, assign its ProcessStartInfo and start it
                //System.Diagnostics.Process proc12 = new System.Diagnostics.Process();
                //proc12.StartInfo = procStartInfo12;
                //proc12.Start();

                //// Get the output into a string
                //result12 = proc12.StandardOutput.ReadToEnd();
                ////END command10 = "whoami /fqdp";
                //ReminderBackupLog("Result12 :" + result12);

                // Display the command output.
                //System.IO.Path.Combine(tempdir, result);
                //System.IO.File.WriteAllText(tempdir, result);
                //Console.WriteLine(result);
                //Console.Read();
                
                //string ldate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", LoggedDate);
                loggeddate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", LoggedDate);
                string[] stringSeparators = new string[] { "\r\n" };


                try
                {
                    if (result != null && result != "" && result != string.Empty && result != " ")
                    {
                        string final = result.Trim();
                        if (final.Split(stringSeparators, StringSplitOptions.None).Length > 0)
                        {

                            ReminderBackupLog("Started inserting into hardware list:");
                            hostname = final.Split(stringSeparators, StringSplitOptions.None)[0].Split(':')[1].Trim();
                            ReminderBackupLog("getting Host Name :" + final.Split(stringSeparators, StringSplitOptions.None)[0].Split(':')[1].Trim());
                            osname = final.Split(stringSeparators, StringSplitOptions.None)[1].Split(':')[1].Trim();
                            ReminderBackupLog("getting OS Name :" + final.Split(stringSeparators, StringSplitOptions.None)[1].Split(':')[1].Trim());
                            osversion = final.Split(stringSeparators, StringSplitOptions.None)[2].Split(':')[1].Trim();
                            ReminderBackupLog("getting OS Version :" + final.Split(stringSeparators, StringSplitOptions.None)[2].Split(':')[1].Trim());
                            osmanufacturer = final.Split(stringSeparators, StringSplitOptions.None)[3].Split(':')[1].Trim();
                            ReminderBackupLog("getting OS Manufacturer :" + final.Split(stringSeparators, StringSplitOptions.None)[3].Split(':')[1].Trim());
                            osconfig = final.Split(stringSeparators, StringSplitOptions.None)[4].Split(':')[1].Trim();
                            ReminderBackupLog("getting OS Configuration :" + final.Split(stringSeparators, StringSplitOptions.None)[4].Split(':')[1].Trim());
                            osbuildtype = final.Split(stringSeparators, StringSplitOptions.None)[5].Split(':')[1].Trim();
                            ReminderBackupLog("getting OS Build Type :" + final.Split(stringSeparators, StringSplitOptions.None)[5].Split(':')[1].Trim());
                            regowner = final.Split(stringSeparators, StringSplitOptions.None)[6].Split(':')[1].Trim();
                            ReminderBackupLog("getting Registered Owner :" + final.Split(stringSeparators, StringSplitOptions.None)[6].Split(':')[1].Trim());
                            regorg = final.Split(stringSeparators, StringSplitOptions.None)[7].Split(':')[1].Trim();
                            ReminderBackupLog("getting Registered Organization :" + final.Split(stringSeparators, StringSplitOptions.None)[7].Split(':')[1].Trim());
                            prodid = final.Split(stringSeparators, StringSplitOptions.None)[8].Split(':')[1].Trim();
                            ReminderBackupLog("getting Product ID :" + final.Split(stringSeparators, StringSplitOptions.None)[8].Split(':')[1].Trim());
                            orginstalldate = Convert.ToDateTime(final.Split(stringSeparators, StringSplitOptions.None)[9].Split(':')[1].Trim().Split(',')[0]);
                            ReminderBackupLog("getting Original Install Date :" + final.Split(stringSeparators, StringSplitOptions.None)[9].Split(':')[1].Trim().Split(',')[0]);
                            sysboottime = Convert.ToDateTime(final.Split(stringSeparators, StringSplitOptions.None)[10].Split(':')[1].Trim().Split(',')[0]);
                            ReminderBackupLog("getting System Boot Time :" + final.Split(stringSeparators, StringSplitOptions.None)[10].Split(':')[1].Trim().Split(',')[0]);
                            sysman = final.Split(stringSeparators, StringSplitOptions.None)[11].Split(':')[1].Trim();
                            ReminderBackupLog("getting System Manufacturer :" + final.Split(stringSeparators, StringSplitOptions.None)[12].Split(':')[1].Trim());
                            sysmodel = final.Split(stringSeparators, StringSplitOptions.None)[12].Split(':')[1].Trim();
                            ReminderBackupLog("getting System Model :" + final.Split(stringSeparators, StringSplitOptions.None)[12].Split(':')[1].Trim());
                            systype = final.Split(stringSeparators, StringSplitOptions.None)[13].Split(':')[1].Trim();
                            ReminderBackupLog("getting System Type :" + final.Split(stringSeparators, StringSplitOptions.None)[13].Split(':')[1].Trim());
                            processor = final.Split(stringSeparators, StringSplitOptions.None)[14].Split(':')[1].Trim() + System.Environment.NewLine + final.Split(stringSeparators, StringSplitOptions.None)[15].Trim();
                            ReminderBackupLog("getting Processor :" + final.Split(stringSeparators, StringSplitOptions.None)[14].Split(':')[1].Trim() + System.Environment.NewLine + final.Split(stringSeparators, StringSplitOptions.None)[15].Trim());
                            string data = getBetween(hostname, serialnumber, final, "Total Physical ", "Virtual");

                            if (data != string.Empty)
                            {
                                totpmem = data.Split(stringSeparators, StringSplitOptions.None)[0].Split(':')[1].Trim();
                                ReminderBackupLog("getting Total Physical Memory :" + data.Split(stringSeparators, StringSplitOptions.None)[0].Split(':')[1].Trim());
                                avlpmem = data.Split(stringSeparators, StringSplitOptions.None)[1].Split(':')[1].Trim();
                                ReminderBackupLog("getting Available Physical Memory :" + data.Split(stringSeparators, StringSplitOptions.None)[1].Split(':')[1].Trim());
                            }
                        }
                    }

                    if (result2 != null && result2 != "" && result2 != string.Empty && result2 != " ")
                    {
                        whoami = result2.Trim();
                        ReminderBackupLog("getting Whoami :" + result2.Trim());
                    }

                    if (result3 != null && result3 != "" && result3 != string.Empty && result3 != " ")
                    {
                        if (result3.Split(stringSeparators, StringSplitOptions.None).Length > 0)
                        {
                            serialnumber = result3.Split(stringSeparators, StringSplitOptions.None)[1].Trim();
                        }
                        ReminderBackupLog("getting Serial Number :" + result3.Split(stringSeparators, StringSplitOptions.None)[1]);
                    }

                    if (result5 != null && result5 != "" && result5 != string.Empty && result5 != " ")
                    {
                        string[] ip = result5.Trim().Split(':');
                        if (ip.Length > 2)
                        {
                            ipconfig = result5.Trim().Split(stringSeparators, StringSplitOptions.None)[0].Split(':')[1].Trim();
                            ReminderBackupLog("getting ipconfig1 :" + result5.Trim().Split(stringSeparators, StringSplitOptions.None)[0].Split(':')[1].Trim());
                            ipconfig2 = result5.Trim().Split(stringSeparators, StringSplitOptions.None)[1].Split(':')[1].Trim();
                            ReminderBackupLog("getting ipconfig2 :" + result5.Trim().Split(stringSeparators, StringSplitOptions.None)[1].Split(':')[1].Trim());
                        }
                        else
                        {
                            ipconfig = ip[1].Trim();
                            ReminderBackupLog("getting ipconfig :" + ip[1].Trim());
                        }
                    }

                    if (result6 != null && result6 != "" && result6 != string.Empty && result6 != " ")
                    {
                        if (result6.Split(stringSeparators, StringSplitOptions.None).Length > 0)
                        {
                            if (result6.Split(stringSeparators, StringSplitOptions.None)[1].Trim() != "")
                            {
                                dsize = Convert.ToDouble(result6.Split(stringSeparators, StringSplitOptions.None)[1].Trim()) / (1024 * 1024 * 1024);
                                ReminderBackupLog("getting size by formulae :" + Convert.ToDouble(result6.Split(stringSeparators, StringSplitOptions.None)[1]) / (1024 * 1024 * 1024));
                                disksize = Convert.ToString(dsize);
                                ReminderBackupLog("getting disk drive size :" + Convert.ToString(dsize));
                            }
                            else if (result6.Split(stringSeparators, StringSplitOptions.None)[2].Trim() != "")
                            {
                                dsize = Convert.ToDouble(result6.Split(stringSeparators, StringSplitOptions.None)[2].Trim()) / (1024 * 1024 * 1024);
                                ReminderBackupLog("getting size by formulae :" + Convert.ToDouble(result6.Split(stringSeparators, StringSplitOptions.None)[2]) / (1024 * 1024 * 1024));
                                disksize = Convert.ToString(dsize);
                                ReminderBackupLog("getting disk drive size :" + Convert.ToString(dsize));
                            }
                        }
                    }

                    if (result7 != null && result7 != "" && result7 != string.Empty && result7 != " ")
                    {
                        cpuname = result7.Split(stringSeparators, StringSplitOptions.None)[1].Trim();
                        ReminderBackupLog("getting CPU Name :" + result7.Split(stringSeparators, StringSplitOptions.None)[1]);
                    }

                    if (result8 != null && result8 != "" && result8 != string.Empty && result8 != " ")
                    {
                        biosdate = result8.Split(stringSeparators, StringSplitOptions.None)[1].Trim().Split('.')[0].Substring(0, 8);
                        ReminderBackupLog("getting bios date :" + result8.Split(stringSeparators, StringSplitOptions.None)[1].Split('.')[0].Substring(0, 8));
                        biosreldate = biosdate.Substring(6, 2) + "-" + biosdate.Substring(4, 2) + "-" + biosdate.Substring(0, 4);
                        ReminderBackupLog("getting Bios Release Date in dd-mm-yyyy :" + biosdate.Substring(6, 2) + "-" + biosdate.Substring(4, 2) + "-" + biosdate.Substring(0, 4));
                    }

                    if (result9 != null && result9 != "" && result9 != string.Empty && result9 != " ")
                    {
                        biosversion = result9.Split(stringSeparators, StringSplitOptions.None)[1].Trim();
                        ReminderBackupLog("getting Bios Version :" + result9.Split(stringSeparators, StringSplitOptions.None)[1]);
                    }

                    if (result10 != null && result10 != "" && result10 != string.Empty && result10 != " ")
                    {
                        emailid = result10.Trim();
                        ReminderBackupLog("getting Email ID :" + result10.Trim());
                    }


                    if (result11 != null && result11 != "" && result11 != string.Empty && result11 != " ")
                    {
                        fqdn = result11.Trim();
                        ReminderBackupLog("getting FQDN Details :" + result11.Trim());
                    }

                    //if (result12 != null && result12 != "" && result12 != string.Empty && result12 != " ")
                    //{
                    //    if (result12.Trim() != "" && result12.Trim() != string.Empty)
                    //    {
                    //        if (result12.Trim().Split(stringSeparators, StringSplitOptions.None).Length > 0)
                    //        {
                    //            location = result12.Trim().Split(stringSeparators, StringSplitOptions.None)[0].Split(':')[2].Trim().Split(',')[0].Replace("\"", "");
                    //            ReminderBackupLog("getting Location :" + result12.Trim().Split(stringSeparators, StringSplitOptions.None)[0].Split(':')[2].Trim().Split(',')[0].Replace("\"", ""));
                    //        }
                    //    }
                    //}
                    codeversion = "RegularInstallerV6_19FEB";
                    //item["BiosVersionwmic"] = result9.Trim();
                    //item["curlipinfo"] = result10.Split(stringSeparators, StringSplitOptions.None)[1];
                    //item["CPUSystemName"] = result11.Trim();
                    
                }
                catch (Exception ex)
                {
                    flag = false;
                    logtype = "Exception";
                    ReminderBackupLog("while retrieving hardware data :" + ex.Message.ToString());
                    UpdateExceptioninList(hostname, serialnumber, "while retrieving hardware data :" + ex.Message.ToString());
                }
                SoftwareInfo.SoftwareDetails sftdetails = new SoftwareInfo.SoftwareDetails();

                sftdetails.softwaredt = new List<SoftwareInfo.AssetDataSoftware>();
                try
                {
                    if (result4 != null && result4 != "" && result4 != string.Empty && result4 != " ")
                    {
                        if (result4.Trim().Split(stringSeparators, StringSplitOptions.None).Length > 0)
                        {

                            string[] alldata = result4.Trim().Split(stringSeparators, StringSplitOptions.None);
                            int first = result4.Trim().Split(stringSeparators, StringSplitOptions.None)[0].IndexOf("Name");
                            ReminderBackupLog("Getting first position :" + result4.Trim().Split(stringSeparators, StringSplitOptions.None)[0].IndexOf("Name"));
                            int second = result4.Trim().Split(stringSeparators, StringSplitOptions.None)[0].IndexOf("Vendor");
                            ReminderBackupLog("Getting second position :" + result4.Trim().Split(stringSeparators, StringSplitOptions.None)[0].IndexOf("Vendor"));
                            int third = result4.Trim().Split(stringSeparators, StringSplitOptions.None)[0].IndexOf("Version");
                            ReminderBackupLog("Getting third position :" + result4.Trim().Split(stringSeparators, StringSplitOptions.None)[0].IndexOf("Version"));


                            //alldata = Regex.Replace(alldata, @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline);

                            foreach (string data2 in alldata)
                            {
                                string insdate = string.Empty; string name = string.Empty; string vendor = string.Empty; string version = string.Empty;
                                SoftwareInfo.AssetDataSoftware sftind = new SoftwareInfo.AssetDataSoftware();


                                int length = data2.Length;
                                if (data2.Trim().Length != 0)
                                {
                                    if (data2.Substring(first, second - first).Trim() != "Name")
                                    {
                                        ReminderBackupLog("Retrieving Software Details");
                                        insdate = data2.Substring(0, first).Trim();
                                        insdate = insdate.Substring(0, 4) + "-" + insdate.Substring(4, 2) + "-" + insdate.Substring(6, 2);
                                        //Console.WriteLine("insdate" + insdate);
                                        sftind.InstallDate = insdate;
                                        ReminderBackupLog("getting Install Date :" + insdate.Substring(0, 4) + "-" + insdate.Substring(4, 2) + "-" + insdate.Substring(6, 2));
                                        name = data2.Substring(first, second - first).Trim();
                                        sftind.Name = name;
                                        ReminderBackupLog("getting Name :" + data2.Substring(first, second - first).Trim());
                                        //Console.WriteLine("insdate" + data.Substring(first, second - first).Trim());
                                        vendor = data2.Substring(second, third - second).Trim();
                                        sftind.Vendor = vendor;
                                        ReminderBackupLog("getting Vendor :" + data2.Substring(second, third - second).Trim());
                                        //Console.WriteLine("vendor" + data.Substring(second, third - second).Trim());
                                        version = data2.Substring(third, length - third).Trim();
                                        sftind.SoftwareVersion = version;
                                        ReminderBackupLog("getting Software Version :" + data2.Substring(third, length - third).Trim());
                                        //Console.WriteLine("version" + data.Substring(third, length - third));
                                        //Console.Read();
                                        sftind.CodeVersion = codeversion;
                                        sftind.HostName = hostname;
                                        sftind.SerialNumber = serialnumber;
                                        sftind.EmailID = emailid;
                                        sftind.CreatedDate = loggeddate.ToString();

                                        sftdetails.softwaredt.Add(sftind);
                                    }
                                }

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    flag = false;
                    //logtype = "Exception";
                    ReminderBackupLog("while retrieving software data :" + ex.Message.ToString());
                    UpdateExceptioninList(hostname, serialnumber, "while retrieving software data :" + ex.Message.ToString());
                    //throw;
                }
                string url = string.Format("https://po.tataprojects.com/api/AssetInventory/InsertAssetDetails");
                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/json";


                string orginstalldate2 = String.Format("{0:yyyy-MM-dd}", orginstalldate);
                string sysbootTime = String.Format("{0:yyyy-MM-dd}", sysboottime);
                string postdata = "{\"AvailablePhysicalMemory\":\"" + avlpmem + "\",\"BiosReleaseDate\":\"" + biosreldate + "\",\"BIOSVersion\":\"" + biosversion.Replace(@"""", "#") + "\",\"CPUName\":\"" + cpuname + "\",\"CreatedDate\":\"" + loggeddate + "\",\"diskdrivesize\":\"" + disksize + "\",\"EmailID\":\"" + emailid + "\",\"FQDNDetails\":\"" + fqdn + "\",\"HostName\":\"" + hostname + "\",\"ipconfig\":\"" + ipconfig + "\",\"OriginalInstallDate\":\"" + orginstalldate2 + "\",\"OSBuildType\":\"" + osbuildtype + "\",\"OSConfiguration\":\"" + osconfig + "\",\"OSManufacturer\":\"" + osmanufacturer + "\",\"OSName\":\"" + osname + "\",\"OSVersion\":\"" + osversion + "\",\"Processor\":\"" + processor + "\",\"ProductID\":\"" + prodid + "\",\"RegisteredOrganization\":\"" + regorg + "\",\"RegisteredOwner\":\"" + regowner + "\",\"SerialNumber\":\"" + serialnumber + "\",\"SystemBootTime\":\"" + sysbootTime + "\",\"SystemManufacturer\":\"" + sysman + "\",\"SystemModel\":\"" + sysmodel + "\",\"SystemType\":\"" + systype + "\",\"TotalPhysicalMemory\":\"" + totpmem + "\",\"Whoami\":\"" + whoami.Replace(@"\", "#") + "\",\"ipconfig2\":\"" + ipconfig2 + "\",\"CodeVersion\":\"" + codeversion + "\",\"LoggedDate\":\"" + loggeddate + "\"}";
                //string postdata = "{\"AvailablePhysicalMemory\":\"" + avlpmem + "\",\"BiosReleaseDate\":\"" + biosreldate + "\",\"CPUName\":\"" + cpuname + "\",\"CreatedDate\":\"" + loggeddate + "\",\"diskdrivesize\":\"" + disksize + "\",\"EmailID\":\"" + emailid + "\",\"FQDNDetails\":\"" + fqdn + "\",\"HostName\":\"" + hostname + "\",\"ipconfig\":\"" + ipconfig + "\",\"Location\":\"" + location + "\",\"OriginalInstallDate\":\"" + orginstalldate + "\",\"OSBuildType\":\"" + osbuildtype + "\",\"OSConfiguration\":\"" + osconfig + "\",\"OSManufacturer\":\"" + osmanufacturer + "\",\"OSName\":\"" + osname + "\",\"OSVersion\":\"" + osversion + "\",\"Processor\":\"" + processor + "\",\"ProductID\":\"" + prodid + "\",\"RegisteredOrganization\":\"" + regorg + "\",\"RegisteredOwner\":\"" + regowner + "\",\"SerialNumber\":\"" + serialnumber + "\",\"SystemBootTime\":\"" + sysboottime + "\",\"SystemManufacturer\":\"" + sysman + "\",\"SystemModel\":\"" + sysmodel + "\",\"SystemType\":\"" + systype + "\",\"TotalPhysicalMemory\":\"" + totpmem + "\",\"Whoami\":\"" + whoami.Replace(@"\", "") +"\",\"ipconfig2\":\"" + ipconfig2 + "\",\"CodeVersion\":\"" + codeversion + "\",\"LoggedDate\":\"" + loggeddate + "\"}";
                try
                {
                    using (var streamwriter = new StreamWriter(request.GetRequestStream()))
                    {
                        streamwriter.Write(postdata);
                        streamwriter.Flush();
                        streamwriter.Close();

                        var httpresponse = (HttpWebResponse)request.GetResponse();

                        using (var streamreader = new StreamReader(httpresponse.GetResponseStream()))
                        {
                            var finresult = streamreader.ReadToEnd();
                            ReminderBackupLog("sql response hardware data insertion :" + finresult.ToString());
                            Console.WriteLine(finresult);
                        }
                    }
                }
                catch (WebException ex)
                {
                    logtype = "Exception";
                    string message = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                    ReminderBackupLog("while posting to hardware webservice:" + message);
                    UpdateExceptioninList(hostname, serialnumber, "while posting to hardware webservice:" + message);
                    //throw;
                }

                string urlsft = string.Format("https://po.tataprojects.com/api/AssetInventory/InsertAssetDetailsSoftware2");
                WebRequest requestsft = WebRequest.Create(urlsft);
                requestsft.Method = "POST";
                requestsft.ContentType = "application/json";
                //request.ContentType = "application/x-www-form-urlencoded";
                //List<AssetDataSoftware> ResponseWithQuestionList = new List<AssetDataSoftware>();
                var json = JsonConvert.SerializeObject(sftdetails);
                try
                {
                    using (var streamwritersft = new StreamWriter(requestsft.GetRequestStream()))
                    {

                        streamwritersft.Write(json);
                        streamwritersft.Flush();
                        streamwritersft.Close();

                        var httpresponse = (HttpWebResponse)requestsft.GetResponse();
                        using (var streamreadersft = new StreamReader(httpresponse.GetResponseStream()))
                        {
                            var finresult = streamreadersft.ReadToEnd();
                            ReminderBackupLog("sql response software data insertion :" + finresult.ToString());
                            Console.WriteLine(finresult);
                        }

                    }

                }
                catch (WebException ex)
                {
                    logtype = "Exception";
                    string message = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                    UpdateExceptioninList(hostname, serialnumber, "while posting to software webservice:" + message);
                    ReminderBackupLog("while posting to software webservice:" + message);
                    //throw;
                }
            }
            catch (Exception ex)
            {
                // Log the exception
                flag = false;
                logtype = "Exception";
                ReminderBackupLog("while executing commands :" + ex.Message.ToString());
                UpdateExceptioninList(hostname, serialnumber, ex.Message.ToString());
            }
            finally
            {
                ExceptionLogging(hostname, serialnumber, loggingtxt, logtype);
            }
            //if (flag == true)
            //{

            //    if (System.IO.File.Exists(filepath))
            //    {
            //        System.IO.File.Delete(filepath);
            //    }
            //}
            
        }
        public static string getBetween(string hostname, string serialnumber, string strSource, string strStart, string strEnd)
        {
            string data = string.Empty;
            try
            {
                if (strSource.Contains(strStart) && strSource.Contains(strEnd))
                {
                    int Start, End;
                    Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                    End = strSource.IndexOf(strEnd, Start);
                    data = strSource.Substring(Start, End - Start);
                }
            }
            catch (Exception ex)
            {
                flag = false;
                logtype = "Exception";
                ReminderBackupLog("while getting physical & available memory :" + ex.Message.ToString());
                UpdateExceptioninList(hostname, serialnumber, ex.Message.ToString());
            }
            return data;
        }

        public static void ReminderBackupLog(string message)
        {

            try
            {
                //if (loggingtxt.Length < 40000)
                //{
                    loggingtxt = loggingtxt + message + System.Environment.NewLine;
                //}
                //else
                //{
                //    loggingtxt2 = loggingtxt2 + message + System.Environment.NewLine;
                //}

            }
            catch (Exception exp)
            {
                ReminderBackupLog(exp.ToString());
            }
        }
        private static void UpdateExceptioninList(string hostname, string serialnumber, string expmessage)
        {
            CreateFile(loggingtxt);
            string siteurl = "https://tataprojects4.sharepoint.com/sites/TPLSampleMig/";
            string listName = "ExceptionLogging";
            var targetSite = new Uri(siteurl);
            var login = "defspuser@tataprojects4.onmicrosoft.com";
            var password = "D3faultTplU5er";
            var securePassword = new SecureString();
            foreach (char c in password)
            {
                securePassword.AppendChar(c);
            }
            var onlineCredentials = new SharePointOnlineCredentials(login, securePassword);
            using (ClientContext clientContext = new ClientContext(targetSite))
            {
                try
                {
                    clientContext.Credentials = onlineCredentials;
                    clientContext.ExecutingWebRequest += delegate(object sender, WebRequestEventArgs e)
                    {
                        e.WebRequestExecutor.WebRequest.UserAgent = "NONISV|Contoso|GovernanceCheck/1.0";
                    };

                    Web web = clientContext.Web;
                    List oList = clientContext.Web.Lists.GetByTitle(listName);
                    ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                    ListItem item = oList.AddItem(itemCreateInfo);
                    item["Title"] = hostname;
                    item["SerialNumber"] = serialnumber;
                    item["ErrorText"] = expmessage;
                    item["CodeVersion"] = "RegularInstallerV6_19FEB";
                    item.Update();
                    clientContext.ExecuteQuery();

                    AttachmentCreationInformation attachment = new AttachmentCreationInformation();
                    string filepath = @"C:\AssetInfo\ErrorLog.txt";
                    attachment.ContentStream = new MemoryStream(System.IO.File.ReadAllBytes(filepath));

                    attachment.FileName = "ErrorLog.txt";

                    item.AttachmentFiles.Add(attachment);

                    //Commiting attachment changes.
                    clientContext.ExecuteQuery();
                    if (System.IO.File.Exists(filepath))
                    {
                        System.IO.File.Delete(filepath);
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        private static void ExceptionLogging(string hostname, string serialnumber, string message, string type)
        {
            //string finalstring = string.Empty;

            //SqlConnection con; int i = 0;
            //DataTable dt = new DataTable();
            //DateTime tdate = DateTime.Now;
            //string date = String.Format("{0:yyyy-MM-dd HH:mm:ss}", tdate);
            
            //try
            //{
            //    using (con = new SqlConnection("Password=tata@123;Persist Security Info=True;User ID=EQSHUAT;Initial Catalog=AssetInventory;Data Source=TPLHYDHOSPUAT"))
            //    {
            //        StringBuilder insertQuery = new StringBuilder();
            //        insertQuery.Append("insert into Exception_AssetExe(HostName,SerialNumber,LoggedDate,LoggingMessage,LogType)");
            //        insertQuery.Append(" values ("); insertQuery.Append("'" + hostname + "',");
            //        insertQuery.Append("'" + serialnumber + "',"); insertQuery.Append("'" + date + "',");
            //        insertQuery.Append("'" + loggingtxt + "',");
            //        insertQuery.Append("'" + type + "')");
            //        using (SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con))
            //        {
            //            con.Open();
            //            cmd.ExecuteNonQuery();
            //            con.Close();
            //        }
            //        if (loggingtxt2 != string.Empty)
            //        {
            //            StringBuilder insertQuery2 = new StringBuilder();
            //            insertQuery2.Append("insert into Exception_AssetExe(HostName,SerialNumber,LoggedDate,LoggingMessage,LogType)");
            //            insertQuery2.Append(" values ("); insertQuery2.Append("'" + hostname + "',");
            //            insertQuery2.Append("'" + serialnumber + "',"); insertQuery2.Append("'" + date + "',");
            //            insertQuery2.Append("'" + loggingtxt2 + "',");
            //            insertQuery2.Append("'" + type + "')");
            //            using (SqlCommand cmd2 = new SqlCommand(insertQuery2.ToString(), con))
            //            {
            //                con.Open();
            //                cmd2.ExecuteNonQuery();
            //                con.Close();
            //            }
            //        }
            //    }

            string url = string.Format("https://po.tataprojects.com/api/AssetInventory/InsertAssetLogging");
                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/json";
                DateTime tdate = DateTime.Now;
                string date = String.Format("{0:yyyy-MM-dd HH:mm:ss}", LoggedDate);
                loggingtxt = loggingtxt.Replace(@"""", "#").Replace(@"\", "^");
                
                string postdata = "{\"HostName\":\"" + hostname + "\",\"SerialNumber\":\"" + serialnumber + "\",\"LoggedDate\":\"" + date + "\",\"LoggingMessage\":\"" + loggingtxt + "\",\"LogType\":\"" + logtype + "\"}";
                //string postdata = "{\"AvailablePhysicalMemory\":\"" + avlpmem + "\",\"BiosReleaseDate\":\"" + biosreldate + "\",\"CPUName\":\"" + cpuname + "\",\"CreatedDate\":\"" + loggeddate + "\",\"diskdrivesize\":\"" + disksize + "\",\"EmailID\":\"" + emailid + "\",\"FQDNDetails\":\"" + fqdn + "\",\"HostName\":\"" + hostname + "\",\"ipconfig\":\"" + ipconfig + "\",\"Location\":\"" + location + "\",\"OriginalInstallDate\":\"" + orginstalldate + "\",\"OSBuildType\":\"" + osbuildtype + "\",\"OSConfiguration\":\"" + osconfig + "\",\"OSManufacturer\":\"" + osmanufacturer + "\",\"OSName\":\"" + osname + "\",\"OSVersion\":\"" + osversion + "\",\"Processor\":\"" + processor + "\",\"ProductID\":\"" + prodid + "\",\"RegisteredOrganization\":\"" + regorg + "\",\"RegisteredOwner\":\"" + regowner + "\",\"SerialNumber\":\"" + serialnumber + "\",\"SystemBootTime\":\"" + sysboottime + "\",\"SystemManufacturer\":\"" + sysman + "\",\"SystemModel\":\"" + sysmodel + "\",\"SystemType\":\"" + systype + "\",\"TotalPhysicalMemory\":\"" + totpmem + "\",\"Whoami\":\"" + whoami.Replace(@"\", "") +"\",\"ipconfig2\":\"" + ipconfig2 + "\",\"CodeVersion\":\"" + codeversion + "\",\"LoggedDate\":\"" + loggeddate + "\"}";
                try
                {
                    using (var streamwriter = new StreamWriter(request.GetRequestStream()))
                    {
                        streamwriter.Write(postdata);
                        streamwriter.Flush();
                        streamwriter.Close();

                        var httpresponse = (HttpWebResponse)request.GetResponse();

                        using (var streamreader = new StreamReader(httpresponse.GetResponseStream()))
                        {
                            var finresult = streamreader.ReadToEnd();
                            ReminderBackupLog("sql response exception logging :" + finresult.ToString());
                            Console.WriteLine(finresult);
                        }
                    }
                }
                catch (WebException ex)
                {
                    logtype = "Exception";
                    string expmsg = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                    ReminderBackupLog("while posting to exception logging webservice:" + expmsg);
                    //UpdateExceptioninList(hostname, serialnumber, "while posting to hardware webservice:" + message);
                    //throw;
                }

            //IDDeletion(finalstring.TrimStart(','));
        }
        public static void CreateFile(string message)
        {
            string root = @"C:\AssetInfo";

            // If directory does not exist, create it. 
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            try
            {
                root = root + @"\ErrorLog" + ".txt";
                StreamWriter sw = new StreamWriter(root, true);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception exp)
            {
                ReminderBackupLog(exp.ToString());
            }
        }
    }
}

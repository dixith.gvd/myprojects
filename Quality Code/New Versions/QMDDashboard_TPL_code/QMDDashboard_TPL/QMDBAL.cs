﻿using System;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using Microsoft.SharePoint;
using OfficeOpenXml;
using OfficeOpenXml.VBA;
using System.Text;
namespace QMDDashboard_TPL
{
    class QMDBAL
    {
        string FileName = string.Empty;
        public bool DownloadFinalTemplate(string path)
        {
            bool Check = false;
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    WebClient req = new WebClient();
                    HttpResponse response = HttpContext.Current.Response;
                    response.Clear();
                    response.AddHeader("Content-Disposition", "attachment;filename=\"" + FileName + "\"");
                    response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    byte[] data = req.DownloadData(path);
                    response.BinaryWrite(data);
                    response.End();
                });
            }
            catch (Exception ex)
            {
                //Acuvate.SP.Core.ULSLogger.LogError("DownLoadFinal Error msg," + ex.Message, "TPL");
            }
            return Check;
        }
        public bool UpdateTheDocument(string pathA, DataTable listofItems, string Template, string IsExsiting, string projectName, string ProjectCode)
        {
            bool Status = false;
            return Status;
        }
        private static bool WritingDataToExcelCellsDate(DataTable listofItems, bool Status, FileInfo newFile, int row, int Column, DateTime Parameter, string path)
        {
            try
            {
                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                    if (worksheet != null)
                    {
                        worksheet.Cells[row, Column].Value = Parameter;
                        try { package.Save(); }
                        catch (Exception ex)
                        {
                            //Acuvate.SP.Core.ULSLogger.LogError(ex.Message, "TPL");
                        }
                        Status = true;
                    }
                    package.Dispose();
                }
            }
            catch (Exception ex)
            {
                //Acuvate.SP.Core.ULSLogger.LogError("In Writing data to excel ()method date type and the exception is" + ex.Message, "TPL");
            }
            return Status;
        }
        private static bool WritingDataToExcelCellsInt(DataTable listofItems, bool Status, FileInfo newFile, int row, int Column, int Parameter, string path)
        {
            try
            {
                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                    if (worksheet != null)
                    {
                        worksheet.Cells[row, Column].Value = Parameter;
                        try { package.Save(); }
                        catch (Exception ex)
                        {
                            //Acuvate.SP.Core.ULSLogger.LogError(ex.Message, "TPL");
                        }
                        Status = true;
                    }
                    package.Dispose();
                }
            }
            catch (Exception ex)
            {
                //Acuvate.SP.Core.ULSLogger.LogError("In Writing data to excel ()method int type and the exception is" + ex.Message, "TPL");
            }
            return Status;
        }
        private static bool WritingDataToExcelCellsString(DataTable listofItems, bool Status, FileInfo newFile, int row, int Column, string Parameter, string path)
        {
            try
            {
                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                    if (worksheet != null)
                    {
                        worksheet.Cells[row, Column].Value = Parameter;
                        try { package.Save(); }
                        catch (Exception ex)
                        {
                            //Acuvate.SP.Core.ULSLogger.LogError(ex.Message, "TPL");
                        }
                        Status = true;
                    }
                    package.Dispose();
                }
            }
            catch (Exception ex)
            {
                //Acuvate.SP.Core.ULSLogger.LogError("In Writing data to excel ()method string type and the exception is" + ex.Message, "TPL");
            }
            return Status;
        }
        protected void WritingDataToBackendExcelCells(DataTable listofItems, FileInfo newFile, int row, int Column, string Parameter, string path)
        {
            try
            {
                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    ExcelWorksheet backendWorksheet = package.Workbook.Worksheets["Backend"];
                    if (backendWorksheet != null)
                    {
                        backendWorksheet.Cells[row, Column].Value = Parameter;
                        try { package.Save(); }
                        catch
                        { }
                    }
                }
            }
            catch (Exception ex)
            {
                //Acuvate.SP.Core.ULSLogger.LogTrace("Exception in WritingDataToBackendExcelCells ", ex.Message);
            }
        }
        public string GetFilefromDocumetLib(string TemplateName)
        {
            string targetUrL = string.Empty;
            try
            {
                SPUserToken SpToken = SPContext.Current.Site.SystemAccount.UserToken;
                using (SPSite osite = new SPSite(SPContext.Current.Site.Url, SpToken))
                {
                    using (SPWeb web = osite.OpenWeb())
                    {
                        //Get File from Templates library and copy it into some place
                        string DocLibName = Constants.DocLibName;
                        targetUrL = Constants.TempFolder;
                        bool IsScuess = CopyFolder(web, DocLibName, targetUrL, TemplateName);
                        if (!IsScuess)
                            targetUrL = Constants.FilenotFound;
                        else
                            targetUrL = targetUrL + '\\' + FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                //Acuvate.SP.Core.ULSLogger.LogError("In GetFilefromDocumetLib() method and the exception is" + ex.Message, "TPL");
            }
            return targetUrL;
        }
        public bool CopyFolder(SPWeb web, string sourceFolder, string destFolder, string TemplateName)
        {
            bool Status = false;
            try
            {
                //copying the template into some folder
                SPFolder spSourceFolder = web.GetFolder(sourceFolder);
                NetworkCredential network = new NetworkCredential();
                if (!Directory.Exists(destFolder))
                    Directory.CreateDirectory(destFolder);
                FileName = GetDocFilename(TemplateName);
                SPFileCollection files = spSourceFolder.Files;
                foreach (SPFile file in files)
                {
                    if (file.Name == FileName)
                    {
                        byte[] b = file.OpenBinary();
                        FileStream fs = new FileStream(destFolder + "\\" + file.Name, FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        bw.Write(b);
                        bw.Close();
                        Status = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                //Acuvate.SP.Core.ULSLogger.LogError("Calling CopyFile folder() method" + ex.Message, "TPL");
            }
            return Status;
        }
        private string GetDocFilename(string TemplateName)
        {
            string FileName = string.Empty;
            try
            {
                SPUserToken adminToken = SPContext.Current.Site.SystemAccount.UserToken;
                using (SPSite site = new SPSite(SPContext.Current.Site.Url, adminToken))
                {
                    using (SPWeb objSPWeb = site.OpenWeb())
                    {
                        SPList oList = SPContext.Current.Web.Lists[Constants.DocLibName];
                        SPQuery query = new SPQuery();
                        query.Query = "<Where><Eq><FieldRef Name='Template' /> <Value Type='Text'>" + TemplateName + "</Value> </Eq></Where>";
                        query.Query = "<Where><Eq><FieldRef Name='TemplateNo' /> <Value Type='Text'>" + TemplateName + "</Value> </Eq></Where>";
                        SPListItemCollection listFiles = oList.GetItems(query);
                        if (listFiles.Count > 0)
                        {
                            FileName = Convert.ToString(listFiles[0]["FileLeafRef"]);
                        }
                        else
                        {
                            //Acuvate.SP.Core.ULSLogger.LogError("GetDocFilename method step2 in else", "TPL");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Acuvate.SP.Core.ULSLogger.LogError("Calling GetDocFilename() method" + ex.Message, "TPL");
            }
            return FileName;
        }
        public DataTable GetExsitingData(string TemplateName)
        {
            DataTable dt = new DataTable();
            return dt;
        }
    }
}

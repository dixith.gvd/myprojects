﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using System.IO.Compression;
namespace QMDDashboard_TPL.ChecklistsModule.ChecklistsForm
{
    [ToolboxItemAttribute(false)]
    public partial class ChecklistsForm : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public ChecklistsForm()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        QualityChecklists objQuality = new QualityChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                //SPListItem projectCodeItem = objQuality.GetProjectCodeByLoggedInUser();

                DataTable projectCodeItem = objQuality.GetProjectCodeByLoggedInUser();

                if (projectCodeItem != null)
                {
                    dvUnAuth.Visible = false;
                    dvMainForm.Visible = true;

                    for (int i = 0; i < projectCodeItem.Rows.Count; i++)
                    {
                        string projectCode = projectCodeItem.Rows[i][0].ToString();

                        DataTable projectItem = objQuality.GetProjectdetailsByProjectCode(projectCode);

                        for (int j = 0; j < projectItem.Rows.Count; j++)
                        {
                            string projectName = projectItem.Rows[j]["Title"].ToString();
                            ddlProjectCode.Items.Add(new ListItem(projectName));
                        }
                        lblSBG.Text = projectItem.Rows[0]["SBGName"].ToString();
                        lblSBU.Text = projectItem.Rows[0]["SBU_x0020_Name"].ToString();
                        lblBU.Text = projectItem.Rows[0]["BU_x0020_Name"].ToString();

                        lblNewSBU.Text = projectItem.Rows[0]["SBU_x0020_Name"].ToString();

                        if (lblBU.Text == "Industrial Infrastructure")
                        {
                            lblInduCoE.Text = "coeqii@tataprojects.com";
                        }
                        else if (lblBU.Text == "Urban Infrastructure")
                        {
                            lblInduCoE.Text = "coeqii@tataprojects.com";
                        }
                        else
                        {
                            lblInduCoE.Text = "coeqii@tataprojects.com";
                        }

                    }
                    ddlProjectCode.Items.Insert(0, "--Select--");

                    if (ddlProjectCode.SelectedIndex > 0)
                    {
                        string projectNameForRCM = ddlProjectCode.SelectedItem.Text;
                        //DataTable projectCodeFromList = objQuality.GetProjectCodeByProjectName(projectNameForRCM);

                        //for (int i = 0; i < projectCodeFromList.Rows.Count; i++)
                        //{
                        //    string projectCode = projectCodeFromList.Rows[i][0].ToString();
                        string projectCode = ddlProjectCode.SelectedItem.Text.Split('-')[1];
                        SPListItem projectCodeFromPMRCM = objQuality.GetProjectCodeByProjectNameForRCM(projectCode);

                        if (projectCodeFromPMRCM["RCM"] != null)
                        {
                            string strRCM = projectCodeFromPMRCM["RCM"].ToString();
                            if (!string.IsNullOrEmpty(strRCM))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strRCM);
                                lblRCMEmail.Text = userValue.User.Email.ToString();
                                lblRCM.Text = userValue.User.Name;
                                lblRCMID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                            }

                        }
                        if (projectCodeFromPMRCM["BU_x0020_Head"] != null)
                        {
                            string strbuHead = projectCodeFromPMRCM["BU_x0020_Head"].ToString();
                            if (!string.IsNullOrEmpty(strbuHead))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strbuHead);
                                lblbuheadid.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                                lblbuheadName.Text = userValue.User.Name;
                                lblbuheadEmail.Text = userValue.User.Email.ToString();
                            }
                        }
                        if (projectCodeFromPMRCM["PM"] != null)
                        {
                            string strPM = projectCodeFromPMRCM["PM"].ToString();
                            if (!string.IsNullOrEmpty(strPM))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPM);
                                lblPMEmail.Text = userValue.User.Email.ToString();
                                lblPM.Text = userValue.User.Name;
                                lblPMID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                            }

                        }

                        if (projectCodeFromPMRCM["Head_x0020_Operations"] != null)
                        {
                            string strHead = projectCodeFromPMRCM["Head_x0020_Operations"].ToString();
                            if (!string.IsNullOrEmpty(strHead))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strHead);
                                lblHeadEmail.Text = userValue.User.Email.ToString();
                                lblHead.Text = userValue.User.Name;
                                lblHeadID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                            }

                        }
                        if (projectCodeFromPMRCM["Planning_x0020_Engineer"] != null)
                        {
                            string strPE = projectCodeFromPMRCM["Planning_x0020_Engineer"].ToString();
                            if (!string.IsNullOrEmpty(strPE))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPE);
                                lblPEEmail.Text = userValue.User.Email.ToString();
                                lblPE.Text = userValue.User.Name;
                                lblPEID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                            }

                        }

                        //}

                    }

                    //Bind Project Types
                    DataTable dtprojectTypes = objQuality.GetProjectTypesByProcess();
                    if (dtprojectTypes != null)
                    {
                        // ddlProjectTypes.Items.Insert(0, "Select");
                        ddlProjectTypes.DataSource = dtprojectTypes;
                        ddlProjectTypes.DataTextField = "Project type";
                        ddlProjectTypes.DataValueField = "Project type";
                        ddlProjectTypes.DataBind();
                        ddlProjectTypes.Items.Insert(0, "--Select--");
                    }
                    //Bind Desciplines
                    DataTable dtDesciplines = objQuality.GetDesciplinesByProcess();
                    if (dtDesciplines != null)
                    {
                        // ddlDesciplines.Items.Insert(0, "Select");
                        ddlDesciplines.DataSource = dtDesciplines;
                        ddlDesciplines.DataTextField = "Discipline";
                        ddlDesciplines.DataValueField = "Discipline";
                        ddlDesciplines.DataBind();
                        ddlDesciplines.Items.Insert(0, "--Select--");
                    }

                    //Bind locations
                    DataTable dtLocations = objQuality.GetLocationsByProjectCode(lblProjectCode.Text);
                    if (dtLocations != null)
                    {
                        // ddlLocations.Items.Insert(0, "Select");
                        ddlLocations.DataSource = dtLocations;
                        ddlLocations.DataTextField = "Location";
                        ddlLocations.DataValueField = "Location";
                        ddlLocations.DataBind();
                        ddlLocations.Items.Insert(0, "--Select--");
                    }
                    DataTable dtmainactivitydata = objQuality.GetMainActivityforFQP();
                    if (dtmainactivitydata != null)
                    {
                        ddlMainactivity.DataSource = dtmainactivitydata;
                        ddlMainactivity.DataTextField = "MainActivity";
                        ddlMainactivity.DataValueField = "MainActivity";
                        ddlMainactivity.DataBind();
                        ddlMainactivity.Items.Insert(0, "--Select--");

                    }
                    lblFQEEmail.Text = SPContext.Current.Web.CurrentUser.Email;
                    lblFQEName.Text = SPContext.Current.Web.CurrentUser.Name;
                    dtSWIdate.Enabled = false;
                }

                else
                {
                    dvUnAuth.Visible = true;
                    dvMainForm.Visible = false;
                }

            }
        }


        protected void ddlDesciplines_SelectedIndexChanged(object sender, EventArgs e)
        {

            string projectTypeID = ddlProjectTypes.SelectedItem.Value;
            string disciplineID = ddlDesciplines.SelectedItem.Value;
            DataTable dtActivitiesdata = objQuality.GetActivities(projectTypeID, disciplineID);
            if (dtActivitiesdata != null)
            {
                ddlActivities.DataSource = dtActivitiesdata;
                ddlActivities.DataTextField = "Activity";
                ddlActivities.DataValueField = "Activity";
                ddlActivities.DataBind();
                dvActivities.Visible = true;
                ddlActivities.Items.Insert(0, "--Select--");
            }
            else
            {
                lblError.Text = "There are no Activities for the selected filter";
            }
        }

        protected void ddlActivities_SelectedIndexChanged(object sender, EventArgs e)
        {
            string projectType = ddlProjectTypes.SelectedItem.Value;
            string discipline = ddlDesciplines.SelectedItem.Value;
            
            string activity = ddlActivities.SelectedItem.Text;
            DataTable dtActivitiesdata = objQuality.GetSubactivities(activity, projectType, discipline);
            if (dtActivitiesdata != null)
            {
                ddlSubActivities.DataSource = dtActivitiesdata;
                ddlSubActivities.DataTextField = "Sub Activity";
                ddlSubActivities.DataValueField = "Sub Activity";
                ddlSubActivities.DataBind();
                ddlSubActivities.Items.Insert(0, "--Select--");
                dvSubActivities.Visible = true;
            }
            else
            {
                lblError.Text = "There are no Sub-activities for the selected filter";
            }
        }

        protected void ddlSubActivities_SelectedIndexChanged(object sender, EventArgs e)
        {
            string projectType = ddlProjectTypes.SelectedItem.Value;
            string discipline = ddlDesciplines.SelectedItem.Value;
            
            string activity = ddlActivities.SelectedItem.Text;
            string subActivity = ddlSubActivities.SelectedItem.Text;
            DataTable dtCompliances = objQuality.GetCompliances(activity, subActivity, projectType, discipline);
            if (dtCompliances != null)
            {
                lblGridHeading.Text = "Compliances for : <b>'" + subActivity + "'</b>.";
                grdCompliances.DataSource = dtCompliances;
                grdCompliances.DataBind();
                dvCompliances.Visible = true;
            }
            else
            {
                lblError.Text = "There are no Complainces for the selected filter";
            }
        }

        protected void grdCompliances_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int count = 0;
            int countNo = 0;
            foreach (GridViewRow gvr in grdCompliances.Rows)
            {
                RadioButton rdbYes = (RadioButton)gvr.FindControl("rdbYes");
                RadioButton rdbNo = (RadioButton)gvr.FindControl("rdbNo");
                RadioButton rdbNA = (RadioButton)gvr.FindControl("rdbNA");
                if (rdbYes.Checked || rdbNA.Checked)
                {
                    count += 1;
                }
                if (rdbNo.Checked)
                {
                    count += 1;
                    countNo += 1;
                }
            }
            if (countNo > 0)
            {
                swiReq.Visible = true;
                isSWISubmit.Visible = true;
                trSave.Visible = false;

            }
            else
            {
                swiReq.Visible = false;
            }
            if (grdCompliances.Rows.Count.Equals(count))
            {

                lblError.Text = string.Empty;
                string stage = string.Empty;

                string isComplaint = "Yes";
                if (countNo > 0)
                {
                    isComplaint = "No";
                    stage = "Pending with RCM";
                }
                else
                {
                    stage = "Compliant and Closed";
                }

                #region Insert into main master

                string sbg = lblSBG.Text;
                string bu = lblBU.Text;
                string project = ddlProjectCode.SelectedItem.Text;
                string projectCode = lblProjectCode.Text;
                string location = ddlLocations.SelectedItem.Text;
                string subLocation = ddlSubLocations.SelectedItem.Text;
                string projectType = ddlProjectTypes.SelectedItem.Text;
                string discipline = ddlDesciplines.SelectedItem.Text;
                string mainactivity = ddlMainactivity.SelectedItem.Text;
                string activity = ddlActivities.SelectedItem.Text;
                string subActivity = ddlSubActivities.SelectedItem.Text;
                string loginUser = SPContext.Current.Web.CurrentUser.LoginName;
                string fqeID = objQuality.GetOnlyEmployeeID(loginUser);
                string fqeEmail = lblFQEEmail.Text;
                string fqeName = lblFQEName.Text;

                string newSBUName = lblNewSBU.Text;
                string hop_Name = lblHead.Text;
                string hop_Email = lblHeadEmail.Text;
                string pe_Id = lblPEID.Text;
                string pe_Name = lblPE.Text;
                string pe_Email = lblPEEmail.Text;


                DateTime today = DateTime.Now;
                string todayStr = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                string transID = string.Empty;
                bool isSaved = objQuality.SaveChecklistByMaster(stage, bu, project, projectCode, location, subLocation, "QPR", projectType, discipline, activity, subActivity, isComplaint, fqeID, fqeName, fqeEmail, todayStr, lblRCMID.Text, lblRCM.Text, lblRCMEmail.Text, lblPMID.Text, lblPM.Text, lblPMEmail.Text, newSBUName, hop_Name, hop_Email, pe_Id, pe_Name, pe_Email, lblbuheadid.Text, lblbuheadName.Text, lblbuheadEmail.Text, txtidentify.Text.Replace("'", "''"), sbg, mainactivity);

                bool isSavedChild = false;
                if (isSaved)
                {
                    transID = objQuality.GetLatestTransactionID();
                    lblTransactionID.Text = transID;
                    if (!string.IsNullOrEmpty(transID))
                    {
                        InsertEvidenceDocument(transID);
                        foreach (GridViewRow gvr in grdCompliances.Rows)
                        {
                            string complainceText = grdCompliances.Rows[gvr.RowIndex].Cells[0].Text;
                            RadioButton rdbYes = (RadioButton)gvr.FindControl("rdbYes");
                            RadioButton rdbNo = (RadioButton)gvr.FindControl("rdbNo");
                            RadioButton rdbNA = (RadioButton)gvr.FindControl("rdbNA");
                            TextBox txtRemarks = (TextBox)gvr.FindControl("txtFQECompRemarks");
                            string isComplaintEach = string.Empty;
                            if (rdbYes.Checked)
                            {
                                isComplaintEach = rdbYes.Text;
                            }
                            if (rdbNo.Checked)
                            {
                                isComplaintEach = rdbNo.Text;
                            }
                            if (rdbNA.Checked)
                            {
                                isComplaintEach = rdbNA.Text;
                            }
                            isSavedChild = objQuality.SaveChecklistChild(transID, complainceText, isComplaintEach, txtRemarks.Text.Replace("'", "''"));
                        }


                        btnSave.Visible = false;
                    }
                }

                if (isSaved && isSavedChild)
                {
                    if (!string.IsNullOrEmpty(transID))
                    {
                        if (countNo > 0)
                        {
                            swiReq.Visible = true;
                            Random rndNew = new Random();
                            int randNumber = rndNew.Next(1, 9);
                            string swiNumber = string.Concat(transID, "/", randNumber.ToString());
                            lblSWINO.Text = swiNumber;
                            //  dtSWIdate.SelectedDate = DateTime.Now;
                            lblProjectLocation.Text = ddlProjectCode.SelectedItem.Text;
                            lblProjectType.Text = ddlProjectTypes.SelectedItem.Text;
                            lblDiscipline.Text = ddlDesciplines.SelectedItem.Text;
                            lblActivity.Text = ddlActivities.SelectedItem.Text;
                            lblSubActivity.Text = ddlSubActivities.SelectedItem.Text;
                            lblRCMNameSWI.Text = lblRCM.Text;
                            lblRCMEmailSWI.Text = lblRCMEmail.Text;
                            lblFQEEmailSWI.Text = lblFQEEmail.Text;
                            lblFQENameSWI.Text = lblFQEName.Text;

                            DataTable dtChildData = objQuality.GetComplaincesDatabyTransactionID(transID);
                            if (dtChildData != null)
                            {
                                grdSWIComplainces.DataSource = dtChildData;
                                grdSWIComplainces.DataBind();
                            }

                        }
                        else
                        {
                            objQuality.SendCompRCMForClosureMail(lblRCM.Text, lblRCMEmail.Text, fqeName, fqeEmail, project, activity, subActivity, location, transID, lblPMEmail.Text, lblInduCoE.Text);
                            // objQuality.SendRCMMail(lblRCM.Text, lblRCMEmail.Text, fqeName, fqeEmail, project, activity, subActivity, location, transID, lblPMEmail.Text);
                            lblSWIsuccessNotification.Text = "You have submitted successfully";

                            string navigateUrl = Constants.link + "/Pages/QMDCheklistForm.aspx";
                            lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to submit another QPR compliance", navigateUrl);
                            //string message = "You have submitted successfully";
                            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            //sb.Append("<script type = 'text/javascript'>");
                            //sb.Append("window.onload=function(){");
                            //sb.Append("alert('");
                            //sb.Append(message);
                            //sb.Append("')};");
                            //sb.Append("</script>");
                            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                        }
                    }
                }
                #endregion
            }
            else
            {
                lblError.Text = "All complied's to be selected";
            }
        }



        protected void chkSWIrequired_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSWIrequired.Checked)
            {
                tblSWI.Visible = true;
                trSave.Visible = false;
                dtSWIdate.SelectedDate = DateTime.Now;
            }
            else
            {
                tblSWI.Visible = false;
                trSave.Visible = false;
            }
        }

        protected void btnSWISubmit_Click(object sender, EventArgs e)
        {
            //UpdateSWIInfo parent
            string swiNumber = lblSWINO.Text;

            string swidate = dtSWIdate.SelectedDate.Date.ToString("yyyy-MM-dd HH:mm:ss:fff");

            string package = txtPackage.Text.Replace("'", "''"); ;
            string subContractor = txtSubcontractors.Text.Replace("'", "''"); ;
            string SWirequired = string.Empty;
            string swiissuedby = ddlissuedby.SelectedItem.Text;
            if (chkSWIrequired.Checked)
            {
                SWirequired = "Yes";
            }
            objQuality.UpdateSWIInfoInParent(lblTransactionID.Text, swiNumber, package, subContractor, swidate, SWirequired, swiissuedby);

            bool isMailSent = objQuality.SendRCMMail(lblRCM.Text, lblRCMEmail.Text, lblFQEName.Text, lblFQEEmail.Text, ddlProjectCode.SelectedItem.Text, lblActivity.Text, lblSubActivity.Text, ddlLocations.SelectedItem.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);

            lblSWIsuccessNotification.Text = "Submitted successfully";
            string navigateUrl = Constants.link + "/Pages/QMDCheklistForm.aspx";
            lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to submit another QPR compliance", navigateUrl);
            btnSWISubmit.Enabled = false;
            btnSWIcancel.Enabled = false;

        }

        protected void ddlLocations_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlLocations.SelectedItem.Text) && ddlLocations.SelectedIndex > 0)
                {
                    string pcode = ddlProjectCode.SelectedItem.Text;
                    string[] code = pcode.Split('-');
                    string projcode = code[1];
                    DataTable dtSubLocations = objQuality.GetSubLocationsBylocations(ddlLocations.SelectedItem.Text, projcode);
                    if (dtSubLocations != null)
                    {
                        if (dtSubLocations.Rows.Count > 0)
                        {
                            ddlSubLocations.DataSource = dtSubLocations;
                            ddlSubLocations.DataTextField = "Sub Location";
                            ddlSubLocations.DataValueField = "Sub Location";
                            ddlSubLocations.DataBind();
                            ddlSubLocations.Items.Insert(0, "--Select--");
                            ddlSubLocations.Enabled = true;
                        }
                        else
                        {
                            lblError.Text = "No sub locations available";
                        }
                    }
                }
                else
                {
                    lblError.Text = "Please select valid location";
                }

            }
            catch (Exception ex)
            {

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int count = 0;
            int countNo = 0;
            foreach (GridViewRow gvr in grdCompliances.Rows)
            {
                RadioButton rdbYes = (RadioButton)gvr.FindControl("rdbYes");
                RadioButton rdbNo = (RadioButton)gvr.FindControl("rdbNo");
                RadioButton rdbNA = (RadioButton)gvr.FindControl("rdbNA");
                if (rdbYes.Checked || rdbNA.Checked)
                {
                    count += 1;
                }
                if (rdbNo.Checked)
                {
                    count += 1;
                    countNo += 1;
                }

            }


            lblSWIsuccessNotification.Text = "Submitted successfully";
            string navigateUrl = Constants.link + "/Pages/QMDCheklistForm.aspx";
            lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to submit another QPR compliance", navigateUrl);
        }

        private void InsertEvidenceDocument(string transactionID)
        {

            string filePath = uploadEvidenFUP.PostedFile.FileName;
            string filename = Path.GetFileName(filePath);
            string ext = Path.GetExtension(filename);
            string contenttype = String.Empty;

            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
            }
            if (contenttype != String.Empty)
            {

                Stream fs = uploadEvidenFUP.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                byte[] compressedData = Compress(bytes);
                //Update the file into database
                String strQuery = "update QChecklist_TnxMain set FQE_Ev_DocName=@FQE_Ev_DocName,FQE_Ev_DocContentType=@FQE_Ev_DocContentType,FQE_Ev_Doc=@FQE_Ev_Doc where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@FQE_Ev_DocName", SqlDbType.VarChar).Value = filename;
                cmd.Parameters.Add("@FQE_Ev_DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@FQE_Ev_Doc", SqlDbType.Binary).Value = compressedData;
                InsertEvidenceDoc(cmd);
            }

        }

        private Boolean InsertEvidenceDoc(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddlProjectCode_SelectedIndexChanged(object sender, EventArgs e)
        {

            string projectName = ddlProjectCode.SelectedItem.Text;
            lblProjectCode.Text = ddlProjectCode.SelectedItem.Text;

            //DataTable dtProjectCode = objQuality.GetProjectCodeByProjectName(projectName);
            if (!string.IsNullOrEmpty(ddlProjectCode.SelectedItem.Text) && ddlProjectCode.SelectedIndex > 0)
            {

                //for (int i = 0; i < dtProjectCode.Rows.Count; i++)
                //{
                //    string projectCode = dtProjectCode.Rows[i][1].ToString();
                string projectCode = ddlProjectCode.SelectedItem.Text.Split('-')[1];
                lblProjectCode.Text = projectCode;

                DataTable dtLocationdata = objQuality.GetLocationsByProjectCode(projectCode);

                for (int j = 0; j < dtLocationdata.Rows.Count; j++)
                {
                    ddlLocations.DataSource = dtLocationdata;
                    ddlLocations.DataTextField = "Location";
                    ddlLocations.DataValueField = "Location";
                    ddlLocations.DataBind();
                    ddlLocations.Items.Insert(0, "--Select--");
                }

                SPListItem projectCodeFromPMRCM = objQuality.GetProjectCodeByProjectNameForRCM(projectCode);

                if (projectCodeFromPMRCM["RCM"] != null)
                {
                    string strRCM = projectCodeFromPMRCM["RCM"].ToString();
                    if (!string.IsNullOrEmpty(strRCM))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strRCM);
                        lblRCMEmail.Text = userValue.User.Email.ToString();
                        lblRCM.Text = userValue.User.Name;
                        lblRCMID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                    }

                }
                if (projectCodeFromPMRCM["PM"] != null)
                {
                    string strPM = projectCodeFromPMRCM["PM"].ToString();
                    if (!string.IsNullOrEmpty(strPM))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPM);
                        lblPMEmail.Text = userValue.User.Email.ToString();
                        lblPM.Text = userValue.User.Name;
                        lblPMID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                    }

                }
                if (projectCodeFromPMRCM["BU_x0020_Head"] != null)
                {
                    string strbuHead = projectCodeFromPMRCM["BU_x0020_Head"].ToString();
                    if (!string.IsNullOrEmpty(strbuHead))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strbuHead);
                        lblbuheadid.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                        lblbuheadName.Text = userValue.User.Name;
                        lblbuheadEmail.Text = userValue.User.Email.ToString();
                    }
                }
                if (projectCodeFromPMRCM["Head_x0020_Operations"] != null)
                {
                    string strHead = projectCodeFromPMRCM["Head_x0020_Operations"].ToString();
                    if (!string.IsNullOrEmpty(strHead))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strHead);
                        lblHeadEmail.Text = userValue.User.Email.ToString();
                        lblHead.Text = userValue.User.Name;
                        lblHeadID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                    }

                }
                if (projectCodeFromPMRCM["Planning_x0020_Engineer"] != null)
                {
                    string strPE = projectCodeFromPMRCM["Planning_x0020_Engineer"].ToString();
                    if (!string.IsNullOrEmpty(strPE))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPE);
                        lblPEEmail.Text = userValue.User.Email.ToString();
                        lblPE.Text = userValue.User.Name;
                        lblPEID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                    }

                }
                //}

            }
            else
            {

            }


        }

        protected void ddlMainactivity_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private byte[] Compress(byte[] data)
        {
            var output = new MemoryStream();
            using (var gzip = new GZipStream(output, CompressionMode.Compress, true))
            {
                gzip.Write(data, 0, data.Length);
                gzip.Close();
            }
            return output.ToArray();
        }

    }
}

﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteSafetyUploads
{
    public static class SiteUploads
    {
        public static List<string> GetAttachmentUrls(this SPListItem listItem)
        {
            List<string> attachments = new List<string>();

            if (listItem == null || listItem.Attachments == null)
                return attachments;

            try
            {
                attachments = listItem.Attachments
                                        .Cast<string>()
                                        .Select(s => s = SPUrlUtility.CombineUrl(listItem.Attachments.UrlPrefix, s))
                                        .ToList();
            }
            catch
            {

            }

            return attachments;
        }
    }
}

﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PendingItems.ascx.cs" Inherits="QMDDashboard_TPL.ChecklistsModule.PendingItems.PendingItems" %>
<style type="text/css">
    .Initial {
        display: block;
        float: left;
        background: url("https://tplnet.tataprojects.com/SiteAssets/InitialImage.png") no-repeat right top;
        color: Black;
        font-weight: bold;
    }
        .Initial:hover {
            color: White;
            background: url("https://tplnet.tataprojects.com/SiteAssets/SelectedButton.png") no-repeat right top;
        }
    .Clicked {
        float: left;
        display: block;
        background: url("https://tplnet.tataprojects.com/SiteAssets/SelectedButton.png") no-repeat right top;
        color: Black;
        font-weight: bold;
        color: White;
    }
</style>
<table width="100%">
    <tr>
        <td>
            <asp:Button Text="RCM" Font-Bold="true" ForeColor="White" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                OnClick="Tab1_Click" />
            <asp:Button Text="Assignee" Font-Bold="true" ForeColor="White" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server"
                OnClick="Tab2_Click" />
            <asp:Button Text="RCM Closure" Font-Bold="true" ForeColor="White" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                OnClick="Tab3_Click" />
            <asp:MultiView ID="MainView" runat="server">
                <asp:View ID="View1" runat="server">
                    <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                        <tr>
                            <td>
                               <asp:GridView ID="grdRCMPendings" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" Visible="false" GridLines="None" Width="100%">
                                    <Columns>
                                        <%--<asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" HeaderStyle-HorizontalAlign="Left" HeaderText="Sub Activity" DataTextField="Sub Activity" DataNavigateUrlFormatString="https://tplnet.tataprojects.com/Pages/QMDChecklistActions.aspx?Tid={0}" Target="_blank" />
                                        <asp:BoundField DataField="Activity" HeaderStyle-HorizontalAlign="Left" HeaderText="Activity" />--%>
                                        <asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" HeaderStyle-HorizontalAlign="Left" HeaderText="Activity" DataTextField="Activity" DataNavigateUrlFormatString="/Pages/QMDChecklistActions.aspx?Tid={0}" Target="_blank" />
                                        <asp:BoundField DataField="Sub Activity" HeaderStyle-HorizontalAlign="Left" HeaderText="Sub Activity" />
                                        <asp:BoundField DataField="isCompliant" HeaderStyle-HorizontalAlign="Left" HeaderText="Compliance" />
                                        <asp:BoundField DataField="ProjectName" HeaderStyle-HorizontalAlign="Left" HeaderText="Project" />
                                        <asp:BoundField DataField="Location" HeaderStyle-HorizontalAlign="Left" HeaderText="Location" />
                                        <asp:BoundField DataField="SubLocation" HeaderStyle-HorizontalAlign="Left" HeaderText="SubLocation" />                                        
                                    </Columns>
                                    <AlternatingRowStyle BackColor="White" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                        <tr>
                            <td>
                                <asp:GridView ID="grdAssigneePendings" runat="server" CellPadding="4" ForeColor="#333333" AutoGenerateColumns="False" Visible="false" GridLines="None" Width="100%">
                                    <Columns>
                                        <%--<asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" HeaderStyle-HorizontalAlign="Left" HeaderText="Sub Activity" DataTextField="Sub Activity" DataNavigateUrlFormatString="https://tplnet.tataprojects.com/Pages/QMDChecklistActions.aspx?Tid={0}" Target="_blank" />
                                        <asp:BoundField DataField="Activity" HeaderStyle-HorizontalAlign="Left" HeaderText="Activity" />--%>
                                       <asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" HeaderStyle-HorizontalAlign="Left" HeaderText="Activity" DataTextField="Activity" DataNavigateUrlFormatString="/Pages/QMDChecklistActions.aspx?Tid={0}" Target="_blank" />
                                        <asp:BoundField DataField="Sub Activity" HeaderStyle-HorizontalAlign="Left" HeaderText="Sub Activity" />
                                        <asp:BoundField DataField="isCompliant" HeaderStyle-HorizontalAlign="Left" HeaderText="Compliance" />
                                        <asp:BoundField DataField="ProjectName" HeaderStyle-HorizontalAlign="Left" HeaderText="Project" />
                                        <asp:BoundField DataField="Location" HeaderStyle-HorizontalAlign="Left" HeaderText="Location" />
                                        <asp:BoundField DataField="SubLocation" HeaderStyle-HorizontalAlign="Left" HeaderText="SubLocation" />                                        
                                    </Columns>
                                    <AlternatingRowStyle BackColor="White" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="View3" runat="server">
                    <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                        <tr>
                            <td>
                                <asp:GridView ID="grdRCMClosurePendings" runat="server" CellPadding="4" ForeColor="#333333" AutoGenerateColumns="False" Visible="false" GridLines="None" Width="100%">
                                    <Columns>
                                       <%--<asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" HeaderStyle-HorizontalAlign="Left" HeaderText="Sub Activity" DataTextField="Sub Activity" DataNavigateUrlFormatString="~/Pages/QMDChecklistActions.aspx?Tid={0}" Target="_blank" />--%>
                                       <asp:HyperLinkField DataNavigateUrlFields="Transaction_ID" HeaderStyle-HorizontalAlign="Left" HeaderText="Activity" DataTextField="Activity" DataNavigateUrlFormatString="/Pages/QMDChecklistActions.aspx?Tid={0}" Target="_blank" />
                                        <%--<asp:BoundField DataField="Activity" HeaderStyle-HorizontalAlign="Left" HeaderText="Activity" />--%>
                                        <asp:BoundField DataField="Sub Activity" HeaderStyle-HorizontalAlign="Left" HeaderText="Sub Activity" />
                                        <asp:BoundField DataField="isCompliant" HeaderStyle-HorizontalAlign="Left" HeaderText="Compliance" />
                                        <asp:BoundField DataField="ProjectName" HeaderStyle-HorizontalAlign="Left" HeaderText="Project" />
                                        <asp:BoundField DataField="Location" HeaderStyle-HorizontalAlign="Left" HeaderText="Location" />
                                        <asp:BoundField DataField="SubLocation" HeaderStyle-HorizontalAlign="Left" HeaderText="SubLocation" />                                        
                                    </Columns>
                                    <AlternatingRowStyle BackColor="White" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
        </td>
    </tr>
</table>
<div id="dvRCM" runat="server" visible="false">
    <asp:Label ID="lblRCM" runat="server" ForeColor="Red" Font-Size="Large" Text="You do not have any QPRC non-compliance checklists uploaded by a FQE to process"></asp:Label>
</div>
<div id="dvAssignee" runat="server" visible="false">
    <asp:Label ID="lblAssignee" runat="server" ForeColor="Red" Font-Size="Large" Text="You do not have any QPRC non-compliance checklists assigned."></asp:Label>
</div>
<div id="dvRCMClose" runat="server" visible="false">
    <asp:Label ID="lblRCMClose" runat="server" ForeColor="Red" Font-Size="Large" Text="You do not have any QPRC non-compliance checklists to close "></asp:Label>
</div>

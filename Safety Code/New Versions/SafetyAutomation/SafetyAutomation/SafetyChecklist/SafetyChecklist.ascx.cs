﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SharePoint.WebControls;
using System.IO.Compression;
namespace SafetyAutomation.SafetyChecklist
{
    [ToolboxItemAttribute(false)]
    public partial class SafetyChecklist : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public SafetyChecklist()
        {
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        private bool IsLoggedInAuthorisedUser(string email)
        {
            bool isValid = SPContext.Current.Web.CurrentUser.Email.Equals(email);
            return isValid;
        }
        SafetyChecklists objSafety = new SafetyChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            dvMainForm.Visible = true;
            if (!this.Page.IsPostBack)
            {
                panelehs.Visible = true;
                PanelDaily.Visible = false;
                PanelWeekly.Visible = false;
                PanelMonthly.Visible = false;
                PanelQuarterly.Visible = false;
                PanelMain.Visible = false;
                //}
                //else
                //{
                //    if (panelehs.Visible == true)
                //    {
                //    }
                //    else
                //    {
                DataTable projectCodeItem = objSafety.GetProjectCodeByLoggedInUserSafety();
                if (projectCodeItem != null)
                {
                    dvUnAuth.Visible = false;
                    dvMainFormChecklist.Visible = true;
                    //PanelMain.Visible = true;
                    for (int i = 0; i < projectCodeItem.Rows.Count; i++)
                    {
                        string projectCode = projectCodeItem.Rows[i][0].ToString();
                        DataTable projectItem = objSafety.GetProjectdetailsByProjectCode(projectCode);
                        for (int j = 0; j < projectItem.Rows.Count; j++)
                        {
                            string projectName = projectItem.Rows[j]["Title"].ToString();
                            if (!ddlProjectCode.Items.Contains(new ListItem(projectName)))
                            {
                                ddlProjectCode.Items.Add(projectName);
                            }
                        }
                        lblBU.Text = projectItem.Rows[0]["BU_x0020_Name"].ToString();
                        lblNewSBU.Text = projectItem.Rows[0]["SBU_x0020_Name"].ToString();
                        if (lblBU.Text == "Industrial Infrastructure")
                        {
                            lblInduCoE.Text = "coesheii@tataprojects.com";
                        }
                        else if (lblBU.Text == "Urban Infrastructure")
                        {
                            lblInduCoE.Text = "coesheii@tataprojects.com";
                        }
                        else
                        {
                            lblInduCoE.Text = "coesheii@tataprojects.com";
                        }
                    }
                    if (!ddlProjectCode.Items.Contains(new ListItem("--Select--")))
                    {
                        ddlProjectCode.Items.Insert(0, "--Select--");
                    }
                    //if (ViewState["SelectedIndex"] != null && PanelMain.Visible == false)
                    //{
                    //    if (ViewState["SelectedIndex"].ToString() == "0")
                    //    {
                    //        ddlProjectCode.SelectedIndex = 0;
                    //    }
                    //}
                    if (ddlProjectCode.SelectedIndex > 0)
                    {
                        string projectNameForRCM = ddlProjectCode.SelectedItem.Text;
                        //DataTable projectCodeFromList = objSafety.GetProjectCodeByProjectName(projectNameForRCM);
                        //for (int i = 0; i < projectCodeFromList.Rows.Count; i++)
                        //{
                        //    string projectCode = projectCodeFromList.Rows[i][0].ToString();
                        string projectCode = ddlProjectCode.SelectedItem.Text.Split('-')[1];
                        SPListItem projectCodeFromPMRCM = objSafety.GetProjectCodeByProjectNameForRCM(projectCode);
                        if (projectCodeFromPMRCM["RCM"] != null)
                        {
                            string strRCM = projectCodeFromPMRCM["RCM"].ToString();
                            if (!string.IsNullOrEmpty(strRCM))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strRCM);
                                lblRCMEmail.Text = userValue.User.Email.ToString();
                                lblRCMName.Text = userValue.User.Name;
                                lblRCMID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                            }
                        }
                        if (projectCodeFromPMRCM["BU_x0020_Head"] != null)
                        {
                            string strbuHead = projectCodeFromPMRCM["BU_x0020_Head"].ToString();
                            if (!string.IsNullOrEmpty(strbuHead))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strbuHead);
                                lblbuheadid.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                                lblbuheadName.Text = userValue.User.Name;
                                lblbuheadEmail.Text = userValue.User.Email.ToString();
                            }
                        }
                        if (projectCodeFromPMRCM["PM"] != null)
                        {
                            string strPM = projectCodeFromPMRCM["PM"].ToString();
                            if (!string.IsNullOrEmpty(strPM))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPM);
                                lblPMEmail.Text = userValue.User.Email.ToString();
                                lblPMName.Text = userValue.User.Name;
                                lblPMID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                            }
                        }
                        if (projectCodeFromPMRCM["Head_x0020_Operations"] != null)
                        {
                            string strHead = projectCodeFromPMRCM["Head_x0020_Operations"].ToString();
                            if (!string.IsNullOrEmpty(strHead))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strHead);
                                lblHeadEmail.Text = userValue.User.Email.ToString();
                                lblHead.Text = userValue.User.Name;
                                lblHeadID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                            }
                        }
                        //}
                    }
                    lblSOEmail.Text = SPContext.Current.Web.CurrentUser.Email;
                    lblSOName.Text = SPContext.Current.Web.CurrentUser.Name;
                    SPUser user = SPContext.Current.Web.EnsureUser(lblSOEmail.Text);
                    SPFieldUserValue user2 = new SPFieldUserValue(SPContext.Current.Web, user.ID, user.Email.ToString());
                    lblSOID.Text = objSafety.GetOnlyEmployeeID(user2.User.LoginName);
                }
                else
                {
                    dvUnAuth.Visible = true;
                    dvMainFormChecklist.Visible = false;
                    PanelMain.Visible = true;
                    panelehs.Visible = false;
                }
                //    }
            }
        }
        protected void ddlProjectCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string projectName = ddlProjectCode.SelectedItem.Text;
            lblProjectCode.Text = ddlProjectCode.SelectedItem.Text;
            //DataTable dtProjectCode = objSafety.GetProjectCodeByProjectName(projectName);
            if (!string.IsNullOrEmpty(ddlProjectCode.SelectedItem.Text) && ddlProjectCode.SelectedIndex > 0)
            {
                //for (int i = 0; i < dtProjectCode.Rows.Count; i++)
                //{
                //    string projectCode = dtProjectCode.Rows[i][1].ToString();
                string projectCode = ddlProjectCode.SelectedItem.Text.Split('-')[1];
                lblProjectCode.Text = projectCode;
                SPListItem projectCodeFromPMRCM = objSafety.GetProjectCodeByProjectNameForRCM(projectCode);
                if (projectCodeFromPMRCM["RCM"] != null)
                {
                    string strRCM = projectCodeFromPMRCM["RCM"].ToString();
                    if (!string.IsNullOrEmpty(strRCM))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strRCM);
                        lblRCMEmail.Text = userValue.User.Email.ToString();
                        lblRCMName.Text = userValue.User.Name;
                        lblRCMID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                    }
                }
                if (projectCodeFromPMRCM["PM"] != null)
                {
                    string strPM = projectCodeFromPMRCM["PM"].ToString();
                    if (!string.IsNullOrEmpty(strPM))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPM);
                        lblPMEmail.Text = userValue.User.Email.ToString();
                        lblPMName.Text = userValue.User.Name;
                        lblPMID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                    }
                }
                if (projectCodeFromPMRCM["BU_x0020_Head"] != null)
                {
                    string strbuHead = projectCodeFromPMRCM["BU_x0020_Head"].ToString();
                    if (!string.IsNullOrEmpty(strbuHead))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strbuHead);
                        lblbuheadid.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                        lblbuheadName.Text = userValue.User.Name;
                        lblbuheadEmail.Text = userValue.User.Email.ToString();
                    }
                }
                if (projectCodeFromPMRCM["Head_x0020_Operations"] != null)
                {
                    string strHead = projectCodeFromPMRCM["Head_x0020_Operations"].ToString();
                    if (!string.IsNullOrEmpty(strHead))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strHead);
                        lblHeadEmail.Text = userValue.User.Email.ToString();
                        lblHead.Text = userValue.User.Name;
                        lblHeadID.Text = objSafety.GetOnlyEmployeeID(userValue.User.LoginName);
                    }
                }
                //}
            }
            else
            {
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string compliance = string.Empty;
            string stage = string.Empty;
            if (ddlProjectCode.SelectedIndex != 0)
            {
                if (rdyes.Checked || rdno.Checked)
                {
                    if (rdyes.Checked)
                    {
                        compliance = rdyes.Text;
                        stage = "Closed";
                        uploadattach.Enabled = false;
                    }
                    if (rdno.Checked)
                    {
                        compliance = rdno.Text;
                        stage = "Pending with RCM";
                    }
                    if (!rdno.Checked || lblRCMEmail.Text != "")
                    {
                        if (!rdno.Checked || txtobservation.Text != "")
                        {
                            if (uploaddoc.HasFile)
                            {
                                if (rbyes.Checked)
                                {
                                    string frequency = lblfrequency.Text;
                                    string checklist = lblchecklist.Text;
                                    string bu = lblBU.Text;
                                    string project = ddlProjectCode.SelectedItem.Text;
                                    string projectCode = lblProjectCode.Text;
                                    DateTime date = DateTimeControl1.SelectedDate;
                                    string inspdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", date);
                                    string observations = txtobservation.Text;
                                    string location = txtlocation.Text;
                                    string transID = string.Empty;
                                    string loginUser = SPContext.Current.Web.CurrentUser.LoginName;
                                    string soemail = SPContext.Current.Web.CurrentUser.Email;
                                    string soname = SPContext.Current.Web.CurrentUser.Name;
                                    string hop_id = lblHeadID.Text;
                                    string hop_Name = lblHead.Text;
                                    string hop_Email = lblHeadEmail.Text;
                                    string selfcert = rbyes.Text;
                                    DateTime today = DateTime.Now;
                                    string LastStageUpdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                                    int TransactionID = Convert.ToInt32(objSafety.GetLatestTransactionIDSafety()) + 1;
                                    bool isSaved = objSafety.SaveChecklist(TransactionID, frequency, checklist, bu, project, projectCode, inspdate, compliance, stage, location.Replace("'", "''"), observations.Replace("'", "''"), lblSOID.Text, lblSOName.Text, lblSOEmail.Text, lblRCMID.Text, lblRCMName.Text, lblRCMEmail.Text, lblPMID.Text, lblPMName.Text, lblPMEmail.Text, hop_id, hop_Name, hop_Email, lblbuheadid.Text, lblbuheadName.Text, lblbuheadEmail.Text, LastStageUpdate, selfcert, lblNewSBU.Text);
                                    if (isSaved)
                                    {
                                        //transID = objSafety.GetLatestTransactionIDSafety();
                                        transID = TransactionID.ToString();
                                        lblTransactionID.Text = transID;
                                        if (!string.IsNullOrEmpty(transID))
                                        {
                                            InsertAttachment();
                                            InsertDocument();
                                        }
                                        btnsubmit.Visible = false;
                                        if (stage == "Closed")
                                        {
                                            objSafety.SendClosureMailSafety(lblRCMName.Text, lblRCMEmail.Text, lblBU.Text, project, checklist, SPContext.Current.Web.CurrentUser.Name, observations.Replace("'", "''"), lblPMEmail.Text, lblInduCoE.Text, lblTransactionID.Text);
                                        }
                                        else
                                        {
                                            objSafety.SendCompRCMForClosureMailSafety(lblRCMName.Text, lblRCMEmail.Text, lblBU.Text, project, checklist, SPContext.Current.Web.CurrentUser.Name, observations.Replace("'", "''"), lblPMEmail.Text, lblInduCoE.Text, lblTransactionID.Text);
                                        }
                                        lblSOSubNotification.Text = "You have submitted successfully";
                                        string navigateUrl = Constants.link + "Pages/EHSCheckList.aspx";
                                        lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to submit another Safety Checklist", navigateUrl);
                                        lblError.Text = "";
                                        btnhome.Visible = false;
                                        //string navigateUrl = Constants.link + "/Pages/QMDCheklistForm.aspx";
                                        //lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to submit another QPR compliance", navigateUrl);
                                    }
                                }
                                else
                                {
                                    lblError.Text = "Please certify that above information is accurate.";
                                }
                            }
                            else
                            {
                                lblError.Text = "Checklist Document upload is mandatory";
                            }
                        }
                        else
                        {
                            lblError.Text = "Observation is mandatory";
                        }
                    }
                    else
                    {
                        lblError.Text = "There is no RCM associated with this Project. Please contact the system administrator";
                    }
                }
                else
                {
                    lblError.Text = "Please select the compliance";
                }
            }
            else
            {
                lblError.Text = "Please select Project";
            }
        }
        private void InsertAttachment()
        {
            string filePath = uploadattach.PostedFile.FileName;
            string filename = Path.GetFileName(filePath);
            string ext = Path.GetExtension(filename).ToLower();
            string contenttype = String.Empty;
            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
                case ".pptx":
                    contenttype = "application/pptx";
                    break;
                case ".jpeg":
                    contenttype = "image/jpeg";
                    break;
                case ".tif":
                    contenttype = "image/tif";
                    break;
                case ".tiff":
                    contenttype = "image/tiff";
                    break;
                case ".bmp":
                    contenttype = "image/bmp";
                    break;
                case ".rar":
                    contenttype = "application/rar";
                    break;
                case ".zip":
                    contenttype = "application/zip";
                    break;
            }
            if (contenttype != String.Empty)
            {
                Stream fs = uploadattach.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                byte[] compressedData = Compress(bytes);
                //Update the file into database
                String strQuery = "update EHSChecklist set NonComp_DocName=@NonComp_DocName,NonComp_DocContentType=@NonComp_DocContentType,NonComp_Document=@NonComp_Document where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@NonComp_DocName", SqlDbType.VarChar).Value = filename;
                cmd.Parameters.Add("@NonComp_DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@NonComp_Document", SqlDbType.Binary).Value = compressedData;
                InsertEvidenceDoc(cmd);
            }
        }
        private Boolean InsertEvidenceDoc(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private void InsertDocument()
        {
            string filePath = uploaddoc.PostedFile.FileName;
            string filename = Path.GetFileName(filePath);
            string ext = Path.GetExtension(filename).ToLower();
            string contenttype = String.Empty;
            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
                case ".pptx":
                    contenttype = "application/pptx";
                    break;
                case ".jpeg":
                    contenttype = "image/jpeg";
                    break;
                case ".tif":
                    contenttype = "image/tif";
                    break;
                case ".tiff":
                    contenttype = "image/tiff";
                    break;
                case ".bmp":
                    contenttype = "image/bmp";
                    break;
                case ".rar":
                    contenttype = "application/rar";
                    break;
                case ".zip":
                    contenttype = "application/zip";
                    break;
            }
            if (contenttype != String.Empty)
            {
                Stream fs = uploaddoc.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                byte[] compressedData = Compress(bytes);
                //Update the file into database
                String strQuery = "update EHSChecklist set Checklist_DocName=@Checklist_DocName,Checklist_DocContentType=@Checklist_DocContentType,Checklist_Document=@Checklist_Document where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@Checklist_DocName", SqlDbType.VarChar).Value = filename;
                cmd.Parameters.Add("@Checklist_DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@Checklist_Document", SqlDbType.Binary).Value = compressedData;
                InsertEvidenceDoc(cmd);
            }
        }
        protected void btndaily_Click(object sender, EventArgs e)
        {
            PanelDaily.Visible = true;
            panelehs.Visible = false;
        }
        protected void btnweekly_Click(object sender, EventArgs e)
        {
            PanelWeekly.Visible = true;
            panelehs.Visible = false;
        }
        protected void btnmonthly_Click(object sender, EventArgs e)
        {
            PanelMonthly.Visible = true;
            panelehs.Visible = false;
        }
        protected void btnquarterly_Click(object sender, EventArgs e)
        {
            PanelQuarterly.Visible = true;
            panelehs.Visible = false;
        }
        protected void lblbackdaily_Click(object sender, EventArgs e)
        {
            panelehs.Visible = true;
            PanelDaily.Visible = false;
            PanelMain.Visible = false;
            ViewState["SelectedIndex"] = -1;
        }
        protected void btnhouse_Click(object sender, EventArgs e)
        {
            //dvMainForm.Visible = true;
            //PanelMain.Visible = true;
            //panelehs.Visible = false;
            //PanelDaily.Visible = false;
            //PanelWeekly.Visible = false;
            //PanelMonthly.Visible = false;
            //PanelQuarterly.Visible = false;
            //lblfrequency.Text = "Daily";
            //lblchecklist.Text = btnhouse.Text;
            this.Page.Response.Redirect("https://tplnet.tataprojects.com/Pages/HousekeepingChecklist.aspx");
        }
        protected void lblbackweekly_Click(object sender, EventArgs e)
        {
            panelehs.Visible = true;
            PanelWeekly.Visible = false;
            PanelMain.Visible = false;
            ViewState["SelectedIndex"] = -1;
        }
        protected void lblbackmonthly_Click(object sender, EventArgs e)
        {
            panelehs.Visible = true;
            PanelMonthly.Visible = false;
            PanelMain.Visible = false;
            ViewState["SelectedIndex"] = -1;
        }
        protected void btnbackquarterly_Click(object sender, EventArgs e)
        {
            panelehs.Visible = true;
            PanelQuarterly.Visible = false;
            PanelMain.Visible = false;
            ViewState["SelectedIndex"] = -1;
        }
        protected void btnppe_Click1(object sender, EventArgs e)
        {
            //PanelMain.Visible = true;
            //panelehs.Visible = false;
            //PanelDaily.Visible = false;
            //PanelWeekly.Visible = false;
            //PanelMonthly.Visible = false;
            //PanelQuarterly.Visible = false;
            //lblfrequency.Text = "Daily";
            //lblchecklist.Text = btnppe.Text;
            this.Page.Response.Redirect("https://tplnet.tataprojects.com/Pages/PPEChecklist.aspx");
        }
        protected void btnhira_Click1(object sender, EventArgs e)
        {
            //PanelMain.Visible = true;
            //panelehs.Visible = false;
            //PanelDaily.Visible = false;
            //PanelWeekly.Visible = false;
            //PanelMonthly.Visible = false;
            //PanelQuarterly.Visible = false;
            //lblfrequency.Text = "Daily";
            //lblchecklist.Text = btnhira.Text;
            this.Page.Response.Redirect("https://tplnet.tataprojects.com/Pages/HiratalkChecklist.aspx");
        }
        protected void btnpermit_Click1(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Daily";
            lblchecklist.Text = btnpermit.Text;
        }
        protected void btndailyssi_Click1(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Daily";
            lblchecklist.Text = btndailyssi.Text;
        }
        protected void btnheavy_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Daily";
            lblchecklist.Text = btnheavy.Text;
        }
        protected void btnworking_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Daily";
            lblchecklist.Text = btnworking.Text;
        }
        protected void btnscaffold_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Daily";
            lblchecklist.Text = btnscaffold.Text;
        }
        protected void btnstring_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Daily";
            lblchecklist.Text = btnstring.Text;
        }
        protected void btntfs_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Daily";
            lblchecklist.Text = btntfs.Text;
        }
        protected void btnttt_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Daily";
            lblchecklist.Text = btnttt.Text;
        }
        protected void btntetf_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Daily";
            lblchecklist.Text = btntetf.Text;
        }
        protected void lblesafety_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Weekly";
            lblchecklist.Text = lblesafety.Text;
        }
        protected void lbltraining_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Weekly";
            lblchecklist.Text = lbltraining.Text;
        }
        protected void lblcft_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Weekly";
            lblchecklist.Text = lblcft.Text;
        }
        protected void btnwsm_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Weekly";
            lblchecklist.Text = btnwsm.Text;
        }
        protected void btnworkmethod_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Weekly";
            lblchecklist.Text = btnworkmethod.Text;
        }
        protected void btnttbpi_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Weekly";
            lblchecklist.Text = btnttbpi.Text;
        }
        protected void btnltt_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btnltt.Text;
        }
        protected void btnwmachine_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btnwmachine.Text;
        }
        protected void btneme_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btneme.Text;
        }
        protected void btngascutting_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btngascutting.Text;
        }
        protected void btnhvehicle_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btnhvehicle.Text;
        }
        protected void btnhnci_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btnhnci.Text;
        }
        protected void btnfextnr_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btnfextnr.Text;
        }
        protected void btndgset_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btndgset.Text;
        }
        protected void btnpegm_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btnpegm.Text;
        }
        protected void btnbarb_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btnbarb.Text;
        }
        protected void btnbarc_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btnbarc.Text;
        }
        protected void btncpump_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btncpump.Text;
        }
        protected void btnwinchi_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btnwinchi.Text;
        }
        protected void btnlcinsp_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btnlcinsp.Text;
        }
        protected void btneresource_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Monthly";
            lblchecklist.Text = btneresource.Text;
        }
        protected void sbchecklist_Click(object sender, EventArgs e)
        {
            PanelMain.Visible = true;
            panelehs.Visible = false;
            PanelDaily.Visible = false;
            PanelWeekly.Visible = false;
            PanelMonthly.Visible = false;
            PanelQuarterly.Visible = false;
            lblfrequency.Text = "Quarterly";
            lblchecklist.Text = sbchecklist.Text;
        }
        protected void btnhome_Click(object sender, EventArgs e)
        {
            panelehs.Visible = true;
            PanelMain.Visible = false;
            ViewState["SelectedIndex"] = -1;
        }
        private byte[] Compress(byte[] data)
        {
            var output = new MemoryStream();
            using (var gzip = new GZipStream(output, CompressionMode.Compress, true))
            {
                gzip.Write(data, 0, data.Length);
                gzip.Close();
            }
            return output.ToArray();
        }
    }
}

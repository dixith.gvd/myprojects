﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SafetyChecklistActions.ascx.cs" Inherits="SafetyAutomation.SafetyChecklistActions.SafetyChecklistActions" %>

<script type="text/javascript">
    function OpenDialog(URL) {
        var NewPopUp = SP.UI.$create_DialogOptions();
        NewPopUp.url = URL;
        NewPopUp.width = 700;
        NewPopUp.height = 600;
        SP.UI.ModalDialog.showModalDialog(NewPopUp);
    }
    function onlyNumbers() {
        if (event.keyCode < 48 || event.keyCode > 57) {
            event.returnValue = false;
        }
    }

</script>
<script>
    function ReloadFun() {
        _spFormOnSubmitCalled = false; _spSuppressFormOnSubmitWrapper = true;
    }
</script>

<style type="text/css">
    .required {
        color: Red;
    }

    .auto-style1 {
        height: 30px;
        width: 40%;
    }
    .table {
        padding: 0;
        border-spacing: 0;
        text-align: center;
        font-family: Trebuchet MS;
        font-size: 10pt;
        vertical-align: middle;
        border-collapse: collapse;
    }
    .auto-style2 {
        border-collapse: collapse;
        table-layout: fixed;
        font-size: 10pt;
        font-family:'Trebuchet MS';
        text-transform: none;
        font-weight: normal;
        /*color: blue;
        font-style: normal;
        border-style: none;
        border-color: inherit;
        border-width: medium;*/
    }
    button{
        font-family:'Trebuchet MS';
        font-size:10pt;
    }
    div{
    font-family:'Trebuchet MS';
    font-size:10pt;
}
    .auto-style3 {
        width: 228px;
    }
</style>
<div id="dvFilledForm" runat="server">
    <table border="1" cellpadding="1" cellspacing="1" width="80%">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px; font-family:'Trebuchet MS'">
                <asp:Label ID="Label12" runat="server" ForeColor="White" Text="Safety Checklist Compliance" CssClass="auto-style2" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label11" ForeColor="White" runat="server" Text="Current Stage" CssClass="auto-style2"></asp:Label>
                </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblcurstage" runat="server" CssClass="auto-style2"></asp:Label>
                </td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="lblTID" ForeColor="White" runat="server" Text="Frequency" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblfrequency" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label1" ForeColor="White" runat="server" Text="Checklist" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblchecklist" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label17" ForeColor="White" runat="server" Text="BU" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblBU" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label18" ForeColor="White" runat="server" Text="Project" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblProject" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label2" ForeColor="White" runat="server" Text="Inspection Date" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblinsdate" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label19" ForeColor="White" runat="server" Text="Location" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblLocation" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label20" ForeColor="White" runat="server" Text="Compliance" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblcompliance" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label21" ForeColor="White" runat="server" Text="Observations" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblobservations" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Attachment" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:LinkButton ID="attachment" runat="server" OnClick="attachment_Click" OnClientClick="ReloadFun()" CssClass="auto-style2"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label25" ForeColor="White" runat="server" Text="ChecklistDocument" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:LinkButton ID="chkdoc" runat="server" OnClick="chkdoc_Click" OnClientClick="ReloadFun()" CssClass="auto-style2"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>

            <td colspan="2" style="text-align: center;">
                <%-- <asp:LinkButton ID="lnkSWI" runat="server" Font-Bold="true" onclick="lnkSWI_Click" >View SWI Form</asp:LinkButton>--%>
            </td>
        </tr>
    </table>
</div>

<div id="dvRCM" runat="server" visible="true">
    <table width="80%" border="1">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label4" runat="server" CssClass="auto-style2" ForeColor="White" Text="Please select an assignee for the task, set a target closure date (greater than today) and enter your remarks. 
Note that all three fields are mandatory."
                    Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label5" ForeColor="White" runat="server" ToolTip="Enter TPLID" Text="Select User" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                <SharePoint:PeopleEditor ID="ppAssigneeName" runat="server" AllowEmpty="true" MultiSelect="false" SelectionSet="User" ValidatorEnabled="true" MaximumEntities="1" />
                <asp:Label ID="lblselectAssignee" runat="server" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label8" ForeColor="White" runat="server" Text="RCM Remarks" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 60%; text-align: left; height: 50px;">
                <asp:TextBox ID="txtRCMRemarks" runat="server" TextMode="MultiLine" Width="97%" Height="50px" CssClass="auto-style2"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label7" ForeColor="White" runat="server" Text="Target Closure Date" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                <SharePoint:DateTimeControl ID="dtTargetClosureDate" DateOnly="true" runat="server" />

                <asp:Label ID="lblTargetClosureDate" Text="Please select a date greater than or equal to today." CssClass="auto-style2" runat="server"></asp:Label>
            </td>
        </tr>

    </table>

</div>
<div id="RCMActions" runat="server" visible="false">
    <table width="80%">
        <tr>
            <td style="text-align: right">
                <asp:Button ID="btnRCMActions" Font-Bold="true" runat="server" Text="Submit" CssClass="auto-style2" OnClick="btnRCMActions_Click" />
            </td>
        </tr>
    </table>

</div>
<br />
<div id="dvAssignee" runat="server" visible="false">
    <table width="80%" border="1">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label9" runat="server" ForeColor="White" CssClass="auto-style2" Text="Please provide evidence, enter the actual closure date, enter action taken and certify that all details are true. 
Note that all fields are mandatory."
                    Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label6" ForeColor="White" runat="server" Text="Action taken for closure of non compliance" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                <asp:TextBox ID="assigneeRemarksTXT" runat="server" TextMode="MultiLine" CssClass="auto-style2" Width="97%" Height="50px"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label10" ForeColor="White" runat="server" Text="Evidence that ensures compliance" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:FileUpload ID="assigneeEvidenceFUP" runat="server" Width="40%" />
                <%--<asp:LinkButton ID="upLoadEvidenceDoc" OnClick="upLoadEvidenceDoc_Click" runat="server" Visible="true"></asp:LinkButton>--%>
                <asp:LinkButton ID="Evidencedoc" runat="server" OnClick="Evidencedoc_Click" OnClientClick="ReloadFun()" CssClass="auto-style2"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label15" ForeColor="White" runat="server" Text="Closure date" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <SharePoint:DateTimeControl ID="dtclosure" DateOnly="true" runat="server" />
                <asp:Label ID="lblclosuredateassignee" CssClass="auto-style2" runat="server" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label13" ForeColor="White" runat="server" Text="Self Certification" CssClass="auto-style2"></asp:Label>
            </td>
            <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                <asp:RadioButton ID="rbnyes" runat="server" Text="Yes" ValidationGroup="Selfcert" CssClass="auto-style2" GroupName="Selfcert" />
                <asp:RadioButton ID="rbnno" runat="server" Text="No" ValidationGroup="Selfcert" CssClass="auto-style2" GroupName="Selfcert" />
                <asp:Label ID="lblselfcert" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
            </td>
        </tr>
    </table>
</div>
<div id="AssigneeActions" runat="server" visible="false">
    <table width="80%">
        <tr>
            <td style="text-align: right">
                <asp:Button ID="btnAssigneeSubmit" Font-Bold="true" runat="server" Text="Submit" CssClass="auto-style2" OnClick="btnAssigneeSubmit_Click" />
            </td>
        </tr>
    </table>

</div>

<div id="dvUnAuth" runat="server" visible="false">
    <asp:Label ID="Label14" runat="server" ForeColor="Red" Font-Size="Large" Text="You are not authorized to access" CssClass="auto-style2"></asp:Label>
</div>
<table>
    <tr>
        <td class="auto-style3">
            <asp:Label ID="lblSOSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" CssClass="auto-style2" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style3">
            <asp:Label ID="lblRCMSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" CssClass="auto-style2" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style3">
            <asp:Label ID="lblAsigneeSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" CssClass="auto-style2" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style3">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style3">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style3">
            <asp:Label ID="lblLinkClosureNotification" runat="server" CssClass="auto-style2"></asp:Label>
        </td>
    </tr>
</table>
<asp:Label ID="lblProjectCode" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblTransactionID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblRCMID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblRCMName" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblRCMEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>

<asp:Label ID="lblPMID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblPMName" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblPMEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>

<asp:Label ID="lblAssigneID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblAssigneName" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblAssigneEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>

<asp:Label ID="lblbuheadid" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblbuheadName" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblbuheadEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>

<asp:Label ID="lblHead" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblHeadID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblHeadEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>

<asp:Label ID="lblSOName" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblSOID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblSOEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>

<asp:Label ID="lblPE" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblPEID" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblPEEmail" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblNewSBU" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblError" ForeColor="Red" runat="server" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblInduCoE" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>

<asp:Label ID="lbltargetdate" Visible="false" CssClass="auto-style2" runat="server"></asp:Label>
<asp:Label ID="lblclosuredate" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
<asp:Label ID="lblrcmcomments" runat="server" Visible="false" CssClass="auto-style2"></asp:Label>
﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SharePoint.WebControls;
namespace SafetySelfCertification.QualitySelfCert
{
    [ToolboxItemAttribute(false)]
    public partial class QualitySelfCert : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public QualitySelfCert()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        QualitySelfCertification objcert = new QualitySelfCertification();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                if (this.Page.Request.QueryString["ID"] != null)
                {
                    btnsubmit.Visible = false;
                    lblID.Text = this.Page.Request.QueryString["ID"].ToString();
                    DataTable dtdata = objcert.GetQualitySelfCertificationDatabyID(lblID.Text);
                    if (dtdata.Rows.Count > 0)
                    {
                        if (dtdata.Rows[0]["BU Name"] != null)
                        {
                            lblBU.Text = dtdata.Rows[0]["BU Name"].ToString();
                        }
                        if (dtdata.Rows[0]["Project Name"] != null)
                        {
                            lblProject.Text = dtdata.Rows[0]["Project Name"].ToString();
                            ddlProjectCode.Visible = false;
                            lblProject.Visible = true;
                        }
                        if (dtdata.Rows[0]["Reporting Month"] != null)
                        {
                            lblrepmonth.Text = dtdata.Rows[0]["Reporting Month"].ToString();
                        }
                        if (dtdata.Rows[0]["Rep Week"] != null)
                        {
                            lblrepweek.Text = dtdata.Rows[0]["Rep Week"].ToString();
                        }
                        if (!string.IsNullOrEmpty(dtdata.Rows[0]["Comp 1"] as string))
                        {
                            chkcomp1.Checked = true;
                        }
                        else { chkcomp1.Checked = false; }
                        if (!string.IsNullOrEmpty(dtdata.Rows[0]["Comp 2"] as string))
                        {
                            chkcomp2.Checked = true;
                        }
                        else { chkcomp2.Checked = false; }
                        if (!string.IsNullOrEmpty(dtdata.Rows[0]["Comp 3"] as string))
                        {
                            chkcomp3.Checked = true;
                        }
                        else { chkcomp3.Checked = false; }
                        chkCertify.Checked = true;
                    }
                }
                else
                {
                    DataTable projectCodeItem = objcert.GetProjectCodeByLoggedInUserRCM();
                    if (projectCodeItem != null)
                    {
                        //PanelMain.Visible = true;
                        for (int i = 0; i < projectCodeItem.Rows.Count; i++)
                        {
                            string projectCode = projectCodeItem.Rows[i][0].ToString();

                            DataTable projectItem = objcert.GetProjectdetailsByProjectCode(projectCode);

                            for (int j = 0; j < projectItem.Rows.Count; j++)
                            {
                                string projectName = projectItem.Rows[j][12].ToString();
                                if (!ddlProjectCode.Items.Contains(new ListItem(projectName)))
                                {
                                    ddlProjectCode.Items.Add(projectName);
                                }

                            }
                            lblBU.Text = projectItem.Rows[0]["BUName"].ToString();
                        }
                        if (!ddlProjectCode.Items.Contains(new ListItem("--Select--")))
                        {
                            ddlProjectCode.Items.Insert(0, "--Select--");
                        }
                        DateTime today = DateTime.Today;
                        lblrepmonth.Text = String.Format("{0:MMMM}", DateTime.Today);
                        string weekNumber = GetWeekNumber(today);
                        //lblrepweek.Text = weekNumber + " " + string.Format("{0:dddd}", DateTime.Today);
                        lblrepweek.Text = weekNumber + " " + "Saturday";
                    }
                }
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (ddlProjectCode.SelectedIndex != 0)
            {
                if (chkcomp1.Checked || chkcomp2.Checked || chkcomp3.Checked)
                {
                    if (chkCertify.Checked)
                    {
                        string bu = lblBU.Text;
                        string project = ddlProjectCode.SelectedItem.Text;
                        string projectCode = project.Split('-')[1];
                        DateTime today = DateTime.Now;
                        string Created = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                        string CreatedBy = SPContext.Current.Web.CurrentUser.Name;
                        string comp1 = string.Empty; string comp2 = string.Empty; string comp3 = string.Empty; 
                        string selfCertify = "Yes";
                        if (chkcomp1.Checked) { comp1 = chkcomp1.Text; }
                        else { comp1 = null; }
                        if (chkcomp2.Checked) { comp2 = chkcomp2.Text; }
                        else { comp2 = null; }
                        if (chkcomp3.Checked) { comp3 = chkcomp3.Text; }
                        else { comp3 = null; }
                        //lblrepmonth.Text = String.Format("{0:MMMM}", Created);
                        //string weekNumber = GetWeekNumber(today);
                        //lblrepweek.Text = weekNumber + " " + string.Format("{0:dddd}", Created);
                        int ID = 0;
                        if (!string.IsNullOrEmpty(objcert.GetLatestIDQualitySelfCert()))
                        {
                            ID = Convert.ToInt32(objcert.GetLatestIDQualitySelfCert()) + 1;
                        }
                        else
                        {
                            ID = 1;
                        }
                        objcert.SaveSelfCertification(ID, bu, project, comp1, comp2, comp3, selfCertify, Created, Created, lblrepmonth.Text, lblrepweek.Text, projectCode, CreatedBy, CreatedBy);
                        lblNotification.Text = "You have submitted successfully";
                        btnsubmit.Visible = false;
                        lblError.Text = "";
                    }
                    else
                    {
                        lblError.Text = "Please certify that above information is accurate.";
                    }
                }
                else
                {
                    lblError.Text = "Please select the Compliance";
                }
            }
            else
            {
                lblError.Text = "Please select the Project";
            }
        }
        private string GetWeekNumber(DateTime today)
        {
            string weekno = string.Empty;
            if (today.Day <= 7)
            {
                weekno = "1st";
            }
            else if (today.Day > 7 && today.Day <= 14)
            {
                weekno = "2nd";
            }
            else if (today.Day > 14 && today.Day <= 21)
            {
                weekno = "3rd";
            }
            else if (today.Day > 21 && today.Day <= 28)
            {
                weekno = "4th";
            }
            else if (today.Day > 28)
            {
                weekno = "5th";
            }
            return weekno;
        }

        protected void ddlProjectCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblError.Text = "";
        }
    }
}

﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HiraTalkChecklist.ascx.cs" Inherits="HousekeepingChecklist.HiraTalkChecklist.HiraTalkChecklist" %>

<style type="text/css">
    .table {
        padding: 0;
        border-spacing: 0;
        text-align: center;
        font-family: 'Trebuchet MS';
        font-size: 10pt;
        vertical-align: middle;
        border-collapse: collapse;
    }

    .auto-style1 {
        height: 30px;
    }

    .required {
        color: Red;
    }

    .auto-style2 {
        width: 60%;
        height: 30px;
    }
</style>
<script>
    function ReloadFun() {
        _spFormOnSubmitCalled = false; _spSuppressFormOnSubmitWrapper = true;
    }
</script>

<div id="dvMainFormChecklist" runat="server" visible="false">
    <br />
    <table border="1" cellpadding="1" cellspacing="1" width="100%">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label5" runat="server" ForeColor="White" Text="Upload Safety Checklists" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <asp:Button Font-Size="Medium" ID="btnhome" runat="server" BackColor="#507CD1" ForeColor="White" Text="Home" CssClass="auto-style3" Width="61px" OnClick="btnhome_Click" />
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label21" ForeColor="White" runat="server" Text="Frequency"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:Label ID="lblfrequency" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label23" ForeColor="White" runat="server" Text="Checklist"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblchecklist" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label1" ForeColor="White" runat="server" Text="BU"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:Label ID="lblBU" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Project"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlProjectCode" runat="server" OnSelectedIndexChanged="ddlProjectCode_SelectedIndexChanged" Width="100%" AutoPostBack="true">
                </asp:DropDownList>
                <%--<asp:RequiredFieldValidator ID="RFProject" runat="server" ErrorMessage="Please Select Project" InitialValue="--Select--" ControlToValidate="ddlProjectCode" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                <asp:Label ID="lblProject" runat="server" Visible="false"></asp:Label></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label22" ForeColor="White" runat="server" Text="Inspection Date"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <SharePoint:DateTimeControl ID="DateTimeControl1" DateOnly="true" runat="server" />
                <asp:Label ID="lbldtinitiation" runat="server" Visible="false"></asp:Label></td>
            </td>
        </tr>
    </table>
    <asp:GridView ID="grdCompliances" runat="server" CellPadding="4" AutoGenerateColumns="false" Width="100%" ForeColor="#333333" GridLines="Both" CellSpacing="4" OnRowDataBound="grdCompliances_RowDataBound">
        <Columns>
            <%--<asp:BoundField DataField="Location" HeaderText="Location" />--%>
            <asp:TemplateField HeaderText="Location" ItemStyle-Width="25%">
                <ItemTemplate>
                    <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Location") %>' Visible="false" />
                    <asp:DropDownList ID="ddllocationhtalk" runat="server" Width="95%"></asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Is HIRA talk given?" ItemStyle-Width="15%">
                <ItemTemplate>
                    <asp:RadioButton ID="rdbYes" Text="Yes" runat="server" GroupName="GC" ValidationGroup="GC" Width="30%" TextAlign="Right" /><asp:RadioButton ID="rdbNo" Text="No" GroupName="GC" ValidationGroup="GC" runat="server" Width="30%" TextAlign="Right" /><br />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Photo evidence" ItemStyle-Width="30%">
                <ItemTemplate>
                    <asp:FileUpload ID="uploadEvidence" runat="server" Width="95%" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.png|.jpg|.gif|.xls|.xlsx)$"
                        ControlToValidate="uploadEvidence" runat="server" ForeColor="Red" ErrorMessage="Please select a valid word,excel,jpg,png,gif or pdf file format."
                        Display="Dynamic" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Reason(If No selected)" ItemStyle-Width="30%">
                <ItemTemplate>
                    <asp:TextBox ID="txtSORemarks" TextMode="MultiLine" runat="server" Width="95%"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <AlternatingRowStyle BackColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:GridView ID="GridView1" runat="server" CellPadding="4" AutoGenerateColumns="false" Width="100%" Visible="false" ForeColor="#333333" GridLines="Both" CellSpacing="4" OnRowDataBound="grdCompliances_RowDataBound">
        <Columns>
            <%--<asp:BoundField DataField="Location" HeaderText="Location" />--%>
            <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="25%"> <HeaderStyle Width="25%" /> </asp:BoundField>
            <asp:BoundField DataField="isCompliant" HeaderText="HIRA talk compliance" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="25%"> <HeaderStyle Width="25%" /> </asp:BoundField>
            <asp:TemplateField HeaderText="Photo evidence Uploaded" ItemStyle-Width="25%"> <HeaderStyle Width="25%" /> 
                <ItemTemplate>
                    <asp:LinkButton runat="server" Width="95%" CausesValidation="false" CommandName="DownLoad NCR" OnClientClick="ReloadFun()" ID="lnkView" Text='<%# Eval("PicEvidence_DocName") %>' OnClick="lnkView_Click"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="SORemarks" HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="25%"> <HeaderStyle Width="25%" /> </asp:BoundField>
            <%--<asp:BoundField DataField="Correction_Details" HeaderText="Corrective actions taken" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%"> <HeaderStyle Width="10%" /> </asp:BoundField>--%>
        </Columns>
        <AlternatingRowStyle BackColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
</div>
<table width="100%">
    <tr>
        <td style="text-align: center">
            <asp:Button Font-Size="Medium" ID="btnsubmit" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
        </td>
    </tr>
</table>
<div id="dvUnAuth" runat="server" visible="false">
    <asp:Label ID="Label13" runat="server" ForeColor="Red" Font-Size="Large" Text="You are not authorized to access"></asp:Label>
    <br />
    <br />
    <br />
    <asp:Label ID="Label114" runat="server" ForeColor="Red" Font-Size="Large" Text="[Can be filled only by safety officer]"></asp:Label>
</div>
<table>
    <tr>
        <td>
            <asp:Label ID="lblSOSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblLinkClosureNotification" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<asp:Label ID="lblProjectCode" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblTransactionID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblRCMID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblRCMName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblRCMEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPMID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPMName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPMEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblbuheadid" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblbuheadName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblbuheadEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblHead" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblHeadID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblHeadEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblSOName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblSOID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblSOEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblNewSBU" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
<asp:Label ID="lblInduCoE" runat="server" Visible="false"></asp:Label>
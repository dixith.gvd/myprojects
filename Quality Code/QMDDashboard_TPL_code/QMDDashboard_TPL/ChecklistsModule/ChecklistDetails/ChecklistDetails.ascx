﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChecklistDetails.ascx.cs" Inherits="QMDDashboard_TPL.ChecklistsModule.ChecklistDetails.ChecklistDetails" %>


<asp:Panel ID="panelQPR" runat="server">

    <style type="text/css">
        .required {
            color: Red;
        }

        .auto-style1 {
            height: 30px;
            width: 40%;
        }
    </style>

    <script>
        function ReloadFun() {
            _spFormOnSubmitCalled = false; _spSuppressFormOnSubmitWrapper = true;
        }
    </script>

    <div id="dvFilledForm" runat="server">
        <table border="1" cellpadding="1" cellspacing="1" width="80%">
            <tr>
                <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label12" runat="server" ForeColor="White" Text="QPR Compliance" Font-Size="Large"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="lblTID" ForeColor="White" runat="server" Text="Transaction ID"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblTransID" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="lblstage" runat="server" ForeColor="White" Text="Current Stage"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblcurstage" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                    <asp:Label ID="Label61" ForeColor="White" runat="server" Text="SBG"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                    <asp:Label ID="lblSBG" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                    <asp:Label ID="Label74" ForeColor="White" runat="server" Text="SBU"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                    <asp:Label ID="lblSBU" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label17" ForeColor="White" runat="server" Text="BU"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblBU" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label18" ForeColor="White" runat="server" Text="Project"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblProject" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label19" ForeColor="White" runat="server" Text="Location"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblLocation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label20" ForeColor="White" runat="server" Text="SubLocation"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblSubLocation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label21" ForeColor="White" runat="server" Text="Project Type"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblProjectType" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label22" ForeColor="White" runat="server" Text="Discipline"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblDiscipline" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label63" ForeColor="White" runat="server" Text="Main Activity"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblMainactivity" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label6" ForeColor="White" runat="server" Text="Activity"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblActivity" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label11" ForeColor="White" runat="server" Text="Sub Activity"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblSubActivity" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label25" ForeColor="White" runat="server" Text="FQE Evidence"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:LinkButton ID="fqeEvidence" runat="server" OnClick="fqeEvidence_Click" OnClientClick="ReloadFun()"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label59" ForeColor="White" runat="server" Text="SWI Issued By"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblissuedqpr" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label14" ForeColor="White" runat="server" Text="Compliance"></asp:Label>
                </td>

            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="grdCompliances" runat="server" AutoGenerateColumns="false" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="Both">
                        <Columns>
                            <asp:BoundField DataField="SNo" HeaderText="S No" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="Compliance_Text" HeaderText="Prerequisite" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="isCompliant" HeaderText="Complied" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="SWIRemarks" HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left" />
                        </Columns>
                        <AlternatingRowStyle BackColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>

                <td colspan="2" style="text-align: center;">
                    <%-- <asp:LinkButton ID="lnkSWI" runat="server" Font-Bold="true" onclick="lnkSWI_Click" >View SWI Form</asp:LinkButton>--%>
                    <asp:HyperLink ID="hplSWI" runat="server" Visible="false" Target="_blank">View SWI Form</asp:HyperLink>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div id="dvRCM" runat="server" visible="true">
        <table style="width: 80%" border="1">
            <tr>
                <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label5" runat="server" ForeColor="White" Text="RCM Actions" Font-Size="Large"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label2" ForeColor="White" runat="server" Text="Assignee Name"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <%--<SharePoint:PeopleEditor ID="ppAssigneeName" runat="server" AllowEmpty="true" MultiSelect="false" SelectionSet="User" ValidatorEnabled="true" MaximumEntities="1" />--%>
                    <asp:Label ID="lblassignee" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label1" ForeColor="White" runat="server" Text="Target Closure Date"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lbltargclosdate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Remarks"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblremarks" runat="server"></asp:Label>
                </td>
            </tr>
        </table>

    </div>

    <br />
    <div id="dvAssignee" runat="server" visible="false">
        <table style="width: 80%" border="1">
            <tr>
                <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label4" runat="server" ForeColor="White" Text="Assignee Actions" Font-Size="Large"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                    <asp:Label ID="Label9" ForeColor="White" runat="server" Text="Uploaded Evidence"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                    <asp:LinkButton ID="upLoadEvidenceDoc" OnClick="upLoadEvidenceDoc_Click" OnClientClick="ReloadFun()" runat="server" Visible="true"></asp:LinkButton>

                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                    <asp:Label ID="Label76" ForeColor="White" runat="server" Text="Actual Cost of Rework"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblactualreworkcost" runat="server"></asp:Label>
                </td>

            </tr>
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                    <asp:Label ID="Label78" ForeColor="White" runat="server" Text="Cost Borne By"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblcostborneby" runat="server"></asp:Label>
                </td>

            </tr>
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                    <asp:Label ID="Label7" ForeColor="White" runat="server" Text="Action Taken Date"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 90%; text-align: left; height: 30px;">
                    <asp:Label ID="lblactiondateassignee" runat="server"></asp:Label>
                </td>

            </tr>
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                    <asp:Label ID="Label8" ForeColor="White" runat="server" Text="Remarks"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblremarksassignee" runat="server"></asp:Label>
                </td>

            </tr>
        </table>
    </div>
    <br />
    <div id="dvRCMClose" runat="server" visible="false">
        <table style="width: 80%" border="1">
            <tr>
                <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label10" runat="server" ForeColor="White" Text="RCM/FQE Closure Actions" Font-Size="Large"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label111" ForeColor="White" runat="server" Text="Verification remarks"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                    <asp:Label ID="lblverifremarks" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label13" ForeColor="White" runat="server" Text="Closure Date"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 90%; text-align: left; height: 30px;">
                    <%--<asp:CompareValidator ID="ValidateEventDate" ControlToValidate="dtClosuredate" Operator="GreaterThanEqual" Type="Date" runat="server" ErrorMessage="Can't pick a date in the past"></asp:CompareValidator>--%>
                    <asp:Label ID="lblclosuredatercm" runat="server"></asp:Label>
                </td>

            </tr>
        </table>
    </div>
    <p>
        &nbsp;
    </p>

</asp:Panel>
<p>
</p>
<asp:Panel ID="panelFQP" runat="server">

    <style type="text/css">
        .required {
            color: Red;
        }

        .auto-style1 {
            height: 30px;
            width: 30%;
        }

        .auto-style2 {
            height: 30px;
            width: 40%;
        }
    </style>
    <script>
        function ReloadFun() {
            _spFormOnSubmitCalled = false; _spSuppressFormOnSubmitWrapper = true;
        }
    </script>

    <div id="dvFilledFormFQP" runat="server" visible="false">
        <table border="1" cellpadding="1" cellspacing="1" width="80%">
            <tr>
                <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label15" runat="server" ForeColor="White" Text="FQP Compliance" Font-Size="Large"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label16" ForeColor="White" runat="server" Text="Transaction ID"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblTransIDFQP" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label112" runat="server" ForeColor="White" Text="Current Stage"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblcurstageFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                    <asp:Label ID="Label62" ForeColor="White" runat="server" Text="SBG"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                    <asp:Label ID="lblSBGFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                    <asp:Label ID="Label75" ForeColor="White" runat="server" Text="SBU"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                    <asp:Label ID="lblSBUFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label23" ForeColor="White" runat="server" Text="BU"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblBUFQP" runat="server"></asp:Label>

                </td>

            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label24" ForeColor="White" runat="server" Text="Project"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblProjectFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label26" ForeColor="White" runat="server" Text="Location"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblLocationFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label27" ForeColor="White" runat="server" Text="SubLocation"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblSubLocationFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label28" ForeColor="White" runat="server" Text="Project Type"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblProjectTypeFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label29" ForeColor="White" runat="server" Text="Discipline"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblDisciplineFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label64" ForeColor="White" runat="server" Text="Main Activity"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblMainactivityFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label30" ForeColor="White" runat="server" Text="Activity"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblActivityFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label31" ForeColor="White" runat="server" Text="Sub Activity"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblSubActivityFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label60" ForeColor="White" runat="server" Text="NC Issued By"></asp:Label></td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblissuedfqp" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label65" ForeColor="White" runat="server" Text="Main Defect Category"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblDefectCategory" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label67" ForeColor="White" runat="server" Text="Sub Defect Category"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblSubDefectCategory" runat="server"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label68" ForeColor="White" runat="server" Text="Why1"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblWhy1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label70" ForeColor="White" runat="server" Text="Why2"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblWhy2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label71" ForeColor="White" runat="server" Text="Why3"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblWhy3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label72" ForeColor="White" runat="server" Text="Why4"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblWhy4" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label73" ForeColor="White" runat="server" Text="Why5"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblWhy5" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label66" ForeColor="White" runat="server" Text="Root Cause Category"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblRootCauseCategory" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label69" ForeColor="White" runat="server" Text="Root Cause"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblRootCause" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label57" ForeColor="White" runat="server" Text="Identification"></asp:Label></td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblidentify" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label32" runat="server" ForeColor="White" Text="Compliance"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="grdCompliancesFQP" runat="server" AutoGenerateColumns="false" CellPadding="4" ForeColor="#333333" GridLines="Both" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="SNo" HeaderStyle-HorizontalAlign="Left" HeaderText="S No" />
                            <asp:BoundField DataField="Compliance_Text" HeaderStyle-HorizontalAlign="Left" HeaderText="FQP Compliance" />
                            <asp:BoundField DataField="isCompliant" HeaderStyle-HorizontalAlign="Left" HeaderText="Complied" />
                            <asp:BoundField DataField="SWIRemarks" HeaderStyle-HorizontalAlign="Left" HeaderText="Remarks" />
                            <asp:TemplateField HeaderText="Upload Evidence">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkView" runat="server" CausesValidation="false" CommandName="DownLoad NCR" OnClick="lnkView_Click" OnClientClick="ReloadFun()" Text='<%# Eval("Supp_DocName") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Upload Photograph" ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" CausesValidation="false" CommandName="DownLoad Photo" OnClientClick="ReloadFun()" ID="lnkView2" Text='<%# Eval("Evi_DocName") %>' OnClick="lnkView2_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle BackColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <div id="dvuploadNCR" runat="server" visible="false">
            <table style="width: 80%" border="1">
                <tr>
                    <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                        <asp:Label ID="Label58" runat="server" ForeColor="White" Text="Uploaded NCR \ FIN :"></asp:Label>
                    </td>
                    <td>
                        <asp:GridView ID="GridView1" runat="server" ShowHeader="false" AutoGenerateColumns="false" CellPadding="4" ForeColor="#333333" GridLines="None" Width="60%">
                            <Columns>
                                <asp:TemplateField HeaderText="" HeaderStyle-Width="0" HeaderStyle-BorderWidth="0" ShowHeader="false">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" CausesValidation="false" CommandName="DownLoad NCR Doc" OnClientClick="ReloadFun()" ID="lnkViewNCR" Text='<%# Eval("NCR_DocName") %>' OnClick="lnkViewNCR_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>

                    <caption>
                        <br />
                    </caption>
                </tr>
            </table>
        </div>
    </div>
    <table border="1" style="width: 80%; font-family: 'Trebuchet MS'; font-size: 14px" id="tblSWI" runat="server">
        <tr>
            <td>
                <table runat="server" id="tblReworkBanner" visible="false">
                    <tr>
                        <td>
                            <asp:Image ID="imgTataLogo" ImageUrl="../../_layouts/15/images/TataLogo.jpg" runat="server" Visible="false" />
                        </td>
                        <td style="width: 100%;">
                            <table style="width: 100%">
                                <tr>
                                    <td style="text-align: center; width: 700px">
                                        <asp:Label ID="Label33" runat="server" Text="TATA PROJECTS LTD" Font-Size="Large"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; width: 700px">
                                        <asp:Label ID="Label34" runat="server" Text="REWORK CARD (RWC)" Font-Size="Large"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div id="Div2" runat="server" visible="false">
        <table border="1" style="width: 80%; font-family: 'Trebuchet MS'; font-size: 14px" id="RWC1">
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label35" runat="server" Text="RWC No:" Style="font-weight: 600"></asp:Label>
                    <asp:Label ID="lblRWCNO" runat="server"></asp:Label>
                </td>

                <td>
                    <table>
                        <tr>
                            <td>&nbsp;<asp:Label ID="lblDateTime" runat="server" Text="Date" Style="font-weight: 600"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblRWCDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>


                </td>
            </tr>

            <tr style="height: 28px">
                <td class="auto-style1">
                    <asp:Label ID="Label36" runat="server" Style="font-weight: 600" Text="Project/Location :"></asp:Label>
                    <asp:Label ID="lblProjectLocation" runat="server" Text="Auto populated"></asp:Label>
                </td>
                <td>&nbsp;
            <asp:Label ID="lblTPLJobNo" runat="server" Style="font-weight: 600" Text="TPL Job No. :"></asp:Label>
                    <asp:Label ID="lblTplProjectCode" runat="server" Text="Auto populated"></asp:Label>
                </td>

            </tr>
            <tr style="height: 28px">
                <td class="auto-style1">
                    <asp:Label ID="Label37" runat="server" Style="font-weight: 600" Text="Subcontractor:"></asp:Label>
                    <asp:Label ID="lblSubContract" runat="server" Text="Auto populated"></asp:Label>
                    &nbsp;</td>
                <td class="auto-style3">&nbsp;
                <asp:Label ID="Label38" runat="server" Style="font-weight: 600" Text="P.O./W.O. No.:"></asp:Label>
                    <asp:Label ID="lblPoWo" runat="server" Text="Auto populated"></asp:Label>
                </td>

            </tr>
            <tr style="height: 28px">
                <td colspan="2">
                    <asp:Label ID="Label39" Font-Bold="true" runat="server" Text="Description of Package / Work :"></asp:Label>
                    <asp:Label ID="lblPackage" runat="server" Text="Auto populated"></asp:Label>
                </td>

            </tr>
            <tr style="height: 28px">
                <td colspan="2">
                    <asp:Label ID="Label40" runat="server" Style="font-weight: 600" Text="NCR \ FIN \ Customer Complaint reference:"></asp:Label>
                    <%--<asp:RadioButton ID="rdCustomerComplaint" GroupName="RWC" Text="Customer Complaint" runat="server" />
                <asp:RadioButton ID="rdNCR" GroupName="RWC" Text="NCR" runat="server" />
                <asp:RadioButton ID="rdFIN" GroupName="RWC" Text="FIN" runat="server" />
                <asp:RadioButton ID="rdNote" GroupName="RWC" Text="Note" runat="server" />
                <asp:RadioButton ID="rdEmail" GroupName="RWC" Text="E-mail Reference" runat="server" />--%>
                    <asp:Label ID="lblReworkDue" runat="server"></asp:Label>
                </td>
            </tr>
            <tr style="height: 28px">
                <td colspan="2">
                    <asp:Label ID="lbldesc" runat="server" Text="Details of Non-Conformity / Discrepancy :" Style="font-weight: 700"></asp:Label>
                </td>

            </tr>
            <tr style="height: 28px">
                <td colspan="2">
                    <asp:TextBox ID="txtNCDetails" TextMode="MultiLine" Width="95%" runat="server"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <asp:Label ID="lblRWCroot" runat="server" Text="Root Cause for Non-Conformity / Discrepancy :" Style="font-weight: 700"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblNC_RootCause" runat="server"></asp:Label>
                </td>

            </tr>
            <%-- <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Button ID="Button1" runat="server" Font-Bold="true" OnClick="btnSWISubmit_Click" Text="Submit" />
                    <asp:Button ID="Button2" runat="server" Font-Bold="true" Text="Cancel" />
                </td>
            </tr>--%>
        </table>
    </div>
    <div id="Div3" runat="server" visible="false">
        <table border="1" style="width: 80%; font-family: 'Trebuchet MS'; font-size: 14px" id="RWC2">
            <tr>
                <td>
                    <asp:Label ID="RWCDetails" runat="server" Text="Details of Reworks : " Style="font-weight: 700"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtRWCdetails" Width="95%" TextMode="MultiLine" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rftxtRWCdetails" runat="server" ErrorMessage="Please Add Rework Details" ControlToValidate="txtRWCdetails" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label41" runat="server" Text="Approximate cost of Rework :"></asp:Label>
                    INR:<asp:TextBox ID="txtINR" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFtxtINR" runat="server" ErrorMessage="Please Add Approximate Cost" ControlToValidate="txtINR" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <%--<tr>
                <td colspan="2" style="text-align: center">
                    <asp:Button ID="Button3" runat="server" Font-Bold="true" OnClick="btnSWISubmit_Click" Text="Submit" />
                    <asp:Button ID="Button4" runat="server" Font-Bold="true" Text="Cancel" />
                </td>
            </tr>--%>
        </table>
    </div>
    <div id="Div4" runat="server" visible="false">
        <table border="1" style="width: 80%; font-family: 'Trebuchet MS'; font-size: 14px" id="RWC3">
            <tr>
                <td>
                    <asp:Label ID="Label42" runat="server" Text="Quality of Rework after completion :" Style="font-weight: 700"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtRCM" runat="server" Width="95%" TextMode="MultiLine"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFtxtRCM" runat="server" ErrorMessage="Please Add Quality of Rework Details." ControlToValidate="txtRCM" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label43" runat="server" Text="Actual cost of Rework :"></asp:Label>
                    INR:<asp:TextBox ID="txtRWCRCMCost" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFtxtRWCRCMCost" runat="server" ErrorMessage="Please Add Actual Cost" ControlToValidate="txtRWCRCMCost" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Cost Borne By :
                <asp:DropDownList ID="ddlborne" runat="server" Width="152px">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>Sub-Contractor</asp:ListItem>
                    <asp:ListItem>TPL</asp:ListItem>
                </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RFborne" runat="server" ErrorMessage="Please Select Cost borne by" InitialValue="--Select--" ControlToValidate="ddlborne" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <%--<tr>
                <td colspan="2" style="text-align: center">
                    <asp:Button ID="btnSWISubmit" runat="server" Font-Bold="true" OnClick="btnSWISubmit_Click" Text="Submit" />
                    <asp:Button ID="btnSWIcancel" runat="server" Font-Bold="true" Text="Cancel" />
                </td>
            </tr>--%>
        </table>
    </div>


    <br />
    <div id="divPE" runat="server" visible="false">
        <table style="width: 80%" border="1">
            <tr>
                <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label44" runat="server" ForeColor="White" Text="Planning Engineer Actions" Font-Size="Large"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label45" ForeColor="White" runat="server" Text="Remarks"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblperemarks" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="lblRCM" ForeColor="White" runat="server" Text="RCM Email"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblrcmemail" runat="server"></asp:Label>
                </td>
            </tr>
            <%--<tr>
            <td style="background-color:#507CD1; width:20%; text-align:left; height:30px; padding-left:10px;">
                <asp:Label ID="Label30" ForeColor="White" runat="server" Text="Target Closure Date"></asp:Label>
            </td>
            <td style="background-color:#fff; width:60%; text-align:left; height:30px;">
                <SharePoint:DateTimeControl ID="dtPEDate" DateOnly="true" runat="server" />
                 
                <asp:Label ID="Label31" Text="Please select a date greater than or equal to today." runat="server"></asp:Label>              
            </td>            
        </tr>--%>
        </table>

    </div>
    <br />
    <div id="dvRCMFQP" runat="server" visible="false">
        <table style="width: 80%" border="1">
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label46" runat="server" ForeColor="White" Text="RCM Actions" Font-Size="Large"></asp:Label>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
        <table style="width: 80%" border="1">
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label47" ForeColor="White" runat="server" ToolTip="Enter TPLID" Text="Assignee Name"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <%--<SharePoint:PeopleEditor ID="ppAssigneeName" runat="server" AllowEmpty="true" MultiSelect="false" SelectionSet="User" ValidatorEnabled="true" MaximumEntities="1" />--%>
                    <asp:Label ID="lblassigneeFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label48" ForeColor="White" runat="server" Text="Target Closure Date"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lbltrgclosdateFQP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label49" ForeColor="White" runat="server" Text="Remarks"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblrcmremarksFQP" runat="server"></asp:Label>
                </td>
            </tr>
        </table>

    </div>
    <br />
    <div id="dvAssigneeFQP" runat="server" visible="false">
        <table style="width: 80%" border="1">
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label50" runat="server" ForeColor="White" Text="Assignee Actions" Font-Size="Large"></asp:Label>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
        <table id="Table1" style="width: 80%" border="1" runat="server">
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style2">
                    <asp:Label ID="Label51" ForeColor="White" runat="server" Text="Uploaded Evidence"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                    <asp:LinkButton ID="upLoadEvidenceDocFQP" runat="server" Visible="true" OnClick="upLoadEvidenceDocFQP_Click" OnClientClick="ReloadFun()"></asp:LinkButton>

                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style2">
                    <asp:Label ID="Label52" ForeColor="White" runat="server" Text="Action Taken Date"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 90%; text-align: left; height: 30px;">
                    <asp:Label ID="lblassigneedateFQP" runat="server"></asp:Label>
                </td>

            </tr>
            <tr>
                <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style2">
                    <asp:Label ID="Label53" ForeColor="White" runat="server" Text="Remarks"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 60%; text-align: left; height: 30px;">
                    <asp:Label ID="lblassigneeremarksFQP" runat="server"></asp:Label>
                </td>

            </tr>
        </table>
    </div>

    <br />
    <div id="dvRCMCloseFQP" runat="server" visible="false">
        <table style="width: 80%" border="1">
            <tr>
                <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label54" runat="server" ForeColor="White" Text="RCM/FQE Closure Actions" Font-Size="Large"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>

            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label55" ForeColor="White" runat="server" Text="Verification remarks"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 40%; text-align: left; height: 30px;">
                    <%--<asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="txtverifi_Remarks" ErrorMessage="Enter Remarks." CssClass="required" Display="Dynamic" />--%>
                    <asp:Label ID="lblverifremarksFQP" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label56" ForeColor="White" runat="server" Text="Closure Date"></asp:Label>
                </td>
                <td style="background-color: #fff; width: 90%; text-align: left; height: 30px;">
                    <%--<asp:CompareValidator ID="ValidateEventDate" ControlToValidate="dtClosuredate" Operator="GreaterThanEqual" Type="Date" runat="server" ErrorMessage="Can't pick a date in the past"></asp:CompareValidator>--%>
                    <asp:Label ID="lblclosuredateFQP" runat="server"></asp:Label>
                </td>

            </tr>
        </table>
    </div>

</asp:Panel>

<asp:Label ID="lblTransactionID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="FQESubmittedDate" runat="server" Visible="false"></asp:Label>
<asp:Label ID="ltScriptLoader" runat="server"></asp:Label>
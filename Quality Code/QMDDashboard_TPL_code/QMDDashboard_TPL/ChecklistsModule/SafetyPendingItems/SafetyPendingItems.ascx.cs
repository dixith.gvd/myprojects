﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.SharePoint;
using System.IO;
using System.Collections.Specialized;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;

namespace QMDDashboard_TPL.ChecklistsModule.SafetyPendingItems
{
    [ToolboxItemAttribute(false)]
    public partial class SafetyPendingItems : WebPart
    {
        public SortDirection dir
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }

        }
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public SafetyPendingItems()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        [WebBrowsable(true), Category("Custom Properties"), Personalizable(PersonalizationScope.Shared), WebDisplayName("Role"), WebDescription("Role")]
        public string Role
        {
            get;
            set;
        }
        [WebBrowsable(true), Category("Custom Properties"), Personalizable(PersonalizationScope.Shared), WebDisplayName("Stage"), WebDescription("Stage")]
        public string Stage
        {
            get;
            set;
        }
        QualityChecklists objQuality = new QualityChecklists();
        private bool IsLoggedInAuthorisedUser(string email)
        {
            bool isValid = SPContext.Current.Web.CurrentUser.Email.Equals(email);
            return isValid;
        }

        bool IsDataValidAtCustomProperties()
        {
            bool IsValidData = false;
            try
            {
                string ln = this.Role;
                string st = this.Stage;
                if (!string.IsNullOrEmpty(ln) && !string.IsNullOrEmpty(st))
                {
                    IsValidData = true;
                }
            }
            catch (Exception exp)
            {
                this.Page.Response.Write("Please set the custom properties");
            }
            return IsValidData;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                if (IsDataValidAtCustomProperties())
                {
                    DataTable dt = new DataTable();
                    dt = GetValuesAndSetToControls();
                    if (dt != null)
                    {
                        DataView dv = new DataView();
                        dv = dt.DefaultView;
                        dv.Sort = "Transaction_ID" + " DESC";

                        //ViewState["dtdata"] = dv;
                        grdPendingsSafety.DataSource = dv;
                        grdPendingsSafety.DataBind();
                    }
                    else
                    {
                        DataTable dtdata = new DataTable();
                        grdPendingsSafety.DataSource = dtdata;
                        grdPendingsSafety.DataBind();
                    }
                }
                else
                {
                    lblError.Text = "Please set the custom properties";
                }

            }
        }

        private DataTable GetValuesAndSetToControls()
        {
            DataTable dt = new DataTable();
            string Email = string.Empty;
            if (this.Role == "RCM" && this.Stage == "Open")
            {
                DataTable dtdataRCM = objQuality.GetSafetyPendingItemsforRCM(SPContext.Current.Web.CurrentUser.Email, "Pending with RCM");
                if (dtdataRCM.Rows.Count > 0)
                {
                    if (dtdataRCM.Rows[0]["RCM_Email"] != null)
                    {
                        Email = dtdataRCM.Rows[0]["RCM_Email"].ToString();
                        if (IsLoggedInAuthorisedUser(Email))
                        {
                            grdPendingsSafety.DataSource = dtdataRCM;
                            grdPendingsSafety.DataBind();
                        }
                    }
                }
            }
            else if (this.Role == "RCM" && this.Stage == "Closed")
            {
                DataTable dtdataRCM = objQuality.GetSafetyPendingItemsforRCM(SPContext.Current.Web.CurrentUser.Email, "Closed");
                if (dtdataRCM.Rows.Count > 0)
                {
                    if (dtdataRCM.Rows[0]["RCM_Email"] != null)
                    {
                        Email = dtdataRCM.Rows[0]["RCM_Email"].ToString();
                        if (IsLoggedInAuthorisedUser(Email))
                        {
                            grdPendingsSafety.DataSource = dtdataRCM;
                            grdPendingsSafety.DataBind();
                        }
                    }
                }
            }
            else if (this.Role == "Assignee" && this.Stage == "Open")
            {
                DataTable dtdataAssignee = objQuality.GetSafetyPendingItemsforAssignee(SPContext.Current.Web.CurrentUser.Email, "Pending with Assignee");
                if (dtdataAssignee.Rows.Count > 0)
                {
                    if (dtdataAssignee.Rows[0]["Assignee_Email"] != null)
                    {
                        Email = dtdataAssignee.Rows[0]["Assignee_Email"].ToString();
                        if (IsLoggedInAuthorisedUser(Email))
                        {
                            grdPendingsSafety.DataSource = dtdataAssignee;
                            grdPendingsSafety.DataBind();
                        }
                    }
                }
            }
            else if (this.Role == "Assignee" && this.Stage == "Closed")
            {
                DataTable dtdataAssignee = objQuality.GetSafetyPendingItemsforAssignee(SPContext.Current.Web.CurrentUser.Email, "Closed");
                if (dtdataAssignee.Rows.Count > 0)
                {
                    if (dtdataAssignee.Rows[0]["Assignee_Email"] != null)
                    {
                        Email = dtdataAssignee.Rows[0]["Assignee_Email"].ToString();
                        if (IsLoggedInAuthorisedUser(Email))
                        {
                            grdPendingsSafety.DataSource = dtdataAssignee;
                            grdPendingsSafety.DataBind();
                        }
                    }
                }
            }
            else if (this.Role == "SO" && this.Stage == "Open")
            {
                DataTable dtdataSO = objQuality.GetSafetyPendingItemsforSO(SPContext.Current.Web.CurrentUser.Email);
                if (dtdataSO.Rows.Count > 0)
                {
                    if (dtdataSO.Rows[0]["SO_Email"] != null)
                    {
                        Email = dtdataSO.Rows[0]["SO_Email"].ToString();
                        if (IsLoggedInAuthorisedUser(Email))
                        {
                            grdPendingsSafety.DataSource = dtdataSO;
                            grdPendingsSafety.DataBind();
                        }
                    }
                }
            }
            else if (this.Role == "SO" && this.Stage == "Closed")
            {
                DataTable dtdataSO = objQuality.GetSafetyPendingItemsforSOClosed(SPContext.Current.Web.CurrentUser.Email);
                if (dtdataSO.Rows.Count > 0)
                {
                    if (dtdataSO.Rows[0]["SO_Email"] != null)
                    {
                        Email = dtdataSO.Rows[0]["SO_Email"].ToString();
                        if (IsLoggedInAuthorisedUser(Email))
                        {
                            grdPendingsSafety.DataSource = dtdataSO;
                            grdPendingsSafety.DataBind();
                        }
                    }
                }
            }
            else if (this.Role == "HO" && this.Stage == "Open")
            {
                DataTable dtdata = objQuality.GetSafetyPendingItemsforHO();
                grdPendingsSafety.DataSource = dtdata;
                grdPendingsSafety.DataBind();
            }
            else if (this.Role == "HO" && this.Stage == "Closed")
            {
                DataTable dtdata = objQuality.GetSafetyPendingItemsforHOClosed();
                grdPendingsSafety.DataSource = dtdata;
                grdPendingsSafety.DataBind();
            }
            return grdPendingsSafety.DataSource as DataTable;
        }

        protected void grdPendingsSafety_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            grdPendingsSafety.PageIndex = e.NewPageIndex;
            DataTable dt = new DataTable();
            dt = GetValuesAndSetToControls();
            DataView dv = new DataView();
            dv = dt.DefaultView;
            dv.Sort = "Transaction_ID" + " DESC";
            grdPendingsSafety.DataSource = dv;
            grdPendingsSafety.DataBind();
        }

        protected void grdPendingsSafety_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            DataTable dt = new DataTable();

            string SortDir = string.Empty;
            if (dir == SortDirection.Ascending)
            {
                dir = SortDirection.Descending;
                SortDir = "Desc";
            }
            else
            {
                dir = SortDirection.Ascending;
                SortDir = "Asc";
            }
            dt = GetValuesAndSetToControls();
            DataView dv = new DataView();
            dv = dt.DefaultView;
            dv.Sort = "Transaction_ID" + " DESC";
            DataView sortedView = new DataView(dv.ToTable());
            sortedView.Sort = e.SortExpression + " " + SortDir;

            grdPendingsSafety.DataSource = sortedView;
            grdPendingsSafety.DataBind();

        }

        protected void grdPendingsSafety_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink lnk = e.Row.Cells[0].Controls[0] as HyperLink;
                if (e.Row.Cells[1].Text == "Housekeeping")
                {
                    if (lnk != null)
                    {
                        //lnk.NavigateUrl = "http://tplhydhospuat/Pages/Housekeeping-Test.aspx?Tid=" + lnk.Text + "";
                    }
                }
                else if (e.Row.Cells[1].Text=="Personal Protective Equipment (PPE)")
                {
                    if (lnk != null)
                    {
                        //lnk.NavigateUrl = "http://tplhydhospuat/SitePages/PPEChecklist.aspx?Tid=" + lnk.Text + "";
                    }
                }
                else if (e.Row.Cells[1].Text == "HIRA talk")
                {
                    if (lnk != null)
                    {
                        //lnk.NavigateUrl = "http://tplhydhospuat/SitePages/HiraTalkChecklist.aspx?Tid=" + lnk.Text + "";
                    }
                }
            }
        }

    }
}

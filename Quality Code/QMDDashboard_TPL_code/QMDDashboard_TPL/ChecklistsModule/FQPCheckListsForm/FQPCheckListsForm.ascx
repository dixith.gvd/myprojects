﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FQPCheckListsForm.ascx.cs" Inherits="QMDDashboard_TPL.ChecklistsModule.FQECheckListsForm.FQECheckListsForm" %>

<style type="text/css">
    .table {
        padding: 0;
        border-spacing: 0;
        text-align: center;
        font-family: 'Trebuchet MS';
        vertical-align: middle;
        border-collapse: collapse;
    }

    .auto-style1 {
        width: 20%;
        height: 30px;
    }

    .required {
        color: Red;
    }

    .auto-style2 {
        width: 60%;
        height: 30px;
    }

    .auto-style3 {
        width: 50%;
    }
</style>
<script type="text/javascript">
    function ValidateRootCause(source, args) {
        var chkListModules = document.getElementById('<%= rootCauseForNon.ClientID %>');
        var chkListinputs = chkListModules.getElementsByTagName("input");
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }
</script>
<div id="dvMainForm" runat="server">
    <table border="1" cellpadding="1" cellspacing="1" width="89%">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label5" runat="server" ForeColor="White" Text="FQP Compliance" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label22" ForeColor="White" runat="server" Text="SBG"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:Label ID="lblSBG" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label34" ForeColor="White" runat="server" Text="SBU"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:Label ID="lblSBU" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label1" ForeColor="White" runat="server" Text="BU"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:Label ID="lblBU" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Project"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlProjectCode" runat="server" OnSelectedIndexChanged="ddlProjectCode_SelectedIndexChanged" Width="100%" AutoPostBack="true"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFProject" runat="server" ErrorMessage="Please Select Project" InitialValue="--Select--" ControlToValidate="ddlProjectCode" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                <%--<asp:Label ID="lblProject" runat="server"></asp:Label>--%>
            </td>

        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label6" ForeColor="White" runat="server" Text="Location"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlLocations" runat="server" OnSelectedIndexChanged="ddlLocations_SelectedIndexChanged" Width="100%" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ddlLocation" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--" ControlToValidate="ddlLocations" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label8" ForeColor="White" runat="server" Text="Sub Location"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlSubLocations" Enabled="false" runat="server" Width="100%" ValidationGroup="Req1">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlSubLocations" runat="server" ErrorMessage="Please Select Sub Location" InitialValue="--Select--" ControlToValidate="ddlSubLocations" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label7" ForeColor="White" runat="server" Text="Project Type"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlProjectTypes" runat="server" Width="100%"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlProjectTypes" runat="server" ErrorMessage="Please Select Project Type" InitialValue="--Select--" ControlToValidate="ddlProjectTypes" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label35" ForeColor="White" runat="server" Text="Main Activity"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlMainactivity" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlMainactivity_SelectedIndexChanged"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlMainactivity" runat="server" ErrorMessage="Please Select Main Activity" InitialValue="--Select--" ControlToValidate="ddlMainactivity" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label9" ForeColor="White" runat="server" Text="Discipline"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlDesciplines" runat="server" OnSelectedIndexChanged="ddlDesciplines_SelectedIndexChanged" Width="100%" AutoPostBack="true"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlDesciplines" runat="server" ErrorMessage="Please Select Desciplines" InitialValue="--Select--" ControlToValidate="ddlDesciplines" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
    </table>
    <table id="tblPlaningEng" runat="server" border="1" cellpadding="1" cellspacing="1" width="89%">

        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="lblPlaningEng" ForeColor="White" runat="server" Text="Planning Engineer"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:Label ID="lblPlaningEngi" runat="server" Visible="false"></asp:Label>
                <SharePoint:PeopleEditor ID="ppPlanningEngineer" runat="server" Visible="false" AllowEmpty="true" MultiSelect="false" SelectionSet="User" ValidatorEnabled="true" MaximumEntities="1" />
                <%--<asp:RequiredFieldValidator EnableClientScript="true" ID="rfvBusinessExcellence" ControlToValidate="ppPlanningEngineer" runat="server" CssClass="required" Display="Dynamic" ErrorMessage="You must specify a value for Planning Engineer"></asp:RequiredFieldValidator>--%>
               
            </td>
        </tr>

        <tr>
            <td colspan="2"></td>
        </tr>

    </table>
</div>

<div id="dvActivities" runat="server" visible="false">
    <table style="width: 89%" border="1">
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label10" ForeColor="White" runat="server" Text="Activity"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlActivities" runat="server" OnSelectedIndexChanged="ddlActivities_SelectedIndexChanged" AutoPostBack="true" Width="100%"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlActivities" runat="server" ErrorMessage="Please Select Activity" InitialValue="--Select--" ControlToValidate="ddlActivities" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</div>
<div id="dvSubActivities" runat="server" visible="false">
    <table style="width: 89%" border="1">
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label11" ForeColor="White" runat="server" Text="Sub Activity"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlSubActivities" runat="server" OnSelectedIndexChanged="ddlSubActivities_SelectedIndexChanged" AutoPostBack="true" Width="100%"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlSubActivities" runat="server" ErrorMessage="Please Select Sub Activity" InitialValue="--Select--" ControlToValidate="ddlSubActivities" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <table style="width: 89%" border="1">
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label19" runat="server" ForeColor="White" Text="Identification"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:TextBox ID="txtidentify" Width="97%" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFIdentify" runat="server" ErrorMessage="Please enter text" InitialValue="" ControlToValidate="txtidentify" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
    </table>
</div>
<div id="dvCompliances" runat="server" visible="false">
    <table style="width: 89%" border="1">
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="lblGridHeading" ForeColor="White" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="grdCompliances" runat="server" CellPadding="4" AutoGenerateColumns="false" Width="100%" ForeColor="#333333" GridLines="Both" Style="margin-top: 2px">
                    <Columns>
                        <asp:BoundField DataField="Compliance" HeaderText="FQP Compliance" ItemStyle-Width="15%" />
                        <asp:TemplateField HeaderText="Complied" ItemStyle-Width="15%">

                            <ItemTemplate>
                                <asp:RadioButton ID="rdbYes" Text="Yes" runat="server" GroupName="GC" ValidationGroup="GC" /><br />
                                <asp:RadioButton ID="rdbNo" Text="No" GroupName="GC" ValidationGroup="GC" runat="server" /><br />
                                <asp:RadioButton ID="rdbNA" Text="NA" GroupName="GC" ValidationGroup="GC" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:TextBox ID="txtFQECompRemarks" TextMode="MultiLine" runat="server" Width="95%"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Upload Evidence" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <asp:FileUpload ID="uploadNCR" runat="server" Width="95%" />
                                <%--<asp:RequiredFieldValidator ID="RFuploadNCR" runat="server" ErrorMessage="file upload is mandatory" ControlToValidate="uploadNCR" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.PDF|.xls|.xlsx|.DOC|.DOCX|.XLS|.XLSX)$"
                                    ControlToValidate="uploadNCR" runat="server" ForeColor="Red" ErrorMessage="Please select a valid word,excel or pdf file format."
                                    Display="Dynamic" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Upload Photograph" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <asp:FileUpload ID="uploadPhoto" runat="server" Width="95%" />
                                <%--<asp:RequiredFieldValidator ID="RFuploadPhoto" runat="server" ErrorMessage="file upload is mandatory" ControlToValidate="uploadPhoto" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg|.PNG|.JPG|.JPEG|.GIF)$"
                                    ControlToValidate="uploadPhoto" runat="server" ForeColor="Red" ErrorMessage="Please select a valid jpg,jpeg,png or gif file format."
                                    Display="Dynamic" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <div id="dvuploadNCR" runat="server" visible="false">
        <table style="width: 89%" border="1">
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="lblissuedby" ForeColor="White" runat="server" Text="NC Issued BY"></asp:Label>
                </td>
                <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                    <asp:DropDownList ID="ddlissuedby" runat="server" Width="100%" AutoPostBack="true">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem>FQE</asp:ListItem>
                        <asp:ListItem>Customer</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvissuedby" runat="server" ErrorMessage="Please Select NC Issued BY" InitialValue="--Select--" ControlToValidate="ddlissuedby" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="lblMaindefect" ForeColor="White" runat="server" Text="Main Defect Category"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlDefectCategory" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlDefectCategory_SelectedIndexChanged"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RFDefectCategory" runat="server" ErrorMessage="Please Select Main Defect Category" InitialValue="--Select--" ControlToValidate="ddlDefectCategory" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="lblSubDefect" ForeColor="White" runat="server" Text="Sub Defect Category"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlSubdefect" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlSubdefect_SelectedIndexChanged"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RFSubdefectCategory" runat="server" ErrorMessage="Please Select Sub Defect Category" InitialValue="--Select--" ControlToValidate="ddlSubdefect" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label26" ForeColor="White" runat="server" Text="Why1"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtWhy1" runat="server" TextMode="MultiLine" Width="90%" Height="50px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFWhy1" runat="server" ErrorMessage="Why1 is Mandatory" ControlToValidate="txtWhy1" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label30" ForeColor="White" runat="server" Text="Why2"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtWhy2" runat="server" TextMode="MultiLine" Width="90%" Height="50px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFWhy2" runat="server" ErrorMessage="Why2 is Mandatory" ControlToValidate="txtWhy2" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label31" ForeColor="White" runat="server" Text="Why3"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtWhy3" runat="server" TextMode="MultiLine" Width="90%" Height="50px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFWhy3" runat="server" ErrorMessage="Why3 is Mandatory" ControlToValidate="txtWhy3" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label32" ForeColor="White" runat="server" Text="Why4"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtWhy4" runat="server" TextMode="MultiLine" Width="90%" Height="50px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label33" ForeColor="White" runat="server" Text="Why5"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtWhy5" runat="server" TextMode="MultiLine" Width="90%" Height="50px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label24" ForeColor="White" runat="server" Text="Root Cause Category"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlRootCauseCategory" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlRootCauseCategory_SelectedIndexChanged"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RFRootcauseCat" runat="server" ErrorMessage="Please Select Root Cause Category" InitialValue="--Select--" ControlToValidate="ddlRootCauseCategory" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label25" ForeColor="White" runat="server" Text="Root Cause"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlRootCause" runat="server" AutoPostBack="true" Width="100%"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RFRootCause" runat="server" ErrorMessage="Please Select Root Cause" InitialValue="--Select--" ControlToValidate="ddlRootCause" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                    <asp:Label ID="Label21" runat="server" ForeColor="White" Text="Upload NCR \ FIN :"></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="uploadEvidenNCR" AllowMultiple="true" runat="server" />
                    <asp:RequiredFieldValidator ID="RFuploadNCR" runat="server" ErrorMessage="NCR\FIN upload is mandatory" ControlToValidate="uploadEvidenNCR" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.PDF|.png|.jpg|.jpeg|.gif|.xls|.xlsx|.DOC|.DOCX|.XLS|.XLSX|.PNG|.JPG|.JPEG|.GIF)$"
                        ControlToValidate="uploadEvidenNCR" runat="server" ForeColor="Red" ErrorMessage="Please select a valid word,excel,jpg,jpeg,png,gif or pdf file format."
                        Display="Dynamic" />
                </td>

                <br />
            </tr>
        </table>
    </div>
    <table style="width: 89%">
        <tr>
            <asp:Label ID="lblrb" runat="server"></asp:Label>
            <td style="text-align: center">
                <asp:Button ID="btnSave" Font-Bold="true" runat="server" OnClick="btnSave_Click" Text="Save" />
            </td>
        </tr>

    </table>
</div>

<asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>

<asp:Label ID="lblRCM" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblRCMID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblRCMEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPM" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPMID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPMEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblNewSBU" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblMainactivity" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblActivity" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblSubActivity" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblRootCause" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblHead" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblHeadID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblHeadEmail" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblPE" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPEID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPEEmail" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblAbc" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblAbc1" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblAbc2" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblPlanngEngName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPlanngEngEmail" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblProjectCode" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblFQEName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblFQEEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblTransactionID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblErrorNotification" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblUrbalCoE" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblInduCoE" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblbuheadid" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblbuheadName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblbuheadEmail" runat="server" Visible="false"></asp:Label>

<table id="rwcReq" runat="server" style="width: 89%; text-align: center" border="1" visible="false">
    <tr>
        <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
            <asp:Label ID="Label20" ForeColor="White" Font-Bold="true" runat="server" Text="Is Rework form required"></asp:Label>
            <asp:CheckBox ID="chkRWCrequired" AutoPostBack="true" runat="server" OnCheckedChanged="chkSWIrequired_CheckedChanged" />
        </td>

    </tr>
</table>
<table id="isNotSWI" runat="server" visible="false" style="width: 89%; text-align: center" border="1">
    <tr id="trSave" runat="server" style="text-align: center">
        <td>
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
        </td>

    </tr>
    <tr>
        <td>
            <asp:Label ID="lblMainSuccessNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" runat="server"></asp:Label>
        </td>
    </tr>
</table>

<table border="1" style="width: 89%; font-family: 'Trebuchet MS'; font-size: 14px" id="tblRWC" runat="server" visible="false">
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td>
                        <asp:Image ID="imgTataLogo" ImageUrl="../../_layouts/15/images/TataLogo.jpg" runat="server" />
                    </td>
                    <td style="width: 100%;">
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="Label12" runat="server" Text="TATA PROJECTS LTD" Font-Size="Large"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="Label13" runat="server" Text="REWORK CARD(RWC)" Font-Size="Large"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<div id="Div1" style="width: 89%" runat="server" visible="false">
    <table border="1" style="font-family: 'Trebuchet MS'; font-size: 14px" id="RWC1">
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label4" runat="server" Text="RWC No:" Style="font-weight: 600"></asp:Label>
                <asp:Label ID="lblRWCNO" runat="server"></asp:Label>
            </td>

            <td>
                <table>
                    <tr>
                        <td>&nbsp;<asp:Label ID="lblDateTime" runat="server" Text="Date" Style="font-weight: 600"></asp:Label></td>
                        <td>
                            <SharePoint:DateTimeControl ID="dtRWCdate" DateOnly="true" runat="server" Enabled="false" />
                        </td>
                    </tr>
                </table>


            </td>
        </tr>
        <tr style="height: 28px">
            <td>
                <asp:Label ID="Label14" runat="server" Style="font-weight: 600" Text="Project/Location :"></asp:Label>
                <asp:Label ID="lblProjectLocation" runat="server"></asp:Label>
            </td>
            <td>&nbsp;
            <asp:Label ID="lblTPLJob" runat="server" Style="font-weight: 600" Text="TPL Job No. :"></asp:Label>
                <asp:Label ID="lblTPLJobNo" runat="server" Text="Auto populated"></asp:Label>
            </td>

        </tr>
        <tr style="height: 28px">
            <td>
                <asp:Label ID="Label29" runat="server" Style="font-weight: 600" Text="Subcontractor:"></asp:Label>
                <asp:TextBox ID="txtSubcontractors" runat="server" Width="60%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFsubcontractor" runat="server" ErrorMessage="Sub Contractor details are mandatory" InitialValue="" ControlToValidate="txtSubcontractors" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                &nbsp;</td>
            <td class="auto-style3">&nbsp;<asp:Label ID="Label28" runat="server" Style="font-weight: 600" Text="P.O./W.O. No.:"></asp:Label>
                <asp:TextBox ID="txtPoWoNo" runat="server" Width="50%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFpowonumber" runat="server" ErrorMessage="PO \ WO Number is mandatory" InitialValue="" ControlToValidate="txtPoWoNo" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr style="height: 28px">
            <td colspan="2">
                <asp:Label ID="Label27" Font-Bold="true" runat="server" Text="Description of Package / Work :"></asp:Label>
            </td>

        </tr>
        <tr style="height: 28px">
            <td colspan="2">
                <asp:TextBox ID="txtDescof" TextMode="MultiLine" Width="95%" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFdescpackage" runat="server" ErrorMessage="Description of Package / Work is mandatory" InitialValue="" ControlToValidate="txtDescof" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr style="height: 28px">
            <td colspan="2">
                <asp:Label ID="Label2" runat="server" Style="font-weight: 600" Text="NCR \ FIN \ Customer Complaint reference:"></asp:Label>
                <asp:TextBox ID="txtNCRFINCC" runat="server" Width="30%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFncrfincc" runat="server" ErrorMessage="NCR \ FIN \ Customer Complaint reference is mandatory" InitialValue="" ControlToValidate="txtNCRFINCC" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                &nbsp;
                   <%-- <asp:Label ID="Label2" runat="server" Text="Rework due to :" style="font-weight: 600"></asp:Label>
                    <asp:RadioButton ID="rdCustomerComplaint" GroupName="RWC" Text="Customer Complaint" runat="server" />
                    <asp:RadioButton ID="rdNCR" GroupName="RWC" Text="NCR" runat="server" />
                    <asp:RadioButton ID="rdFIN" GroupName="RWC" Text="FIN" runat="server" />
                    <asp:RadioButton ID="rdNote" GroupName="RWC" Text="Note" runat="server" />
                    <asp:RadioButton ID="rdEmail" GroupName="RWC" Text="E-mail Reference" runat="server" />--%>
            </td>
        </tr>
        <tr style="height: 28px">
            <td colspan="2">
                <asp:Label ID="lbldesc" runat="server" Text="Details of Non-Conformity / Discrepancy :" Style="font-weight: 700"></asp:Label>
            </td>
        </tr>
        <tr style="height: 28px">
            <td colspan="2">
                <asp:TextBox ID="txtNCDetails" TextMode="MultiLine" Width="95%" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFncdetails" runat="server" ErrorMessage="Details of Non-Conformity / Discrepancy are mandatory" InitialValue="" ControlToValidate="txtNCDetails" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr style="height: 28px">
            <td colspan="2">
                <asp:Label ID="lblNCRootCause" runat="server" Text="Root Cause for Non-Conformity / Discrepancy :" Style="font-weight: 700"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBoxList ID="rootCauseForNon" runat="server" RepeatLayout="table" RepeatColumns="2" RepeatDirection="vertical" Style="width: 800px">
                    <asp:ListItem Text="Defect Material (Supply / Procured at Site)" Value="Defect Material (Supply / Procured at Site)"></asp:ListItem>
                    <asp:ListItem Text="Engineering Error" Value="Engineering Error"></asp:ListItem>
                    <asp:ListItem Text="Defective workmanship of Sub-Contractor" Value="Defective workmanship of Sub-Contractor"></asp:ListItem>
                    <asp:ListItem Text="Defective workmanship of TPL In-house" Value="Defective workmanship of TPL In-house"></asp:ListItem>
                    <asp:ListItem Text="Missmatch between Civil & Mech. Drawings" Value="Missmatch between Civil & Mech. Drawings"></asp:ListItem>
                    <asp:ListItem Text="Modifications asked by Client" Value="Modifications asked by Client"></asp:ListItem>
                    <asp:ListItem Text=" Mis-handling of Materials / Equipments" Value=" Mis-handling of Materials / Equipments"></asp:ListItem>
                    <asp:ListItem Text="TPL Scope of work increased" Value="TPL Scope of work increased"></asp:ListItem>
                    <asp:ListItem Text="Drawing Revisions after completion of works" Value="Drawing Revisions after completion of works"></asp:ListItem>
                    <asp:ListItem Text="Inspection Error" Value="Inspection Error"></asp:ListItem>
                </asp:CheckBoxList>
                <asp:CustomValidator runat="server" ID="cvrootcause" ClientValidationFunction="ValidateRootCause" ErrorMessage="Please select one or more of the Root Cause for Non-Conformity / Discrepancy" CssClass="required" Display="Dynamic" ></asp:CustomValidator>
            </td>
        </tr>
    </table>
</div>
<table id="isSWISubmit" runat="server" visible="false" style="width: 89%; text-align: center" border="1">
    <tr>
        <td colspan="2" style="text-align: center">
            <asp:Button ID="fqesubmit" runat="server" Font-Bold="true" Text="Submit" OnClick="fqesubmit_Click" />
            <%--<asp:Button ID="fqeCancel" runat="server" Font-Bold="true" Text="Cancel" />--%>
        </td>
    </tr>
</table>

<div id="Div2" runat="server" visible="false" style="width: 89%">
    <table border="1" style="width: 80%; font-family: 'Trebuchet MS'; font-size: 14px" id="RWC2">
        <tr>
            <td colspan="2">
                <asp:Label ID="RWCDetails" runat="server" Text="Details of Reworks : " Style="font-weight: 700"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox ID="txtRWCdetails" Width="96%" TextMode="MultiLine" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label15" runat="server" Text="Approximate cost of Rework :"></asp:Label>
                INR:<asp:TextBox ID="txtINR" runat="server"></asp:TextBox>
                Number field:<asp:TextBox ID="txtNumberField" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="Button3" runat="server" Font-Bold="true" Text="Submit" />
                <asp:Button ID="Button4" runat="server" Font-Bold="true" Text="Cancel" />
            </td>
        </tr>
    </table>
</div>
<div id="Div3" runat="server" visible="false" style="width: 89%">
    <table border="1" style="width: 80%; font-family: 'Trebuchet MS'; font-size: 14px" id="RWC3">
        <tr>
            <td colspan="2">
                <asp:Label ID="Label16" runat="server" Text="Quality of Rework after completion :" Style="font-weight: 700"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox ID="txtRCM" runat="server" Width="95%" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label17" runat="server" Text="Approximate cost of Rework :"></asp:Label>
                INR:<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>Number field:<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnSWISubmit" runat="server" Font-Bold="true" Text="Submit" />
                <asp:Button ID="btnSWIcancel" runat="server" Font-Bold="true" Text="Cancel" />
            </td>
        </tr>
    </table>
</div>

<asp:HyperLink ID="hplNavigateUrl" runat="server" Target="_self" Visible="false">Here</asp:HyperLink>
<div id="dvUnAuth" runat="server" visible="false">
    <asp:Label ID="Label18" runat="server" ForeColor="Red" Font-Size="Large" Text="You are not authorized to access"></asp:Label>
</div>
<table>
    <tr>
        <td>
            <asp:Label ID="lblSWIsuccessNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblLinkClosureNotification" runat="server"></asp:Label>
        </td>
    </tr>
</table>



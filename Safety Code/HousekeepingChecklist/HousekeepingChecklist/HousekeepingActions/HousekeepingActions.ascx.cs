﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
namespace HousekeepingChecklist.HousekeepingActions
{
    [ToolboxItemAttribute(false)]
    public partial class HousekeepingActions : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public HousekeepingActions()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        private bool IsLoggedInAuthorisedUser(string email)
        {
            bool isValid = SPContext.Current.Web.CurrentUser.Email.Equals(email);
            return isValid;
        }
        SafetyChecklists objSafety = new SafetyChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            dvFilledForm.Visible = true;
            if (!this.Page.IsPostBack)
            {
                if (this.Page.Request.QueryString["Tid"] != null)
                {
                    lblTransactionID.Text = this.Page.Request.QueryString["Tid"].ToString();
                    DataTable dtdata = objSafety.GetChecklistDatabyTransactionIDSafety(lblTransactionID.Text);
                    if (dtdata.Rows.Count > 0)
                    {
                        string rcmEmail = string.Empty;
                        string assigneeEmail = string.Empty;
                        bool mainFormLogic = true;
                        string stage = string.Empty;
                        if (dtdata.Rows[0]["Stage"] != null)
                        {
                            stage = dtdata.Rows[0]["Stage"].ToString();
                        }
                        if (stage.Equals("Pending with RCM"))
                        {
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                rcmEmail = dtdata.Rows[0]["RCM_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(rcmEmail))
                                {
                                    //dvFilledForm.Visible = true;
                                    mainFormLogic = true;
                                    RCMSubmit.Visible = true;
                                    dvRCM.Visible = true;
                                    SOSubmit.Visible = false;
                                    dvRCMClose.Visible = false;
                                }
                                else
                                {
                                    mainFormLogic = true;
                                    RCMSubmit.Visible = false;
                                    dvRCM.Visible = true;
                                    SOSubmit.Visible = false;
                                    dvRCMClose.Visible = false;
                                }
                            }
                        }
                        else if (stage.Equals("Pending with Assignee"))
                        {
                            if (dtdata.Rows[0]["Assignee_Email"] != null)
                            {
                                assigneeEmail = dtdata.Rows[0]["Assignee_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(assigneeEmail))
                                {
                                    SOSubmit.Visible = true;
                                    mainFormLogic = true;
                                    RCMSubmit.Visible = false;
                                    dvRCM.Visible = true;
                                    SetRCMActionDetails();
                                    txtRCMRemarks.Enabled = false;
                                    dtTargetClosureDate.Visible = false;
                                    dvRCMClose.Visible = false;
                                }
                                else
                                {
                                    SOSubmit.Visible = false;
                                    mainFormLogic = true;
                                    RCMSubmit.Visible = false;
                                    dvRCM.Visible = true;
                                    SetRCMActionDetails();
                                    txtRCMRemarks.Enabled = false;
                                    dtTargetClosureDate.Visible = false;
                                    dvRCMClose.Visible = false;
                                }
                            }
                        }
                        else if (stage.Equals("Pending for Closure"))
                        {
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                rcmEmail = dtdata.Rows[0]["RCM_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(rcmEmail))
                                {
                                    mainFormLogic = true;
                                    RCMSubmit.Visible = false;
                                    dvRCM.Visible = true;
                                    SOSubmit.Visible = false;
                                    SetRCMActionDetails();
                                    txtRCMRemarks.Enabled = false;
                                    dtTargetClosureDate.Visible = false;
                                    dvRCMClose.Visible = true;
                                }
                                else
                                {
                                    mainFormLogic = true;
                                    RCMSubmit.Visible = false;
                                    dvRCM.Visible = true;
                                    SOSubmit.Visible = false;
                                    SetRCMActionDetails();
                                    txtRCMRemarks.Enabled = false;
                                    dtTargetClosureDate.Visible = false;
                                    dvRCMClose.Visible = false;
                                }
                            }
                        }
                        else
                        {
                            mainFormLogic = true;
                            RCMSubmit.Visible = false;
                            dvRCM.Visible = true;
                            SOSubmit.Visible = false;
                            dvRCM.Visible = false;
                            SetRCMActionDetails();
                            txtRCMRemarks.Enabled = false;
                            dtTargetClosureDate.Visible = false;
                            dvRCMClose.Visible = false;
                        }
                        if (mainFormLogic)
                        {
                            if (dtdata.Rows[0]["BUName"] != null)
                            {
                                lblBU.Text = dtdata.Rows[0]["BUName"].ToString();
                            }
                            lblfrequency.Text = "Daily";
                            lblchecklist.Text = "Housekeeping";
                            if (dtdata.Rows[0]["SBUName"] != null)
                            {
                                lblNewSBU.Text = dtdata.Rows[0]["SBUName"].ToString();
                                if (lblNewSBU.Text == "Industrial Infrastructure")
                                {
                                    lblInduCoE.Text = "coesheii@tataprojects.com";
                                }
                                else if (lblNewSBU.Text == "Urban Infrastructure")
                                {
                                    lblInduCoE.Text = "coesheii@tataprojects.com";
                                }
                                else
                                {
                                    lblInduCoE.Text = "coesheii@tataprojects.com";
                                }
                            }
                            if (dtdata.Rows[0]["ProjectName"] != null)
                            {
                                lblProject.Text = dtdata.Rows[0]["ProjectName"].ToString();
                            }
                            if (dtdata.Rows[0]["Stage"] != null)
                            {
                                lblcurstage.Text = dtdata.Rows[0]["Stage"].ToString();
                            }
                            if (dtdata.Rows[0]["Frequency"] != null)
                            {
                                lblfrequency.Text = dtdata.Rows[0]["Frequency"].ToString();
                            }
                            if (dtdata.Rows[0]["Checklist Name"] != null)
                            {
                                lblchecklist.Text = dtdata.Rows[0]["Checklist Name"].ToString();
                            }
                            if (dtdata.Rows[0]["Inspection Date"] != null)
                            {
                                lblinsdate.Text = Convert.ToDateTime(dtdata.Rows[0]["Inspection Date"]).ToShortDateString();
                            }
                            //if (dtdata.Rows[0]["Target Closure Date"] != null)
                            //{
                            //    lbltargetdate.Text = dtdata.Rows[0]["Target Closure Date"].ToString();
                            //}
                            if (dtdata.Rows[0]["RCM_ID"] != null)
                            {
                                lblRCMID.Text = dtdata.Rows[0]["RCM_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Name"] != null)
                            {
                                lblRCMName.Text = dtdata.Rows[0]["RCM_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                lblRCMEmail.Text = dtdata.Rows[0]["RCM_Email"].ToString();
                            }
                            if (dtdata.Rows[0]["PM_ID"] != null)
                            {
                                lblPMID.Text = dtdata.Rows[0]["PM_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["PM_Name"] != null)
                            {
                                lblPMName.Text = dtdata.Rows[0]["PM_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["PM_Email"] != null)
                            {
                                lblPMEmail.Text = dtdata.Rows[0]["PM_Email"].ToString();
                            }
                            if (dtdata.Rows[0]["SO_ID"] != null)
                            {
                                lblSOID.Text = dtdata.Rows[0]["SO_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["SO_Name"] != null)
                            {
                                lblSOName.Text = dtdata.Rows[0]["SO_Name"].ToString();
                                lblselectAssignee.Text = dtdata.Rows[0]["SO_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["SO_Email"] != null)
                            {
                                lblSOEmail.Text = dtdata.Rows[0]["SO_Email"].ToString();
                            }
                            DataTable dtChildData = objSafety.GetLocationComplianceDatabyTransactionID(lblTransactionID.Text);
                            DataTable dtFilledData = objSafety.GetLocationComplianceDataFilled(lblTransactionID.Text);
                            if (dtFilledData != null)
                            {
                                GridView1.Visible = true;
                                GridView1.DataSource = dtFilledData;
                                GridView1.DataBind();
                            }
                            if (dtChildData != null)
                            {
                                grdCompliances.DataSource = dtChildData;
                                grdCompliances.DataBind();
                                DataTable dtEmptyEvidence = objSafety.GetEmptyLocationEvidenceDetails(lblTransactionID.Text);
                                if (dtEmptyEvidence != null)
                                {
                                    foreach (DataRow row in dtEmptyEvidence.Rows)
                                    {
                                        foreach (GridViewRow gvr in grdCompliances.Rows)
                                        {
                                            string location = gvr.Cells[0].Text;
                                            if (row["Location"].ToString() == location)
                                            {
                                                FileUpload fu = (FileUpload)gvr.FindControl("uploadCorrectiveEvidence");
                                                fu.Enabled = true;
                                                TextBox txtRemarks = (TextBox)gvr.FindControl("txtActionDetails");
                                                txtRemarks.Enabled = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        dvUnAuth.Visible = true;
                        dvFilledForm.Visible = false;
                        dvRCM.Visible = false;
                        RCMSubmit.Visible = false;
                    }
                }
            }
        }

        private void SetRCMActionDetails()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select [Target Closure Date],[RCM Comments] from EHSChecklist where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["Target Closure Date"] != null)
                        {

                            DateTime dtDate = Convert.ToDateTime(dr["Target Closure Date"]);
                            lblTargetClosureDate.Text = dtDate.ToString("dd/MM/yyyy");
                        }
                        if (dr["RCM Comments"] != null)
                        {
                            txtRCMRemarks.Text = dr["RCM Comments"].ToString();
                        }

                    }
                }
                else
                {

                }
                con.Close();
            }
            catch (Exception exp)
            {

            }


        }

        protected void lnkView_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            LinkButton btn = sender as LinkButton;
            string docName = btn.Text;

            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select PicEvidence_DocName,PicEvidence_DocContentType,PicEvidence from EHSChecklist_Details where Transaction_ID='" + lblTransactionID.Text + "'and PicEvidence_DocName='" + docName + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["PicEvidence_DocName"].ToString();
                    contentType = sdr["PicEvidence_DocContentType"].ToString();
                    bytes = (byte[])sdr["PicEvidence"];
                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
            }


            catch (Exception ex)
            {

            }
           
        }

        protected void lnkView1_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            LinkButton btn = sender as LinkButton;
            string docName = btn.Text;

            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select CorrEvidence_DocName,CorrEvidence_DocContentType,CorrEvidence from EHSChecklist_Details where Transaction_ID='" + lblTransactionID.Text + "'and CorrEvidence_DocName='" + docName + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["CorrEvidence_DocName"].ToString();
                    contentType = sdr["CorrEvidence_DocContentType"].ToString();
                    bytes = (byte[])sdr["CorrEvidence"];
                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
            }


            catch (Exception ex)
            {

            }

        }

        protected void btnRCMActions_Click(object sender, EventArgs e)
        {
            if (txtRCMRemarks.Text != "")
            {
                bool isSaved = UpdateRCMActions();
                if (isSaved)
                {
                    string targetdate = string.Empty;
                    DataTable dtdata = objSafety.GetChecklistDatabyTransactionIDSafety(lblTransactionID.Text);
                    if (dtdata.Rows[0]["Assignee_Name"] != null)
                    {
                        lblAssigneName.Text = dtdata.Rows[0]["Assignee_Name"].ToString();
                    }
                    if (dtdata.Rows[0]["Assignee_Email"] != null)
                    {
                        lblAssigneEmail.Text = dtdata.Rows[0]["Assignee_Email"].ToString();
                    }
                    if (dtdata.Rows[0]["Target Closure Date"] != null)
                    {
                        targetdate = dtdata.Rows[0]["Target Closure Date"].ToString();
                    }
                    objSafety.SendAssigneeMailSafetyHousekeeping(lblRCMName.Text, lblRCMEmail.Text, lblAssigneEmail.Text, lblSOName.Text, lblProject.Text, lblchecklist.Text, lblAssigneName.Text, targetdate, txtRCMRemarks.Text, lblPMEmail.Text, lblInduCoE.Text, lblTransactionID.Text);
                    btnRCMActions.Enabled = false;

                    lblRCMSubNotification.Text = "Submitted successfully";
                    lblError.Text = "";
                    string navigateUrl = Constants.link + "Pages/EHSRCMView.aspx";
                    lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
                }
            }
        }

        private bool UpdateRCMActions()
        {
            bool isSaved = false;
            DataTable dtDiscipline = new DataTable();
            DateTime dtSelected = dtTargetClosureDate.SelectedDate;
            string targetdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dtSelected);
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                DateTime today = DateTime.Now;
                string LastStageUpdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                String sqlQuery = "update EHSChecklist set Stage='Pending with Assignee', Assignee_ID='" + lblSOID.Text + "',Assignee_Name='" + lblSOName.Text + "',Assignee_Email='" + lblSOEmail.Text + "',[Target Closure Date]='" + targetdate + "',[RCM Comments]='" + txtRCMRemarks.Text + "',[RCM Submission Date]='" + LastStageUpdate + "',LastStageUpdate='" + LastStageUpdate + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                isSaved = true;
                con.Close();

            }
            catch (Exception exp)
            {

            }

            return isSaved;
        }

        protected void btnSOActions_Click(object sender, EventArgs e)
        {
            DataTable dt = objSafety.GetEmptyCorrectiveEvidenceDetails(lblTransactionID.Text);
            foreach (DataRow row in dt.Rows)
            {
                foreach (GridViewRow gvr in grdCompliances.Rows)
                {
                    //string location = GridView1.Rows[gvr.RowIndex].Cells[0].Text;
                    string location = gvr.Cells[0].Text;
                    if (row["Location"].ToString() == location)
                    {
                        TextBox txtRemarks = (TextBox)gvr.FindControl("txtActionDetails");
                        FileUpload fu = (FileUpload)gvr.FindControl("uploadCorrectiveEvidence");
                        string filePath = fu.PostedFile.FileName;
                        string filename = Path.GetFileName(filePath);
                        string ext = Path.GetExtension(filename);
                        string contenttype = String.Empty;
                        switch (ext)
                        {
                            case ".doc":
                                contenttype = "application/vnd.ms-word";
                                break;
                            case ".docx":
                                contenttype = "application/vnd.ms-word";
                                break;
                            case ".xls":
                                contenttype = "application/vnd.ms-excel";
                                break;
                            case ".xlsx":
                                contenttype = "application/vnd.ms-excel";
                                break;
                            case ".jpg":
                                contenttype = "image/jpg";
                                break;
                            case ".png":
                                contenttype = "image/png";
                                break;
                            case ".gif":
                                contenttype = "image/gif";
                                break;
                            case ".pdf":
                                contenttype = "application/pdf";
                                break;
                        }
                        Stream fs = fu.PostedFile.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        String strQuery = "update EHSChecklist_Details set CorrEvidence_DocName=@CorrEvidence_DocName,CorrEvidence_DocContentType=@CorrEvidence_DocContentType,CorrEvidence=@CorrEvidence,Correction_Details=@Correction_Details where Transaction_ID='" + lblTransactionID.Text + "' and Location='" + location + "'";
                        try
                        {
                            SqlCommand cmd = new SqlCommand(strQuery);
                            cmd.Parameters.AddWithValue("@CorrEvidence_DocName", filename);
                            cmd.Parameters.AddWithValue("@CorrEvidence_DocContentType", contenttype);
                            cmd.Parameters.AddWithValue("@CorrEvidence", bytes);
                            cmd.Parameters.AddWithValue("@Transaction_ID", lblTransactionID.Text);

                            cmd.Parameters.AddWithValue("@Correction_Details", txtRemarks.Text);

                            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                        catch (Exception ex)
                        {
                        }
                        fu.Enabled = false;
                    }
                }
            }
            //btnSOActions.Enabled = false;
            //lblAsigneeSubNotification.Text = "You have submitted successfully";
            lblError.Text = "";
            //lblRCMSubNotification.Text = "Submitted successfully";
            //string navigateUrl = Constants.link + "Pages/EHSAssigneeView.aspx";
            //lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
            this.Page.Response.Redirect("http://tplhydhospuat/SitePages/Housekeeping%20Test.aspx?Tid=" + lblTransactionID.Text + "");
        }

        protected void btnSOSubmit_Click(object sender, EventArgs e)
        {
            DataTable dt = objSafety.GetEmptyCorrectiveEvidenceDetails(lblTransactionID.Text);
            foreach (DataRow row in dt.Rows)
            {
                foreach (GridViewRow gvr in grdCompliances.Rows)
                {
                    //string location = GridView1.Rows[gvr.RowIndex].Cells[0].Text;
                    string location = gvr.Cells[0].Text;
                    if (row["Location"].ToString() == location)
                    {
                        TextBox txtRemarks = (TextBox)gvr.FindControl("txtActionDetails");
                        FileUpload fu = (FileUpload)gvr.FindControl("uploadCorrectiveEvidence");
                        string filePath = fu.PostedFile.FileName;
                        string filename = Path.GetFileName(filePath);
                        string ext = Path.GetExtension(filename);
                        string contenttype = String.Empty;
                        switch (ext)
                        {
                            case ".doc":
                                contenttype = "application/vnd.ms-word";
                                break;
                            case ".docx":
                                contenttype = "application/vnd.ms-word";
                                break;
                            case ".xls":
                                contenttype = "application/vnd.ms-excel";
                                break;
                            case ".xlsx":
                                contenttype = "application/vnd.ms-excel";
                                break;
                            case ".jpg":
                                contenttype = "image/jpg";
                                break;
                            case ".png":
                                contenttype = "image/png";
                                break;
                            case ".gif":
                                contenttype = "image/gif";
                                break;
                            case ".pdf":
                                contenttype = "application/pdf";
                                break;
                        }
                        Stream fs = fu.PostedFile.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        String strQuery = "update EHSChecklist_Details set CorrEvidence_DocName=@CorrEvidence_DocName,CorrEvidence_DocContentType=@CorrEvidence_DocContentType,CorrEvidence=@CorrEvidence,Correction_Details=@Correction_Details where Transaction_ID='" + lblTransactionID.Text + "' and Location='" + location + "'";
                        try
                        {
                            SqlCommand cmd = new SqlCommand(strQuery);
                            cmd.Parameters.AddWithValue("@CorrEvidence_DocName", filename);
                            cmd.Parameters.AddWithValue("@CorrEvidence_DocContentType", contenttype);
                            cmd.Parameters.AddWithValue("@CorrEvidence", bytes);
                            cmd.Parameters.AddWithValue("@Transaction_ID", lblTransactionID.Text);

                            cmd.Parameters.AddWithValue("@Correction_Details", txtRemarks.Text);

                            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                        catch (Exception ex)
                        {
                        }
                        fu.Enabled = false;
                    }
                }
            }
            SaveAssigeeActions();
            objSafety.SendRCMForClosureMailHousekeeping(lblRCMName.Text, lblRCMEmail.Text, lblSOName.Text, lblSOEmail.Text, lblProject.Text, lblchecklist.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
            btnSOSubmit.Enabled = false;
            btnSOActions.Enabled = false;
            lblAsigneeSubNotification.Text = "You have submitted successfully";
            lblError.Text = "";
            //lblRCMSubNotification.Text = "Submitted successfully";
            string navigateUrl = Constants.link + "Pages/EHSAssigneeView.aspx";
            lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
        }

        private void SaveAssigeeActions()
        {
            DataTable dtDiscipline = new DataTable();
            DateTime dtSelected = dtTargetClosureDate.SelectedDate;
            string targetdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dtSelected);
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                DateTime today = DateTime.Now;
                string LastStageUpdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                String sqlQuery = "update EHSChecklist set Stage='Pending for Closure',[Assignee Submission Date]='" + LastStageUpdate + "',LastStageUpdate='" + LastStageUpdate + "' where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                con.Close();
            }
            catch (Exception exp)
            {

            }
        }

        protected void btnClosure_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDiscipline = new DataTable();
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                DateTime today = DateTime.Now;
                string LastStageUpdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                string sqlQuery = "update EHSChecklist set Stage='Closed',LastStageUpdate='" + LastStageUpdate + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                con.Close();
                btnClosure.Enabled = false;
                //lblFQEEmail.Text
                objSafety.SendFinalClosureMailHousekeeping(lblRCMName.Text, lblSOEmail.Text, lblSOName.Text, lblSOEmail.Text, lblProject.Text, lblchecklist.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                lblRCMSubNotification.Text = "Submitted successfully";
                lblError.Text = "";
                string navigateUrl = Constants.link + "Pages/EHSRCMView.aspx";
                lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
            }
            catch (Exception exp)
            {
                //lblError.Text = exp.Message;
            }
        }

    }
}

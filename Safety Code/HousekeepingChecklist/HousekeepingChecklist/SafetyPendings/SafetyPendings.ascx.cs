﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Data;

namespace HousekeepingChecklist.SafetyPendings
{
    [ToolboxItemAttribute(false)]
    public partial class SafetyPendings : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public SafetyPendings()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        SafetyChecklists objSafety = new SafetyChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                Tab1.CssClass = "Clicked";
                //DataTable dtData = objSafety.GetPEPendingItemsforSO(SPContext.Current.Web.CurrentUser.Email, "Pending with PE");
                //if (dtData.Rows.Count > 0)
                //{
                //    dvSO.Visible = false;
                //    dvRCM.Visible = false;
                //    dvAssignee.Visible = false;
                //    dvRCMClose.Visible = false;
                //    grdPendings.Visible = true;
                //    grdPendings.DataSource = dtData;
                //    grdPendings.DataBind();
                //}
                //else
                //{
                //    dvSO.Visible = true;
                //    dvRCM.Visible = false;
                //    dvAssignee.Visible = false;
                //    dvRCMClose.Visible = false;
                //    grdPendings.Visible = false;
                //}

            }
        }

        protected void Tab1_Click(object sender, EventArgs e)
        {

        }

        protected void Tab2_Click(object sender, EventArgs e)
        {

        }

        protected void Tab3_Click(object sender, EventArgs e)
        {

        }

        protected void Tab4_Click(object sender, EventArgs e)
        {

        }
    }
}

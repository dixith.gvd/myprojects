﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualityUploadDetails
{
    class Program
    {
        static void Main(string[] args)
        {
            UploadDetails objRep = new UploadDetails();
            Console.WriteLine("Started");
            objRep.StartProcess();
            Console.WriteLine("Completed...");
        }
    }
}

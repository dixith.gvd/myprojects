﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using Renci.SshNet;
using System.Net;
using System.Net.Mail;
using System.Collections;
namespace CSVToServer
{
    class Program
    {
        static string connectionString = ConfigurationManager.AppSettings["ConnectionStringSql"].ToString();
        static string logsPath = ConfigurationManager.AppSettings["logsPath"].ToString();
        static StringBuilder sb = new StringBuilder();
        //static ArrayList emplist = new ArrayList();

        static void Main(string[] args)
        {
            //string tdate = DateTime.Today.AddDays(-3).ToString("yyyy-MM-dd");
            DataTable dt = new DataTable();
            //string sqlQuery = "select * from AllEmployeeData_Prod where LastWorkingDay>'" + tdate + "'";
            string sqlQuery = "select * from TPLEmployeeMaster_08032021 where LastWorkingDay=CONVERT(date, getdate()) or (RTRIM(ActiveStatus)='Inactive' and MovementType='Exit' and CONVERT(date, LastupdatedDate)=CONVERT(date, getdate()) and LastWorkingDay<CONVERT(date, getdate()))";

            //string strdelim = ",";

            //sb.Append("TPL_Emp_ID,First_Name,Middle_Name,Last_Name,Title,Gender,Date_of_Birth,Mobile_Number,Email_Id,Last_Working_Day,Date_of_Joining,SBG_Name,SBU_Name,BU_Name,Location,State,Grade,Designation,Parent_Dept_Name,Department,Manager_Emp_ID,Active_Status,Movement_Type,Last_Updated_Date");
            sb.Append("emailaddress");

            sb.Append("\r\n");
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, conn))
                    {
                        conn.Open();
                        daAdap.Fill(dt);
                        //foreach (DataRow row in dt.Rows)
                        if (dt.Rows.Count > 0)
                        {
                            DataRowCollection row = dt.Rows;
                            for (int i = 0; i < row.Count; i++)
                            {
                                bool sentcsv = false;

                                if (row[i][8].ToString() != "")
                                {
                                    if (row[i][0].ToString() != "TPL6275")
                                    {
                                        sentcsv = true;
                                        string empid = string.Empty; string emailid = string.Empty; string lastworkingday = string.Empty;
                                        string logdate = string.Empty;

                                        empid = row[i][0].ToString();
                                        emailid = row[i][8].ToString();
                                        lastworkingday = row[i][9].ToString();
                                        Logging(empid, emailid, lastworkingday);

                                        //emplist.Add(empid);
                                        
                                        sb.Append(row[i][8].ToString().Replace(",", " "));
                                        

                                        sb.Append("\r\n");
                                    }
                                    // UpdateCSVStatus(sentcsv, row[i][0].ToString());
                                }
                            }
                        }
                        else
                        {
                            SendMail("No records found", "There are no email ids found with last working day as tomorrow");
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                ReminderBackupLog(exp.Message + ", while getting data from Vendor Table");
                SendMail("Error while generating the CSV File", exp.Message.ToString());
            }
            string dtFormat = String.Format("{0:ddMMMMyyyy}", DateTime.Today);
            string path = logsPath + "Disable-Account" + ".csv";
            StreamWriter file = new StreamWriter(path);
            file.WriteLine(sb.ToString().TrimEnd(new char[] { '\r', '\n' }));
            file.Close();
            file.Dispose();
            MoveFiletoSFTP(path);
        }
        public static void MoveFiletoSFTP(string path)
        {
            FileStream stream = null;
            try
            {
                //prod code
                if (File.Exists(@"\\TPLHYDHOEHS1\Account Disable\Disable-Account.csv"))
                {
                    string dtFormat = String.Format("{0:ddMMMMyyyy_HHmmss}", DateTime.Now);
                    string newfname = "Disable-Account_" + dtFormat + ".csv";
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    File.Move(@"\\TPLHYDHOEHS1\Account Disable\Disable-Account.csv", @"\\TPLHYDHOEHS1\Account Disable\" + newfname + "");
                    File.Copy(path, @"\\TPLHYDHOEHS1\Account Disable\Disable-Account.csv");
                    SendMail("CSV Moved to server", "CSV file is moved to the following server path : \\\\TPLHYDHOEHS1\\Account Disable for below Emails : ");
                }
                else
                {
                    File.Copy(path, @"\\TPLHYDHOEHS1\Account Disable\Disable-Account.csv");
                    SendMail("CSV Moved to server", "CSV file is moved to the following server path : \\\\TPLHYDHOEHS1\\Account Disable for below Emails : ");
                }

                //test code 9th April 2021
                //if (File.Exists(@"\\TPLHYDHOEHS1\Account Disable\Disable-Account_test.csv"))
                //{
                //    string dtFormat = String.Format("{0:ddMMMMyyyy_HHmmss}", DateTime.Now);
                //    string newfname = "Disable-Account_test_" + dtFormat + ".csv";
                //    GC.Collect();
                //    GC.WaitForPendingFinalizers();
                //    File.Move(@"\\TPLHYDHOEHS1\Account Disable\Disable-Account_test.csv", @"\\TPLHYDHOEHS1\Account Disable\" + newfname + "");
                //    File.Copy(path, @"\\TPLHYDHOEHS1\Account Disable\Disable-Account_test.csv");
                //    SendMail("CSV Moved to server - testing", "CSV file is moved to the following server path : \\\\TPLHYDHOEHS1\\Account Disable for below Emails : ");
                //}
                //else
                //{
                //    File.Copy(path, @"\\TPLHYDHOEHS1\Account Disable\Disable-Account_test.csv");
                //    SendMail("CSV Moved to server - testing", "CSV file is moved to the following server path : \\\\TPLHYDHOEHS1\\Account Disable for below Emails : ");
                //}

                //UpdateEmailStatus(emplist);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
            }
            catch (Exception exp)
            {
                ReminderBackupLog(exp.Message + ", while moving the csv to server");
                SendMail("Error while moving the csv to server", exp.Message.ToString());
            }
        }
        private static void UpdateEmailStatus(ArrayList emplist)
        {
            SqlConnection con;

            try
            {
                foreach (string arrempid in emplist)
                {
                    using (con = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = con;
                            con.Open();
                            cmd.CommandText = "Update TPLEmployeeMaster_08032021 set Disabled='Yes' where TPLEmpID='" + arrempid + "'";

                            cmd.ExecuteNonQuery();
                            con.Close();
                        }

                    }
                }
            }
            catch (Exception exp)
            {
                ReminderBackupLog(exp.Message + ", while logging the mail Id's");
                SendMail("Error while logging the mail Id's", exp.Message.ToString());
            }
        }
        private static void Logging(string empid, string email, string lastworkingday)
        {
            //string finalstring = string.Empty;

            SqlConnection con;
            DateTime tdate = DateTime.Now;
            string date = String.Format("{0:yyyy-MM-dd HH:mm:ss}", tdate);

            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        con.Open();
                        cmd.CommandText = "insert into LogTable_TPLEmpMaster(TPLEmpID,Emailid,LastWorkingDay,LoggedDate) Values(@TPLEmpID,@Emailid,@LastWorkingDay,@LoggedDate)";
                        cmd.Parameters.Add("@LastWorkingDay", SqlDbType.DateTime);
                        cmd.Parameters.AddWithValue("@LoggedDate", date);
                        cmd.Parameters.AddWithValue("@TplEmpID", empid);
                        cmd.Parameters.AddWithValue("@Emailid", email);
                        if (lastworkingday == "" || lastworkingday == null)
                        {
                            cmd.Parameters["@LastWorkingDay"].Value = DBNull.Value;
                        }
                        else
                        {
                            DateTime lday = Convert.ToDateTime(lastworkingday);
                            lastworkingday = String.Format("{0:yyyy-MM-dd}", lday);
                            cmd.Parameters["@LastWorkingDay"].Value = lastworkingday;
                        }
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }

                }
            }
            catch (Exception exp)
            {
                ReminderBackupLog(exp.Message + ", while logging the mail Id's");
                SendMail("Error while logging the mail Id's", exp.Message.ToString());
            }
        }
        public static void ReminderBackupLog(string message)
        {
            try
            {
                string PathName = logsPath;
                PathName = PathName + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                StreamWriter sw = new StreamWriter(PathName, true);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception exp)
            {
            }
        }
        public static void SendMail(string text, string message)
        {
            try
            {
                // SenderMail = Exceldata.SenderMailID.ToString();

                MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
                //MailAddress SendTo = new MailAddress("BG Desk");

                MailMessage MyMessage = new MailMessage();
                string ToMail = "deekshithgvd-t@tataprojects.com;keerthi-t@tataprojects.com;srimanrao-t@tataprojects.com";
                if (!string.IsNullOrEmpty(ToMail))
                {
                    string strTo = ToMail.Trim();
                    string[] ToEmailids = strTo.Split(';');
                    foreach (string ToEmail in ToEmailids)
                    {
                        if (!MyMessage.To.Contains(new MailAddress(ToEmail)))
                        {
                            MyMessage.To.Add(new MailAddress(ToEmail));
                        }
                    }
                }
                MyMessage.Subject = text;
                MyMessage.Body = TextFile(message);
                //MyMessage.Body = MailBody.ToString();
                MyMessage.From = SendFrom;

                //MyMessage.CC.Add(data.initiatorEmail);
                try
                {
                    //emailClient.Send(MyMessage);
                    if (MyMessage != null)
                    {
                        SmtpClient emailClient = new SmtpClient();
                        emailClient.UseDefaultCredentials = false;
                        emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                        emailClient.Host = "smtp.office365.com";
                        emailClient.Port = 25;
                        emailClient.EnableSsl = true;
                        emailClient.TargetName = "STARTTLS/smtp.office365.com";
                        emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        emailClient.Send(MyMessage);

                    }

                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {

            }

        }
        public static String TextFile(string msg)
        {
            StringBuilder strbody = new StringBuilder();
            strbody.Append("Hi,");
            strbody.Append("\n");
            strbody.Append("\n");
            strbody.Append("" + msg + "");
            strbody.Append("\n");
            strbody.Append("" + sb.ToString() + "");
            strbody.Append("\n");
            strbody.Append("Thanks & Regards,");

            return strbody.ToString();
        }
    }
}

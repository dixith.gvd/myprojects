﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
namespace PlayAudioOnSchedule
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var file = GetFile();
            var lst = new string[] { ".mp3", ".avi", ".mp4", ".wmv" };
            if (!lst.Contains(Path.GetExtension(file)))
            {
                MessageBox.Show("Please select proper file.");
            }
            else
            {
                var result = SaveToDataBase(Path.GetFileName(file), GetCompressedData(file, ConvertFileToByteData(file)));
                if (result)
                {
                    cmbPlayList.Items.Add(Path.GetFileName(file));
                    MessageBox.Show("!! File Saved Successfully !!");
                }
            }
        }
        private string GetFile()
        {
            try
            {
                
                ofdPlayer.Filter = "Solution Files (*.*)|*.*";
                //ofdPlayer.InitialDirectory = @"E:\\";
                ofdPlayer.Multiselect = true;
                if (ofdPlayer.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    return ofdPlayer.FileName;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return string.Empty;
        }
    }
}

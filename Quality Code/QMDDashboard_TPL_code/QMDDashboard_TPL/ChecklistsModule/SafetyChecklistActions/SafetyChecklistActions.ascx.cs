﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Utilities;
namespace QMDDashboard_TPL.ChecklistsModule.SafetyChecklistActions
{
    [ToolboxItemAttribute(false)]
    public partial class SafetyChecklistActions : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public SafetyChecklistActions()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        [WebBrowsable(true), Category("Custom Properties"), Personalizable(PersonalizationScope.Shared), WebDisplayName("Stage"), WebDescription("Stage")]
        public string URLToAfterSubmission
        {
            get;
            set;
        }

        private bool IsLoggedInAuthorisedUser(string email)
        {
            bool isValid = SPContext.Current.Web.CurrentUser.Email.Equals(email);
            return isValid;
        }
        QualityChecklists objQuality = new QualityChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                if (this.Page.Request.QueryString["Tid"] != null)
                {
                    lblTransactionID.Text = this.Page.Request.QueryString["Tid"].ToString();
                    //lblTransID.Text = lblTransactionID.Text;
                    DataTable dtdata = objQuality.GetChecklistDatabyTransactionIDSafety(lblTransactionID.Text);
                    if (dtdata.Rows.Count > 0)
                    {

                        string rcmEmail = string.Empty;
                        string assigneeEmail = string.Empty;
                        bool mainFormLogic = true;
                        string stage = string.Empty;
                        string pmemail = string.Empty;
                        if (dtdata.Rows[0]["Stage"] != null)
                        {
                            stage = dtdata.Rows[0]["Stage"].ToString();
                        }

                        if (stage.Equals("Pending with Assignee"))
                        {
                            if (dtdata.Rows[0]["Assignee_Email"] != null)
                            {
                                assigneeEmail = dtdata.Rows[0]["Assignee_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(assigneeEmail))
                                {
                                    //dvFilledForm.Visible = true;
                                    dvAssignee.Visible = true;
                                    AssigneeActions.Visible = true;
                                    mainFormLogic = true;
                                    RCMActions.Visible = false;
                                    dvRCM.Visible = true;
                                    SetRCMActionDetails();
                                    txtRCMRemarks.Enabled = false;
                                    lblselectAssignee.Enabled = false;
                                    ppAssigneeName.Visible = false;
                                    dtTargetClosureDate.Visible = false;
                                }
                                else
                                {
                                    dvAssignee.Visible = true;
                                    AssigneeActions.Visible = false;
                                    mainFormLogic = true;
                                    RCMActions.Visible = false;
                                    dvRCM.Visible = true;
                                    SetRCMActionDetails();
                                    txtRCMRemarks.Enabled = false;
                                    lblselectAssignee.Enabled = false;
                                    ppAssigneeName.Visible = false;
                                    dtTargetClosureDate.Visible = false;
                                }
                            }
                        }
                        else if (stage.Equals("Pending with RCM"))
                        {
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                rcmEmail = dtdata.Rows[0]["RCM_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(rcmEmail))
                                {
                                    //dvFilledForm.Visible = true;
                                    mainFormLogic = true;
                                    dvAssignee.Visible = false;
                                    AssigneeActions.Visible = false;
                                    RCMActions.Visible = true;
                                    dvRCM.Visible = true;
                                }
                                else
                                {
                                    //dvUnAuth.Visible = true;
                                    //dvFilledForm.Visible = true;
                                    mainFormLogic = true;
                                    dvAssignee.Visible = false;
                                    AssigneeActions.Visible = false;
                                    RCMActions.Visible = false;
                                    dvRCM.Visible = true;
                                }

                            }
                        }
                        else if (stage.Equals("Closed"))
                        {
                            if (dtdata.Rows[0]["isCompliant"].ToString().Trim() != "Yes")
                            {
                                //dvFilledForm.Visible = true;
                                mainFormLogic = true;
                                dvAssignee.Visible = true;
                                AssigneeActions.Visible = false;
                                RCMActions.Visible = false;
                                dvRCM.Visible = true;
                                SetRCMActionDetails();
                                txtRCMRemarks.Enabled = false;
                                lblselectAssignee.Enabled = false;
                                ppAssigneeName.Visible = false;
                                dtTargetClosureDate.Visible = false;
                                SetAssigneeActionDetails();
                                GetEvidenceDocument();
                                assigneeRemarksTXT.Enabled = false;
                                assigneeEvidenceFUP.Visible = false;
                                dtclosure.Visible = false;
                                lblclosuredateassignee.Visible = true;
                                rbnno.Visible = false;
                                rbnyes.Visible = false;
                                lblselfcert.Visible = true;
                                btnAssigneeSubmit.Visible = false;
                            }
                            else
                            {
                                mainFormLogic = true;
                                dvAssignee.Visible = false;
                                AssigneeActions.Visible = false;
                                RCMActions.Visible = false;
                                dvRCM.Visible = false;
                            }
                        }
                        if (mainFormLogic)
                        {
                            if (dtdata.Rows[0]["BUName"] != null)
                            {
                                lblBU.Text = dtdata.Rows[0]["BUName"].ToString();

                            }
                            if (dtdata.Rows[0]["SBUName"] != null)
                            {
                                lblNewSBU.Text = dtdata.Rows[0]["SBUName"].ToString();
                                if (lblNewSBU.Text == "Industrial Infrastructure")
                                {
                                    lblInduCoE.Text = "coesheii@tataprojects.com";
                                }
                                else if (lblNewSBU.Text == "Urban Infrastructure")
                                {
                                    lblInduCoE.Text = "coesheii@tataprojects.com";
                                }
                                else
                                {
                                    lblInduCoE.Text = "coesheii@tataprojects.com";
                                }
                            }
                            if (dtdata.Rows[0]["ProjectName"] != null)
                            {
                                lblProject.Text = dtdata.Rows[0]["ProjectName"].ToString();
                            }
                            if (dtdata.Rows[0]["Location"] != null)
                            {
                                lblLocation.Text = dtdata.Rows[0]["Location"].ToString();
                            }
                            if (dtdata.Rows[0]["Stage"] != null)
                            {
                                lblcurstage.Text = dtdata.Rows[0]["Stage"].ToString();
                            }
                            if (dtdata.Rows[0]["Frequency"] != null)
                            {
                                lblfrequency.Text = dtdata.Rows[0]["Frequency"].ToString();
                            }
                            if (dtdata.Rows[0]["Checklist Name"] != null)
                            {
                                lblchecklist.Text = dtdata.Rows[0]["Checklist Name"].ToString();
                            }
                            if (dtdata.Rows[0]["Inspection Date"] != null)
                            {
                                lblinsdate.Text = Convert.ToDateTime(dtdata.Rows[0]["Inspection Date"]).ToShortDateString();
                            }
                            if (dtdata.Rows[0]["isCompliant"] != null)
                            {
                                lblcompliance.Text = dtdata.Rows[0]["isCompliant"].ToString();
                            }
                            if (dtdata.Rows[0]["SO Remarks"] != null)
                            {
                                lblobservations.Text = dtdata.Rows[0]["SO Remarks"].ToString();
                            }
                            if (dtdata.Rows[0]["Target Closure Date"] != null)
                            {
                                lbltargetdate.Text = dtdata.Rows[0]["Target Closure Date"].ToString();
                            }
                            if (dtdata.Rows[0]["Assignee Actual Closed Date"] != null)
                            {
                                lblclosuredate.Text = dtdata.Rows[0]["Assignee Actual Closed Date"].ToString();
                            }


                            if (dtdata.Rows[0]["RCM_ID"] != null)
                            {
                                lblRCMID.Text = dtdata.Rows[0]["RCM_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Name"] != null)
                            {
                                lblRCMName.Text = dtdata.Rows[0]["RCM_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                lblRCMEmail.Text = dtdata.Rows[0]["RCM_Email"].ToString();
                            }
                            if (dtdata.Rows[0]["PM_ID"] != null)
                            {
                                lblPMID.Text = dtdata.Rows[0]["PM_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["PM_Name"] != null)
                            {
                                lblPMName.Text = dtdata.Rows[0]["PM_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["PM_Email"] != null)
                            {
                                lblPMEmail.Text = dtdata.Rows[0]["PM_Email"].ToString();
                            }
                            if (dtdata.Rows[0]["SO_ID"] != null)
                            {
                                lblSOID.Text = dtdata.Rows[0]["SO_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["SO_Name"] != null)
                            {
                                lblSOName.Text = dtdata.Rows[0]["SO_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["SO_Email"] != null)
                            {
                                lblSOEmail.Text = dtdata.Rows[0]["SO_Email"].ToString();
                            }
                            if (dtdata.Rows[0]["Assignee_Email"] != null)
                            {
                                lblAssigneEmail.Text = dtdata.Rows[0]["Assignee_Email"].ToString();

                            }


                            GetAttachment();
                            GetChecklistDocument();
                        }
                    }
                    else
                    {
                        dvUnAuth.Visible = true;
                        dvFilledForm.Visible = false;
                        dvRCM.Visible = false;
                        RCMActions.Visible = false;
                        dvAssignee.Visible = false;
                        //dvRCMClose.Visible = false;
                    }
                }
            }
        }

        private void SetRCMActionDetails()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select Assignee_Name,[Target Closure Date],[RCM Comments] from EHSChecklist where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["Assignee_Name"] != null)
                        {
                            lblselectAssignee.Text = dr["Assignee_Name"].ToString();
                        }
                        if (dr["Target Closure Date"] != null)
                        {

                            DateTime dtDate = Convert.ToDateTime(dr["Target Closure Date"]);
                            lblTargetClosureDate.Text = dtDate.ToString("dd/MM/yyyy");
                        }
                        if (dr["RCM Comments"] != null)
                        {
                            txtRCMRemarks.Text = dr["RCM Comments"].ToString();
                        }

                    }
                }
                else
                {

                }
                con.Close();
            }
            catch (Exception exp)
            {

            }


        }

        private void SetAssigneeActionDetails()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select [Assignee Action Details],[Assignee Actual Closed Date],[Self Certification] from EHSChecklist where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["Assignee Action Details"] != null)
                        {
                            assigneeRemarksTXT.Text = dr["Assignee Action Details"].ToString();
                        }
                        if (dr["Assignee Actual Closed Date"] != null)
                        {

                            DateTime dtDate = Convert.ToDateTime(dr["Assignee Actual Closed Date"]);
                            lblclosuredateassignee.Text = dtDate.ToString("dd/MM/yyyy");
                        }
                        if (dr["Self Certification"] != null)
                        {
                            lblselfcert.Text = dr["Self Certification"].ToString();
                        }

                    }
                }
                else
                {

                }
                con.Close();
            }
            catch (Exception exp)
            {

            }


        }

        private void GetAttachment()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select NonComp_DocName from EHSChecklist where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["NonComp_DocName"] != null)
                        {
                            attachment.Text = dr["NonComp_DocName"].ToString();
                        }

                    }
                }
                else
                {
                    attachment.Text = "File not uploaded";
                }
                con.Close();
            }
            catch (Exception exp)
            {

            }

        }

        private void GetChecklistDocument()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select Checklist_DocName from EHSChecklist where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["Checklist_DocName"] != null)
                        {
                            chkdoc.Text = dr["Checklist_DocName"].ToString();
                        }

                    }
                }
                else
                {
                    chkdoc.Text = "File not uploaded";
                }
                con.Close();
            }
            catch (Exception exp)
            {

            }

        }

        private void GetEvidenceDocument()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select Evidence_DocName from EHSChecklist where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["Evidence_DocName"] != null)
                        {
                            Evidencedoc.Text = dr["Evidence_DocName"].ToString();
                        }

                    }
                }
                else
                {
                    Evidencedoc.Text = "File not uploaded";
                }
                con.Close();
            }
            catch (Exception exp)
            {

            }

        }
        private void SaveAssigeeActions()
        {
            try
            {
                DataTable dtDiscipline = new DataTable();
                string assremarks = assigneeRemarksTXT.Text;
                DateTime date = dtclosure.SelectedDate;
                string closuredate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", date);
                string selfcert = string.Empty;
                if (rbnyes.Checked)
                {
                    selfcert = rbnyes.Text;
                }
                if (rbnno.Checked)
                {
                    selfcert = rbnno.Text;
                }
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                DateTime today = DateTime.Now;
                string LastStageUpdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                string sqlQuery = "update EHSChecklist set Stage='Closed',[Assignee Actual Closed Date]='" + closuredate + "',[Assignee Action Details]='" + assremarks + "',[Assignee Self Certification]='" + selfcert + "',LastStageUpdate='" + LastStageUpdate + "',[Assignee Submission Date]='" + LastStageUpdate + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                con.Close();
            }
            catch (Exception exp)
            {
                //lblError.Text = exp.Message;
            }

        }


        private bool UpdateRCMActions()
        {
            bool isSaved = false;
            DataTable dtDiscipline = new DataTable();
            DateTime dtSelected = dtTargetClosureDate.SelectedDate;
            string targetdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dtSelected);
            try
            {
                PickerEntity pckEntity = (PickerEntity)ppAssigneeName.ResolvedEntities[0];
                if (pckEntity != null)
                {

                    string assignee = pckEntity.Key;
                    SPUser userAssignee = SPContext.Current.Web.EnsureUser(assignee);
                    if (userAssignee != null)
                    {
                        string assigneeID = objQuality.GetOnlyEmployeeID(userAssignee.LoginName);
                        string assigneeName = userAssignee.Name;
                        string assigneeEmail = userAssignee.Email;
                        lblAssigneEmail.Text = userAssignee.Email;
                        SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                        DateTime today = DateTime.Now;
                        string LastStageUpdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                        String sqlQuery = "update EHSChecklist set Stage='Pending with Assignee', Assignee_ID='" + assigneeID + "',Assignee_Name='" + assigneeName + "',Assignee_Email='" + assigneeEmail + "',[Target Closure Date]='" + targetdate + "',[RCM Comments]='" + txtRCMRemarks.Text + "',[RCM Submission Date]='" + LastStageUpdate + "',LastStageUpdate='" + LastStageUpdate + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                        SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                        daAdap.Fill(dtDiscipline);
                        isSaved = true;
                        con.Close();
                    }

                }
            }
            catch (Exception exp)
            {

            }

            return isSaved;

        }
        private void EvidenceDocument()
        {

            string filePath = assigneeEvidenceFUP.PostedFile.FileName;
            string filename = Path.GetFileName(filePath);
            string ext = Path.GetExtension(filename).ToLower();
            string contenttype = String.Empty;

            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
                case ".pptx":
                    contenttype = "application/pptx";
                    break;
                case ".jpeg":
                    contenttype = "image/jpeg";
                    break;
                case ".tif":
                    contenttype = "image/tif";
                    break;
                case ".tiff":
                    contenttype = "image/tiff";
                    break;
                case ".bmp":
                    contenttype = "image/bmp";
                    break;
                case ".rar":
                    contenttype = "application/rar";
                    break;
                case ".zip":
                    contenttype = "application/zip";
                    break;
            }
            if (contenttype != String.Empty)
            {

                Stream fs = assigneeEvidenceFUP.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                //Update the file into database
                string strQuery = "UPDATE EHSChecklist SET Evidence_Document = @Evidence_Document, Evidence_DocContentType = @Evidence_DocContentType, Evidence_DocName = @Evidence_DocName where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@Evidence_DocName", SqlDbType.VarChar).Value = filename;
                cmd.Parameters.Add("@Evidence_DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@Evidence_Document", SqlDbType.Binary).Value = bytes;
                InsertEvidenceDoc(cmd);
                // lblMessage.ForeColor = System.Drawing.Color.Green;
                // lblMessage.Text = "File Uploaded Successfully";
            }
            else
            {
                // lblMessage.ForeColor = System.Drawing.Color.Red;
                // lblMessage.Text = "File format not recognised." + " Upload Image/Word/PDF/Excel formats";
            }
        }
        private Boolean InsertEvidenceDoc(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void btnRCMActions_Click(object sender, EventArgs e)
        {
            if (ppAssigneeName.ResolvedEntities.Count != 0)
            {
                if (txtRCMRemarks.Text != "")
                {
                    if (dtTargetClosureDate.SelectedDate.Date >= DateTime.Today.Date || dtTargetClosureDate.SelectedDate.Date != DateTime.MinValue.Date)
                    {
                        bool isSaved = UpdateRCMActions();
                        if (isSaved)
                        {

                            string targetdate = string.Empty;
                            DataTable dtdata = objQuality.GetChecklistDatabyTransactionIDSafety(lblTransactionID.Text);
                            if (dtdata.Rows[0]["Assignee_Name"] != null)
                            {
                                lblAssigneName.Text = dtdata.Rows[0]["Assignee_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["Assignee_Email"] != null)
                            {
                                lblAssigneEmail.Text = dtdata.Rows[0]["Assignee_Email"].ToString();
                            }
                            if (dtdata.Rows[0]["Target Closure Date"] != null)
                            {
                                targetdate = dtdata.Rows[0]["Target Closure Date"].ToString();
                            }

                            objQuality.SendAssigneeMailSafety(lblRCMName.Text, lblRCMEmail.Text, lblAssigneEmail.Text, lblSOName.Text, lblProject.Text, lblchecklist.Text, lblAssigneName.Text, lblobservations.Text, targetdate, txtRCMRemarks.Text, lblPMEmail.Text, lblInduCoE.Text, lblTransactionID.Text);
                            //string message = "You have updated successfully";
                            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            //sb.Append("<script type = 'text/javascript'>");
                            //sb.Append("window.onload=function(){");
                            //sb.Append("alert('");
                            //sb.Append(message);
                            //sb.Append("')};");
                            //sb.Append("</script>");
                            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                            btnRCMActions.Enabled = false;

                            lblRCMSubNotification.Text = "Submitted successfully";
                            lblError.Text = "";
                            string navigateUrl = Constants.link + "Pages/EHSRCMView.aspx";
                            lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
                        }
                    }
                    else
                    {
                        lblError.Text = "Target closure date should be greater than or equal to Today";
                    }
                }
                else
                {
                    lblError.Text = "Please enter the remarks";
                }
            }
            else
            {
                lblError.Text = "Please select valid assignee name";
            }
        }

        protected void btnAssigneeSubmit_Click(object sender, EventArgs e)
        {
            dvAssignee.Visible = true;
            if (assigneeRemarksTXT.Text != "")
            {
                if (assigneeEvidenceFUP.HasFile)
                {
                    if (dtclosure.SelectedDate.Date != DateTime.MinValue.Date)
                    {
                        if (rbnyes.Checked)
                        {
                            SaveAssigeeActions();
                            EvidenceDocument();
                            DataTable dtdata = objQuality.GetChecklistDatabyTransactionIDSafety(lblTransactionID.Text);
                            if (dtdata.Rows[0]["Target Closure Date"] != null)
                            {
                                lbltargetdate.Text = dtdata.Rows[0]["Target Closure Date"].ToString();
                            }
                            if (dtdata.Rows[0]["Assignee Actual Closed Date"] != null)
                            {
                                lblclosuredate.Text = dtdata.Rows[0]["Assignee Actual Closed Date"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM Comments"] != null)
                            {
                                lblrcmcomments.Text = dtdata.Rows[0]["RCM Comments"].ToString();
                            }
                            objQuality.SendRCMForClosureMailsafety(lblRCMName.Text, lblRCMEmail.Text, lblProject.Text, lblchecklist.Text, lblSOName.Text, lblAssigneName.Text, lblobservations.Text, lbltargetdate.Text, lblrcmcomments.Text, assigneeRemarksTXT.Text, lblclosuredate.Text, lblPMEmail.Text, lblInduCoE.Text, lblTransactionID.Text);
                            btnAssigneeSubmit.Visible = false;
                            assigneeEvidenceFUP.Enabled = false;
                            lblAsigneeSubNotification.Text = "You have submitted successfully";
                            lblError.Text = "";
                            //lblRCMSubNotification.Text = "Submitted successfully";
                            string navigateUrl = Constants.link + "Pages/EHSAssigneeView.aspx";
                            lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
                        }
                        else
                        {
                            lblError.Text = "Self certification is mandatory";
                        }
                    }
                    else
                    {
                        lblError.Text = "Please enter actual closure date";
                    }
                }
                else
                {
                    lblError.Text = "Document upload is mandatory";
                }
            }
            else
            {
                lblError.Text = "Please enter the action taken comments";
            }
        }

        protected void attachment_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select NonComp_DocName,NonComp_DocContentType,NonComp_Document from EHSChecklist where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["NonComp_DocName"].ToString();
                    bytes = (byte[])sdr["NonComp_Document"];
                    contentType = sdr["NonComp_DocContentType"].ToString();

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }
            attachment.OnClientClick = "this.form.onsubmit = function() {return true;}";
        }

        protected void chkdoc_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select Checklist_DocName,Checklist_DocContentType,Checklist_Document from EHSChecklist where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["Checklist_DocName"].ToString();
                    bytes = (byte[])sdr["Checklist_Document"];
                    contentType = sdr["Checklist_DocContentType"].ToString();

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }
            chkdoc.OnClientClick = "this.form.onsubmit = function() {return true;}";
        }

        protected void Evidencedoc_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select Evidence_DocName,Evidence_DocContentType,Evidence_Document from EHSChecklist where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["Evidence_DocName"].ToString();
                    bytes = (byte[])sdr["Evidence_Document"];
                    contentType = sdr["Evidence_DocContentType"].ToString();

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }
            Evidencedoc.OnClientClick = "this.form.onsubmit = function() {return true;}";

        }
    }
}
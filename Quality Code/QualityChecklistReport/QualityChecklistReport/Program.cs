﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualityChecklistReport
{
    class Program
    {
        static void Main(string[] args)
        {
            ChecklistReport objChecklist = new ChecklistReport();
            Console.WriteLine("Started");
            objChecklist.StartProcess();
            Console.WriteLine("Completed...");
        }
    }
}

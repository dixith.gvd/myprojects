﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Collections;
namespace SendDailyMailAlerts
{
    class SendMail
    {
        public void pendingWithRCM()
        {
            DataTable pendingrcm = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select RCM_Email, RCM_Name, count(*) from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with RCM' group by RCM_Email, RCM_Name";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingrcm);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingrcm);
        }
        public void PendingWithAssignee()
        {
            DataTable pendingassignee = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select Assignee_Email, Assignee_Name, count(*) from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with Assignee' and Target_Closure_Date > GETDATE() group by Assignee_Email, Assignee_Name";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingassignee);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingassignee);
        }
        public void PendingWithPE()
        {
            DataTable pendingpe = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select PE_Email, PE_Name, count(*) from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with PE' group by PE_Email, PE_Name";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingpe);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingpe);
        }
        public void StartProcess()
        {
            pendingWithRCM();
            PendingWithAssignee();
            PendingWithPE();
        }
        public void SendMailList(DataTable pending)
        {
            string CCMailQuality = ConfigurationManager.AppSettings["CCQualityEmailList"].ToString();
            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
            SmtpClient emailClient = new SmtpClient();
            emailClient.UseDefaultCredentials = false;
            emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
            emailClient.Host = "smtp.office365.com";
            emailClient.Port = 25;
            emailClient.EnableSsl = true;
            emailClient.TargetName = "STARTTLS/smtp.office365.com";
            emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            DataTable dt = pending;
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(CCMailQuality))
            {
                string strTo = CCMailQuality.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(ccEmail));
                }
            }
            MailAddressCollection ccMail = MyMessage.CC;

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row[0].ToString() != "")
                    {
                        MyMessage.To.Add(row[0].ToString());
                        MyMessage.From = SendFrom;
                        MyMessage.Subject = "Daily Quality Checklist Reminder";
                        MyMessage.Body = FormMailBodyQuality(row[1].ToString(), row[2].ToString());
                        MyMessage.IsBodyHtml = true;
                        emailClient.Send(MyMessage);
                        MyMessage.To.Clear();
                    }
                }
            }
        }
        string FormMailBodyQuality(string user, string number)
        {

            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + user + ",");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("There are <font color='red'>" + number + "</font> no of records pending with you. We request you to take necessary action on priority.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            return sbBody.ToString();
        }
    }
}

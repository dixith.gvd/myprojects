﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Web;
using System.IO;
using System.Collections.Specialized;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO.Compression;
namespace QMDDashboard_TPL.ChecklistsModule.QualityUploadsEditForm
{
    [ToolboxItemAttribute(false)]
    public partial class QualityUploadsEditForm : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public QualityUploadsEditForm()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        QualityChecklists objQuality = new QualityChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                GetValuesAndSetToControls();
            }
        }
        private void GetValuesAndSetToControls()
        {
            SPWeb web = SPContext.Current.Web;
            if (this.Page.Request.QueryString["Tid"] != null)
            {
                lblTransactionID.Text = this.Page.Request.QueryString["Tid"].ToString();
                DataTable dtdata = objQuality.GetQualityUploadsDatabyTransactionID(lblTransactionID.Text);
                //string itemIDinString = this.Page.Request.QueryString["Tid"].ToString();
                //lblItemID.Text = itemIDinString;
                //if (!string.IsNullOrEmpty(itemIDinString))
                //{

                //    int itemID = Convert.ToInt32(itemIDinString);

                //    SPListItem dataItem = GetItemDetailsForEdirForm(itemID);
                if (dtdata.Rows.Count > 0)
                {

                    if (dtdata.Rows[0]["ProjectName"] != null)
                    {
                        lblProject.Text = dtdata.Rows[0]["ProjectName"].ToString();
                    }
                    string projectCode = dtdata.Rows[0]["ProjectCode"].ToString();
                    SPListItem rcmItem = GetRCMEmail(projectCode);
                    bool isLoggedinByRCM = false;
                    if (rcmItem != null)
                    {
                        if (rcmItem["RCM"] != null)
                        {
                            SPFieldUserValueCollection usercol = new SPFieldUserValueCollection(web, rcmItem["RCM"].ToString());
                            foreach (SPFieldUserValue user in usercol)
                            {
                                //lblRCMEmail.Text = rcmItem["RCM"].ToString();
                                lblRCMEmail.Text = user.User.Email;
                                if (lblRCMEmail.Text.ToLower().Equals(SPContext.Current.Web.CurrentUser.Email.ToLower()))
                                {
                                    isLoggedinByRCM = true;
                                    lblLoggedInUserIsRCM.Text = "t";
                                    lblLoggedInUserIsCreated.Text = "f";
                                }
                            }
                        }
                    }
                    if (isLoggedinByRCM == false)
                    {
                        lblRCMEmail.Text = string.Empty;
                        if (dtdata.Rows[0]["RCM_Email"] != null)
                        {
                            lblRCMEmail.Text = dtdata.Rows[0]["RCM_Email"].ToString();
                            if (lblRCMEmail.Text.ToLower().Equals(SPContext.Current.Web.CurrentUser.Email.ToLower()))
                            {
                                isLoggedinByRCM = true;
                                lblLoggedInUserIsRCM.Text = "t";
                                lblLoggedInUserIsCreated.Text = "f";
                            }
                        }
                    }
                    bool isLoggeduserEqualtoCreatedBy = CheckCreatedBy(lblTransactionID.Text);
                    if (isLoggeduserEqualtoCreatedBy)
                    {
                        lblLoggedInUserIsRCM.Text = "f";
                        lblLoggedInUserIsCreated.Text = "t";
                    }
                    else
                    {
                        if (rcmItem != null)
                        {
                            if (rcmItem["Field_x0020_Quality_x0020_Engine"] != null)
                            {
                                SPFieldUserValueCollection usercol = new SPFieldUserValueCollection(web, rcmItem["Field_x0020_Quality_x0020_Engine"].ToString());
                                foreach (SPFieldUserValue user in usercol)
                                {
                                    lblRCMEmail.Text = user.User.Email;
                                    if (lblRCMEmail.Text.ToLower().Equals(SPContext.Current.Web.CurrentUser.Email.ToLower()))
                                    {
                                        lblLoggedInUserIsRCM.Text = "f";
                                        lblLoggedInUserIsCreated.Text = "t";
                                        isLoggeduserEqualtoCreatedBy = true;
                                    }

                                }
                            }
                        }
                    }
                    //if (isLoggedinByRCM == true || isLoggeduserEqualtoCreatedBy == true)
                    //{

                    if (dtdata.Rows[0]["BUName"] != null)
                    {
                        lblBU.Text = dtdata.Rows[0]["BUName"].ToString();
                    }
                    if (dtdata.Rows[0]["Reporting_On"] != null)
                    {
                        string reportingOn = dtdata.Rows[0]["Reporting_On"].ToString();
                        //if (reportingOn.ToLower().Contains("good"))
                        //{
                        //    rdbGP.Checked = true;
                        //}
                        //if (reportingOn.ToLower().Contains("area"))
                        //{
                        //    rdbAOI.Checked = true;
                        //}
                        //rdbGP.Enabled = false;
                        //rdbAOI.Enabled = false;
                        lblreportingOn.Text = reportingOn;
                    }
                    if (dtdata.Rows[0]["Evidence_For"] != null)
                    {
                        lblEvidenceFor.Text = dtdata.Rows[0]["Evidence_For"].ToString();
                    }
                    if (dtdata.Rows[0]["Comments"] != null)
                    {
                        lblComments.Text = dtdata.Rows[0]["Comments"].ToString();
                        //txtComments.Enabled = false;
                    }
                    if (dtdata.Rows[0]["Reporting_Date"] != null && dtdata.Rows[0]["Reporting_Date"] != DBNull.Value)
                    {
                        string reportingDate = dtdata.Rows[0]["Reporting_Date"].ToString();
                        //if (!string.IsNullOrEmpty(reportingDate))
                        //{
                        //    DateTime dtDate = Convert.ToDateTime(reportingDate);
                        //    dtRepDate.SelectedDate = dtDate.Date;
                        //}
                        lblrepdate.Text = Convert.ToDateTime(reportingDate).ToShortDateString();
                    }
                    if (dtdata.Rows[0]["Target_Date"] != null && dtdata.Rows[0]["Target_Date"] != DBNull.Value)
                    {
                        string targetDate = dtdata.Rows[0]["Target_Date"].ToString();
                        //if (!string.IsNullOrEmpty(targetDate))
                        //{
                        //    DateTime dtDate = Convert.ToDateTime(targetDate);
                        //    dtTargetDate.SelectedDate = dtDate.Date;
                        //}
                        lbltargetDate.Text = Convert.ToDateTime(targetDate).ToShortDateString();
                    }
                    if (!string.IsNullOrEmpty(dtdata.Rows[0]["Corrective_Action"] as string))
                    {
                        txtCorrectiveAction.Text = dtdata.Rows[0]["Corrective_Action"].ToString();
                        lblCorrAction.Visible = true;
                        lblCorrAction.Text = dtdata.Rows[0]["Corrective_Action"].ToString();
                        txtCorrectiveAction.Visible = false;
                    }
                    //BindItemStatus();

                    if (dtdata.Rows[0]["ItemStatus"] != null)
                    {
                        string itemStatus = dtdata.Rows[0]["ItemStatus"].ToString();
                        if (!string.IsNullOrEmpty(itemStatus))
                        {
                            ddlItemStatus.Items.FindByValue(itemStatus).Selected = true;

                            if (itemStatus.ToLower().Equals("closed"))
                            {
                                btnSubmit.Visible = false;
                                ddlItemStatus.Enabled = false;
                            }
                            else
                            {
                                btnSubmit.Visible = true;
                            }

                        }
                    }

                    if (dtdata.Rows[0]["ReadyForClosure"] != null)
                    {
                        string closure = dtdata.Rows[0]["ReadyForClosure"].ToString();
                        if (closure.ToLower().Trim().Equals("true") || closure.ToLower().Trim().Equals("yes"))
                        {
                            chkClosure.Checked = true;

                        }
                        if (closure.ToLower().Trim().Equals("false") || closure.ToLower().Trim().Equals("no"))
                        {
                            chkClosure.Checked = false;

                        }
                    }
                    DataTable dtChildData = objQuality.GetAttachmentDatabyTransactionID(lblTransactionID.Text, "Quality");
                    if (dtChildData != null)
                    {
                        GridView1.DataSource = dtChildData;
                        GridView1.DataBind();
                    }
                    DataTable dtEvidenceChildData = objQuality.GetEvidenceAttachmentDatabyTransactionID(lblTransactionID.Text, "Quality");
                    if (dtEvidenceChildData.Rows.Count > 0)
                    {
                        if (chkClosure.Checked == true)
                        {
                            FileUpload1.Visible = false;
                            lblmsg.Visible = false;
                        }
                        else
                        {
                            FileUpload1.Visible = true;
                        }
                        GridView2.Visible = true;
                        GridView2.DataSource = dtEvidenceChildData;
                        GridView2.DataBind();
                    }
                    if (isLoggeduserEqualtoCreatedBy == true && isLoggedinByRCM == false)
                    {
                        lblLoggedInUserIsRCM.Text = "f";
                        lblLoggedInUserIsCreated.Text = "t";
                        if (dtdata.Rows[0]["ItemStatus"].ToString() != "Closed")
                        {
                            if (chkClosure.Checked == true)
                            {
                                btnSubmit.Enabled = false;
                            }
                            ddlItemStatus.Enabled = false;
                            //dtRepDate.Enabled = true;
                            //dtTargetDate.Enabled = true;
                            txtCorrectiveAction.Enabled = true;
                            chkClosure.Enabled = true;
                            FileUpload1.Enabled = true;
                        }
                        else
                        {
                            ddlItemStatus.Enabled = false;
                            //dtRepDate.Enabled = false;
                            //dtTargetDate.Enabled = false;
                            txtCorrectiveAction.Enabled = false;
                            chkClosure.Enabled = false;
                            FileUpload1.Enabled = false;
                            lblmsg.Visible = false;
                        }

                    }
                    if (isLoggedinByRCM == true && isLoggeduserEqualtoCreatedBy == false)
                    {
                        lblLoggedInUserIsRCM.Text = "t";
                        lblLoggedInUserIsCreated.Text = "f";
                        if (dtdata.Rows[0]["ItemStatus"].ToString() != "Closed")
                        {
                            ddlItemStatus.Enabled = true;
                            //dtRepDate.Enabled = false;
                            //dtTargetDate.Enabled = false;
                            txtCorrectiveAction.Enabled = false;
                            chkClosure.Enabled = false;
                            FileUpload1.Enabled = false;
                            lblmsg.Visible = false;
                        }
                        else
                        {
                            ddlItemStatus.Enabled = false;
                            //dtRepDate.Enabled = false;
                            //dtTargetDate.Enabled = false;
                            txtCorrectiveAction.Enabled = false;
                            chkClosure.Enabled = false;
                            FileUpload1.Enabled = false;
                            lblmsg.Visible = false;
                        }
                    }
                    if (isLoggedinByRCM == true && isLoggeduserEqualtoCreatedBy == true)
                    {
                        lblLoggedInUserIsRCM.Text = "t";
                        lblLoggedInUserIsCreated.Text = "t";
                        if (dtdata.Rows[0]["ItemStatus"].ToString() != "Closed")
                        {
                            ddlItemStatus.Enabled = true;
                            //dtRepDate.Enabled = true;
                            //dtTargetDate.Enabled = true;
                            txtCorrectiveAction.Enabled = true;
                            chkClosure.Enabled = true;
                            FileUpload1.Enabled = true;
                        }
                        else
                        {
                            ddlItemStatus.Enabled = false;
                            //dtRepDate.Enabled = false;
                            //dtTargetDate.Enabled = false;
                            txtCorrectiveAction.Enabled = false;
                            chkClosure.Enabled = false;
                            FileUpload1.Enabled = false;
                            lblmsg.Visible = false;
                        }
                    }
                    if (isLoggedinByRCM == false && isLoggeduserEqualtoCreatedBy == false)
                    {
                        //lblError.Text = "You are not authorised to modify these details";
                        //tblEditForm.Visible = false;
                        ddlItemStatus.Enabled = false;
                        //dtRepDate.Enabled = false;
                        //dtTargetDate.Enabled = false;
                        txtCorrectiveAction.Enabled = false;
                        chkClosure.Enabled = false;
                        FileUpload1.Enabled = false;
                        lblmsg.Visible = false;
                        btnSubmit.Visible = false;
                    }
                }
            }
        }

        protected void lnkView_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            LinkButton btn = sender as LinkButton;
            string docName = btn.Text;

            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select DocName,DocContentType,Document from UploadAttachments where Transaction_ID='" + lblTransactionID.Text + "' and DocName='" + docName + "' and DocumentType='Supporting' and List='Quality'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["DocName"].ToString();
                    contentType = sdr["DocContentType"].ToString();
                    if (Convert.ToInt32(lblTransactionID.Text) > 12187)
                    {
                        bytes = Decompress((byte[])sdr["Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Document"];
                    }
                    // lnkView.Text = fileName;


                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }


            catch (Exception ex)
            {

            }

        }

        protected void lnkView1_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            LinkButton btn = sender as LinkButton;
            string docName = btn.Text;

            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select DocName,DocContentType,Document from UploadAttachments where Transaction_ID='" + lblTransactionID.Text + "'and DocName='" + docName + "' and DocumentType='Evidence' and List='Quality'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["DocName"].ToString();
                    contentType = sdr["DocContentType"].ToString();
                    DateTime lastUpdateDate = LastDate();
                    DateTime dt2 = DateTime.Parse("2017-12-27 18:05:00.000");
                    if (lastUpdateDate > dt2)
                    {
                        bytes = Decompress((byte[])sdr["Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Document"];
                    }
                    // lnkView.Text = fileName;


                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }


            catch (Exception ex)
            {

            }

        }

        private bool CheckCreatedBy(string Tid)
        {
            bool CurrentUserIsCreator = false;
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QualityUploads where Transaction_ID='" + Tid + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
                SPUser currentUser = SPContext.Current.Web.CurrentUser;
                if (dt.Rows[0]["FQE_Email"].Equals(currentUser.Email))
                {
                    CurrentUserIsCreator = true;
                }
            }
            catch (Exception exp)
            {

            }
            return CurrentUserIsCreator;
        }
        public SPListItem GetRCMEmail(string projecCode)
        {
            SPListItem emailRCM = null;
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPList lst = null;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        lst = web.Lists["Project PM RCM"];
                    }
                    if (lst != null)
                    {
                        SPQuery query = new SPQuery();
                        query.Query = "<Where><Eq><FieldRef Name='Project_x0020_Code' /><Value Type='Text'>" + projecCode + "</Value></Eq></Where>";
                        SPListItemCollection items = lst.GetItems(query);
                        foreach (SPListItem item in items)
                        {
                            emailRCM = item;
                            break;
                        }
                    }
                }
            }
            return emailRCM;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool isSubmitted = false;

            bool createdbyLoggedinUser = false;
            if (lblLoggedInUserIsCreated.Text.Equals("t"))
            {
                createdbyLoggedinUser = true;
            }
            bool loggedInUserRCM = false;
            if (lblLoggedInUserIsRCM.Text.Equals("t"))
            {
                loggedInUserRCM = true;
            }

            if (loggedInUserRCM)
            {
                string itemStatus = ddlItemStatus.SelectedItem.Text;
                DateTime today = DateTime.Now;
                string rcmsubdate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                if (!string.IsNullOrEmpty(itemStatus))
                {
                    objQuality.UpdateQualityStatus(lblTransactionID.Text, itemStatus, rcmsubdate);
                    isSubmitted = true;
                }
            }

            if (createdbyLoggedinUser)
            {
                if (txtCorrectiveAction.Text != "")
                {
                    lblError.Text = "";
                    string correctiveAction = txtCorrectiveAction.Text.Replace("'", "''");

                    string readyForClosure = string.Empty;
                    if (chkClosure.Checked)
                    {
                        readyForClosure = "True";
                    }
                    else
                    {
                        readyForClosure = "False";
                    }

                    if (!string.IsNullOrEmpty(lblTransactionID.Text))
                    {
                        //string repdate = string.Empty;
                        //string targetdate = string.Empty;
                        string rfclosure = string.Empty;
                        string caction = string.Empty;
                        DataTable dtdata = objQuality.GetQualityUploadsDatabyTransactionID(lblTransactionID.Text);
                        //if (dtRepDate.IsDateEmpty)
                        //{
                        //    //repdate = Convert.ToDateTime(dtdata.Rows[0]["Reporting_Date"]);
                        //}
                        //else
                        //{
                        //    DateTime reportingdate = dtRepDate.SelectedDate;
                        //    repdate = String.Format("{0:yyyy-MM-dd}", reportingdate);
                        //}

                        //if (dtTargetDate.IsDateEmpty)
                        //{
                        //    //targetdate = Convert.ToDateTime(dtdata.Rows[0]["Target_Date"]);
                        //}
                        //else
                        //{
                        //    DateTime tardate = dtTargetDate.SelectedDate;
                        //    targetdate = String.Format("{0:yyyy-MM-dd}", tardate);
                        //}

                        if (!string.IsNullOrEmpty(readyForClosure))
                        {
                            rfclosure = readyForClosure;
                        }
                        if (!string.IsNullOrEmpty(correctiveAction))
                        {
                            caction = correctiveAction;
                        }
                        DataTable dtChildData = objQuality.GetAttachmentDatabyTransactionID(lblTransactionID.Text, "Quality");
                        if (dtChildData != null)
                        {
                            GridView1.DataSource = dtChildData;
                            GridView1.DataBind();
                        }
                        if (FileUpload1.HasFile || FileUpload1.HasFiles)
                        {
                            IList<System.Web.HttpPostedFile> files = FileUpload1.PostedFiles;
                            foreach (HttpPostedFile file in files)
                            {
                                InsertAttachment(file, lblTransactionID.Text);
                            }
                            string ids = GetEvidenceDocIDsQuality(lblTransactionID.Text);
                            UpdateEvidenceDocsQuality(lblTransactionID.Text, ids);
                        }

                        else if (loggedInUserRCM == true)
                        {
                            //Do Nothing
                        }
                        else
                        {
                            if (FileUpload1.Visible == false)
                            {
                                lblError2.Text = "You have already marked as ready for closure. This is pending with RCM";
                                return;
                            }
                            else
                            {
                                lblError2.Text = "It is mandatory to upload an evidence document";
                                return;
                            }
                        }
                        DateTime today = DateTime.Now;
                        string FQECorrectionDate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                        objQuality.UpdateQualityUploadsEditDetails(lblTransactionID.Text, caction, rfclosure, FQECorrectionDate);
                        isSubmitted = true;
                        string projectCode = dtdata.Rows[0]["ProjectCode"].ToString();
                        SPListItem rcmItem = GetRCMEmail(projectCode);

                        if (chkClosure.Checked)
                        {
                            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
                            {
                                using (SPWeb web = site.OpenWeb())
                                {
                                    if (rcmItem != null)
                                    {
                                        string rcmName = string.Empty;
                                        string rcmEmail = string.Empty;
                                        if (rcmItem["RCM"] != null)
                                        {
                                            SPFieldUserValueCollection usercol = new SPFieldUserValueCollection(web, rcmItem["RCM"].ToString());
                                            foreach (SPFieldUserValue user in usercol)
                                            {
                                                rcmName = user.User.Name;
                                                rcmEmail = user.User.Email;
                                                string fullUrl = "https://tplnet.tataprojects.com/Pages/QualityUploadsEditForm.aspx?Tid=" + lblTransactionID.Text;
                                                string body = FormMailBody(rcmName, SPContext.Current.Web.CurrentUser.Name, fullUrl);
                                                SendMailToRCM(web, rcmEmail, body, "Notification");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    lblError.Text = "Corrective Action should not be empty";
                    return;
                }
            }
            if (isSubmitted)
            {
                //this.Page.Response.Redirect("https://tplnet.tataprojects.com/Pages/QualityUploads.aspx");
                btnSubmit.Enabled = false;
                lblNotification.Text = "You have submitted successfully";
                lblError.Text = "";
                lblError2.Text = "";
            }
        }
        private void UpdateEvidenceDocsQuality(string Tid, string docid)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update QualityUploads Set Evidence_Docs='" + docid + "' where Transaction_ID='" + Tid + "'";
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {

            }
        }

        private string GetEvidenceDocIDsQuality(string Tid)
        {
            string ids = string.Empty;
            DataTable IDdata = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();
                string sqlQuery = "select id from uploadAttachments where List='Quality' and DocumentType='Evidence' and Transaction_ID='" + Tid + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(IDdata);
                con.Close();
                if (IDdata.Rows.Count > 0)
                {
                    foreach (DataRow row in IDdata.Rows)
                    {
                        ids = ids + "," + row["ID"].ToString();
                    }
                }
            }
            catch (Exception exp)
            {

            }
            return ids.TrimStart(',');
        }

        private void InsertAttachment(HttpPostedFile file, string Tid)
        {

            string filePath = file.FileName;
            string filename = Path.GetFileName(filePath);
            string ext = Path.GetExtension(filename).ToLower();
            string contenttype = String.Empty;

            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
                case ".pptx":
                    contenttype = "application/pptx";
                    break;
                case ".jpeg":
                    contenttype = "image/jpeg";
                    break;
                case ".tif":
                    contenttype = "image/tif";
                    break;
                case ".tiff":
                    contenttype = "image/tiff";
                    break;
                case ".bmp":
                    contenttype = "image/bmp";
                    break;
                case ".rar":
                    contenttype = "application/rar";
                    break;
            }
            if (contenttype != String.Empty)
            {

                Stream fs = file.InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                byte[] compressedData = Compress(bytes);
                //Update the file into database
                String strQuery = "insert into UploadAttachments(DocName,DocContentType,Document,Transaction_ID,List,DocumentType) values (@DocName, @DocContentType, @Document,@Transaction_ID,'Quality','Evidence')";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@DocName", SqlDbType.VarChar).Value = filename;
                cmd.Parameters.Add("@DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@Document", SqlDbType.Binary).Value = compressedData;
                cmd.Parameters.AddWithValue("@Transaction_ID", Tid);
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        private string FormMailBody(string rcmName, string loggedInUser, string itemUrl)
        {
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear ");
            sbBody.Append(rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(loggedInUser);
            sbBody.Append(" has marked this upload as 'Ready For Closure'.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Please review the same and set the status as 'Closed' in case you are satisfied.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Click ");
            sbBody.Append("<a href='");
            sbBody.Append(itemUrl);
            sbBody.Append("'>here</a>");
            sbBody.Append(" to access the item. ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards");
            return sbBody.ToString();
        }

        public void SendMailToRCM(SPWeb web, string mailID, string body, string subject)
        {
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            SmtpClient emailClient = new SmtpClient();
            emailClient.UseDefaultCredentials = false;
            emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
            emailClient.Host = "smtp.office365.com";
            emailClient.Port = 587;
            emailClient.EnableSsl = true;
            emailClient.TargetName = "STARTTLS/smtp.office365.com";
            emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            MailMessage MyMessage = new MailMessage();
            MyMessage.To.Add(mailID);
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Quality Notification";
            MyMessage.Body = body;
            MyMessage.IsBodyHtml = true;
            emailClient.Send(MyMessage);
            //if (!string.IsNullOrEmpty(mailID))
            //{
            //    StringDictionary headers = new StringDictionary
            //    {
            //        {"to", mailID},
            //        {"cc", ""},
            //        {"bcc", ""},
            //        {"subject", subject },
            //        {"content-type", "text/html"}
            //   };
            //    SPUtility.SendEmail(web, headers, body);
            //}
        }
        private byte[] Compress(byte[] data)
        {
            var output = new MemoryStream();
            using (var gzip = new GZipStream(output, CompressionMode.Compress, true))
            {
                gzip.Write(data, 0, data.Length);
                gzip.Close();
            }
            return output.ToArray();
        }
        private byte[] Decompress(byte[] data)
        {
            var output = new MemoryStream();
            var input = new MemoryStream();
            input.Write(data, 0, data.Length);
            input.Position = 0;
            using (var gzip = new GZipStream(input, CompressionMode.Decompress, true))
            {
                var buff = new byte[64];
                var read = gzip.Read(buff, 0, buff.Length);
                while (read > 0)
                {
                    output.Write(buff, 0, read);
                    read = gzip.Read(buff, 0, buff.Length);
                }
                gzip.Close();
            }
            return output.ToArray();
        }
        public DateTime LastDate()
        {
            string date = string.Empty;
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select FQE_Correction_Date from QualityUploads where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
                if (dt != null)
                {
                    date = dt.Rows[0]["FQE_Correction_Date"].ToString();
                }
            }
            catch (Exception exp)
            {
            }
            return Convert.ToDateTime(date);
        }
    }
}

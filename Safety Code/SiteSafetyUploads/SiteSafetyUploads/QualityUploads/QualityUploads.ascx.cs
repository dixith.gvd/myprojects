﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SharePoint.WebControls;
using System.Collections.Specialized;
namespace SiteSafetyUploads.QualityUploads
{
    [ToolboxItemAttribute(false)]
    public partial class QualityUploads : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public QualityUploads()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        Uploads objup = new Uploads();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                dvMainForm.Visible = true;
                DataTable projectCodeItem = objup.GetProjectCodeByLoggedInUserSafety();
                if (projectCodeItem != null)
                {
                    for (int i = 0; i < projectCodeItem.Rows.Count; i++)
                    {
                        string projectCode = projectCodeItem.Rows[i][0].ToString();

                        DataTable projectItem = objup.GetProjectdetailsByProjectCode(projectCode);

                        for (int j = 0; j < projectItem.Rows.Count; j++)
                        {
                            string projectName = projectItem.Rows[j][12].ToString();
                            if (!ddlplist.Items.Contains(new ListItem(projectName)))
                            {
                                ddlplist.Items.Add(projectName);
                            }

                        }
                        lblBU.Text = projectItem.Rows[0]["BUName"].ToString();
                    }
                    if (!ddlplist.Items.Contains(new ListItem("--Select--")))
                    {
                        ddlplist.Items.Insert(0, "--Select--");
                    }
                    //DateTime today = DateTime.Now;
                    dtreporting.SelectedDate = DateTime.Today;
                    
                    DataTable dtEvidence = objup.GetEvidenceData();
                    if (dtEvidence != null)
                    {
                        ddlevidence.DataSource = dtEvidence;
                        ddlevidence.DataTextField = "EvidenceFor";
                        ddlevidence.DataValueField = "EvidenceFor";
                        ddlevidence.DataBind();
                        ddlevidence.Items.Insert(0, "--Select--");
                    }
                    lblFQEEmail.Text = SPContext.Current.Web.CurrentUser.Email;
                    lblFQEName.Text = SPContext.Current.Web.CurrentUser.Name;
                    SPUser user = SPContext.Current.Web.EnsureUser(lblFQEEmail.Text);
                    SPFieldUserValue user2 = new SPFieldUserValue(SPContext.Current.Web, user.ID, user.Email.ToString());
                    lblFQEID.Text = objup.GetOnlyEmployeeID(user2.User.LoginName);
                }
                else
                {
                    dvUnAuth.Visible = true;
                    dvMainForm.Visible = false;
                }
            }
        }

        protected void ddlevidence_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Evidence = ddlevidence.SelectedItem.Value;
            DataTable dtCategory = objup.GetCategoryList(Evidence);
            if (dtCategory != null)
            {
                ddlcategory.DataSource = dtCategory;
                ddlcategory.DataTextField = "Category";
                ddlcategory.DataValueField = "Category";
                ddlcategory.DataBind();
                ddlcategory.Items.Insert(0, "--Select--");
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            string bu = lblBU.Text;
            string projectname = ddlplist.SelectedItem.Text;
            DateTime today = DateTime.Now;
            string reportingon = string.Empty;
            string itemStatus = string.Empty;
            if (rbgoodpractice.Checked)
            {
                reportingon = rbgoodpractice.Text;
                itemStatus = "Closed";
                dttarget.Enabled = false;
            }
            else if (rbareas.Checked)
            {
                reportingon = rbareas.Text;
                itemStatus = "Active";
            }
            string evidencefor = ddlevidence.SelectedItem.Text;
            string category = ddlcategory.SelectedItem.Text;
            string comments = txtcomments.Text;
            DateTime reportingdate = dtreporting.SelectedDate;
            DateTime targetdate = dttarget.SelectedDate;
            string rpdate = String.Format("{0:yyyy-MM-dd}", reportingdate);
            string tardate = String.Format("{0:yyyy-MM-dd}", targetdate);
            string createddate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
            string transID = string.Empty;
            int TransactionID;
            string createdby = SPContext.Current.Web.CurrentUser.Name;
            string projectCode = ddlplist.SelectedItem.Text.Split('-')[1];
            if (!string.IsNullOrEmpty(objup.GetLatestTransactionIDQuality()))
            {
                TransactionID = Convert.ToInt32(objup.GetLatestTransactionIDQuality()) + 1;
            }
            else
            {
                TransactionID = 1;
            }
            if (dttarget.SelectedDate.Date >= DateTime.Today.Date)
            {
                bool isSaved = objup.SaveQualityChecklist(TransactionID, bu, projectname, projectCode, reportingon, evidencefor, category, comments, rpdate, tardate, itemStatus, createddate, createddate, lblFQEID.Text, lblFQEName.Text, lblFQEEmail.Text, lblRCMID.Text, lblRCMName.Text, lblRCMEmail.Text, lblPMID.Text, lblPMName.Text, lblPMEmail.Text, lblPEID.Text, lblPEName.Text, lblPEEmail.Text, lblHeadID.Text, lblHead.Text, lblHeadEmail.Text, lblbuheadid.Text, lblbuheadName.Text, lblbuheadEmail.Text);
                if (isSaved)
                {
                    transID = TransactionID.ToString();
                    if (FileUpload1.HasFile || FileUpload1.HasFiles)
                    {
                        IList<System.Web.HttpPostedFile> files = FileUpload1.PostedFiles;
                        foreach (HttpPostedFile file in files)
                        {
                            InsertAttachment(file, transID);
                        }
                    }
                    string ids = GetFQEDocIDs(transID);
                    UpdateFQEDocs(transID, ids);
                    btnsubmit.Enabled = false;
                    lblNotification.Text = "You have submitted successfully";
                    lblError.Text = "";
                }
            }
            else
            {
                lblError.Text = "Please enter target date greater than or equals Today.";
            }
        }

        private string GetFQEDocIDs(string Tid)
        {
            string ids = string.Empty;
            DataTable IDdata = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();
                string sqlQuery = "select id from uploadAttachments where List='Quality' and DocumentType='Supporting' and Transaction_ID='" + Tid + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(IDdata);
                con.Close();
                if (IDdata.Rows.Count > 0)
                {
                    foreach (DataRow row in IDdata.Rows)
                    {
                        ids = ids + "," + row["ID"].ToString();
                    }
                }
            }
            catch (Exception exp)
            {

            }
            return ids.TrimStart(',');
        }


        private void InsertAttachment(HttpPostedFile file, string Tid)
        {

            string filePath = file.FileName;
            string filename = Path.GetFileName(filePath);
            string ext = Path.GetExtension(filename).ToLower();
            string contenttype = String.Empty;

            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
                case ".pptx":
                    contenttype = "application/pptx";
                    break;
                case ".jpeg":
                    contenttype = "image/jpeg";
                    break;
                case ".tif":
                    contenttype = "image/tif";
                    break;
                case ".tiff":
                    contenttype = "image/tiff";
                    break;
                case ".bmp":
                    contenttype = "image/bmp";
                    break;
                case ".rar":
                    contenttype = "application/rar";
                    break;
            }
            if (contenttype != String.Empty)
            {

                Stream fs = file.InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                //Update the file into database
                String strQuery = "insert into UploadAttachments(DocName,DocContentType,Document,Transaction_ID,List,DocumentType) values (@DocName, @DocContentType, @Document,@Transaction_ID,'Quality','Supporting')";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@DocName", SqlDbType.VarChar).Value = filename;
                cmd.Parameters.Add("@DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@Document", SqlDbType.Binary).Value = bytes;
                cmd.Parameters.AddWithValue("@Transaction_ID", Tid);
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        private Boolean InsertEvidenceDoc(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        private void UpdateFQEDocs(string Tid, string docid)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update QualityUploads Set FQE_Docs='" + docid + "' where Transaction_ID='" + Tid + "'";
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {

            }
        }

        protected void ddlplist_SelectedIndexChanged(object sender, EventArgs e)
        {
            string projectName = ddlplist.SelectedItem.Text;
            lblProjectCode.Text = ddlplist.SelectedItem.Text;

            DataTable dtProjectCode = objup.GetProjectCodeByProjectName(projectName);
            if (!string.IsNullOrEmpty(ddlplist.SelectedItem.Text) && ddlplist.SelectedIndex > 0)
            {

                for (int i = 0; i < dtProjectCode.Rows.Count; i++)
                {
                    string projectCode = dtProjectCode.Rows[i][1].ToString();
                    lblProjectCode.Text = projectCode;

                    SPListItem projectCodeFromPMRCM = objup.GetProjectCodeByProjectNameForRCM(projectCode);

                    if (projectCodeFromPMRCM["RCM"] != null)
                    {
                        string strRCM = projectCodeFromPMRCM["RCM"].ToString();
                        if (!string.IsNullOrEmpty(strRCM))
                        {
                            SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strRCM);
                            lblRCMEmail.Text = userValue.User.Email.ToString();
                            lblRCMName.Text = userValue.User.Name;
                            lblRCMID.Text = objup.GetOnlyEmployeeID(userValue.User.LoginName);
                        }

                    }
                    if (projectCodeFromPMRCM["PM"] != null)
                    {
                        string strPM = projectCodeFromPMRCM["PM"].ToString();
                        if (!string.IsNullOrEmpty(strPM))
                        {
                            SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPM);
                            lblPMEmail.Text = userValue.User.Email.ToString();
                            lblPMName.Text = userValue.User.Name;
                            lblPMID.Text = objup.GetOnlyEmployeeID(userValue.User.LoginName);
                        }

                    }
                    if (projectCodeFromPMRCM["Planning_x0020_Engineer"] != null)
                    {
                        string strPE = projectCodeFromPMRCM["Planning_x0020_Engineer"].ToString();
                        if (!string.IsNullOrEmpty(strPE))
                        {
                            SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPE);
                            lblPEEmail.Text = userValue.User.Email.ToString();
                            lblPEName.Text = userValue.User.Name;
                            lblPEID.Text = objup.GetOnlyEmployeeID(userValue.User.LoginName);
                        }

                    }
                    if (projectCodeFromPMRCM["BU_x0020_Head"] != null)
                    {
                        string strbuHead = projectCodeFromPMRCM["BU_x0020_Head"].ToString();
                        if (!string.IsNullOrEmpty(strbuHead))
                        {
                            SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strbuHead);
                            lblbuheadid.Text = objup.GetOnlyEmployeeID(userValue.User.LoginName);
                            lblbuheadName.Text = userValue.User.Name;
                            lblbuheadEmail.Text = userValue.User.Email.ToString();
                        }
                    }
                    if (projectCodeFromPMRCM["Head_x0020_Operations"] != null)
                    {
                        string strHead = projectCodeFromPMRCM["Head_x0020_Operations"].ToString();
                        if (!string.IsNullOrEmpty(strHead))
                        {
                            SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strHead);
                            lblHeadEmail.Text = userValue.User.Email.ToString();
                            lblHead.Text = userValue.User.Name;
                            lblHeadID.Text = objup.GetOnlyEmployeeID(userValue.User.LoginName);
                        }

                    }
                    //lblAssigneEmail.Text = SPContext.Current.Web.CurrentUser.Email;
                    //lblAssigneName.Text = SPContext.Current.Web.CurrentUser.Name;

                }

            }
            else
            {

            }
        }

    }
}

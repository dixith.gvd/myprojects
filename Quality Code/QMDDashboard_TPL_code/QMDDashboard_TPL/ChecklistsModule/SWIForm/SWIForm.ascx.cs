﻿using Microsoft.SharePoint;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls.WebParts;

namespace QMDDashboard_TPL.ChecklistsModule.SWIForm
{
    [ToolboxItemAttribute(false)]
    public partial class SWIForm : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public SWIForm()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        QualityChecklists objQuality = new QualityChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {

                if (this.Page.Request.QueryString["Tid"] != null)
                {
                    lblTransactionID.Text = this.Page.Request.QueryString["Tid"].ToString();
                    DataTable dtdata = objQuality.GetChecklistDatabyTransactionID(lblTransactionID.Text);
                    if (dtdata != null)
                    {

                        string rcmEmail = string.Empty;
                        string assigneeEmail = string.Empty;
                        if (dtdata.Rows[0]["RCM_Email"] != null)
                        {
                            rcmEmail = dtdata.Rows[0]["RCM_Email"].ToString();
                            if (IsLoggedInAuthorisedUser(rcmEmail))
                            {
                                dvSWIForm.Visible = true;
                              //  dvSWIRCMForm.Visible = true;
                               // dvAssignSubmi.Visible = false;
                               // dvMain.Visible = true;

                                if (dtdata.Rows[0]["Assignee_SWI_Remarks"] != null)
                                {
                                    txtReInspection.Text = dtdata.Rows[0]["Assignee_SWI_Remarks"].ToString();
                                    txtReInspection.ReadOnly = true;
                                }
                            }

                        }
                        if (dtdata.Rows[0]["Assignee_Email"] != null)
                        {
                            assigneeEmail = dtdata.Rows[0]["Assignee_Email"].ToString();
                            if (IsLoggedInAuthorisedUser(assigneeEmail))
                            {
                                dvSWIForm.Visible = true;
                              //  dvSWIRCMForm.Visible = false;
                              //  dvAssignSubmi.Visible = true;
                              //  dvMain.Visible = false;
                            }
                        }
                        // Closure form for Assignee for "awaiting stage "
                        //if (dtdata.Rows[0]["Assignee_Email"] != null)
                        //{
                        //    assigneeEmail = dtdata.Rows[0]["Assignee_Email"].ToString();
                        //    if (IsLoggedInAuthorisedUser(assigneeEmail))
                        //    {
                        //        dvFilledForm.Visible = true;
                        //        dvAssignee.Visible = true;
                        //    }
                        //}

                       
                            if (dtdata.Rows[0]["SWIID"] != null)
                            {
                                lblSWINO.Text = dtdata.Rows[0]["SWIID"].ToString();
                            }                            
                            if (dtdata.Rows[0]["ProjectName"] != null)
                            {
                                lblProject.Text = dtdata.Rows[0]["ProjectName"].ToString();
                            }
                            if (dtdata.Rows[0]["ProjectType"] != null)
                            {
                                lblProjectType.Text = dtdata.Rows[0]["ProjectType"].ToString();
                            }
                            dtTargetClosureDate.SelectedDate = DateTime.Now;

                            if (dtdata.Rows[0]["Package"] != null)
                            {
                                txtPackage.Text = dtdata.Rows[0]["Package"].ToString();
                            }
                            if (dtdata.Rows[0]["Subcontractor"] != null)
                            {
                                txtSubcontractors.Text = dtdata.Rows[0]["Subcontractor"].ToString();
                            }
                            if (dtdata.Rows[0]["Discipline"] != null)
                            {
                                lblDiscipline.Text = dtdata.Rows[0]["Discipline"].ToString();
                            }                           
                            if (dtdata.Rows[0]["Activity"] != null)
                            {
                                lblActivity.Text = dtdata.Rows[0]["Activity"].ToString();
                            }
                            if (dtdata.Rows[0]["Sub Activity"] != null)
                            {
                                lblSubActivity.Text = dtdata.Rows[0]["Sub Activity"].ToString();
                            }
                            if (dtdata.Rows[0]["FQE_Name"] != null)
                            {
                                lblIssuedby.Text = dtdata.Rows[0]["FQE_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["FQE_Email"] != null)
                            {
                                lblIssueEmail.Text = dtdata.Rows[0]["FQE_Email"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Name"] != null)
                            {
                                lblIssuedTo.Text = dtdata.Rows[0]["RCM_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                lblIssuetoEmail.Text = dtdata.Rows[0]["RCM_Email"].ToString();
                            }
                            DataTable dtChildData = objQuality.GetComplaincesDatabyTransactionID(lblTransactionID.Text);
                            if (dtChildData != null)
                            {
                                grdSWIComplainces.DataSource = dtChildData;
                                grdSWIComplainces.DataBind();
                            }

                                                     
                       
                    }
                }
               
            }
        }

        private bool IsLoggedInAuthorisedUser(string email)
        {
            bool isValid = SPContext.Current.Web.CurrentUser.Email.Equals(email);
            return isValid;
        }

        protected void btnassignsub_Click(object sender, EventArgs e)
        {
            AssigneeSubmit();
        }
        private void AssigneeSubmit()
        {
            DataTable dtDiscipline = new DataTable();
            DateTime dtSelected = dtTargetClosureDate.SelectedDate;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            String sqlQuery = "insert into QChecklist_TnxMain(Assignee_SWI_Remarks) values ('" + txtReInspection.Text.Replace("'", "''") + "')";
            SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
            daAdap.Fill(dtDiscipline);
            con.Close();
            txtReInspection.Text = null;

        }

        //private void RCMSubmit()
        //{
        //    DataTable dtDiscipline = new DataTable();
        //    DateTime dtSelected = dtRCM.SelectedDate;
        //    if (rdAccepted.Checked)
        //    {
        //        rdAccepted.Text = "Yes";
        //    }
        //    else { rdAccepted.Text = "No"; }
        //    SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
        //    String sqlQuery = "insert into QChecklist_TnxMain(Assignee_SWI_Date,RCM_SWI_Accept) values ('" + dtSelected.Date.ToString("yyyy-MM-dd HH:mm:ss") + "','" + rdAccepted.Text + "')";
        //    SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
        //    daAdap.Fill(dtDiscipline);
        //    con.Close();
        //}

        protected void btnRCMSWI_Click(object sender, EventArgs e)
        {
          //  RCMSubmit();
        }

             
    }
}

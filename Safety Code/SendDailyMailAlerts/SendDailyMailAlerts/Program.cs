﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendDailyMailAlerts
{
    class Program
    {
        static void Main(string[] args)
        {
            SendMail objsend = new SendMail();
            Console.WriteLine("Started");
            objsend.StartProcess();
            Console.WriteLine("Completed...");
        }
    }
}

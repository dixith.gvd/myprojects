﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO.Compression;
namespace QMDDashboard_TPL.ChecklistsModule.ChecklistsActions
{
    [ToolboxItemAttribute(false)]
    public partial class ChecklistsActions : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public ChecklistsActions()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
            if (this.Page.Request.QueryString["Tid"] != null)
            {
                lblTransactionID.Text = this.Page.Request.QueryString["Tid"].ToString();

                DataTable dtdata = objQuality.GetChecklistDatabyTransactionID(lblTransactionID.Text);
                if (dtdata.Rows.Count > 0)
                {
                    if (dtdata.Rows[0]["FQE_Submitted_Date"] != null)
                    {
                        string dtAssSubDate = dtdata.Rows[0]["FQE_Submitted_Date"].ToString();
                        if (!string.IsNullOrEmpty(dtAssSubDate))
                        {
                            DateTime dtFqeDate = Convert.ToDateTime(dtAssSubDate);
                            FQESubmittedDate.Text = dtFqeDate.ToString("dd/MM/yyyy");
                            assigneeClosureDateDATE.MinDate = dtFqeDate;
                            dtTargetClosureDate.MinDate = dtFqeDate;


                        }

                    }
                }
            }
            assigneeClosureDateDATE.MaxDate = DateTime.Now.Date;
        }

        QualityChecklists objQuality = new QualityChecklists();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {

                if (this.Page.Request.QueryString["Tid"] != null)
                {
                    lblTransactionID.Text = this.Page.Request.QueryString["Tid"].ToString();
                    lblTransID.Text = lblTransactionID.Text;
                    DataTable dtdata = objQuality.GetChecklistDatabyTransactionID(lblTransactionID.Text);
                    if (dtdata.Rows.Count > 0)
                    {

                        string rcmEmail = string.Empty;
                        string assigneeEmail = string.Empty;
                        bool mainFormLogic = true;
                        string fqeEmail = string.Empty;
                        string stage = string.Empty;
                        string pmemail = string.Empty;
                        if (dtdata.Rows[0]["Stage"] != null)
                        {
                            stage = dtdata.Rows[0]["Stage"].ToString();
                        }

                        if (stage.Equals("Pending with Assignee"))
                        {
                            if (dtdata.Rows[0]["Assignee_Email"] != null)
                            {
                                assigneeEmail = dtdata.Rows[0]["Assignee_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(assigneeEmail))
                                {
                                    dvFilledForm.Visible = true;
                                    dvAssignee.Visible = true;
                                    mainFormLogic = true;
                                    RCMActions.Visible = false;
                                    dvRCM.Visible = false;
                                }
                            }
                        }
                        else if (stage.Equals("Pending with RCM"))
                        {
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                rcmEmail = dtdata.Rows[0]["RCM_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(rcmEmail))
                                {
                                    dvFilledForm.Visible = true;
                                    mainFormLogic = true;
                                    dvAssignee.Visible = false;
                                    RCMActions.Visible = true;
                                    dvRCM.Visible = true;
                                }

                            }
                        }
                        else if (stage.Equals("Compliant and Closed"))
                        {
                            dvFilledForm.Visible = true;
                            mainFormLogic = true;
                            dvAssignee.Visible = false;
                            RCMActions.Visible = false;
                            dvRCM.Visible = false;
                        }
                        else if (stage.Trim().Equals("Awaiting Closure"))
                        {
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                rcmEmail = dtdata.Rows[0]["RCM_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(rcmEmail))
                                {
                                    dvFilledForm.Visible = true;
                                    mainFormLogic = true;

                                    RCMActions.Visible = false; ;
                                    dvRCM.Visible = false;
                                    if (Convert.ToInt32(lblTransactionID.Text) > 43561)
                                    {
                                        dvRCMClose.Visible = false;
                                        dvfqeclose.Visible = true;
                                        LinkButton1.Visible = false;
                                        LinkButton2.Visible = true;
                                    }
                                    else
                                    {
                                        dvRCMClose.Visible = true;
                                        dvfqeclose.Visible = false;
                                        LinkButton2.Visible = false;
                                        LinkButton1.Visible = true;
                                    }


                                    dvAssignee.Visible = false;


                                    //GeteAssigneeEvidenceDocument();

                                    assigneeClosureDateDATE.SelectedDate = DateTime.Today;
                                    GeteAssigneeEvidenceDocument();
                                }

                            }
                        }

                            //if (dtdata.Rows[0]["PM_Email"] != null)
                        //{
                        //    pmemail = dtdata.Rows[0]["PM_Email"].ToString();
                        //    if (IsLoggedInAuthorisedUser(pmemail))
                        //    {
                        //        dvFilledForm.Visible = true;
                        //        dvRCM.Visible = true;
                        //        RCMActions.Visible = true;
                        //        dvAssignee.Visible = true;
                        //        dvRCMClose.Visible = true;
                        //        mainFormLogic = true;
                        //        dtTargetClosureDate.Visible = false;
                        //        ppAssigneeName.Visible = false;
                        //        txtRCMRemarks.ReadOnly = true;

                            //        ppAssigneeName.Enabled = false;
                        //        dtTargetClosureDate.Enabled = false;
                        //        txtRCMRemarks.ReadOnly = false;
                        //        assigneeClosureDateDATE.Enabled = false;
                        //        assigneeRemarksTXT.ReadOnly = true;
                        //        txtverifi_Remarks.ReadOnly = true;
                        //        dtClosuredate.Enabled = false;
                        //        btnAssigneeSubmit.Enabled = false;
                        //        btnRCMActions.Enabled = false;
                        //        chkClosureYes.Enabled = false;
                        //        upLoadEvidenceDoc.Visible = true;
                        //        assigneeEvidenceFUP.Visible = false;

                            //        LinkButton1.Visible = true;
                        //        assigneeClosureDateDATE.SelectedDate = DateTime.Today;
                        //        GetFQEvidenceDocument();
                        //        GeteAssigneeEvidenceDocument();
                        //        GetAlltheDetails();

                            //    }
                        //}


                        else if (dtdata.Rows[0]["FQE_Email"] != null)
                        {
                            fqeEmail = dtdata.Rows[0]["FQE_Email"].ToString();
                            if (IsLoggedInAuthorisedUser(fqeEmail))
                            {
                                dvFilledForm.Visible = true;
                                dvRCM.Visible = false;
                                mainFormLogic = true;
                                dvAssignee.Visible = false;
                                RCMActions.Visible = false;
                            }

                        }



                        if (mainFormLogic)
                        {
                            if (dtdata.Rows[0]["BUName"] != null)
                            {
                                lblBU.Text = dtdata.Rows[0]["BUName"].ToString();

                            }
                            if (dtdata.Rows[0]["SBGName"] != null)
                            {
                                lblSBG.Text = dtdata.Rows[0]["SBGName"].ToString();
                            }
                            if (dtdata.Rows[0]["SBUName"] != null)
                            {
                                lblSBU.Text = dtdata.Rows[0]["SBUName"].ToString();
                                lblNewSBU.Text = dtdata.Rows[0]["SBUName"].ToString();
                                if (lblNewSBU.Text == "Industrial Infrastructure")
                                {
                                    lblInduCoE.Text = "coeqii@tataprojects.com";
                                }
                                else if (lblNewSBU.Text == "Urban Infrastructure")
                                {
                                    lblInduCoE.Text = "coeqii@tataprojects.com";
                                }
                                else
                                {
                                    lblInduCoE.Text = "coeqii@tataprojects.com";
                                }
                            }
                            if (dtdata.Rows[0]["ProjectName"] != null)
                            {
                                lblProject.Text = dtdata.Rows[0]["ProjectName"].ToString();
                            }
                            if (dtdata.Rows[0]["Location"] != null)
                            {
                                lblLocation.Text = dtdata.Rows[0]["Location"].ToString();
                            }
                            if (dtdata.Rows[0]["SubLocation"] != null)
                            {
                                lblSubLocation.Text = dtdata.Rows[0]["SubLocation"].ToString();
                            }
                            if (dtdata.Rows[0]["ProjectType"] != null)
                            {
                                lblProjectType.Text = dtdata.Rows[0]["ProjectType"].ToString();
                            }
                            if (dtdata.Rows[0]["Discipline"] != null)
                            {
                                lblDiscipline.Text = dtdata.Rows[0]["Discipline"].ToString();
                            }
                            if (dtdata.Rows[0]["MainActivity"] != null)
                            {
                                lblMainactivity.Text = dtdata.Rows[0]["MainActivity"].ToString();
                            }
                            if (dtdata.Rows[0]["Activity"] != null)
                            {
                                lblActivity.Text = dtdata.Rows[0]["Activity"].ToString();
                            }
                            if (dtdata.Rows[0]["Sub Activity"] != null)
                            {
                                lblSubActivity.Text = dtdata.Rows[0]["Sub Activity"].ToString();
                            }
                            if (dtdata.Rows[0]["Identification"] != null)
                            {
                                lblidentify.Text = dtdata.Rows[0]["Identification"].ToString();
                            }
                            if (dtdata.Rows[0]["SWIIssuedBy"] != null)
                            {
                                lblissuedby.Text = dtdata.Rows[0]["SWIIssuedBy"].ToString();
                            }
                            if (dtdata.Rows[0]["FQE_ID"] != null)
                            {
                                lblFQEID.Text = dtdata.Rows[0]["FQE_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["FQE_Name"] != null)
                            {
                                lblFQEName.Text = dtdata.Rows[0]["FQE_Name"].ToString();
                            }

                            if (dtdata.Rows[0]["FQE_Email"] != null)
                            {
                                lblFQEEmail.Text = dtdata.Rows[0]["FQE_Email"].ToString();
                            }

                            if (dtdata.Rows[0]["RCM_ID"] != null)
                            {
                                lblRCMID.Text = dtdata.Rows[0]["RCM_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Name"] != null)
                            {
                                lblRCMName.Text = dtdata.Rows[0]["RCM_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                lblRCMEmail.Text = dtdata.Rows[0]["RCM_Email"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Remarks"] != null)
                            {
                                txtRCMRemarks.Text = dtdata.Rows[0]["RCM_Remarks"].ToString();
                                lblAssigneeRCMRemarks.Text = dtdata.Rows[0]["RCM_Remarks"].ToString();
                            }
                            if (dtdata.Rows[0]["PM_ID"] != null)
                            {
                                lblPMID.Text = dtdata.Rows[0]["PM_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["PM_Name"] != null)
                            {
                                lblPMName.Text = dtdata.Rows[0]["PM_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["PM_Email"] != null)
                            {
                                lblPMEmail.Text = dtdata.Rows[0]["PM_Email"].ToString();
                            }
                            if (dtdata.Rows[0]["Assignee_Submitted_Date"] != null)
                            {
                                string dtAssSubDate = dtdata.Rows[0]["Assignee_Submitted_Date"].ToString();
                                if (!string.IsNullOrEmpty(dtAssSubDate))
                                {
                                    DateTime dtDate = Convert.ToDateTime(dtAssSubDate);
                                    lblAssigneeActiondate.Text = dtDate.ToString("dd/MM/yyyy");
                                    lblAssigneeActiondatefqe.Text = dtDate.ToString("dd/MM/yyyy");
                                }

                            }


                            DateTime dtNow = DateTime.Now;
                            dtClosuredate.SelectedDate = dtNow;

                            if (dtdata.Rows[0]["Assignee_Remarks"] != null)
                            {
                                assigneeRemarksTXT.Text = dtdata.Rows[0]["Assignee_Remarks"].ToString();
                                lblRCMAssigneeRemarks.Text = dtdata.Rows[0]["Assignee_Remarks"].ToString();
                                lblRCMAssigneeRemarksfqe.Text = dtdata.Rows[0]["Assignee_Remarks"].ToString();
                            }
                            //
                            if (dtdata.Rows[0]["Assignee_Email"] != null)
                            {
                                lblAssigneEmail.Text = dtdata.Rows[0]["Assignee_Email"].ToString();

                            }
                            if (dtdata.Rows[0]["Target_Closure_Date"] != null)
                            {
                                string dtTCD = dtdata.Rows[0]["Target_Closure_Date"].ToString();
                                if (!string.IsNullOrEmpty(dtTCD))
                                {
                                    DateTime dtDate = Convert.ToDateTime(dtTCD);
                                    lblTargetClosureDateAssRCM.Text = dtDate.ToString("dd/MM/yyyy");

                                    //TimeSpan difference = dtFqeDate - dtDate;
                                    //var days = difference.TotalDays;

                                }

                            }

                            GeteEvidenceDocument();
                            DataTable dtChildData = objQuality.GetComplaincesDatabyTransactionID(lblTransactionID.Text);
                            if (dtChildData != null)
                            {
                                grdCompliances.DataSource = dtChildData;
                                grdCompliances.DataBind();


                            }
                            string swi_id = dtdata.Rows[0]["SWIID"].ToString();
                            if (!string.IsNullOrEmpty(swi_id))
                            {
                                hplSWI.Visible = true;
                            }
                            //for (int i = 0; i < dtChildData.Rows.Count - 1; i++)
                            //{
                            //    var isComplied = dtChildData.Rows[i][2];
                            //    isComplied = isComplied.ToString().Trim();

                            //    if (isComplied.Equals("No"))
                            //    {
                            //        hplSWI.Visible = true;
                            //    }
                            //}

                            hplSWI.NavigateUrl = Constants.link + "Pages/QMDSWIForm.aspx?Tid=" + lblTransactionID.Text;


                        }
                    }
                    else
                    {
                        dvUnAuth.Visible = true;
                        dvFilledForm.Visible = false;
                        dvRCM.Visible = false;
                        RCMActions.Visible = false;
                        dvAssignee.Visible = false;
                        dvRCMClose.Visible = false;
                        dvfqeclose.Visible = false;
                    }
                }

            }
        }

        protected void dateDateBorrowed_OnDateChanged(object sender, EventArgs e)
        {
            // dtTargetClosureDate.MinDate = dtDate;
            dtTargetClosureDate.MaxDate = DateTime.Now;

        }

        private bool IsLoggedInAuthorisedUser(string email)
        {
            bool isValid = SPContext.Current.Web.CurrentUser.Email.Equals(email);
            return isValid;
        }

        private bool UpdateRCMActions()
        {
            bool isSaved = false;
            DataTable dtDiscipline = new DataTable();
            DateTime dtSelected = dtTargetClosureDate.SelectedDate;
            PickerEntity pckEntity = (PickerEntity)ppAssigneeName.ResolvedEntities[0];
            if (pckEntity != null)
            {
                try
                {
                    string assignee = pckEntity.Key;
                    SPUser userAssignee = SPContext.Current.Web.EnsureUser(assignee);
                    if (userAssignee != null)
                    {
                        string assigneeID = objQuality.GetOnlyEmployeeID(userAssignee.LoginName);
                        string assigneeName = userAssignee.Name;
                        string assigneeEmail = userAssignee.Email;
                        lblAssigneEmail.Text = userAssignee.Email;
                        SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                        DateTime today = DateTime.Now;
                        string todayStr = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                        String sqlQuery = "update QChecklist_TnxMain set Stage='Pending with Assignee', Assignee_ID='" + assigneeID + "',Assignee_Name='" + assigneeName + "',Assignee_Email='" + assigneeEmail + "',Target_Closure_Date='" + dtSelected.Date.ToString("yyyy-MM-dd HH:mm:ss") + "',RCM_Remarks='" + txtRCMRemarks.Text.Replace("'", "''") + "',[RCM_Submitted Date]='" + todayStr + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                        SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                        daAdap.Fill(dtDiscipline);
                        isSaved = true;
                        con.Close();
                    }
                }
                catch (Exception exp)
                {

                }

            }
            return isSaved;

        }

        protected void btnRCMActions_Click(object sender, EventArgs e)
        {
            bool isSaved = UpdateRCMActions();
            if (isSaved)
            {
                objQuality.SendAssigneeMail(lblRCMName.Text, lblAssigneEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                //string message = "You have updated successfully";
                //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                //sb.Append("<script type = 'text/javascript'>");
                //sb.Append("window.onload=function(){");
                //sb.Append("alert('");
                //sb.Append(message);
                //sb.Append("')};");
                //sb.Append("</script>");
                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());


                btnRCMActions.Enabled = false;

                lblRCMClosureSubmitNotification.Text = "Submitted successfully";
                string navigateUrl = Constants.link + "Pages/QMDPendingItems.aspx";
                lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
            }



        }

        protected void ddlDesciplines_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnAssigneeSubmit_Click(object sender, EventArgs e)
        {
            if (assigneeEvidenceFUP.HasFile)
            {
                SaveAssigeeActions();
                EvidenceDocument();
                objQuality.SendRCMForClosureMail(lblRCMName.Text, lblRCMEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                btnAssigneeSubmit.Visible = false;
                assigneeEvidenceFUP.Enabled = false;
                lblRCMSubNotification.Text = "Submitted successfully";
                string navigateUrl = Constants.link + "Pages/QMDPendingItems.aspx";
                lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
            }
            else
            {
                lblError.Text = "Document upload is mandatory";
            }
        }

        private void SaveAssigeeActions()
        {
            try
            {
                DataTable dtDiscipline = new DataTable();
                DateTime dt = assigneeClosureDateDATE.SelectedDate;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                DateTime today = DateTime.Now;
                string todayStr = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                string sqlQuery = "update QChecklist_TnxMain set Stage='Awaiting Closure',Assignee_Action_Date='" + dt.Date.ToString("yyyy-MM-dd HH:mm:ss") + "',Assignee_Submitted_Date='" + todayStr + "',Assignee_Remarks='" + assigneeRemarksTXT.Text.Replace("'", "''") + "',Assignee_SWI_Remarks='" + assigneeRemarksTXT.Text.Replace("'", "''") + "',ActualReworkCostQPR='" + txtRWCRCMCost.Text + "',CostBorneByQPR='" + ddlborne.SelectedItem.Text + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                con.Close();
            }
            catch (Exception exp)
            {
                //lblError.Text = exp.Message;
            }

        }


        private void EvidenceDocument()
        {

            string filePath = assigneeEvidenceFUP.PostedFile.FileName;
            string filename = Path.GetFileName(filePath);
            string ext = Path.GetExtension(filename);
            string contenttype = String.Empty;

            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
            }
            if (contenttype != String.Empty)
            {

                Stream fs = assigneeEvidenceFUP.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                byte[] compressedData = Compress(bytes);
                //Update the file into database
                string strQuery = "UPDATE QChecklist_TnxMain SET Evidence_Document = @Evidence_Document, DocContentType = @DocContentType, DocName = @DocName where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@DocName", SqlDbType.VarChar).Value = filename;
                cmd.Parameters.Add("@DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@Evidence_Document", SqlDbType.Binary).Value = compressedData;
                UpdateData(cmd);
                // lblMessage.ForeColor = System.Drawing.Color.Green;
                // lblMessage.Text = "File Uploaded Successfully";
            }
            else
            {
                // lblMessage.ForeColor = System.Drawing.Color.Red;
                // lblMessage.Text = "File format not recognised." + " Upload Image/Word/PDF/Excel formats";
            }
        }

        private Boolean UpdateData(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        private void GeteEvidenceDocument()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select FQE_Ev_DocName from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["FQE_Ev_DocName"] != null)
                        {
                            fqeEvidence.Text = dr["FQE_Ev_DocName"].ToString();
                        }

                    }
                }
                else
                {
                    fqeEvidence.Text = "File not uploaded";
                }
                con.Close();
            }
            catch (Exception exp)
            {

            }

        }
        private void GeteAssigneeEvidenceDocument()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select DocName from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["DocName"] != null)
                        {
                            LinkButton1.Text = dr["DocName"].ToString();
                            LinkButton2.Text = dr["DocName"].ToString();
                        }

                    }
                }
                else
                {
                    LinkButton1.Text = "File not uploaded";
                    LinkButton2.Text = "File not uploaded";
                }
                con.Close();
            }
            catch (Exception exp)
            {

            }

        }

        private void GetAlltheDetails()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select RCM_Name,Target_Closure_Date,RCM_Remarks from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["RCM_Name"] != null)
                        {
                            lblselectAssignee.Text = dr["RCM_Name"].ToString();
                        }
                        if (dr["Target_Closure_Date"] != null)
                        {

                            DateTime dtDate = Convert.ToDateTime(dr["Target_Closure_Date"]);
                            lblTargetClosureDate.Text = dtDate.ToString("dd/MM/yyyy");
                        }
                        if (dr["RCM_Remarks"] != null)
                        {
                            txtRCMRemarks.Text = dr["RCM_Remarks"].ToString();
                        }

                    }
                }
                else
                {

                }
                con.Close();
            }
            catch (Exception exp)
            {

            }


        }

        protected void btnClosure_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDiscipline = new DataTable();
                DateTime dt = dtClosuredate.SelectedDate;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                DateTime today = DateTime.Now;
                string todayStr = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                string sqlQuery = "update QChecklist_TnxMain set Stage='Non Compliant and Closed',Closure_Date='" + dt.Date.ToString("yyyy-MM-dd HH:mm:ss") + "',Actual_Closed_Date='" + todayStr + "',RCM_Closure_Remarks='" + txtverifi_Remarks.Text.Replace("'", "''") + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                con.Close();
                btnClosure.Enabled = false;
                //lblFQEEmail.Text
                objQuality.SendFinalClosureMail(lblRCMName.Text, lblFQEEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                lblAssigneeClosure.Text = "Submitted successfully";
                string navigateUrl = Constants.link + "Pages/QMDPendingItems.aspx";
                lblRCMClosureSubmitNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
            }
            catch (Exception exp)
            {
                //lblError.Text = exp.Message;
            }

            //
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select DocName,DocContentType,Evidence_Document from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["DocName"].ToString();
                    DateTime lastUpdateDate = LastDate();
                    DateTime dt2 = DateTime.Parse("2017-12-27 18:05:00.000");
                    if (lastUpdateDate > dt2)
                    {
                        bytes = Decompress((byte[])sdr["Evidence_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Evidence_Document"];
                    }
                    contentType = sdr["DocContentType"].ToString();
                    LinkButton1.Text = fileName;

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }

            LinkButton1.OnClientClick = "this.form.onsubmit = function() {return true;}";
        }


        private void GetFQEvidenceDocument()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select FQE_Ev_DocName from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["FQE_Ev_DocName"] != null)
                        {
                            upLoadEvidenceDoc.Text = dr["FQE_Ev_DocName"].ToString();
                        }

                    }
                }
                else
                {
                    upLoadEvidenceDoc.Text = "File not uploaded";
                }
                con.Close();
            }
            catch (Exception exp)
            {

            }

        }

        protected void chkClosureYes_CheckedChanged(object sender, EventArgs e)
        {
            if (chkClosureYes.Checked)
            {
                btnClosure.Enabled = true;
            }
            else
            {
                btnClosure.Enabled = false;
            }
        }


        protected void upLoadEvidenceDoc_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select FQE_Ev_DocName,FQE_Ev_DocContentType,FQE_Ev_Doc from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["FQE_Ev_DocName"].ToString();
                    if (Convert.ToInt32(lblTransactionID.Text) > 31467)
                    {
                        bytes = Decompress((byte[])sdr["FQE_Ev_Doc"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["FQE_Ev_Doc"];
                    }
                    contentType = sdr["FQE_Ev_DocContentType"].ToString();
                    upLoadEvidenceDoc.Text = fileName;

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }
        }

        private void ShowBasicDialog(string Url, string Title)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"<script type=""text/ecmascript"">");
            sb.AppendLine(@"ExecuteOrDelayUntilScriptLoaded(openBasicServerDialog, ""sp.js"");");
            sb.AppendLine(@"    function openBasicServerDialog()");
            sb.AppendLine(@"    {");
            sb.AppendLine(@"        var options = {");
            sb.AppendLine(string.Format(@"            url: '{0}',", Url));
            sb.AppendLine(string.Format(@"            title: '{0}'", Title));
            sb.AppendLine(@"        };");
            sb.AppendLine(@"        SP.UI.ModalDialog.showModalDialog(options);");
            sb.AppendLine(@"    }");
            sb.AppendLine(@"</script>");
            ltScriptLoader.Text = sb.ToString();

        }

        protected void lnkSWI_Click(object sender, EventArgs e)
        {

            string url = "https://tplnet.tataprojects.com/Pages/QMDSWIForm.aspx?Tid=" + lblTransactionID.Text + "";
            //string url = "https://tplnet.tataprojects.com/Pages/QualityChecklistsActions.aspx?Tid=" + lblTransactionID.Text;
            //  ShowBasicDialog(url, Title);

            //string s = "window.open('" + url + "', 'popup_window', 'width=750,height=900,left=400,top=50,resizable=yes');";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

        }

        protected void grdCompliances_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        protected void fqeEvidence_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select FQE_Ev_DocName,FQE_Ev_DocContentType,FQE_Ev_Doc from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["FQE_Ev_DocName"].ToString();
                    if (Convert.ToInt32(lblTransactionID.Text) > 31467)
                    {
                        bytes = Decompress((byte[])sdr["FQE_Ev_Doc"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["FQE_Ev_Doc"];
                    }
                    contentType = sdr["FQE_Ev_DocContentType"].ToString();

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }
        }
        private byte[] Compress(byte[] data)
        {
            var output = new MemoryStream();
            using (var gzip = new GZipStream(output, CompressionMode.Compress, true))
            {
                gzip.Write(data, 0, data.Length);
                gzip.Close();
            }
            return output.ToArray();
        }
        private byte[] Decompress(byte[] data)
        {
            var output = new MemoryStream();
            var input = new MemoryStream();
            input.Write(data, 0, data.Length);
            input.Position = 0;
            using (var gzip = new GZipStream(input, CompressionMode.Decompress, true))
            {
                var buff = new byte[64];
                var read = gzip.Read(buff, 0, buff.Length);
                while (read > 0)
                {
                    output.Write(buff, 0, read);
                    read = gzip.Read(buff, 0, buff.Length);
                }
                gzip.Close();
            }
            return output.ToArray();
        }
        public DateTime LastDate()
        {
            string date = string.Empty;
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select Assignee_Submitted_Date from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
                if (dt != null)
                {
                    date = dt.Rows[0]["Assignee_Submitted_Date"].ToString();
                }
            }
            catch (Exception exp)
            {
            }
            return Convert.ToDateTime(date);
        }

        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[3].Text = e.Row.Cells[3].Text.Replace("&amp;", "&");
                //var txt = e.Row.FindControl("txtFQECompRemarks") as TextBox;
                //txt.Text.Replace("&amp;", "&");

            }
        }

        protected void btnclosurefqe_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtfqeclosure = new DataTable();
                DateTime dt = dtClosuredatefqe.SelectedDate;
                SqlConnection con = null;
                DateTime today = DateTime.Now;
                string todayStr = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                string sqlQuery = string.Empty;
                if (ddlclosureverify.SelectedItem.Text == "Accept")
                {
                    using (con = new SqlConnection(Constants.ConnectionStringQMDChecklists))
                    {
                        sqlQuery = "update QChecklist_TnxMain set Stage='Non Compliant and Closed',Closure_Date='" + dt.Date.ToString("yyyy-MM-dd HH:mm:ss") + "',Actual_Closed_Date='" + todayStr + "',RCM_Closure_Remarks='" + txtverifi_Remarksfqe.Text.Replace("'", "''") + "' where Transaction_ID='" + lblTransactionID.Text + "'";
                        SqlCommand cmd = new SqlCommand(sqlQuery, con);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                else
                {
                    using (con = new SqlConnection(Constants.ConnectionStringQMDChecklists))
                    {
                        sqlQuery = "update QChecklist_TnxMain set Stage='Pending with RCM',RCM_Remarks='',[RCM_Submitted Date]=@RCMSubmittedDate,Assignee_Action_Date=@AssigneeActionDate,Assignee_Remarks='',Assignee_Submitted_Date=@AssigneeSubmittedDate,RCM_Closure_Remarks='" + txtverifi_Remarksfqe.Text.Replace("'", "''") + "' where Transaction_ID='" + lblTransactionID.Text + "'";
                        SqlCommand cmd = new SqlCommand(sqlQuery, con);
                        cmd.Parameters.Add("@RCMSubmittedDate", SqlDbType.DateTime).Value = DBNull.Value;
                        cmd.Parameters.Add("@AssigneeActionDate", SqlDbType.DateTime).Value = DBNull.Value;
                        cmd.Parameters.Add("@AssigneeSubmittedDate", SqlDbType.DateTime).Value = DBNull.Value;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                    }
                }
                btnclosurefqe.Enabled = false;
                //lblFQEEmail.Text
                if (ddlclosureverify.SelectedItem.Text == "Accept")
                {
                    objQuality.SendFinalClosureMail(lblRCMName.Text, lblFQEEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                }
                else
                {
                    objQuality.SendBacktoRCM(lblRCMName.Text, lblRCMEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                }
                lblAssigneeClosure.Text = "Submitted successfully";
                string navigateUrl = Constants.link + "Pages/QMDPendingItems.aspx";
                lblRCMClosureSubmitNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
            }
            catch (Exception exp)
            {
                //lblError.Text = exp.Message;
            }


        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select DocName,DocContentType,Evidence_Document from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["DocName"].ToString();
                    DateTime lastUpdateDate = LastDate();
                    DateTime dt2 = DateTime.Parse("2017-12-27 18:05:00.000");
                    if (lastUpdateDate > dt2)
                    {
                        bytes = Decompress((byte[])sdr["Evidence_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Evidence_Document"];
                    }
                    contentType = sdr["DocContentType"].ToString();
                    LinkButton2.Text = fileName;

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }

            LinkButton2.OnClientClick = "this.form.onsubmit = function() {return true;}";
        }


    }
}

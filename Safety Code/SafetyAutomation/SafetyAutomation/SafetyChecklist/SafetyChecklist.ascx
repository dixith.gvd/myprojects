﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SafetyChecklist.ascx.cs" Inherits="SafetyAutomation.SafetyChecklist.SafetyChecklist" %>

<style type="text/css">
    .table {
        padding: 0;
        border-spacing: 0;
        text-align: center;
        font-family:'Trebuchet MS';
        font-size: 10pt;
        vertical-align: middle;
        border-collapse: collapse;
    }

    .auto-style1 {
        height: 30px;
    }

    .required {
        color: Red;
    }

    .auto-style2 {
        width: 60%;
        height: 30px;
    }
    button{
        font-family:'Trebuchet MS';
        font-size:10pt;
    }
    div{
    font-family:'Trebuchet MS';
    font-size:10pt;
}
H1 {
	MARGIN-BOTTOM: 0px; FONT-SIZE: 24pt; FONT-WEIGHT: normal; font-family:'Trebuchet MS'; COLOR: #354d3f; MARGIN-TOP: 0px
}
.xdXButton {
	FONT-SIZE: 10pt; FONT-FAMILY: Trebuchet MS; WIDTH: 150px
}
    .auto-style3 {
        border-collapse: collapse;
        table-layout: fixed;
        font-size: 10pt;
        font-family:'Trebuchet MS';
        text-transform: none;
        font-weight: normal;
        /*color: blue;
        font-style: normal;
        border-style: none;
        border-color: inherit;
        border-width: medium;*/
    }
    .auto-style4 {
        text-align: center;
        border-left: 0pt solid #bfbfbf;
        border-right: 0pt solid #bfbfbf;
        border-top: 0pt solid #bfbfbf;
        border-bottom-color: #d8d8d8;
        /*padding-left: 22px;
        padding-right: 22px;
        padding-top: 32px;
        padding-bottom: 14px;*/
        /*background-color: #ffffff;*/
    }
    .auto-style5 {
        border-left: 0pt solid #bfbfbf;
        border-right: 0pt solid #bfbfbf;
        border-top-color: #d8d8d8;
        border-bottom: 0pt solid #bfbfbf;
        padding: 0px;
        /*background-color: #ffffff;*/
    }
    .auto-style6 {
        width: 544px;
        border-collapse: collapse;
        table-layout: fixed;
        font-size: 10pt;
        font-family:'Trebuchet MS';
        text-transform: none;
        font-weight: normal;
        color: blue;
        /*font-style: normal;
        border-style: none;
        border-color: inherit;
        border-width: medium;*/
    }
    .auto-style7 {
        width: 650px;
        border-collapse: collapse;
        table-layout: fixed;
        font-size: 10pt;
        font-family:'Trebuchet MS';
        text-transform: none;
        font-weight: normal;
        color: blue;
        /*font-style: normal;
        border-style: none;
        border-color: inherit;
        border-width: medium;*/
    }
    .auto-style8 {
        width: 648px;
        border-collapse: collapse;
        table-layout: fixed;
        font-size: 10pt;
        font-family:'Trebuchet MS';
        text-transform: none;
        font-weight: normal;
        color: blue;
        /*font-style: normal;
        border-style: none;
        border-color: inherit;
        border-width: medium;*/
    }
    .auto-style9 {
        width: 651px;
        border-collapse: collapse;
        table-layout: fixed;
        font-size: 10pt;
        font-family:'Trebuchet MS';
        text-transform: none;
        font-weight: normal;
        color: blue;
        /*font-style: normal;
        border-style: none;
        border-color: inherit;
        border-width: medium;*/
    }
    .auto-style10 {
        width: 649px;
        border-collapse: collapse;
        table-layout: fixed;
        font-size: 10pt;
        font-family:'Trebuchet MS';
        text-transform: none;
        font-weight: normal;
        color: blue;
        /*font-style: normal;
        border-style: none;
        border-color: inherit;
        border-width: medium;*/
    }
    .auto-style11 {
        width: 646px;
        border-collapse: collapse;
        table-layout: fixed;
        font-size: 10pt;
        font-family:'Trebuchet MS';
        text-transform: none;
        font-weight: normal;
        color: blue;
        /*font-style: normal;
        border-style: none;
        border-color: inherit;
        border-width: medium;*/
    }
    .auto-style13 {
        width: 295px;
    }
    .auto-style14 {
        width: 248px;
    }
    .auto-style15 {
        height: 23px;
    }
    </style>

<div id="dvMainForm" runat="server" visible="false">

     <asp:Panel ID="PanelMain" runat="server">
        <div id="dvMainFormChecklist" runat="server" visible="false">
            <br />
            <table border="1" cellpadding="1" cellspacing="1" width="100%">
                <tr>
                    <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                        <asp:Label ID="Label5" runat="server" ForeColor="White" Text="Upload Safety Checklists" CssClass="auto-style3" Font-Size="Large"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right" >
                        <asp:Button Font-Size="Medium" ID="btnhome" runat="server" BackColor="#507CD1" ForeColor="White" Text="Home" CssClass="auto-style3" OnClick="btnhome_Click" Width="61px" />
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                        <asp:Label ID="Label21" ForeColor="White" runat="server" Text="Frequency" CssClass="auto-style3"></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <asp:Label ID="lblfrequency" CssClass="auto-style3" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                        <asp:Label ID="Label23" ForeColor="White" runat="server" Text="Checklist" CssClass="auto-style3"></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                        <asp:Label ID="lblchecklist" runat="server" Text="" CssClass="auto-style3"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                        <asp:Label ID="Label1" ForeColor="White" runat="server" Text="BU" CssClass="auto-style3"></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <asp:Label ID="lblBU" runat="server" CssClass="auto-style3"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                        <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Project" CssClass="auto-style3"></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                        <asp:DropDownList ID="ddlProjectCode" runat="server" OnSelectedIndexChanged="ddlProjectCode_SelectedIndexChanged" Width="100%" AutoPostBack="true">
                        </asp:DropDownList>
                        <%--<asp:RequiredFieldValidator ID="RFProject" runat="server" ErrorMessage="Please Select Project" InitialValue="--Select--" ControlToValidate="ddlProjectCode" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                        <%-- <asp:Label ID="lblProject" runat="server"></asp:Label>--%></td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                        <asp:Label ID="Label22" ForeColor="White" runat="server" Text="Inspection Date" CssClass="auto-style3"></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <SharePoint:DateTimeControl ID="DateTimeControl1" DateOnly="true" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                        <asp:Label ID="Label6" ForeColor="White" runat="server" Text="Location" CssClass="auto-style3"></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                        <asp:TextBox ID="txtlocation" runat="server" Width="98%" CssClass="auto-style3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                        <asp:Label ID="Label25" ForeColor="White" runat="server" Text="Compliance" CssClass="auto-style3"></asp:Label>
                        &nbsp;<font color="#ffffff" face="Trebuchet MS"><strong><font color="#ff0000">*</font></strong></font></td>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <asp:RadioButton ID="rdyes" runat="server" Text="Yes" ValidationGroup="Compliance" GroupName="Compliance" />
                        <asp:RadioButton ID="rdno" runat="server" Text="No" ValidationGroup="Compliance" GroupName="Compliance" />
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                        <asp:Label ID="Label30" ForeColor="White" runat="server" Text="Observations" CssClass="auto-style3"></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <asp:TextBox ID="txtobservation" runat="server" TextMode="MultiLine" Width="98%" CssClass="auto-style3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                        <asp:Label ID="Label31" ForeColor="White" runat="server" Text="Upload Attachments(for non-compliance)" CssClass="auto-style3"></asp:Label>
                        <br />
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <asp:FileUpload ID="uploadattach" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                        <asp:Label ID="Label32" ForeColor="White" runat="server" Text="Upload Checklist Document" CssClass="auto-style3"></asp:Label>
                        &nbsp;<font color="#ffffff" face="Trebuchet MS"><strong><font color="#ff0000">*</font></strong></font></td>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <asp:FileUpload ID="uploaddoc" runat="server" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.png|.jpg|.jpeg|.gif|.xls|.xlsx|.DOC|.DOCX|.PDF|.PNG|.JPG|.JPEG|.GIF|.XLS|.XLSX)$"
    ControlToValidate="uploaddoc" runat="server" ForeColor="Red" CssClass="auto-style3" ErrorMessage="Please select a valid word,excel,jpg,jpeg,png,gif or pdf file format."
    Display="Dynamic" />
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #fff; text-align: left; padding-left: 10px; text-align:center" class="auto-style1" colspan="2">
                        <font color="#000000" CssClass="auto-style3" face="Trebuchet MS">
                            I here by self certify above furnished information is accurate.
                        <asp:RadioButton ID="rbyes" runat="server" Text="Yes" CssClass="auto-style3" ValidationGroup="Certify" GroupName="Certify" />
                        <asp:RadioButton ID="rbno" runat="server" Text="No" CssClass="auto-style3" ValidationGroup="Certify" GroupName="Certify" />
                        </font></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center">
                        <asp:Button Font-Size="Medium" ID="btnsubmit" runat="server" Text="Submit" CssClass="auto-style3" OnClick="Button1_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div id="dvUnAuth" runat="server" visible="false">
            <asp:Label ID="Label13" runat="server" ForeColor="Red" Font-Size="Large" CssClass="auto-style3" Text="You are not authorized to access"></asp:Label>
            <br />
            <br />
            <br />
            <asp:Label ID="Label114" runat="server" ForeColor="Red" Font-Size="Large" CssClass="auto-style3" Text="[Can be filled only by safety officer]"></asp:Label>
        </div>
        <table>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="lblSOSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" CssClass="auto-style3" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="lblRCMSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" CssClass="auto-style3" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="lblAsigneeSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" CssClass="auto-style3" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style14">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style14">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style14">
                    <asp:Label ID="lblLinkClosureNotification" runat="server" CssClass="auto-style3" ></asp:Label>
                </td>
            </tr>
        </table>
        <asp:Label ID="lblProjectCode" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblTransactionID" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblRCMID" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblRCMName" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblRCMEmail" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblPMID" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblPMName" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblPMEmail" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblAssigneID" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblAssigneName" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblAssigneEmail" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblbuheadid" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblbuheadName" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblbuheadEmail" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblHead" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblHeadID" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblHeadEmail" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblSOName" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblSOID" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblSOEmail" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblPE" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblPEID" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblPEEmail" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblNewSBU" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
        <asp:Label ID="lblError" ForeColor="Red" runat="server" CssClass="auto-style3" ></asp:Label>
        <asp:Label ID="lblInduCoE" runat="server" Visible="false" CssClass="auto-style3"></asp:Label>
    </asp:Panel>

    <asp:Panel ID="panelehs" runat="server">
        &nbsp;<table align="center" class="auto-style3" style="WORD-WRAP: break-word;" tabindex="-1">
            <colgroup>
                <col style="WIDTH: 651px" />
            </colgroup>
            <tr style="MIN-HEIGHT: 83px">
                <td class="auto-style4" style="valign: bottom;" valign="bottom">
                    <h1>EHS Checklists</h1>
                </td>
            </tr>
            <tr style="MIN-HEIGHT: 140px">
                <td class="auto-style5" style="valign: top;" valign="top">
                    <div>
                        <table border="1" bordercolor="#507CD1" class="auto-style6" style="WORD-WRAP: break-word;" tabindex="-1">
                            <colgroup>
                                <col style="WIDTH: 325px" />
                                <col style="WIDTH: 325px" />
                            </colgroup>
                            <tbody valign="top">
                                <tr style="MIN-HEIGHT: 19px">
                                    <td colspan="2" class="auto-style15">
                                        <div>
                                            &nbsp;</div>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 42px">
                                    <td class="auto-style6">
                                        
                                            <asp:Button Font-Size="Medium" ID="btndaily" runat="server" Text="Daily" CssClass="xdXButton"  BackColor="#507CD1" ForeColor="White" Height="35px" Width="309px" OnClick="btndaily_Click"  />
                                        
                                    </td>
                                    <td>
                                        
                                            <asp:Button Font-Size="Medium" ID="btnweekly" runat="server" Text="Weekly" CssClass="xdXButton" BackColor="#507CD1" ForeColor="White" Height="35px" Width="309px" OnClick="btnweekly_Click"  />
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style13">
                                        
                                            <asp:Button Font-Size="Medium" ID="btnmonthly" runat="server" Text="Monthly" CssClass="xdXButton" BackColor="#507CD1" ForeColor="White"  Height="35px" Width="309px" OnClick="btnmonthly_Click" />
                                        
                                    </td>
                                    <td>
                                        
                                            <asp:Button Font-Size="Medium" ID="btnquarterly" runat="server" Text="Quarterly" CssClass="xdXButton" BackColor="#507CD1" ForeColor="White" Height="35px" Width="309px" OnClick="btnquarterly_Click" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelDaily" runat="server">
        <table align="center" class="auto-style7" style="WORD-WRAP: break-word;" tabindex="-1">
            <colgroup>
                <col style="WIDTH: 650px" />
            </colgroup>
            <tr style="MIN-HEIGHT: 83px">
                <td class="auto-style4" style="valign: bottom;" valign="bottom">
                    <h1><font size="5">Daily Checklists</font></h1>
                </td>
            </tr>
            <tr style="MIN-HEIGHT: 140px">
                <td class="auto-style5" style="valign: top;" valign="top">
                    <div>
                        <table border="1" bordercolor="#507CD1" class="auto-style8" style="WORD-WRAP: break-word;" tabindex="-1">
                            <colgroup>
                                <col style="WIDTH: 325px" />
                                <col style="WIDTH: 323px" />
                            </colgroup>
                            <tbody valign="top">
                                <tr style="MIN-HEIGHT: 19px">
                                    <td colspan="2">
                                        <div align="left">
                                            <asp:Button Font-Size="Medium" ID="lblbackdaily" runat="server" Text="Back" CssClass="xdXButton" BackColor="#507CD1" ForeColor="White" OnClick="lblbackdaily_Click" Width="61px"  />
                                        </div>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td colspan="2">
                                        <div align="center">
                                            <font face="Trebuchet MS" size="4"><strong>Mandatory Checklists</strong></font></div>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnhouse" runat="server" Text="Housekeeping" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btnhouse_Click"  />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnppe" runat="server" Text="Personal Protective Equipment (PPE)" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btnppe_Click1" />
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 42px">
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnhira" runat="server" Text="HIRA talk" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btnhira_Click1" />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnpermit" runat="server" Text="Permit to Work" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btnpermit_Click1" />
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 44px">
                                    <td colspan="2" align="center">
                                        <asp:Button Font-Size="Medium" ID="btndailyssi" runat="server" Text="Daily site safety inspection" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btndailyssi_Click1" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div align="center">
                                            <font face="Trebuchet MS" size="4"><strong>Other Checklists</strong></font></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnheavy" runat="server" Text="Heavy Lifting" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btnheavy_Click" />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnworking" runat="server" Text="Working at Height" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btnworking_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnscaffold" runat="server" Text="Scaffolding" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btnscaffold_Click" />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnstring" runat="server" Text="Stringing" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btnstring_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btntfs" runat="server" Text="Tower Foundation Stringing" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btntfs_Click" />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnttt" runat="server" Text="Tower Tightning &amp; Tack Welding" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btnttt_Click" />
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 44px">
                                    <td colspan="2" align="center">
                                        <asp:Button Font-Size="Medium" ID="btntetf" runat="server" Text="Tower Erection Tower Foundation" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btntetf_Click" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="PanelWeekly" runat="server">
        <table align="center" class="auto-style9" style="WORD-WRAP: break-word;" tabindex="-1">
            <colgroup>
                <col style="WIDTH: 651px" />
            </colgroup>
            <tr style="MIN-HEIGHT: 83px">
                <td class="auto-style4" style="valign: bottom;" valign="bottom">
                    <h1><font size="5">Weekly&nbsp;Checklists</font></h1>
                </td>
            </tr>
            <tr style="MIN-HEIGHT: 140px">
                <td class="auto-style5" style="valign: top;" valign="top">
                    <div>
                        <table border="1" bordercolor="#507CD1" class="auto-style10" style="WORD-WRAP: break-word;" tabindex="-1">
                            <colgroup>
                                <col style="WIDTH: 324px" />
                                <col style="WIDTH: 325px" />
                            </colgroup>
                            <tbody valign="top">
                                <tr style="MIN-HEIGHT: 19px">
                                    <td colspan="2">
                                        <div>
                                            <asp:Button Font-Size="Medium" ID="lblbackweekly" runat="server" Text="Back" CssClass="xdXButton" BackColor="#507CD1" ForeColor="White" OnClick="lblbackweekly_Click" Width="61px" />
                                        </div>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td colspan="2">
                                        <div align="center">
                                            <font face="Trebuchet MS" size="4"><strong>Mandatory Checklists</strong></font></div>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td>
                                        <div>
                                            <asp:Button Font-Size="Medium" ID="lblesafety" runat="server" Text="Electrical Safety" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="lblesafety_Click" />
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <asp:Button Font-Size="Medium" ID="lbltraining" runat="server" Text="Training" CssClass="xdXButton" Height="35px" Width="311px" BackColor="#507CD1" ForeColor="White" OnClick="lbltraining_Click" />
                                        </div>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td>
                                        <div>
                                            <asp:Button Font-Size="Medium" ID="lblcft" runat="server" Text="Cross Functional Team (CFT) audit" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="lblcft_Click" />
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <asp:Button Font-Size="Medium" ID="btnwsm" runat="server" Text="Weekly safety meeting" CssClass="xdXButton" Height="35px" Width="311px" BackColor="#507CD1" ForeColor="White" OnClick="btnwsm_Click" />
                                        </div>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 25px">
                                    <td colspan="2">
                                        <div align="center">
                                            <font face="Trebuchet MS" size="4"><strong>Other Checklists</strong></font></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            <asp:Button Font-Size="Medium" ID="btnworkmethod" runat="server" Text="Work method" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btnworkmethod_Click" />
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <asp:Button Font-Size="Medium" ID="btnttbpi" runat="server" Text="Tools & Tackles Batching Plant Inspection" CssClass="xdXButton" Height="35px" Width="311px" BackColor="#507CD1" ForeColor="White" OnClick="btnttbpi_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="PanelMonthly" runat="server">
        <table align="center" class="auto-style8" style="WORD-WRAP: break-word;" tabindex="-1">
            <colgroup>
                <col style="WIDTH: 648px" />
            </colgroup>
            <tr style="MIN-HEIGHT: 83px">
                <td class="auto-style4" style="valign: bottom;" valign="bottom">
                    <h1 align="center"><font size="5">Monthly&nbsp;Checklists</font></h1>
                </td>
            </tr>
            <tr style="MIN-HEIGHT: 140px">
                <td class="auto-style5" style="valign: top;" valign="top">
                    <div>
                        <table border="1" bordercolor="#507CD1" class="auto-style11" style="WORD-WRAP: break-word;" tabindex="-1">
                            <colgroup>
                                <col style="WIDTH: 312px" />
                                <col style="WIDTH: 334px" />
                            </colgroup>
                            <tbody valign="top">
                                <tr style="MIN-HEIGHT: 19px">
                                    <td colspan="2">
                                        <h1 align="left">
                                            <asp:Button Font-Size="Medium" ID="lblbackmonthly" runat="server" Text="Back" CssClass="xdXButton" BackColor="#507CD1" ForeColor="White" OnClick="lblbackmonthly_Click" Width="61px" />
                                        </h1>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td colspan="2">
                                        <div align="center">
                                            <font face="Trebuchet MS" size="4"><strong>Mandatory Checklists</strong></font></div>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnltt" runat="server" Text="Lifting Tools & Tackles" CssClass="xdXButton"  Height="35px" Width="297px" BackColor="#507CD1" ForeColor="White" OnClick="btnltt_Click" />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnwmachine" runat="server" Text="Welding Machine" CssClass="xdXButton"  Height="35px" Width="320px" BackColor="#507CD1" ForeColor="White" OnClick="btnwmachine_Click" />
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btneme" runat="server" Text="Earth Moving Equipment" CssClass="xdXButton" Height="35px" Width="297px" BackColor="#507CD1" ForeColor="White" OnClick="btneme_Click" />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btngascutting" runat="server" Text="Gas Cutting" CssClass="xdXButton" Height="35px" Width="320px" BackColor="#507CD1" ForeColor="White" OnClick="btngascutting_Click" />
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnhvehicle" runat="server" Text="Heavy Vehicle" CssClass="xdXButton" Height="35px" Width="297px" BackColor="#507CD1" ForeColor="White" OnClick="btnhvehicle_Click" />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnhnci" runat="server" Text="Hydra & Cranes Inspection" CssClass="xdXButton" Height="35px" Width="320px" BackColor="#507CD1" ForeColor="White" OnClick="btnhnci_Click" />
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnfextnr" runat="server" Text="Fire Extnr" CssClass="xdXButton" Height="35px" Width="297px" BackColor="#507CD1" ForeColor="White" OnClick="btnfextnr_Click" />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btndgset" runat="server" Text="DG Set" CssClass="xdXButton" Height="35px" Width="320px" BackColor="#507CD1" ForeColor="White" OnClick="btndgset_Click" />
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 25px">
                                    <td colspan="2" align="center">
                                        <asp:Button Font-Size="Medium" ID="btnpegm" runat="server" Text="Portable Electric Grinding Machine" CssClass="xdXButton" Height="35px" Width="309px" BackColor="#507CD1" ForeColor="White" OnClick="btnpegm_Click" />
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 25px">
                                    <td colspan="2">
                                        <div align="center">
                                            <font face="Trebuchet MS" size="4"><strong>Other Checklists</strong></font></div>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnbarb" runat="server" Text="Bar Bending" CssClass="xdXButton" Height="35px" Width="297px" BackColor="#507CD1" ForeColor="White" OnClick="btnbarb_Click" />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnbarc" runat="server" Text="Bar Cutting" CssClass="xdXButton" Height="35px" Width="320px" BackColor="#507CD1" ForeColor="White" OnClick="btnbarc_Click" />
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td style="margin-left: 40px">
                                        <asp:Button Font-Size="Medium" ID="btncpump" runat="server" Text="Concrete Pump" CssClass="xdXButton" Height="35px" Width="297px" BackColor="#507CD1" ForeColor="White" OnClick="btncpump_Click" />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnwinchi" runat="server" Text="Winch inspection" CssClass="xdXButton" Height="35px" Width="320px" BackColor="#507CD1" ForeColor="White" OnClick="btnwinchi_Click" />
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btnlcinsp" runat="server" Text="Labor Camp Inspection" CssClass="xdXButton" Height="35px" Width="297px" BackColor="#507CD1" ForeColor="White" OnClick="btnlcinsp_Click" />
                                    </td>
                                    <td>
                                        <asp:Button Font-Size="Medium" ID="btneresource" runat="server" Text="Emergency resource" CssClass="xdXButton" Height="35px" Width="320px" BackColor="#507CD1" ForeColor="White" OnClick="btneresource_Click" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="PanelQuarterly" runat="server">
        <table align="center" class="auto-style9" style="WORD-WRAP: break-word;" tabindex="-1">
            <colgroup>
                <col style="WIDTH: 651px" />
            </colgroup>
            <tr style="MIN-HEIGHT: 83px">
                <td class="auto-style4" style="valign: bottom;" valign="bottom">
                    <h1><font size="5">Quarterly Checklists</font></h1>
                </td>
            </tr>
            <tr style="MIN-HEIGHT: 140px">
                <td class="auto-style5" style="valign: top;" valign="top">
                    <div>
                        <table border="1" bordercolor="#507CD1" class="auto-style10" style="WORD-WRAP: break-word;" tabindex="-1">
                            <colgroup>
                                <col style="WIDTH: 649px" />
                            </colgroup>
                            <tbody valign="top">
                                <tr style="MIN-HEIGHT: 19px">
                                    <td>
                                        <div>
                                            <asp:Button Font-Size="Medium" ID="btnbackquarterly" runat="server" Text="Back" CssClass="xdXButton" BackColor="#507CD1" ForeColor="White" OnClick="btnbackquarterly_Click" Width="61px" />
                                        </div>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 19px">
                                    <td>
                                        <div align="center">
                                            <font face="Trebuchet MS" size="4"><strong>Mandatory Checklists</strong></font></div>
                                    </td>
                                </tr>
                                <tr style="MIN-HEIGHT: 45px">
                                    <td align="center">
                                        <div>
                                            <asp:Button Font-Size="Medium" ID="sbchecklist" runat="server" Text="Safety Belt Checklist" CssClass="xdXButton" BackColor="#507CD1" ForeColor="White" Height="35px" Width="309px" OnClick="sbchecklist_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
   

</div>
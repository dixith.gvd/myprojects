﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace QMDDashboard_TPL.QMDAdminActions
{
    [ToolboxItemAttribute(false)]
    public partial class QMDAdminActions : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public QMDAdminActions()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        SPOperations objSPO = new SPOperations();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                dvNewProject.Visible = false;
                dvModifyFQE.Visible = false;
            }
        }

        protected void ddlSBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSBU.SelectedIndex > 0)
            {
                string sbuCode = ddlSBU.SelectedValue.ToString();
                DataTable dtProjectCode = objSPO.GetProjectCode(sbuCode);
                if (dtProjectCode != null)
                {
                    ddlProjects.DataSource = dtProjectCode;
                    ddlProjects.DataTextField = "ProjectName";
                    ddlProjects.DataValueField = "Title";
                    ddlProjects.DataBind();
                    ddlProjects.Items.Insert(0, new ListItem("--Select--", "0"));
                    trUser.Visible = true;
                    //ddlProjects.Text = string.Empty;
                }
            }
            else
            {
                //lblError.Text = "Please select valid SBU Code";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (ppeEmpName.ResolvedEntities.Count > 0)
            {
                PickerEntity pckEntity = (PickerEntity)ppeEmpName.ResolvedEntities[0];
                if (pckEntity != null)
                {
                    string fqeName = pckEntity.Key;
                    SPUser userFQE = SPContext.Current.Web.EnsureUser(fqeName);
                    string sbu = ddlSBU.SelectedItem.Text;
                    string projectCode = ddlProjects.SelectedItem.Value;
                    string projectName = ddlProjects.SelectedItem.Text;
                    string Template = ddltemplate.SelectedItem.Text;
                    int TemplateNo = objSPO.GetTemplateNo(Template);
                    string Frequency = objSPO.GetFrequency(ddltemplate.SelectedItem.Text);
                    if (!string.IsNullOrEmpty(fqeName) && !string.IsNullOrEmpty(projectCode))
                    {
                        objSPO.AddProjectInfoInQMD(sbu, projectCode, projectName, userFQE, Template, TemplateNo,Frequency);
                    }
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddlSBUEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSBUEdit.SelectedIndex > 0)
            {
                try
                {
                    string sbuCode = ddlSBUEdit.SelectedValue.ToString();
                    DataTable dtProjectCode = objSPO.GetProjectsFromQMD(sbuCode);
                    if (dtProjectCode != null)
                    {
                        ddlProjectFQEEdit.DataSource = dtProjectCode;
                        ddlProjectFQEEdit.DataTextField = "ProjectName";
                        ddlProjectFQEEdit.DataValueField = "ProjectName";
                        ddlProjectFQEEdit.DataBind();
                        ddlProjectFQEEdit.Items.Insert(0, new ListItem("--Select--", "0"));
                        trProjectFQE.Visible = true;
                    }
                }
                catch (Exception exp)
                {
                    lblError.Text = exp.Message;
                }
            }
            else
            {
                lblError.Text = "Please select valid SBU";
            }
        }

        protected void ddlProjectFQEEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
               // GetTemplates();
                //string fqeName = objSPO.GetFQEFromQMDByProjectCode(ddlProjectFQEEdit.SelectedItem.Text);
                DataTable dttemplates = objSPO.GetTemplateFromQMDByProjectCode(ddlProjectFQEEdit.SelectedItem.Text, ddlSBUEdit.SelectedItem.Text);
                ddlEditTemplate.DataSource = dttemplates;
                ddlEditTemplate.DataTextField = "Template";
                ddlEditTemplate.DataValueField = "Template";
                ddlEditTemplate.DataBind();
                ddlEditTemplate.Items.Insert(0, new ListItem("--Select--", "0"));                        
                trNameFQE.Visible = true;
                trFQEModify.Visible = true;
            }
            catch(Exception exp)
            {
                lblError.Text = exp.Message;
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                PickerEntity pckEntity = (PickerEntity)peFQE.ResolvedEntities[0];
                if (pckEntity != null)
                {
                    string fqeName = pckEntity.Key;
                    SPUser userFQE = SPContext.Current.Web.EnsureUser(fqeName);
                    string projectCode = ddlProjectFQEEdit.SelectedItem.Text;
                    string Template = ddlEditTemplate.SelectedItem.Text;
                    string SBUName = ddlSBUEdit.SelectedItem.Text;
                    if (!string.IsNullOrEmpty(fqeName) && !string.IsNullOrEmpty(projectCode))
                    {
                        objSPO.UpdateFQEInQMD(projectCode, userFQE,Template,SBUName);
                    }
                }
            }
            catch (Exception exp)
            {
                lblError.Text = exp.Message;
            }
        }

        protected void rdbNew_CheckedChanged(object sender, EventArgs e)
        {
            dvNewProject.Visible = true;
            dvModifyFQE.Visible = false;
            try
            {
                Hashtable htSite = new Hashtable();
                htSite = objSPO.GetSBUCodes();
                if (htSite.Count > 0)
                {
                    ddlSBU.DataSource = htSite;
                    ddlSBU.DataTextField = "Value";
                    ddlSBU.DataValueField = "Key";
                    ddlSBU.DataBind();
                    ddlSBU.Items.Insert(0, new ListItem("--Select--", "0"));
                    trProject.Visible = true;
                }
            }
            catch (Exception exp)
            {
                lblError.Text = exp.Message;
            }
        }

        protected void rdbChangeFQE_CheckedChanged(object sender, EventArgs e)
        {
            dvNewProject.Visible = false;
            dvModifyFQE.Visible = true;
            try
            {
                Hashtable htSite = new Hashtable();
                htSite = objSPO.GetSBUCodesFromQMD();
                if (htSite.Count > 0)
                {
                    ddlSBUEdit.DataSource = htSite;
                    ddlSBUEdit.DataTextField = "Key";
                    ddlSBUEdit.DataValueField = "Value";
                    ddlSBUEdit.DataBind();
                    ddlSBUEdit.Items.Insert(0, new ListItem("--Select--", "0"));
                    trSBUFQE.Visible = true;
                }
            }
            catch (Exception exp)
            {
                lblError.Text = exp.Message;
            }
        }

        protected void ddlProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetTemplates();
        }
        public void GetTemplates()
        {        
            if (SPContext.Current.Web.Lists["ProjectCodeProjectName"] != null)
            {
                SPList list = SPContext.Current.Web.Lists["ProjectCodeProjectName"];
                SPFieldChoice field = (SPFieldChoice)list.Fields["Template"];
                ddltemplate.DataSource = field.Choices;
                ddltemplate.DataBind();
                ddlEditTemplate.DataSource = field.Choices;
                ddlEditTemplate.DataBind();
               // ddltemplate.Items.Add(new ListItem("--Select--", "0"));             
            }
        }
       
        protected void ddlEditTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblExistingFQE.Text = objSPO.GetFQEFromQMDByProjectCode(ddlProjectFQEEdit.SelectedItem.Text, ddlSBUEdit.SelectedItem.Text, ddlEditTemplate.SelectedItem.Text);
        }

     

       
        
    }
}

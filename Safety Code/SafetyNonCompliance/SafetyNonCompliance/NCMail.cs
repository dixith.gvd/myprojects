﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Xml;
using System.Xml.XPath;
namespace SafetyNonCompliance
{
    class NCMail
    {
        public void ProcessLogic()
        {
            try
            {
                string pmemail = string.Empty;
                string pmname = string.Empty;
                string hopemail = string.Empty;
                string hopname = string.Empty;
                DataTable ds = PendingRecords();
                string pcode = string.Empty;
                List<string> pcodes = new List<string>();
                foreach (DataRow row in ds.Rows)
                {
                    DataTable dt = getProjectDetails(row["ProjectCode"].ToString());
                    if (!string.IsNullOrEmpty(dt.Rows[0]["PM"].ToString()))
                    {
                        pmemail = dt.Rows[0]["PM"].ToString();
                        pmname = GetPMName(pmemail);
                    }
                    else
                    {
                        pmemail = string.Empty;
                        pmname = string.Empty;
                    }
                    string buname = dt.Rows[0]["BU Name"].ToString();
                    DataTable dsrecords = new DataTable();
                    dsrecords = GetRecords(row["ProjectCode"].ToString());
                    DateTime todaysdate = DateTime.Today;
                    string todaydate = String.Format("{0:dd MMMM yyyy}", todaysdate);
                    string mailBody = FormMailBody(pmname, dsrecords, todaydate);
                    MailAddress SendFrom = new MailAddress("coesheii@tataprojects.com");

                    MailMessage MyMessage = new MailMessage();
                    MyMessage.From = SendFrom;
                    if (pmemail!="")
                    {
                        MyMessage.To.Add(pmemail);
                    }
                    //MyMessage.To.Add("deekshithgvd-t@tataprojects.com");
                    if (dt.Rows[0]["Head Operations"].ToString() != "")
                    {
                        MyMessage.CC.Add(dt.Rows[0]["Head Operations"].ToString());
                    }
                    if (dt.Rows[0]["Safety Officer"].ToString() != "")
                    {
                        //MyMessage.CC.Add(dt.Rows[0]["Safety Officer"].ToString());
                        MyMessage.CC.Add(CCSafetyOfficer(dt.Rows[0]["Safety Officer"].ToString()));
                    }
                    if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
                    {
                        MyMessage.CC.Add(CCMailSafetyUI());
                    }
                    else
                    {
                        if (buname == "Transportation" || buname == "Tata-Aldesa JV")
                        {
                            MyMessage.CC.Add(CCMailSafetyII());
                            MyMessage.CC.Add("pradeepkumars@tataprojects.com");
                        }
                        else
                        {
                            MyMessage.CC.Add(CCMailSafetyII());
                        }
                    }
                    MyMessage.Subject = "Safety Escalation";
                    MyMessage.Body = mailBody;
                    MyMessage.IsBodyHtml = true;
                    if (MyMessage != null)
                    {
                        SmtpClient emailClient = new SmtpClient();
                        emailClient.UseDefaultCredentials = false;
                        emailClient.Credentials = new System.Net.NetworkCredential("coesheii@tataprojects.com", "tata@123");
                        emailClient.Host = "smtp.office365.com";
                        emailClient.Port = 25;
                        emailClient.EnableSsl = true;
                        emailClient.TargetName = "STARTTLS/smtp.office365.com";
                        emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        emailClient.Send(MyMessage);
                    }
                    MyMessage.To.Clear();
                    MyMessage.CC.Clear();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        string CCSafetyOfficer(string sodet)
        {
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(sodet))
            {
                string strTo = sodet.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(ccEmail));
                }
            }
            return MyMessage.CC.ToString();
        }
        private string GetPMName(string pmemail)
        {
            DataTable dt = new DataTable();
            string pmname = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = ConfigurationManager.AppSettings["ConnStringSrc"];
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                string sqlQuery = "select * from TPL_EMP_MASTER where EmailID='" + pmemail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dt);
                con.Open();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            if (dt.Rows.Count > 0)
            {
                pmname = dt.Rows[0]["Name"].ToString();
            }
            return pmname;
        }

        string CCMailSafetyII()
        {
            string CCMailSafety = ConfigurationManager.AppSettings["CCSafetyEmailListII"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(CCMailSafety))
            {
                string strTo = CCMailSafety.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(ccEmail));
                }
            }
            return MyMessage.CC.ToString();
        }
        string CCMailSafetyUI()
        {
            string CCMailSafety = ConfigurationManager.AppSettings["CCSafetyEmailListUI"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(CCMailSafety))
            {
                string strTo = CCMailSafety.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(ccEmail));
                }
            }
            return MyMessage.CC.ToString();
        }

        public DataTable getProjectDetails(string projectCode)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                string sqlQuery = "select * from Monitored_Projects where [Project Code]='" + projectCode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dt);
                con.Open();
                con.Close();
            }
            catch (Exception exp)
            {

            }
            return dt;
        }

        public DataTable PendingRecords()
        {
            DataTable pendingrecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "select distinct ProjectCode from EHSChecklist where (Stage='Pending with RCM' or Stage='Pending with Assignee' or Stage='Pending for Closure') and ([Target Closure Date]<GETDATE() or [Target Closure Date] is null)";
                string sqlQuery = "select distinct ProjectCode from EHSChecklist as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where (A.Stage='Pending with RCM' or A.Stage='Pending with Assignee' or A.Stage='Pending for Closure') and (A.[Target Closure Date]<GETDATE() or A.[Target Closure Date] is null) and B.[Safety Active]=1";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingrecords);
                DataTable dt = pendingrecords;
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingrecords;
        }

        public DataTable GetRecords(string pcode)
        {
            DataTable getrecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "select Transaction_ID,ProjectCode,ProjectName,[Checklist Name],Frequency,[SO Submission Date],Stage,RCM_Name,Assignee_Name from EHSChecklist where (Stage='Pending with RCM' or Stage='Pending with Assignee' or Stage='Pending for Closure') and ProjectCode='" + pcode + "' and ([Target Closure Date]<GETDATE() or [Target Closure Date] is null)";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(getrecords);
                DataTable dt = getrecords;
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return getrecords;
        }

        private string FormMailBody(string pmname, DataTable ds, string dateString)
        {
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + pmname + ",");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            //sbBody.Append("This is for testing ");
            //sbBody.Append(dateString);
            sbBody.Append(" Following records are pending for user action.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("<table width='80%' border='1' cellpadding='1' cellspacing='1';'");
            sbBody.Append("<table><tr><td width='10%'>Project Code</td><td width='10%'>Project Name</td><td width=10%'>Checklist Name</td><td width='10%'>Frequency</td><td width=10%'>Uploaded Date</td><td width='10%'>Stage</td><td width='10%'>RCM Name</td><td width='10%'>Assignee Name</td></tr>");
            for (int i = 0; i < ds.Rows.Count; i++)
            {
                try
                {
                    sbBody.Append("<tr>");
                    sbBody.Append("<a href='https://tplnet.tataprojects.com/Pages/EHSCheckListDetails.aspx?Tid=" + ds.Rows[i]["Transaction_ID"] + "'><td width='10%'>" + Convert.ToString(ds.Rows[i]["ProjectCode"] + "</td></a>"));
                    sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["ProjectName"] + "</td>"));
                    sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Checklist Name"] + "</td>"));
                    sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Frequency"] + "</td>"));
                    sbBody.Append("<td width='10%'>" + DateTime.Parse(ds.Rows[i]["SO Submission Date"].ToString()).ToString("MMMM d yyyy") + "</td>");
                    sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Stage"] + "</td>"));
                    sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["RCM_Name"] + "</td>"));
                    sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Assignee_Name"] + "</td>"));

                    sbBody.Append("</tr>");
                }
                catch (Exception ex)
                {

                }
            }
            sbBody.Append("</table>");
            sbBody.Append("<br>");

            sbBody.Append("<br>");
            sbBody.Append("<br>");

            sbBody.Append("Thanks & Regards");
            sbBody.Append("<br>");
            sbBody.Append("Centre of Excellence Team - SHE");
            return sbBody.ToString();
        }

    }
}

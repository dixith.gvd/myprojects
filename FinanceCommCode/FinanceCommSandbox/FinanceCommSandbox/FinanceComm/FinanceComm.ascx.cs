﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Windows.Forms;
using System.Text;
using System.Web.UI;
using System.Web;
using Microsoft.Office.Interop.Excel;
using System.Diagnostics;
using Microsoft.SharePoint.Client;
using System.Security;
namespace FinanceCommSandbox.FinanceComm
{
    [ToolboxItemAttribute(false)]
    public partial class FinanceComm : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public FinanceComm()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtfolderpath.Text))
            {
                string logText = "Started on " + DateTime.Today.ToString("dd/MM/yyyy") + " at " + DateTime.Now.ToLongTimeString();
                ReminderBackupLog(logText);
                FileNotFoundLog(logText);
                if (!string.IsNullOrEmpty(ddlsentto.SelectedItem.Text))
                {
                    if (!string.IsNullOrEmpty(ddlassessyear.SelectedItem.Text))
                    {
                        var targetSite = new Uri("https://tataprojects4.sharepoint.com/sites/TPLSampleMig");
                        var login = "deekshithgvd-t@tataprojects.com";
                        var password = "dec@2017";
                        var securePassword = new SecureString();
                        foreach (char c in password)
                        {
                            securePassword.AppendChar(c);
                        }

                        var onlineCredentials = new SharePointOnlineCredentials(login, securePassword);
                        using (ClientContext clientContext = new ClientContext(targetSite))
                        {
                            clientContext.Credentials = onlineCredentials;
                            Web web = clientContext.Web;
                            var doclib = clientContext.Web.Lists.GetByTitle("PDF Checking");
                            var rootfolder = doclib.RootFolder;
                            clientContext.Load(doclib);

                            FileCollection collFile = rootfolder.Files;
                            clientContext.Load(collFile);
                            clientContext.ExecuteQuery();
                            if (ddlsentto.SelectedItem.Text == "Employee")
                            {
                                if (!string.IsNullOrEmpty(txtfilepath.Text))
                                {
                                    string savePath = string.Empty;
                                    string xmlData = string.Empty;
                                    string dirPath = string.Empty;
                                    dirPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "FinComm");
                                    savePath = Path.Combine(dirPath, uploadattach.FileName);
                                    try
                                    {
                                        if (!Directory.Exists(dirPath))
                                        {
                                            Directory.CreateDirectory(dirPath);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                    uploadattach.PostedFile.SaveAs(savePath);
                                    if (uploadattach.HasFiles)
                                    {
                                        System.Data.DataTable dtEmps = Data(savePath, uploadattach.FileName);
                                        string[] columnNames = (from dc in dtEmps.Columns.Cast<DataColumn>()
                                                                select dc.ColumnName).ToArray();
                                        string Name = columnNames[0];
                                        string EmailID = columnNames[1];
                                        string PanNumber = columnNames[2];
                                        if (Name != "Name" || EmailID != "EmailID" || PanNumber != "PAN Number")
                                        {
                                            //MessageBox.Show("Please ensure that the column names given in the excel sheet are as per the note");
                                            lblmessage.Visible = true;
                                            lblmessage.Text = "Please ensure that the column names given in the excel sheet are as per the note";
                                            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                                            //sb.Append("<script type = 'text/javascript'>");
                                            //sb.Append("window.onload=function(){");
                                            //sb.Append("alert('");
                                            //sb.Append(message);
                                            //sb.Append("')};");
                                            //sb.Append("</script>");
                                            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                                            return;
                                        }
                                        int Count = 0;
                                        Count = dtEmps.Rows.Count;
                                        //pgbfilesuploaded.Minimum = 0;
                                        //pgbfilesuploaded.Maximum = Count;
                                        //pgbfilesuploaded.Value = 0;
                                        for (int i = 0; i < dtEmps.Rows.Count; i++)
                                        {

                                            if (!string.IsNullOrEmpty(dtEmps.Rows[i]["Name"].ToString()) && !string.IsNullOrEmpty(dtEmps.Rows[i]["EmailID"].ToString()) && !string.IsNullOrEmpty(dtEmps.Rows[i]["PAN Number"].ToString()))
                                            {
                                                string userName = dtEmps.Rows[i]["Name"].ToString();
                                                string mailID = dtEmps.Rows[i]["EmailID"].ToString();
                                                string PAN = dtEmps.Rows[i]["PAN Number"].ToString();
                                                try
                                                {
                                                    if (!string.IsNullOrEmpty(txtsenderemail.Text))
                                                    {
                                                        //MailAddress SendFrom = new MailAddress(txtsenderemail.Text.Trim());
                                                        MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
                                                        MailAddress SendTo = new MailAddress(mailID);
                                                        MailMessage MyMessage = new MailMessage(SendFrom, SendTo);
                                                        MyMessage.Subject = "Employee’s TDS Certificate of AY " + ddlassessyear.SelectedItem.Text + "";
                                                        StringBuilder MailBody = TextFile(userName, PAN);
                                                        MyMessage.Body = MailBody.ToString();
                                                        if (!string.IsNullOrEmpty(txtccemail.Text))
                                                        {
                                                            string strCC = txtccemail.Text.Trim();
                                                            string[] CCEmailids = strCC.Split(';');
                                                            foreach (string CCEmail in CCEmailids)
                                                            {
                                                                MyMessage.CC.Add(new MailAddress(CCEmail)); //Adding Multiple CC email Id
                                                            }
                                                        }
                                                        string fPath = string.Empty;
                                                        if (ddlsentto.SelectedItem.Text == "Vendor")
                                                        {
                                                            fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                            if (System.IO.File.Exists(fPath))
                                                            {
                                                                fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                            }
                                                            else
                                                            {
                                                                fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".pdf";
                                                            }
                                                        }
                                                        if (ddlsentto.SelectedItem.Text == "Employee")
                                                        {
                                                            fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                            if (System.IO.File.Exists(fPath))
                                                            {
                                                                fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                            }
                                                            else
                                                            {
                                                                fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".pdf";
                                                            }
                                                        }

                                                        string pagefilePath = PAN + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                        bool flag = false;
                                                        foreach (Microsoft.SharePoint.Client.File oFile in collFile)
                                                        {
                                                            if (oFile.Name.ToLower() == pagefilePath.ToLower())
                                                            {
                                                                flag = true;
                                                                clientContext.Load(oFile);
                                                                clientContext.ExecuteQuery();

                                                                var fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, oFile.ServerRelativeUrl);
                                                                var fileStream = fileInfo.Stream;

                                                                System.Net.Mail.Attachment attachFile = new System.Net.Mail.Attachment(fileStream, oFile.Name);
                                                                MyMessage.Attachments.Add(attachFile);
                                                                //SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                                                                try
                                                                {
                                                                    //emailClient.Send(MyMessage);
                                                                    if (MyMessage != null)
                                                                    {
                                                                        SmtpClient emailClient = new SmtpClient();
                                                                        emailClient.UseDefaultCredentials = false;
                                                                        emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                                                                        emailClient.Host = "smtp.office365.com";
                                                                        emailClient.Port = 25;
                                                                        emailClient.EnableSsl = true;
                                                                        emailClient.TargetName = "STARTTLS/smtp.office365.com";
                                                                        emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                                                                        emailClient.Send(MyMessage);
                                                                    }
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    //ReminderBackupLog("Exception Occurs :" + userName + " with msg " + ex.Message);
                                                                    ReminderBackupLog("Mail not sent to :" + mailID + " with PAN No : " + PAN);
                                                                }

                                                            }
                                                        }
                                                        if (flag == true)
                                                        {
                                                            //Do Nothing
                                                        }
                                                        else
                                                        {
                                                            FileNotFoundLog("Existing file path not found: " + fPath + ", Mail ID:" + mailID);
                                                        }
                                                        btnsubmit.Enabled = false;
                                                        //pgbfilesuploaded.Value = i;
                                                        //pgbfilesuploaded.Update();
                                                        System.Threading.Thread.Sleep(1000);
                                                        int j = i + 1;
                                                        lblRecordsProcessed.Text = "Record " + j + "/" + Count + " being processed";
                                                        //lblRecordsProcessed.Update();
                                                        System.Threading.Thread.Sleep(1000);
                                                    }

                                                }

                                                catch (Exception ex)
                                                {
                                                    lblmessage.Text = ex.Message;
                                                }
                                            }

                                        }
                                        //MessageBox.Show("Done");
                                        //System.Text.StringBuilder sb2 = new System.Text.StringBuilder();
                                        //sb2.Append("<script type = 'text/javascript'>");
                                        //sb2.Append("window.onload=function(){");
                                        //sb2.Append("alert('");
                                        //sb2.Append("Done");
                                        //sb2.Append("')};");
                                        //sb2.Append("</script>");
                                        //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb2.ToString());
                                        lblmessage.Visible = true;
                                        lblmessage.Text = "Done";

                                    }

                                }

                            }
                            if (ddlsentto.SelectedItem.Text == "Vendor")
                            {
                                if (!string.IsNullOrEmpty(ddlquarter.SelectedItem.Text))
                                {
                                    if (!string.IsNullOrEmpty(txtfilepath.Text))
                                    {
                                        string savePath = string.Empty;
                                        string xmlData = string.Empty;
                                        string dirPath = string.Empty;
                                        dirPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "FinComm");
                                        savePath = Path.Combine(dirPath, uploadattach.FileName);
                                        try
                                        {
                                            if (!Directory.Exists(dirPath))
                                            {
                                                Directory.CreateDirectory(dirPath);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                        uploadattach.PostedFile.SaveAs(savePath);

                                        if (!string.IsNullOrEmpty(uploadattach.PostedFile.FileName))
                                        {
                                            System.Data.DataTable dtEmps = Data(savePath, uploadattach.FileName);
                                            string[] columnNames = (from dc in dtEmps.Columns.Cast<DataColumn>()
                                                                    select dc.ColumnName).ToArray();
                                            string Name = columnNames[0];
                                            string EmailID = columnNames[1];
                                            string PanNumber = columnNames[2];
                                            System.IO.File.Delete(savePath);

                                            if (Name != "Name" || EmailID != "EmailID" || PanNumber != "PAN Number")
                                            {
                                                //MessageBox.Show("Please ensure that the column names given in the excel sheet are as per the note");
                                                lblmessage.Visible = true;
                                                lblmessage.Text = "Please ensure that the column names given in the excel sheet are as per the note";
                                                //System.Text.StringBuilder sb3 = new System.Text.StringBuilder();
                                                //sb3.Append("<script type = 'text/javascript'>");
                                                //sb3.Append("window.onload=function(){");
                                                //sb3.Append("alert('");
                                                //sb3.Append(message);
                                                //sb3.Append("')};");
                                                //sb3.Append("</script>");
                                                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb3.ToString());
                                                return;
                                            }
                                            int Count = 0;
                                            Count = dtEmps.Rows.Count;
                                            //pgbfilesuploaded.Minimum = 0;
                                            //pgbfilesuploaded.Maximum = Count;
                                            //pgbfilesuploaded.Value = 0;
                                            for (int i = 0; i < dtEmps.Rows.Count; i++)
                                            {
                                                if (!string.IsNullOrEmpty(dtEmps.Rows[i]["Name"].ToString()) && !string.IsNullOrEmpty(dtEmps.Rows[i]["EmailID"].ToString()) && !string.IsNullOrEmpty(dtEmps.Rows[i]["PAN Number"].ToString()))
                                                {
                                                    string userName = dtEmps.Rows[i]["Name"].ToString();
                                                    string mailID = dtEmps.Rows[i]["EmailID"].ToString();
                                                    string PAN = dtEmps.Rows[i]["PAN Number"].ToString();
                                                    try
                                                    {
                                                        if (!string.IsNullOrEmpty(txtsenderemail.Text))
                                                        {
                                                            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
                                                            MailAddress SendTo = new MailAddress(mailID);

                                                            MailMessage MyMessage = new MailMessage(SendFrom, SendTo);
                                                            MyMessage.Subject = "TDS Certificate for : " + ddlquarter.SelectedItem.Text + " of AY " + ddlassessyear.SelectedItem.Text + "";
                                                            StringBuilder MailBody = TextFile(userName, PAN);
                                                            MyMessage.Body = MailBody.ToString();
                                                            if (!string.IsNullOrEmpty(txtccemail.Text))
                                                            {
                                                                string strCC = txtccemail.Text.Trim();
                                                                string[] CCEmailids = strCC.Split(';');
                                                                foreach (string CCEmail in CCEmailids)
                                                                {
                                                                    MyMessage.CC.Add(new MailAddress(CCEmail)); //Adding Multiple CC email Id
                                                                }
                                                            }
                                                            string fPath = string.Empty;
                                                            if (ddlsentto.SelectedItem.Text == "Vendor")
                                                            {
                                                                fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                                if (System.IO.File.Exists(fPath))
                                                                {
                                                                    fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                                }
                                                                else
                                                                {
                                                                    fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".pdf";
                                                                }
                                                            }
                                                            if (ddlsentto.SelectedItem.Text == "Employee")
                                                            {
                                                                fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.ToString() + ".PDF";
                                                                if (System.IO.File.Exists(fPath))
                                                                {
                                                                    fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                                }
                                                                else
                                                                {
                                                                    fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".pdf";
                                                                }
                                                            }
                                                            //Copy the files to a generic List of type SPFile
                                                            //List<File> listFiles = new List<File>(collFile.Count);
                                                            string pagefilePath = PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                            bool flag = false;
                                                            foreach (Microsoft.SharePoint.Client.File oFile in collFile)
                                                            {
                                                                if (oFile.Name.ToLower() == pagefilePath.ToLower())
                                                                {
                                                                    flag = true;
                                                                    clientContext.Load(oFile);
                                                                    clientContext.ExecuteQuery();

                                                                    var fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, oFile.ServerRelativeUrl);
                                                                    var fileStream = fileInfo.Stream;
                                                                    System.Net.Mail.Attachment attachFile = new System.Net.Mail.Attachment(fileStream, oFile.Name);
                                                                    MyMessage.Attachments.Add(attachFile);

                                                                    //SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                                                                    try
                                                                    {
                                                                        //emailClient.Send(MyMessage);
                                                                        if (MyMessage != null)
                                                                        {
                                                                            SmtpClient emailClient = new SmtpClient();
                                                                            emailClient.UseDefaultCredentials = false;
                                                                            emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                                                                            emailClient.Host = "smtp.office365.com";
                                                                            emailClient.Port = 25;
                                                                            emailClient.EnableSsl = true;
                                                                            emailClient.TargetName = "STARTTLS/smtp.office365.com";
                                                                            emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                                                                            emailClient.Send(MyMessage);
                                                                        }
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        //ReminderBackupLog("Exception Occurs :" + userName + " with msg " + ex.Message);
                                                                        ReminderBackupLog("Mail not sent to :" + mailID + " with PAN No : " + PAN);
                                                                    }

                                                                }
                                                            }
                                                            if (flag == true)
                                                            {
                                                                //Do Nothing
                                                            }
                                                            else
                                                            {
                                                                FileNotFoundLog("Existing file path not found: " + fPath + ", Mail ID:" + mailID);
                                                            }
                                                            //Attachment attachFile = new Attachment(fPath);                      
                                                            //MyMessage.Attachments.Add(attachFile);                                               
                                                            btnsubmit.Enabled = false;
                                                            //pgbfilesuploaded.Value = i;
                                                            //pgbfilesuploaded.Update();
                                                            System.Threading.Thread.Sleep(1000);
                                                            int j = i + 1;
                                                            lblRecordsProcessed.Text = "Record " + j + "/" + Count + " being processed";
                                                            //lblRecordsProcessed.Update();
                                                            System.Threading.Thread.Sleep(1000);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        lblmessage.Text = ex.Message;
                                                    }

                                                }

                                            }
                                        }
                                        //MessageBox.Show("Done");
                                        //System.Text.StringBuilder sb4 = new System.Text.StringBuilder();
                                        //sb4.Append("<script type = 'text/javascript'>");
                                        //sb4.Append("window.onload=function(){");
                                        //sb4.Append("alert('");
                                        //sb4.Append("Done");
                                        //sb4.Append("')};");
                                        //sb4.Append("</script>");
                                        //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb4.ToString());
                                        lblmessage.Visible = true;
                                        lblmessage.Text = "Done";
                                    }

                                }

                            }
                        }
                    }

                }

            }

            string logCompleteText = "Completed on " + DateTime.Today.ToString("dd/MM/yyyy") + " at " + DateTime.Now.ToLongTimeString();
            ReminderBackupLog(logCompleteText);
            FileNotFoundLog(logCompleteText);
        }

        public void ReminderBackupLog(string message)
        {
            try
            {
                string PathName = txtfolderpath.Text;
                PathName = PathName + @"\EmailNotSent_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                StreamWriter sw = new StreamWriter(PathName, true);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception exp)
            {
                lblmessage.Text = "Not valid Path";
                //MessageBox.Show("Not valid path");
                //System.Text.StringBuilder sb3 = new System.Text.StringBuilder();
                //sb3.Append("<script type = 'text/javascript'>");
                //sb3.Append("window.onload=function(){");
                //sb3.Append("alert('");
                //sb3.Append("Not valid path");
                //sb3.Append("')};");
                //sb3.Append("</script>");
                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb3.ToString());
            }
        }
        public void FileNotFoundLog(string message)
        {
            try
            {

                string PathName = txtfolderpath.Text;
                PathName = PathName + @"\FileNotFound_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                StreamWriter sw = new StreamWriter(PathName, true);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception exp)
            {
                lblmessage.Text = "Not valid Path";
                //MessageBox.Show("Not valid path");
                //System.Text.StringBuilder sb3 = new System.Text.StringBuilder();
                //sb3.Append("<script type = 'text/javascript'>");
                //sb3.Append("window.onload=function(){");
                //sb3.Append("alert('");
                //sb3.Append("Not valid path");
                //sb3.Append("')};");
                //sb3.Append("</script>");
                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb3.ToString());
            }
        }
        public System.Data.DataTable Data(String filePath, string fileName)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            try
            {
                string MyString = string.Empty;
                System.Data.OleDb.OleDbConnection MyConnection;
                System.Data.DataSet DtSet;
                System.Data.OleDb.OleDbDataAdapter MyCommand;
                string extension;
                extension = Path.GetExtension(filePath);
                if (extension.Trim() == ".xls")
                {
                    MyString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                }
                else if (extension.Trim() == ".xlsx")
                {
                    MyString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                }
                MyConnection = new System.Data.OleDb.OleDbConnection(MyString);//Extended Properties=Excel 8.0;
                //MyConnection = new System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0; Data Source='" + filePath + "';Extended Properties=Excel 8.0;");//Extended Properties=Excel 8.0;
                MyCommand = new System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection);
                MyCommand.TableMappings.Add("Table", "TestTable");
                DtSet = new System.Data.DataSet();
                MyCommand.Fill(DtSet);
                dt = DtSet.Tables[0];
                MyConnection.Close();
            }
            catch (Exception ex)
            {
                lblmessage.Text = ex.Message;
            }
            return dt;

        }
        public StringBuilder TextFile(string Name, string PanNumber)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                string[] lines = null;
                if (ddlsentto.SelectedItem.Text == "Employee")
                {
                    lines = System.IO.File.ReadAllLines(Environment.CurrentDirectory + @"\Employee Mail Body.txt");
                    //lines = System.IO.File.ReadAllLines(@"C:\TPL\Ujwal\SivaLaxmi\Finance Communicator\Employee Mail Body.txt");
                }
                if (ddlsentto.SelectedItem.Text == "Vendor")
                {
                    lines = System.IO.File.ReadAllLines(Environment.CurrentDirectory + @"\Vendor  Mail Body.txt");
                    //lines = System.IO.File.ReadAllLines(@"C:\TPL\Ujwal\SivaLaxmi\Finance Communicator\Vendor  Mail Body.txt");
                }
                string content = string.Empty;
                foreach (string line in lines)
                {
                    content = line;
                    if (content.Contains("<%EmpName%>"))
                    {
                        content = content.Replace("<%EmpName%>", Name);
                    }
                    if (content.Contains("<%AssesementYear%>"))
                    {
                        if (ddlassessyear.SelectedItem.Text != "---Select---")
                        {
                            content = content.Replace("<%AssesementYear%>", ddlassessyear.SelectedItem.Text);
                        }

                    }
                    if (content.Contains("<%Quater%>"))
                    {
                        if (ddlsentto.SelectedItem.Text == "Vendor")
                        {
                            if (ddlquarter.SelectedItem.Text != "---Select---")
                            {
                                content = content.Replace("<%Quater%>", ddlquarter.SelectedItem.Text);
                            }

                        }
                    }
                    if (content.Contains("<%PAN%>"))
                    {
                        content = content.Replace("<%PAN%>", PanNumber);
                    }
                    if (string.IsNullOrEmpty(line))
                    {
                        sb.Append(Environment.NewLine);
                        sb.Append(Environment.NewLine);
                        // sb.Append(Environment.NewLine);
                    }
                    else
                    {
                        sb.Append(content);
                    }
                }
            }
            catch (Exception ex)
            {
                lblmessage.Text = ex.Message;
            }
            return sb;
        }

        protected void ddlsentto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlsentto.SelectedItem.Text == "Employee")
            {
                ddlquarter.Visible = false;
                ddlquarter.SelectedItem.Text = "--Select--";
            }
        }

    }
}

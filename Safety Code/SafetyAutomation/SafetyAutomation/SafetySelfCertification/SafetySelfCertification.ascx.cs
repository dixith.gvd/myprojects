﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Utilities;
namespace SafetyAutomation.SafetySelfCertification
{
    [ToolboxItemAttribute(false)]
    public partial class SafetySelfCertification : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public SafetySelfCertification()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        SafetyChecklists objSafety = new SafetyChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            dvMainForm.Visible = true;
            if (!this.Page.IsPostBack)
            {
                if (this.Page.Request.QueryString["ID"] != null)
                {
                    btnsubmit.Visible = false;
                    lblID.Text = this.Page.Request.QueryString["ID"].ToString();
                    DataTable dtdata = objSafety.GetSafetySelfCertificationDatabyID(lblID.Text);
                    if (dtdata.Rows.Count > 0)
                    {
                        if (dtdata.Rows[0]["BU Name"] != null)
                        {
                            lblBU.Text = dtdata.Rows[0]["BU Name"].ToString();
                        }
                        if (dtdata.Rows[0]["Project Name"] != null)
                        {
                            lblProject.Text = dtdata.Rows[0]["Project Name"].ToString();
                            ddlProjectCode.Visible = false;
                            lblProject.Visible = true;
                        }
                        if (dtdata.Rows[0]["Reporting Month"] != null)
                        {
                            lblrepmonth.Text = dtdata.Rows[0]["Reporting Month"].ToString();
                        }
                        if (dtdata.Rows[0]["Rep Week"] != null)
                        {
                            lblrepweek.Visible = true;
                            lblrepweek.Text = dtdata.Rows[0]["Rep Week"].ToString();
                            //ddlweekend.Visible = false;
                        }
                        if (!string.IsNullOrEmpty(dtdata.Rows[0]["Comp 1"] as string))
                        {
                            chkcomp1.Checked = true;
                        }
                        else { chkcomp1.Checked = false; }
                        if (!string.IsNullOrEmpty(dtdata.Rows[0]["Comp 2"] as string))
                        {
                            chkcomp2.Checked = true;
                        }
                        else { chkcomp2.Checked = false; }
                        if (!string.IsNullOrEmpty(dtdata.Rows[0]["Comp 3"] as string))
                        {
                            chkcomp3.Checked = true;
                        }
                        else { chkcomp3.Checked = false; }
                        if (!string.IsNullOrEmpty(dtdata.Rows[0]["Comp 4"] as string))
                        {
                            chkcomp4.Checked = true;
                        }
                        else { chkcomp4.Checked = false; }
                        if (!string.IsNullOrEmpty(dtdata.Rows[0]["Comp 5"] as string))
                        {
                            chkcomp5.Checked = true;
                        }
                        else { chkcomp5.Checked = false; }
                        chkCertify.Checked = true;
                    }
                }
                else
                {
                    DataTable projectCodeItem = objSafety.GetProjectCodeByLoggedInUserRCM();
                    if (projectCodeItem != null)
                    {
                        //PanelMain.Visible = true;
                        for (int i = 0; i < projectCodeItem.Rows.Count; i++)
                        {
                            string projectCode = projectCodeItem.Rows[i][0].ToString();

                            DataTable projectItem = objSafety.GetProjectdetailsByProjectCode(projectCode);

                            for (int j = 0; j < projectItem.Rows.Count; j++)
                            {
                                string projectName = projectItem.Rows[j]["Title"].ToString();
                                if (!ddlProjectCode.Items.Contains(new ListItem(projectName)))
                                {
                                    ddlProjectCode.Items.Add(projectName);
                                }
                            }
                            lblBU.Text = projectItem.Rows[0]["BU_x0020_Name"].ToString();
                        }
                        if (!ddlProjectCode.Items.Contains(new ListItem("--Select--")))
                        {
                            ddlProjectCode.Items.Insert(0, "--Select--");
                        }
                        //List<DateTime> Saturdays = getSaturdays();
                        //foreach (DateTime Satday in Saturdays)
                        //{
                        //    string sday = String.Format("{0:dddd MMMM d yyyy}", Satday);
                        //    if (!ddlweekend.Items.Contains(new ListItem(sday)))
                        //    {
                        //        ddlweekend.Items.Add(sday);
                        //    }
                        //}
                        DateTime satday = GetSaturdayDate();
                        string sday = String.Format("{0:dddd MMMM d yyyy}", satday);
                        lblrepweek.Text = sday;

                        DateTime today = DateTime.Today;
                        lblrepmonth.Text = String.Format("{0:MMMM}", satday);
                        lblrepyear.Text = String.Format("{0:yyyy}", satday);
                        //string weekNumber = GetWeekNumber(today);
                        //lblrepweek.Text = weekNumber + " " + string.Format("{0:dddd}", DateTime.Today);
                        //lblrepweek.Text = weekNumber + " " + "Saturday";
                    }
                    else
                    {
                        dvUnAuth.Visible = true;
                        dvMainForm.Visible = false;
                    }
                }
            }
        }

        protected void ddlProjectCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblError.Text = "";
            string projectCode = ddlProjectCode.SelectedItem.Text.Split('-')[1];
            DataTable dt = GetItemsLastWeek(projectCode);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row["ProjectCode"].ToString() == projectCode)
                    {
                        //this.Page.Response.Redirect("There is already a Self-Certification Submitted for this project for the date - " + ddlweekend.SelectedItem.Text);
                        dvsubmission.Visible = true;
                        dvMainForm.Visible = false;
                        Label2.Text = "There is already a Self-Certification Submitted for this project for the date - " + lblrepweek.Text;
                    }
                }
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (ddlProjectCode.SelectedIndex != 0)
            {
                //if (ddlweekend.SelectedIndex != 0)
                //{
                if (chkcomp1.Checked || chkcomp2.Checked || chkcomp3.Checked || chkcomp4.Checked || chkcomp5.Checked)
                {
                    if (chkCertify.Checked)
                    {
                        btnsubmit.Enabled = false;
                        string bu = lblBU.Text;
                        string project = ddlProjectCode.SelectedItem.Text;
                        //string selectedday = ddlweekend.SelectedItem.Text;
                        string selectedday = lblrepweek.Text;
                        DateTime day = Convert.ToDateTime(selectedday);
                        string weekno = string.Empty;
                        if (day.Day <= 7)
                        {
                            weekno = "1st";
                        }
                        else if (day.Day > 7 && day.Day <= 14)
                        {
                            weekno = "2nd";
                        }
                        else if (day.Day > 14 && day.Day <= 21)
                        {
                            weekno = "3rd";
                        }
                        else if (day.Day > 21 && day.Day <= 28)
                        {
                            weekno = "4th";
                        }
                        else if (day.Day > 28)
                        {
                            weekno = "5th";
                        }
                        string repweek = weekno + " " + "Saturday";
                        string projectCode = project.Split('-')[1];
                        DateTime today = DateTime.Now;
                        string Created = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                        string CreatedBy = SPContext.Current.Web.CurrentUser.Name;
                        string comp1 = string.Empty; string comp2 = string.Empty; string comp3 = string.Empty; string comp4 = string.Empty; string comp5 = string.Empty;
                        string selfCertify = "Yes";
                        if (chkcomp1.Checked) { comp1 = chkcomp1.Text; }
                        else { comp1 = null; }
                        if (chkcomp2.Checked) { comp2 = chkcomp2.Text; }
                        else { comp2 = null; }
                        if (chkcomp3.Checked) { comp3 = chkcomp3.Text; }
                        else { comp3 = null; }
                        if (chkcomp4.Checked) { comp4 = chkcomp4.Text; }
                        else { comp4 = null; }
                        if (chkcomp5.Checked) { comp5 = chkcomp5.Text; }
                        else { comp5 = null; }
                        //lblrepmonth.Text = String.Format("{0:MMMM}", Created);
                        //string weekNumber = GetWeekNumber(today);
                        //lblrepweek.Text = weekNumber + " " + string.Format("{0:dddd}", Created);
                        int ID = 0;
                        if (!string.IsNullOrEmpty(objSafety.GetLatestIDSafetySelfCert()))
                        {
                            ID = Convert.ToInt32(objSafety.GetLatestIDSafetySelfCert()) + 1;
                        }
                        else
                        {
                            ID = 1;
                        }
                        objSafety.SaveSelfCertification(ID, bu, project, comp1, comp2, comp3, comp4, comp5, selfCertify, Created, Created, lblrepmonth.Text, repweek, projectCode, CreatedBy, CreatedBy, lblrepyear.Text);
                        lblNotification.Text = "You have submitted successfully";
                        string navigateUrl = Constants.link + "Pages/SafetySelfCertification.aspx";
                        lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to make another submission", navigateUrl);
                        lblError.Text = "";
                    }
                    else
                    {
                        lblError.Text = "Please check the self-certification statement to submit";
                    }
                }
                else
                {
                    lblError.Text = "Please select one or more compliances before submitting";
                }
            }
            else
            {
                lblError.Text = "Please select the Project";
            }
        }
        //private string GetWeekNumber(DateTime today)
        //{
        //    string weekno = string.Empty;
        //    if (today.Day <= 7)
        //    {
        //        weekno = "1st";
        //    }
        //    else if (today.Day > 7 && today.Day <= 14)
        //    {
        //        weekno = "2nd";
        //    }
        //    else if (today.Day > 14 && today.Day <= 21)
        //    {
        //        weekno = "3rd";
        //    }
        //    else if (today.Day > 21 && today.Day <= 28)
        //    {
        //        weekno = "4th";
        //    }
        //    else if (today.Day > 28)
        //    {
        //        weekno = "5th";
        //    }
        //    return weekno;
        //}

        //public static List<DateTime> getSaturdays()
        //{
        //    List<DateTime> lstSaturdays = new List<DateTime>();
        //    int intMonth = DateTime.Now.Month;
        //    int intYear = DateTime.Now.Year;
        //    int intDaysThisMonth = DateTime.DaysInMonth(intYear, intMonth);
        //    DateTime oBeginnngOfThisMonth = new DateTime(intYear, intMonth, 1);
        //    for (int i = 0; i < intDaysThisMonth; i++)
        //    {
        //        if (oBeginnngOfThisMonth.AddDays(i).DayOfWeek == DayOfWeek.Saturday)
        //        {
        //            lstSaturdays.Add(new DateTime(intYear, intMonth, i + 1));
        //        }
        //    }
        //    return lstSaturdays;
        //}

        public static DateTime GetSaturdayDate()
        {
            DateTime today = DateTime.Today;
            if (today.DayOfWeek == DayOfWeek.Saturday)
            {
                return today;
            }
            else
            {
                DateTime date = DateTime.Today.AddDays(-7);
                while (date.DayOfWeek != DayOfWeek.Saturday)
                {
                    date = date.AddDays(+1);
                }
                return date;
            }
        }

        public static DataTable GetItemsLastWeek(string projectCode)
        {
            DataTable dt = new DataTable();
            DateTime today = DateTime.Today;
            if (today.DayOfWeek == DayOfWeek.Saturday)
            {
                DateTime dtNextWeekFriday = today.AddDays(7);
                string saturday = (SPUtility.CreateISO8601DateTimeFromSystemDateTime(today));
                string friday = (SPUtility.CreateISO8601DateTimeFromSystemDateTime(dtNextWeekFriday));
                try
                {
                    SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                    string sqlQuery = "select * from Safety_SelfCertification where Created >= '" + saturday + "' and Created <= '" + friday + "' and ProjectCode='" + projectCode + "'";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);
                    con.Open();
                    SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                    sda.Fill(dt);
                    con.Close();
                }
                catch (Exception exp)
                {

                }
                return dt;
            }
            else
            {
                DateTime date = DateTime.Today.AddDays(-7);
                while (date.DayOfWeek != DayOfWeek.Saturday)
                {
                    date = date.AddDays(+1);
                }
                DateTime dtPrevWeekSaturday = date;
                DateTime dtNextWeekFriday = date.AddDays(7);
                string saturday = (SPUtility.CreateISO8601DateTimeFromSystemDateTime(dtPrevWeekSaturday));
                string friday = (SPUtility.CreateISO8601DateTimeFromSystemDateTime(dtNextWeekFriday));
                try
                {
                    SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                    string sqlQuery = "select * from Safety_SelfCertification where Created >= '" + saturday + "' and Created <= '" + friday + "' and ProjectCode='" + projectCode + "'";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);
                    con.Open();
                    SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                    sda.Fill(dt);
                    con.Close();
                }
                catch (Exception exp)
                {

                }
                return dt;
            }
        }
    }
}

﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO.Compression;

namespace QMDDashboard_TPL.ChecklistsModule.ChecklistDetails
{
    [ToolboxItemAttribute(false)]
    public partial class ChecklistDetails : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public ChecklistDetails()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        QualityChecklists objQuality = new QualityChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            panelQPR.Visible = false;
            panelFQP.Visible = false;
            if (this.Page.Request.QueryString["Tid"] != null)
            {
                lblTransactionID.Text = this.Page.Request.QueryString["Tid"].ToString();
                DataTable ds = objQuality.GetDatabyTransactionID(lblTransactionID.Text);
                if (ds.Rows[0]["Compliance_Type"].ToString().Trim() == "QPR")
                {
                    panelQPR.Visible = true;
                    lblTransID.Text = lblTransactionID.Text;
                    DataTable dtdata = objQuality.GetChecklistDatabyTransactionID(lblTransactionID.Text);
                    string rcmEmail = string.Empty;
                    string assigneeEmail = string.Empty;
                    bool mainFormLogic = true;
                    string fqeEmail = string.Empty;
                    string stage = string.Empty;
                    string pmemail = string.Empty;
                    if (dtdata.Rows.Count > 0)
                    {
                        if (dtdata.Rows[0]["Stage"] != null)
                        {
                            stage = dtdata.Rows[0]["Stage"].ToString();
                        }
                        if (stage.Equals("Compliant and Closed"))
                        {
                            dvFilledForm.Visible = true;
                            mainFormLogic = true;
                            dvAssignee.Visible = false;
                            dvRCM.Visible = false;
                            dvRCMClose.Visible = false;
                        }
                        else
                        {
                            dvFilledForm.Visible = true;
                            mainFormLogic = true;
                            dvRCM.Visible = true;
                            dvRCMClose.Visible = true;
                            dvAssignee.Visible = true;
                            GeteAssigneeEvidenceDocument();
                        }
                    }
                    if (mainFormLogic)
                    {
                        if (dtdata.Rows[0]["Stage"] != null)
                        {
                            lblcurstage.Text = dtdata.Rows[0]["Stage"].ToString();
                        }
                        if (dtdata.Rows[0]["BUName"] != null)
                        {
                            lblBU.Text = dtdata.Rows[0]["BUName"].ToString();

                        }
                        if (dtdata.Rows[0]["SBGName"] != null)
                        {
                            lblSBG.Text = dtdata.Rows[0]["SBGName"].ToString();
                        }
                        if (dtdata.Rows[0]["SBUName"] != null)
                        {
                            lblSBU.Text = dtdata.Rows[0]["SBUName"].ToString();
                        }
                        if (dtdata.Rows[0]["ProjectName"] != null)
                        {
                            lblProject.Text = dtdata.Rows[0]["ProjectName"].ToString();
                        }
                        if (dtdata.Rows[0]["Location"] != null)
                        {
                            lblLocation.Text = dtdata.Rows[0]["Location"].ToString();
                        }
                        if (dtdata.Rows[0]["SubLocation"] != null)
                        {
                            lblSubLocation.Text = dtdata.Rows[0]["SubLocation"].ToString();
                        }
                        if (dtdata.Rows[0]["ProjectType"] != null)
                        {
                            lblProjectType.Text = dtdata.Rows[0]["ProjectType"].ToString();
                        }
                        if (dtdata.Rows[0]["Discipline"] != null)
                        {
                            lblDiscipline.Text = dtdata.Rows[0]["Discipline"].ToString();
                        }
                        if (dtdata.Rows[0]["MainActivity"] != null)
                        {
                            lblMainactivity.Text = dtdata.Rows[0]["MainActivity"].ToString();
                        }
                        if (dtdata.Rows[0]["Activity"] != null)
                        {
                            lblActivity.Text = dtdata.Rows[0]["Activity"].ToString();
                        }
                        if (dtdata.Rows[0]["Sub Activity"] != null)
                        {
                            lblSubActivity.Text = dtdata.Rows[0]["Sub Activity"].ToString();
                        }
                        if (dtdata.Rows[0]["Identification"] != null)
                        {
                            lblidentify.Text = dtdata.Rows[0]["Identification"].ToString();
                        }
                        if (dtdata.Rows[0]["SWIIssuedBy"] != null)
                        {
                            lblissuedqpr.Text = dtdata.Rows[0]["SWIIssuedBy"].ToString();
                        }
                        if (dtdata.Rows[0]["Assignee_Name"] != null)
                        {
                            lblassignee.Text = dtdata.Rows[0]["Assignee_Name"].ToString();
                        }
                        if (dtdata.Rows[0]["RCM_Remarks"] != null)
                        {
                            lblremarks.Text = dtdata.Rows[0]["RCM_Remarks"].ToString();
                        }
                        if (dtdata.Rows[0]["Assignee_Submitted_Date"] != null)
                        {
                            string dtAssSubDate = dtdata.Rows[0]["Assignee_Submitted_Date"].ToString();
                            if (!string.IsNullOrEmpty(dtAssSubDate))
                            {
                                DateTime dtDate = Convert.ToDateTime(dtAssSubDate);
                                lblactiondateassignee.Text = dtDate.ToString("dd/MM/yyyy");
                            }

                        }
                        if (dtdata.Rows[0]["Closure_Date"] != null)
                        {
                            string dtRCMSubDate = dtdata.Rows[0]["Closure_Date"].ToString();
                            if (!string.IsNullOrEmpty(dtRCMSubDate))
                            {
                                DateTime dtDate = Convert.ToDateTime(dtRCMSubDate);
                                lblclosuredatercm.Text = dtDate.ToString("dd/MM/yyyy");
                            }

                        }

                        if (dtdata.Rows[0]["Assignee_Remarks"] != null)
                        {
                            lblremarksassignee.Text = dtdata.Rows[0]["Assignee_Remarks"].ToString();
                        }
                        if (dtdata.Rows[0]["ActualReworkCostQPR"] != null)
                        {
                            lblactualreworkcost.Text = dtdata.Rows[0]["ActualReworkCostQPR"].ToString();
                        }
                        if (dtdata.Rows[0]["CostBorneByQPR"] != null)
                        {
                            lblcostborneby.Text = dtdata.Rows[0]["CostBorneByQPR"].ToString();
                        }
                        if (dtdata.Rows[0]["RCM_Closure_Remarks"] != null)
                        {
                            lblverifremarks.Text = dtdata.Rows[0]["RCM_Closure_Remarks"].ToString();
                        }
                        if (dtdata.Rows[0]["Target_Closure_Date"] != null)
                        {
                            string dtTCD = dtdata.Rows[0]["Target_Closure_Date"].ToString();
                            if (!string.IsNullOrEmpty(dtTCD))
                            {
                                DateTime dtDate = Convert.ToDateTime(dtTCD);
                                lbltargclosdate.Text = dtDate.ToString("dd/MM/yyyy");

                                //TimeSpan difference = dtFqeDate - dtDate;
                                //var days = difference.TotalDays;

                            }

                        }
                        GeteEvidenceDocument();
                        DataTable dtChildData = objQuality.GetComplaincesDatabyTransactionID(lblTransactionID.Text);
                        if (dtChildData != null)
                        {
                            grdCompliances.DataSource = dtChildData;
                            grdCompliances.DataBind();
                        }
                        string swi_id = dtdata.Rows[0]["SWIID"].ToString();
                        if (!string.IsNullOrEmpty(swi_id))
                        {
                            hplSWI.Visible = true;
                        }

                        hplSWI.NavigateUrl = Constants.link + "Pages/QMDSWIForm.aspx?Tid=" + lblTransactionID.Text;


                    }
                }
                else
                {
                    panelFQP.Visible = true;
                    DataTable dtdata = objQuality.GetChecklistDatabyTransactionIDForFQP(lblTransactionID.Text);
                    DataTable dtRWCdata = objQuality.GetRWCbyTransactionID(lblTransactionID.Text);

                    string rcmEmail = string.Empty;
                    string assigneeEmail = string.Empty;
                    bool mainFormLogic = true;
                    bool rwcFormLogic = true;
                    string fqeEmail = string.Empty;
                    string stage = string.Empty;
                    string peEmail = string.Empty;
                    if (dtdata.Rows.Count > 0 || dtRWCdata.Rows.Count > 0)
                    {
                        if (dtdata.Rows[0]["Stage"] != null)
                        {
                            stage = dtdata.Rows[0]["Stage"].ToString();
                        }
                        if (stage.Equals("Compliant and Closed"))
                        {
                            dvFilledFormFQP.Visible = true;
                            mainFormLogic = true;
                            dvRCMCloseFQP.Visible = false;
                            dvAssigneeFQP.Visible = false;
                            dvRCMFQP.Visible = false;
                            divPE.Visible = false;
                            Div2.Visible = false;
                            Div3.Visible = false;
                            Div4.Visible = false;
                            tblSWI.Visible = false;
                            tblReworkBanner.Visible = false;
                        }
                        else
                        {
                            dvFilledFormFQP.Visible = true;
                            mainFormLogic = true;
                            dvRCMCloseFQP.Visible = true;
                            dvAssigneeFQP.Visible = true;
                            dvRCMFQP.Visible = true;
                            divPE.Visible = true;
                            Div2.Visible = true;
                            Div3.Visible = true;
                            Div4.Visible = true;
                            tblSWI.Visible = true;
                            tblReworkBanner.Visible = true;
                            GetAssigneeEvidenceDocument();
                            if (dtRWCdata.Rows.Count > 0)
                            {
                                if (dtRWCdata.Rows[0]["Rework_Details"] != null)
                                {
                                    txtRWCdetails.Text = dtRWCdata.Rows[0]["Rework_Details"].ToString();
                                }
                                if (dtRWCdata.Rows[0]["Rework_Approx_Cost"] != null)
                                {
                                    txtINR.Text = dtRWCdata.Rows[0]["Rework_Approx_Cost"].ToString();
                                }
                                if (dtRWCdata.Rows[0]["Rework_Quality"] != null)
                                {
                                    txtRCM.Text = dtRWCdata.Rows[0]["Rework_Quality"].ToString();
                                }
                                if (dtRWCdata.Rows[0]["Rework_Actual_Cost"] != null)
                                {
                                    txtRWCRCMCost.Text = dtRWCdata.Rows[0]["Rework_Actual_Cost"].ToString();
                                }
                                if (dtRWCdata.Rows[0]["CostBorneBy"] != null)
                                {
                                    ddlborne.SelectedItem.Text = dtRWCdata.Rows[0]["CostBorneBy"].ToString();
                                }
                                txtRWCdetails.Enabled = false;
                                txtINR.Enabled = false;
                                txtRCM.Enabled = false;
                                txtRWCRCMCost.Enabled = false;
                                tblReworkBanner.Visible = false;
                                ddlborne.Enabled = false;
                            }
                        }
                        if (mainFormLogic)
                        {
                            lblTransIDFQP.Text = lblTransactionID.Text;
                            if (dtdata.Rows[0]["Stage"] != null)
                            {
                                lblcurstageFQP.Text = dtdata.Rows[0]["Stage"].ToString();
                            }
                            if (dtdata.Rows[0]["SBGName"] != null)
                            {
                                lblSBGFQP.Text = dtdata.Rows[0]["SBGName"].ToString();
                            }
                            if (dtdata.Rows[0]["SBUName"] != null)
                            {
                                lblSBUFQP.Text = dtdata.Rows[0]["SBUName"].ToString();
                            }
                            if (dtdata.Rows[0]["BUName"] != null)
                            {
                                lblBUFQP.Text = dtdata.Rows[0]["BUName"].ToString();
                            }
                            if (dtdata.Rows[0]["ProjectName"] != null)
                            {
                                lblProjectFQP.Text = dtdata.Rows[0]["ProjectName"].ToString();
                                lblProjectLocation.Text = dtdata.Rows[0]["ProjectName"].ToString();
                            }
                            if (dtdata.Rows[0]["ProjectCode"] != null)
                            {
                                lblTplProjectCode.Text = dtdata.Rows[0]["ProjectCode"].ToString();
                            }

                            if (dtdata.Rows[0]["Location"] != null)
                            {
                                lblLocationFQP.Text = dtdata.Rows[0]["Location"].ToString();
                            }
                            if (dtdata.Rows[0]["SubLocation"] != null)
                            {
                                lblSubLocationFQP.Text = dtdata.Rows[0]["SubLocation"].ToString();
                            }
                            if (dtdata.Rows[0]["ProjectType"] != null)
                            {
                                lblProjectTypeFQP.Text = dtdata.Rows[0]["ProjectType"].ToString();
                            }
                            if (dtdata.Rows[0]["Discipline"] != null)
                            {
                                lblDisciplineFQP.Text = dtdata.Rows[0]["Discipline"].ToString();
                            }
                            if (dtdata.Rows[0]["MainActivity"] != null)
                            {
                                lblMainactivityFQP.Text = dtdata.Rows[0]["MainActivity"].ToString();
                            }
                            if (dtdata.Rows[0]["Activity"] != null)
                            {
                                lblActivityFQP.Text = dtdata.Rows[0]["Activity"].ToString();
                            }
                            if (dtdata.Rows[0]["Sub Activity"] != null)
                            {
                                lblSubActivityFQP.Text = dtdata.Rows[0]["Sub Activity"].ToString();
                            }
                            if (dtdata.Rows[0]["MainDefectCategory"] != null)
                            {
                                lblDefectCategory.Text = dtdata.Rows[0]["MainDefectCategory"].ToString();
                            }
                            if (dtdata.Rows[0]["SubDefectCategory"] != null)
                            {
                                lblSubDefectCategory.Text = dtdata.Rows[0]["SubDefectCategory"].ToString();
                            }
                            if (dtdata.Rows[0]["RootCauseCategory"] != null)
                            {
                                lblRootCauseCategory.Text = dtdata.Rows[0]["RootCauseCategory"].ToString();
                            }
                            if (dtdata.Rows[0]["RootCause"] != null)
                            {
                                lblRootCause.Text = dtdata.Rows[0]["RootCause"].ToString();
                            }
                            if (dtdata.Rows[0]["Why1"] != null)
                            {
                                lblWhy1.Text = dtdata.Rows[0]["Why1"].ToString();
                            }
                            if (dtdata.Rows[0]["Why2"] != null)
                            {
                                lblWhy2.Text = dtdata.Rows[0]["Why2"].ToString();
                            }
                            if (dtdata.Rows[0]["Why3"] != null)
                            {
                                lblWhy3.Text = dtdata.Rows[0]["Why3"].ToString();
                            }
                            if (dtdata.Rows[0]["Why4"] != null)
                            {
                                lblWhy4.Text = dtdata.Rows[0]["Why4"].ToString();
                            }
                            if (dtdata.Rows[0]["Why5"] != null)
                            {
                                lblWhy5.Text = dtdata.Rows[0]["Why5"].ToString();
                            }
                            if (dtdata.Rows[0]["Identification"] != null)
                            {
                                lblidentify.Text = dtdata.Rows[0]["Identification"].ToString();
                            }
                            if (dtdata.Rows[0]["SWIIssuedBy"] != null)
                            {
                                lblissuedfqp.Text = dtdata.Rows[0]["SWIIssuedBy"].ToString();
                            }
                            if (dtdata.Rows[0]["isCompliant"] != null)
                            {
                                if (dtdata.Rows[0]["isCompliant"].ToString().Trim() == "No")
                                {
                                    dvuploadNCR.Visible = true;
                                    DataTable dtNCRData = objQuality.GetNCRUploadDatabyTransactionIDforFQP(lblTransactionID.Text);
                                    if (dtNCRData != null)
                                    {
                                        GridView1.DataSource = dtNCRData;
                                        GridView1.DataBind();
                                    }
                                }
                            }
                            if (dtdata.Rows[0]["PE_Remarks"] != null)
                            {
                                lblperemarks.Text = dtdata.Rows[0]["PE_Remarks"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                lblrcmemail.Text = dtdata.Rows[0]["RCM_Email"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Remarks"] != null)
                            {
                                lblrcmremarksFQP.Text = dtdata.Rows[0]["RCM_Remarks"].ToString();
                            }
                            if (dtdata.Rows[0]["Assignee_Submitted_Date"] != null)
                            {
                                string dtAssSubDate = dtdata.Rows[0]["Assignee_Submitted_Date"].ToString();
                                if (!string.IsNullOrEmpty(dtAssSubDate))
                                {
                                    DateTime dtDate = Convert.ToDateTime(dtAssSubDate);
                                    lblassigneedateFQP.Text = dtDate.ToString("dd/MM/yyyy");
                                }
                            }
                            if (dtdata.Rows[0]["Target_Closure_Date"] != null)
                            {
                                string dtTCD = dtdata.Rows[0]["Target_Closure_Date"].ToString();
                                if (!string.IsNullOrEmpty(dtTCD))
                                {
                                    DateTime dtDate = Convert.ToDateTime(dtTCD);
                                    lblRWCDate.Text = dtDate.ToString("dd/MM/yyyy");
                                    lbltrgclosdateFQP.Text = dtDate.ToString("dd/MM/yyyy");
                                }
                            }
                            if (dtdata.Rows[0]["Assignee_Name"] != null)
                            {
                                lblassigneeFQP.Text = dtdata.Rows[0]["Assignee_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["Assignee_Remarks"] != null)
                            {
                                lblassigneeremarksFQP.Text = dtdata.Rows[0]["Assignee_Remarks"].ToString();
                            }
                            if (dtdata.Rows[0]["Closure_Date"] != null)
                            {
                                string dtRCMSubDate = dtdata.Rows[0]["Closure_Date"].ToString();
                                if (!string.IsNullOrEmpty(dtRCMSubDate))
                                {
                                    DateTime dtDate = Convert.ToDateTime(dtRCMSubDate);
                                    lblclosuredateFQP.Text = dtDate.ToString("dd/MM/yyyy");
                                }
                            }
                            if (dtdata.Rows[0]["RCM_Closure_Remarks"] != null)
                            {
                                lblverifremarksFQP.Text = dtdata.Rows[0]["RCM_Closure_Remarks"].ToString();
                            }
                            DataTable dtChildData = objQuality.GetComplaincesDatabyTransactionIDforFQP(lblTransactionID.Text);
                            if (dtChildData != null)
                            {
                                grdCompliancesFQP.DataSource = dtChildData;
                                grdCompliancesFQP.DataBind();
                            }
                        }
                        if (rwcFormLogic)
                        {
                            if (dtRWCdata.Rows.Count > 0)
                            {
                                if (dtRWCdata.Rows[0]["RWCID"] != null)
                                {
                                    lblRWCNO.Text = dtRWCdata.Rows[0]["RWCID"].ToString();
                                }
                                if (dtRWCdata.Rows[0]["RWC_Date"] != null)
                                {
                                    string dtTCD = dtRWCdata.Rows[0]["RWC_Date"].ToString();
                                    if (!string.IsNullOrEmpty(dtTCD))
                                    {
                                        DateTime dtDate = Convert.ToDateTime(dtTCD);
                                        lblRWCDate.Text = dtDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                if (dtRWCdata.Rows[0]["Subcontractor"] != null)
                                {
                                    lblSubContract.Text = dtRWCdata.Rows[0]["Subcontractor"].ToString();
                                }
                                if (dtRWCdata.Rows[0]["PO_No"] != null)
                                {
                                    lblPoWo.Text = dtRWCdata.Rows[0]["PO_No"].ToString();
                                }
                                if (dtRWCdata.Rows[0]["Package"] != null)
                                {
                                    lblPackage.Text = dtRWCdata.Rows[0]["Package"].ToString();
                                }
                                if (dtRWCdata.Rows[0]["NCRFINCCR"] != null)
                                {
                                    lblReworkDue.Text = dtRWCdata.Rows[0]["NCRFINCCR"].ToString();
                                }
                                if (dtRWCdata.Rows[0]["NC_RootCause"] != null)
                                {
                                    string s = dtRWCdata.Rows[0]["NC_RootCause"].ToString();
                                    lblNC_RootCause.Text = s.Replace(",", "</br>");
                                }
                                if (dtRWCdata.Rows[0]["NC_Details"] != null)
                                {
                                    txtNCDetails.Text = dtRWCdata.Rows[0]["NC_Details"].ToString();
                                    txtNCDetails.Enabled = false;
                                }
                            }
                            else
                            {
                                tblSWI.Visible = false;
                                Div2.Visible = false;
                                Div3.Visible = false;
                                Div4.Visible = false;
                            }
                        }
                    }
                }
            }
        }

        private void GeteEvidenceDocument()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select FQE_Ev_DocName from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["FQE_Ev_DocName"] != null)
                        {
                            fqeEvidence.Text = dr["FQE_Ev_DocName"].ToString();
                        }

                    }
                }
                else
                {
                    fqeEvidence.Text = "File not uploaded";
                }
                con.Close();
            }
            catch (Exception exp)
            {

            }

        }

        protected void fqeEvidence_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select FQE_Ev_DocName,FQE_Ev_DocContentType,FQE_Ev_Doc from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["FQE_Ev_DocName"].ToString();
                    if (Convert.ToInt32(lblTransactionID.Text) > 31467)
                    {
                        bytes = Decompress((byte[])sdr["FQE_Ev_Doc"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["FQE_Ev_Doc"];
                    }
                    contentType = sdr["FQE_Ev_DocContentType"].ToString();

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }
        }

        private void upLoadEvidenceDoc_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select DocName,DocContentType,Evidence_Document from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["DocName"].ToString();
                    DateTime lastUpdateDate = LastDate();
                    DateTime dt2 = DateTime.Parse("2017-12-27 18:05:00.000");
                    if (lastUpdateDate > dt2)
                    {
                        bytes = Decompress((byte[])sdr["Evidence_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Evidence_Document"];
                    }
                    contentType = sdr["DocContentType"].ToString();
                    upLoadEvidenceDoc.Text = fileName;

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }

            upLoadEvidenceDoc.OnClientClick = "this.form.onsubmit = function() {return true;}";
        }

        private void GeteAssigneeEvidenceDocument()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select DocName from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["DocName"] != null)
                        {
                            upLoadEvidenceDoc.Text = dr["DocName"].ToString();
                        }

                    }
                }
                else
                {
                    upLoadEvidenceDoc.Text = "File not uploaded";
                }
                con.Close();
            }
            catch (Exception exp)
            {

            }

        }

        private void GetAssigneeEvidenceDocument()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select DocName from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["DocName"] != null)
                        {
                            upLoadEvidenceDocFQP.Text = dr["DocName"].ToString();
                        }

                    }
                }
                else
                {
                    upLoadEvidenceDocFQP.Text = "File not uploaded";
                }
                con.Close();
            }
            catch (Exception exp)
            {

            }

        }

        protected void upLoadEvidenceDocFQP_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select DocName,DocContentType,Evidence_Document from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["DocName"].ToString();
                    DateTime lastUpdateDate = LastDate();
                    DateTime dt2 = DateTime.Parse("2017-12-27 18:05:00.000");
                    if (lastUpdateDate > dt2)
                    {
                        bytes = Decompress((byte[])sdr["Evidence_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Evidence_Document"];
                    }
                    contentType = sdr["DocContentType"].ToString();
                    upLoadEvidenceDocFQP.Text = fileName;

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }
        }

        protected void lnkView_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            LinkButton btn = sender as LinkButton;
            string docName = btn.Text;

            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select Supp_DocName,Supp_DocContentType,Supp_Document from QChecklist_TnxChild where Transaction_ID='" + lblTransactionID.Text + "'and Supp_DocName='" + docName + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["Supp_DocName"].ToString();
                    contentType = sdr["Supp_DocContentType"].ToString();
                    if (Convert.ToInt32(lblTransactionID.Text) > 31467)
                    {
                        bytes = Decompress((byte[])sdr["Supp_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Supp_Document"];
                    }
                    // lnkView.Text = fileName;


                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }


            catch (Exception ex)
            {

            }

        }

        protected void lnkView2_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            LinkButton btn = sender as LinkButton;
            string docName = btn.Text;

            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select Evi_DocName,Evi_DocContentType,Evi_Document from QChecklist_TnxChild where Transaction_ID='" + lblTransactionID.Text + "'and Evi_DocName='" + docName + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["Evi_DocName"].ToString();
                    contentType = sdr["Evi_DocContentType"].ToString();
                    if (Convert.ToInt32(lblTransactionID.Text) > 31467)
                    {
                        bytes = Decompress((byte[])sdr["Evi_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Evi_Document"];
                    }
                    // lnkView.Text = fileName;
                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }


            catch (Exception ex)
            {

            }

        }

        private void ShowBasicDialog(string Url, string Title)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"<script type=""text/ecmascript"">");
            sb.AppendLine(@"ExecuteOrDelayUntilScriptLoaded(openBasicServerDialog, ""sp.js"");");
            sb.AppendLine(@"    function openBasicServerDialog()");
            sb.AppendLine(@"    {");
            sb.AppendLine(@"        var options = {");
            sb.AppendLine(string.Format(@"            url: '{0}',", Url));
            sb.AppendLine(string.Format(@"            title: '{0}'", Title));
            sb.AppendLine(@"        };");
            sb.AppendLine(@"        SP.UI.ModalDialog.showModalDialog(options);");
            sb.AppendLine(@"    }");
            sb.AppendLine(@"</script>");
            ltScriptLoader.Text = sb.ToString();

        }

        protected void lnkViewNCR_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            LinkButton btn = sender as LinkButton;
            string docName = btn.Text;

            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select NCR_DocName,NCR_DocContentType,NCR_Document from Qchecklist_NCRdoc where Transaction_ID='" + lblTransactionID.Text + "' and NCR_DocName='" + docName + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["NCR_DocName"].ToString();
                    contentType = sdr["NCR_DocContentType"].ToString();
                    if (Convert.ToInt32(lblTransactionID.Text) > 31459)
                    {
                        bytes = Decompress((byte[])sdr["NCR_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["NCR_Document"];
                    }
                    // lnkView.Text = fileName;


                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }


            catch (Exception ex)
            {

            }

        }
        private byte[] Decompress(byte[] data)
        {
            var output = new MemoryStream();
            var input = new MemoryStream();
            input.Write(data, 0, data.Length);
            input.Position = 0;
            using (var gzip = new GZipStream(input, CompressionMode.Decompress, true))
            {
                var buff = new byte[64];
                var read = gzip.Read(buff, 0, buff.Length);
                while (read > 0)
                {
                    output.Write(buff, 0, read);
                    read = gzip.Read(buff, 0, buff.Length);
                }
                gzip.Close();
            }
            return output.ToArray();
        }
        public DateTime LastDate()
        {
            string date = string.Empty;
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select Assignee_Submitted_Date from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
                if (dt != null)
                {
                    date = dt.Rows[0]["Assignee_Submitted_Date"].ToString();
                }
            }
            catch (Exception exp)
            {
            }
            return Convert.ToDateTime(date);
        }
    }
}

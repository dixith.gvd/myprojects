﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
using Microsoft.SharePoint;
namespace SafetyAutomation
{
    class SafetyChecklists
    {
        public DataTable GetProjectCodeByLoggedInUserSafety()
        {
            DataTable dt = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            //query.Query = "<Where><Eq><FieldRef Name='Safety_x0020_Officer' /><Value Type='Integer'><UserID /></Value></Eq></Where>";
                            //query.Query = "<Where><And><Eq><FieldRef Name='Safety_x0020_Officer' /><Value Type='Integer'><UserID /></Value></Eq><Eq><FieldRef Name='Safety_x0020_Active' /><Value Type='Boolean'>1</Value></Eq></And></Where>";
                            query.Query = "<Where><And><Eq><FieldRef Name='Safety_x0020_Officer' /><Value Type='Integer'><UserID Type='Integer'/></Value></Eq><Eq><FieldRef Name='Safety_x0020_Active' /><Value Type='Boolean'>1</Value></Eq></And></Where>";
                            dt = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dt;
        }
        public DataTable GetProjectCodeByLoggedInUserRCM()
        {
            DataTable dt = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><And><Eq><FieldRef Name='RCM' /><Value Type='Integer'><UserID /></Value></Eq><Eq><FieldRef Name='Safety_x0020_Active' /><Value Type='Boolean'>1</Value></Eq></And></Where>";
                            dt = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dt;
        }
        public DataTable GetProjectdetailsByProjectCode(string projectCode)
        {
            DataTable dtProjectDetails = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            //query.Query = "<Where><Eq><FieldRef Name='Title' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                            query.Query = "<Where><Eq><FieldRef Name='Project_x0020_Code' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                            dtProjectDetails = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dtProjectDetails;
        }
        public DataTable GetProjectCodeByProjectName(string projectCode)
        {
            DataTable dtProjectDetails = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["TPL Projects List"] != null)
                    {
                        SPList list = web.Lists["TPL Projects List"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Contains><FieldRef Name='Final_x0020_Project_x0020_Name' /><Value Type='Text'>" + projectCode + "</Value></Contains></Where>";
                            dtProjectDetails = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dtProjectDetails;
        }
        public SPListItem GetProjectCodeByProjectNameForRCM(string projectCode)
        {
            SPListItem projectCodeForRCM = null;
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='Project_x0020_Code' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                            SPListItemCollection items = list.GetItems(query);
                            foreach (SPListItem item in items)
                            {
                                projectCodeForRCM = item;
                                break;
                            }
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return projectCodeForRCM;
        }
        public string GetOnlyEmployeeID(string loggedInName)
        {
            string empID = string.Empty;
            string loggedNameLowerCase = loggedInName.ToLower().ToString();
            if (loggedNameLowerCase.Contains("tpl"))
            {
                string replaced = loggedNameLowerCase.Replace('\\', '@');
                if (replaced.Contains("@"))
                {
                    string[] seperators = { "@" };
                    string[] nameArray = replaced.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
                    if (nameArray.Length == 2)
                    {
                        if (nameArray[1] != null)
                        {
                            empID = nameArray[1];
                        }
                    }
                }
                else
                {
                    string replacedTest = loggedNameLowerCase.Replace('\\', '@');
                }
            }
            return empID.ToUpper();
        }
        public DataTable GetCategoryList()
        {
            DataTable dtData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct Category from EHS_Categories order by Category asc";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtData);
            }
            catch (Exception exp)
            {
            }
            return dtData;
        }
        public string GetLatestTransactionIDSafety()
        {
            string transactionID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 Transaction_ID FROM [EQHSAutomation].[dbo].[EHSChecklist] ORDER BY Transaction_ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Transaction_ID"] != null)
                        {
                            transactionID = dt.Rows[0]["Transaction_ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return transactionID;
        }
        public string GetLatestTransactionIDSafetyUploads()
        {
            string transactionID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 Transaction_ID FROM [EQHSAutomation].[dbo].[SafetyUploads] ORDER BY Transaction_ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Transaction_ID"] != null)
                        {
                            transactionID = dt.Rows[0]["Transaction_ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return transactionID;
        }
        public bool SaveChecklist(int Tid, string frequency, string checklist, string bu, string project, string projectcode, string inspdate, string compliance, string stage, string location, string observations, string SOID, string SOName, string SOEmail, string RCMID, string RCMName, string RCMEmail, string PMID, string PMName, string PMEmail, string hop_id, string hop_Name, string hop_Email, string buheadid, string buheadName, string buheadEmail, string LastStageUpdate, string selfcert, string newsbu)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into EHSChecklist(Transaction_ID,Frequency,[Checklist Name],BUName,ProjectName,ProjectCode,[Inspection Date],isCompliant,Stage,Location,[SO Remarks],SO_ID,SO_Name,SO_Email,RCM_ID,RCM_Name,RCM_Email,PM_ID,PM_Name,PM_Email,HOP_ID,HOP_Name,HOP_Email,BUHead_ID,BUHead_Name,BUHead_Email,[SO Submission Date],LastStageUpdate,[Self Certification],SBUName)");
            insertQuery.Append(" values ("); insertQuery.Append("" + Tid + ","); insertQuery.Append("'" + frequency + "',"); insertQuery.Append("'" + checklist + "',"); insertQuery.Append("'" + bu + "',"); insertQuery.Append("'" + project + "',"); insertQuery.Append("'" + projectcode + "',"); insertQuery.Append("'" + inspdate + "',"); insertQuery.Append("'" + compliance + "',");
            insertQuery.Append("'" + stage + "',"); insertQuery.Append("'" + location + "',"); insertQuery.Append("'" + observations + "',"); insertQuery.Append("'" + SOID + "',"); insertQuery.Append("'" + SOName + "',"); insertQuery.Append("'" + SOEmail + "',");
            insertQuery.Append("'" + RCMID + "',"); insertQuery.Append("'" + RCMName + "',"); insertQuery.Append("'" + RCMEmail + "',"); insertQuery.Append("'" + PMID + "',"); insertQuery.Append("'" + PMName + "',");
            insertQuery.Append("'" + PMEmail + "',"); insertQuery.Append("'" + hop_id + "',"); insertQuery.Append("'" + hop_Name + "',"); insertQuery.Append("'" + hop_Email + "',");
            insertQuery.Append("'" + buheadid + "',"); insertQuery.Append("'" + buheadName + "',"); insertQuery.Append("'" + buheadEmail + "',"); insertQuery.Append("'" + LastStageUpdate + "',"); insertQuery.Append("'" + LastStageUpdate + "',"); insertQuery.Append("'" + selfcert + "',"); insertQuery.Append("'" + newsbu + "')");
            try
            {
                SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public bool SendClosureMailSafety(string rcmName, string EmailToSent, string buname, string project, string checklist, string createdby, string observation, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist submitted";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that meet compliance standards.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            sbBody.Append("Observation - " + observation);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendCompRCMForClosureMailSafety(string rcmName, string EmailToSent, string buname, string project, string checklist, string createdby, string observation, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist submitted";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that does not meet compliance standards.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            sbBody.Append("Non Compliance observation - " + observation);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public DataTable GetChecklistDatabyTransactionIDSafety(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public bool SendAssigneeMailSafety(string rcmname, string rcmEmail, string EmailToSent, string soname, string buname, string project, string checklist, string assigneename, string observation, string targetclosuredate, string remarks, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail;
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", rcmEmail, ";", CCMails2, ";", CCMails);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist is assigned to you";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + assigneename);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(soname + " has uploaded a checklist that does not meet compliance standards so it is assigned to you by " + rcmname + " for your action.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by -" + soname);
            sbBody.Append("<br>");
            sbBody.Append("Non compliance observations - " + observation);
            sbBody.Append("<br>");
            sbBody.Append("Target Date for closure - " + Convert.ToDateTime(targetclosuredate).ToShortDateString());
            sbBody.Append("<br>");
            sbBody.Append("RCM comments - " + remarks);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking link  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            //sbBody.Append("Non Complience");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendRCMForClosureMailsafety(string rcmName, string EmailToSent, string buname, string project, string checklist, string soname, string assigneename, string observation, string targetdate, string rcmremarks, string assigneeremarks, string closuredate, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", EmailToSent, ";", CCMails2, ";", CCMails);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist closed";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Safety Officer " + soname + " had uploaded a checklist that does not meet compliance standards, which was assigned by You to " + assigneename + "who has taken the actions to address the gaps.");
            sbBody.Append("<br>");
            sbBody.Append("You are requested to verify and validate the actions towards closure.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + soname);
            sbBody.Append("<br>");
            sbBody.Append("Non compliance observations - " + observation);
            sbBody.Append("<br>");
            sbBody.Append("Target Date for closure - " + Convert.ToDateTime(targetdate).ToShortDateString());
            sbBody.Append("<br>");
            sbBody.Append("RCM comments -" + rcmremarks);
            sbBody.Append("<br>");
            sbBody.Append("Assignee action taken details - " + assigneeremarks);
            sbBody.Append("<br>");
            sbBody.Append("Closure Date - " + Convert.ToDateTime(closuredate).ToShortDateString());
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("You can also view the item by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public DataTable GetSafetyPendingItemsforRCM(string rcmEmail, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist where RCM_Email='" + rcmEmail + "' and (stage='" + stage + "' or stage='Pending for Closure')";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        public DataTable GetSafetyPendingItemsforRCMClosed(string rcmEmail, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from EHSChecklist where RCM_Email='" + rcmEmail + "' and stage='" + stage + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        public DataTable GetSafetyPendingItemsforAssignee(string assigneeEmail, string stage)
        {
            DataTable dtAssignee = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from EHSChecklist where Assignee_Email='" + assigneeEmail + "' and stage='" + stage + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtAssignee);
            }
            catch (Exception exp)
            {
            }
            return dtAssignee;
        }
        public DataTable GetSafetyPendingItemsforSO(string SOEmail)
        {
            DataTable dtSO = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from EHSChecklist where (Stage='Pending with RCM' or Stage='Pending with Assignee') and SO_Email='" + SOEmail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtSO);
            }
            catch (Exception exp)
            {
            }
            return dtSO;
        }
        public DataTable GetSafetyPendingItemsforSOClosed(string SOEmail)
        {
            DataTable dtSO = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = " select * from EHSChecklist where Stage='Closed' and SO_Email='" + SOEmail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtSO);
            }
            catch (Exception exp)
            {
            }
            return dtSO;
        }
        public DataTable GetSafetyPendingItemsforHO()
        {
            DataTable dtSO = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist where Stage='Pending with RCM' or Stage='Pending with Assignee'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtSO);
            }
            catch (Exception exp)
            {
            }
            return dtSO;
        }
        public DataTable GetSafetyPendingItemsforHOClosed()
        {
            DataTable dtSO = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist where Stage='Closed'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtSO);
            }
            catch (Exception exp)
            {
            }
            return dtSO;
        }
        public DataTable GetLocationsByProjectCodeSafety(string projectCode)
        {
            DataTable dtProjectTypes = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                //con.Open();
                string sqlQuery = "select distinct Location from [EHSLocations] where Project='" + projectCode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtProjectTypes);
                //con.Close();
            }
            catch (Exception exp)
            {
            }
            return dtProjectTypes;
        }
        public DataTable GetEmptyRows()
        {
            DataTable dtProjectTypes = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                //con.Open();
                string sqlQuery = "select top 5 Location from EHSLocations";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtProjectTypes);
                //con.Close();
            }
            catch (Exception exp)
            {
            }
            return dtProjectTypes;
        }
        public DataTable GetEmptyRowsPPE()
        {
            DataTable dtProjectTypes = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                //con.Open();
                string sqlQuery = "select top 15 Location from EHSLocations";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtProjectTypes);
                //con.Close();
            }
            catch (Exception exp)
            {
            }
            return dtProjectTypes;
        }
        public DataTable GetEmptyLocationEvidenceDetails(string TransID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist_Details where Transaction_ID='" + TransID + "' and PicEvidence_DocName!=''";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
            }
            catch (Exception exp)
            {
            }
            return dt;
        }
        public DataTable GetEmptyCorrectiveEvidenceDetails(string TransID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist_Details where Transaction_ID='" + TransID + "' and (CorrEvidence_DocName is null or CorrEvidence_DocName='') and PicEvidence_DocName!=''";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
            }
            catch (Exception exp)
            {
            }
            return dt;
        }
        public bool SaveHousekeepingChecklist(int Tid, string frequency, string checklist,string  iscompliant,string bu, string project, string projectcode, string inspdate, string stage, string SOID, string SOName, string SOEmail, string RCMID, string RCMName, string RCMEmail, string PMID, string PMName, string PMEmail, string hop_id, string hop_Name, string hop_Email, string buheadid, string buheadName, string buheadEmail, string LastStageUpdate, string newsbu,string selfcert)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into EHSChecklist(Transaction_ID,Frequency,[Checklist Name],isCompliant,BUName,ProjectName,ProjectCode,[Inspection Date],Stage,SO_ID,SO_Name,SO_Email,RCM_ID,RCM_Name,RCM_Email,PM_ID,PM_Name,PM_Email,HOP_ID,HOP_Name,HOP_Email,BUHead_ID,BUHead_Name,BUHead_Email,[SO Submission Date],LastStageUpdate,SBUName,[Self Certification])");
            insertQuery.Append(" values ("); insertQuery.Append("" + Tid + ","); insertQuery.Append("'" + frequency + "',"); insertQuery.Append("'" + checklist + "',"); insertQuery.Append("'" + iscompliant + "',"); insertQuery.Append("'" + bu + "',"); insertQuery.Append("'" + project + "',"); insertQuery.Append("'" + projectcode + "',"); insertQuery.Append("'" + inspdate + "',");
            insertQuery.Append("'" + stage + "',"); insertQuery.Append("'" + SOID + "',"); insertQuery.Append("'" + SOName + "',"); insertQuery.Append("'" + SOEmail + "',");
            insertQuery.Append("'" + RCMID + "',"); insertQuery.Append("'" + RCMName + "',"); insertQuery.Append("'" + RCMEmail + "',"); insertQuery.Append("'" + PMID + "',"); insertQuery.Append("'" + PMName + "',");
            insertQuery.Append("'" + PMEmail + "',"); insertQuery.Append("'" + hop_id + "',"); insertQuery.Append("'" + hop_Name + "',"); insertQuery.Append("'" + hop_Email + "',");
            insertQuery.Append("'" + buheadid + "',"); insertQuery.Append("'" + buheadName + "',"); insertQuery.Append("'" + buheadEmail + "',"); insertQuery.Append("'" + LastStageUpdate + "',");
            insertQuery.Append("'" + LastStageUpdate + "',"); insertQuery.Append("'" + newsbu + "',"); insertQuery.Append("'" + selfcert + "')");
            try
            {
                SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public bool SendClosureMailHousekeeping(string rcmName, string EmailToSent, string buname, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist submitted";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that meets compliance standards.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendClosureMailPPE(string rcmName, string EmailToSent, string buname, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids=GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist submitted";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that meets compliance standards.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendClosureMailHiraTalk(string rcmName, string EmailToSent, string buname, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist submitted";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that meets compliance standards.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendCompRCMForClosureMailHousekeeping(string rcmName, string EmailToSent, string buname, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist submitted";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that does not meet compliance standards.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendCompRCMForClosureMailPPE(string rcmName, string EmailToSent, string buname, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist submitted";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that does not meet compliance standards.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendCompRCMForClosureMailHiratalk(string rcmName, string EmailToSent, string buname, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist submitted";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that does not meet compliance standards.";
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public DataTable GetLocationComplianceDatabyTransactionID(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist_Details where Transaction_ID='" + transactionID + "' and (CorrEvidence_DocName is null or CorrEvidence_DocName='')";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetLocationComplianceDataFilled(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist_Details where Transaction_ID='" + transactionID + "' and CorrEvidence_DocName!=''";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public bool SendAssigneeMailSafetyHousekeeping(string rcmname, string rcmEmail, string EmailToSent, string soname, string buname, string project, string checklist, string assigneename, string targetclosuredate, string remarks, string pmEmail, string CoEEmail, string transactionId)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail;
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", rcmEmail, ";", CCMails2, ";", CCMails);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Checklist is assigned to you";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + assigneename);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The RCM " + rcmname + " has set a target date for the Housekeeping Checklist that you uploaded and has sent it back to you..");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by -" + soname);
            sbBody.Append("<br>");
            sbBody.Append("Target Date for closure - " + Convert.ToDateTime(targetclosuredate).ToShortDateString());
            sbBody.Append("<br>");
            sbBody.Append("RCM comments - " + remarks);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking link  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            //sbBody.Append("Non Complience");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendRCMForClosureMailHousekeeping(string rcmName, string EmailToSent, string soName, string soEmail, string buname, string project, string checklist, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", EmailToSent, ";", CCMails2, ";", CCMails);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Safety Checklist - Verification & Close";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The assignee has taken necessary actions and closed the Non-compliance in Safety.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SendFinalClosureMailHousekeeping(string rcmName, string EmailToSent, string soName, string soEmail, string buname, string project, string checklist, string transactionId, string pmEmail, string CoEEmail)
        {
            bool isSent = false;
            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            string CCMails2 = string.Empty;
            if (buname == "Buildings & Airports" || buname == "Metros, Tunnels & Waterways" || buname == "Roads, Bridges & Ports" || buname == "Smart Cities" || buname == "Transportation & Hydro" || buname == "Urban Infrastructure")
            {
                string mailids = GetEmailIds("EHSUI");
                CCMails2 = mailids;
                //CCMails2 = "jana-c@tataprojects.com;rshankar@tataprojects.com ";
            }
            else
            {
                string mailids = GetEmailIds("EHSII");
                CCMails2 = mailids;
                //CCMails2 = "rasheedsa@tataprojects.com;vinodbhaskar@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com";
            }
            string ccMail = string.Concat(pmEmail, ";", EmailToSent, ";", CCMails2, ";", CCMails);
            //string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("tplintranet@tataprojects4.onmicrosoft.com");
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;
            MyMessage.Subject = "Safety Closure Email ";
            string Link = Constants.link + "Pages/EHSCheckListDetails.aspx?Tid=" + transactionId;
            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + soName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The Non-compliance in Safety for the below mentioned Checklist has been closed.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;
            try
            {
                SmtpClient emailClient = new SmtpClient();
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential("tplintranet@tataprojects.com", "tata@123");
                emailClient.Host = "smtp.office365.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.TargetName = "STARTTLS/smtp.office365.com";
                emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {
            }
            return isSent;
        }
        public bool SaveSafetyChecklist(int Tid, string bu, string projectname, string projectcode, string reportingon, string evidencefor, string comments, string rdate, string tdate, string itemstatus, string submissiondate, string lastupdatedate, string SOID, string SOName, string SOEmail, string RCMID, string RCMName, string RCMEmail, string PMID, string PMName, string PMEmail, string hop_id, string hop_Name, string hop_Email, string buheadid, string buheadName, string buheadEmail)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into SafetyUploads(Transaction_ID,BUName,ProjectName,ProjectCode,Reporting_On,Evidence_For,Comments,Reporting_Date,Target_Date,ItemStatus,SO_Submission_Date,Last_Update_Date,SO_ID,SO_Name,SO_Email,RCM_ID,RCM_Name,RCM_Email,PM_ID,PM_Name,PM_Email,HOP_ID,HOP_Name,HOP_Email,BUHead_ID,BUHead_Name,BUHead_Email)");
            insertQuery.Append(" values ("); insertQuery.Append("" + Tid + ","); insertQuery.Append("'" + bu + "',"); insertQuery.Append("'" + projectname + "',"); insertQuery.Append("'" + projectcode + "',"); insertQuery.Append("'" + reportingon + "',");
            insertQuery.Append("'" + evidencefor + "',"); insertQuery.Append("'" + comments + "',"); insertQuery.Append("'" + rdate + "',"); insertQuery.Append("@targetdate,"); insertQuery.Append("'" + itemstatus + "',");
            insertQuery.Append("'" + submissiondate + "',"); insertQuery.Append("'" + lastupdatedate + "',"); insertQuery.Append("'" + SOID + "',"); insertQuery.Append("'" + SOName + "',"); insertQuery.Append("'" + SOEmail + "',");
            insertQuery.Append("'" + RCMID + "',"); insertQuery.Append("'" + RCMName + "',"); insertQuery.Append("'" + RCMEmail + "',"); insertQuery.Append("'" + PMID + "',"); insertQuery.Append("'" + PMName + "',");
            insertQuery.Append("'" + PMEmail + "',"); insertQuery.Append("'" + hop_id + "',"); insertQuery.Append("'" + hop_Name + "',"); insertQuery.Append("'" + hop_Email + "',");
            insertQuery.Append("'" + buheadid + "',"); insertQuery.Append("'" + buheadName + "',"); insertQuery.Append("'" + buheadEmail + "')");
            SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
            cmd.Parameters.Add("@targetdate", SqlDbType.DateTime);
            if (reportingon == "Good Practice")
            {
                cmd.Parameters["@targetdate"].Value = DBNull.Value;
            }
            else
            {
                cmd.Parameters["@targetdate"].Value = DateTime.Parse(tdate);
            }
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
            }
            return isSaved;
        }
        public void UpdateSafetyUploadsEditDetails(string Tid, string corraction, string rfclosure, string socorrectiondate)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update SafetyUploads set Corrective_Action='" + corraction + "',ReadyForClosure='" + rfclosure + "',SO_Correction_Date='" + socorrectiondate + "',Last_Update_Date='" + socorrectiondate + "' where Transaction_ID='" + Tid + "'";
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {
            }
        }
        public DataTable GetSafetyUploadsDatabyTransactionID(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from SafetyUploads where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public DataTable GetAttachmentDatabyTransactionID(string transactionID, string listname)
        {
            DataTable dtData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from UploadAttachments where Transaction_ID='" + transactionID + "' and List='" + listname + "' and DocumentType='Supporting'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtData);
            }
            catch (Exception exp)
            {
            }
            return dtData;
        }
        public DataTable GetEvidenceAttachmentDatabyTransactionID(string transactionID, string listname)
        {
            DataTable dtData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from UploadAttachments where Transaction_ID='" + transactionID + "' and List='" + listname + "' and DocumentType='Evidence'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtData);
            }
            catch (Exception exp)
            {
            }
            return dtData;
        }
        public void UpdateSafetyStatus(string Tid, string status, string rcmsubdate)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update SafetyUploads set ItemStatus='" + status + "',RCM_Submission_Date='" + rcmsubdate + "',Last_Update_Date='" + rcmsubdate + "' where Transaction_ID='" + Tid + "'";
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {
            }
        }
        public DataTable GetSafetyUploadsPendingItems(string pcode, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from SafetyUploads where ProjectCode in (" + pcode + ") and ItemStatus='" + stage + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        public DataTable GetSafetyUploadsPendingItemsRCM(string pcode, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from SafetyUploads where ProjectCode in (" + pcode + ") and ItemStatus='" + stage + "' and ReadyForClosure='True'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        public DataTable GetSafetySelfCertPendingItemsRCM(string pcode)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from Safety_SelfCertification where ProjectCode in (" + pcode + ")";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {
            }
            return dtRCM;
        }
        public DataTable GetSafetySelfCertificationDatabyID(string ID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from Safety_SelfCertification where ID='" + ID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {
            }
            return dtChecklistData;
        }
        public void SaveSelfCertification(int id, string buname, string projectname, string comp1, string comp2, string comp3, string comp4, string comp5, string certification, string created, string modified, string repmonth, string repweek, string projectcode, string createdby, string modifiedby,string repyear)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into Safety_SelfCertification(ID,[BU Name],[Project Name],[Comp 1],[Comp 2],[Comp 3],[Comp 4],[Comp 5],Certification,Created,Modified,[Reporting Month],[Rep Week],ProjectCode,[Created By],[Modified By],[Rep Year])");
            insertQuery.Append(" values ("); insertQuery.Append("" + id + ","); insertQuery.Append("'" + buname + "',"); insertQuery.Append("'" + projectname + "',");
            insertQuery.Append("'" + comp1 + "',"); insertQuery.Append("'" + comp2 + "',"); insertQuery.Append("'" + comp3 + "',"); insertQuery.Append("'" + comp4 + "',"); insertQuery.Append("'" + comp5 + "',");
            insertQuery.Append("'" + certification + "',"); insertQuery.Append("'" + created + "',"); insertQuery.Append("'" + modified + "',"); insertQuery.Append("'" + repmonth + "',");
            insertQuery.Append("'" + repweek + "',"); insertQuery.Append("'" + projectcode + "',"); insertQuery.Append("'" + createdby + "',"); insertQuery.Append("'" + modifiedby + "',"); insertQuery.Append("'" + repyear + "')");
            try
            {
                SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {
            }
        }
        public string GetLatestIDSafetySelfCert()
        {
            string ID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 ID FROM [EQHSAutomation].[dbo].[Safety_SelfCertification] ORDER BY ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["ID"] != null)
                        {
                            ID = dt.Rows[0]["ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return ID;
        }
        public string GetEmailIds(string key)
        {
            DataTable dt = new DataTable();
            string value = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select value from EmailList where [key]='"+ key +"'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Value"] != null)
                        {
                            value = dt.Rows[0]["Value"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return value;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SharePoint;

namespace SiteSafetyUploads
{
    class Uploads
    {
        public DataTable GetProjectCodeByLoggedInUserSafety()
        {
            DataTable dt = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='Safety_x0020_Officer' /><Value Type='Integer'><UserID /></Value></Eq></Where>";
                            dt = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dt;
        }

        public DataTable GetProjectCodeByLoggedInUserRCM()
        {
            DataTable dt = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='RCM' /><Value Type='Integer'><UserID /></Value></Eq></Where>";
                            dt = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                }
            }
            return dt;
        }

        public DataTable GetProjectCodeByLoggedInUserFQE()
        {
            //SPListItem projectCode = null;
            DataTable dt = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='Field_x0020_Quality_x0020_Engine' /><Value Type='Integer'><UserID Type='Integer'/></Value></Eq></Where>";
                            dt = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }

                }
            }
            return dt;
        }

        public DataTable GetProjectdetailsByProjectCode(string projectCode)
        {
            DataTable dtProjectDetails = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["TPL Projects List"] != null)
                    {
                        SPList list = web.Lists["TPL Projects List"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='Title' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";

                            dtProjectDetails = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }

                }
            }
            return dtProjectDetails;
        }

        public DataTable GetProjectCodeByProjectName(string projectCode)
        {
            DataTable dtProjectDetails = new DataTable();
            // SPListItem projectCodeItem = null;
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["TPL Projects List"] != null)
                    {
                        SPList list = web.Lists["TPL Projects List"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Contains><FieldRef Name='Final_x0020_Project_x0020_Name' /><Value Type='Text'>" + projectCode + "</Value></Contains></Where>";
                            dtProjectDetails = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }

                }
            }
            return dtProjectDetails;
        }

        public SPListItem GetProjectCodeByProjectNameForRCM(string projectCode)
        {
            SPListItem projectCodeForRCM = null;
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='Project_x0020_Code' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                            //query.ViewFields = "<FieldRef Name='Final_x0020_Project_x0020_Name' /><FieldRef Name='SBUName' />";
                            SPListItemCollection items = list.GetItems(query);
                            foreach (SPListItem item in items)
                            {
                                projectCodeForRCM = item;
                                break;
                            }
                        }
                    }

                }
            }
            return projectCodeForRCM;
        }

        public bool SaveSafetyChecklist(int Tid, string bu, string projectname, string projectcode, string reportingon,string evidencefor, string comments, string rdate, string tdate, string itemstatus,string submissiondate, string lastupdatedate, string SOID, string SOName, string SOEmail, string RCMID, string RCMName, string RCMEmail, string PMID, string PMName, string PMEmail, string hop_id, string hop_Name, string hop_Email, string buheadid, string buheadName, string buheadEmail)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into SafetyUploads(Transaction_ID,BUName,ProjectName,ProjectCode,Reporting_On,Evidence_For,Comments,Reporting_Date,Target_Date,ItemStatus,SO_Submission_Date,Last_Update_Date,SO_ID,SO_Name,SO_Email,RCM_ID,RCM_Name,RCM_Email,PM_ID,PM_Name,PM_Email,HOP_ID,HOP_Name,HOP_Email,BUHead_ID,BUHead_Name,BUHead_Email)");
            insertQuery.Append(" values ("); insertQuery.Append("" + Tid + ","); insertQuery.Append("'" + bu + "',"); insertQuery.Append("'" + projectname + "',"); insertQuery.Append("'" + projectcode + "',"); insertQuery.Append("'" + reportingon + "',");
            insertQuery.Append("'" + evidencefor + "',"); insertQuery.Append("'" + comments + "',"); insertQuery.Append("'" + rdate + "',"); insertQuery.Append("@targetdate,"); insertQuery.Append("'" + itemstatus + "',");
            insertQuery.Append("'" + submissiondate + "',"); insertQuery.Append("'" + lastupdatedate + "',"); insertQuery.Append("'" + SOID + "',"); insertQuery.Append("'" + SOName + "',"); insertQuery.Append("'" + SOEmail + "',");
            insertQuery.Append("'" + RCMID + "',"); insertQuery.Append("'" + RCMName + "',"); insertQuery.Append("'" + RCMEmail + "',"); insertQuery.Append("'" + PMID + "',"); insertQuery.Append("'" + PMName + "',");
            insertQuery.Append("'" + PMEmail + "',"); insertQuery.Append("'" + hop_id + "',"); insertQuery.Append("'" + hop_Name + "',"); insertQuery.Append("'" + hop_Email + "',");
            insertQuery.Append("'" + buheadid + "',"); insertQuery.Append("'" + buheadName + "',"); insertQuery.Append("'" + buheadEmail + "')");
            SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
            cmd.Parameters.Add("@targetdate", SqlDbType.DateTime);
            if (reportingon == "Good Practice")
            {
                cmd.Parameters["@targetdate"].Value = DBNull.Value;
            }
            else
            {
                cmd.Parameters["@targetdate"].Value = DateTime.Parse(tdate);
            }
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {

            }
            return isSaved;
        }

        public bool SaveQualityChecklist(int Tid, string bu, string projectname, string projectcode, string reportingon, string evidencefor, string category, string comments, string rdate, string tdate, string itemstatus, string submissiondate, string lastupdatedate, string SOID, string SOName, string SOEmail, string RCMID, string RCMName, string RCMEmail, string PMID, string PMName, string PMEmail, string PEID, string PEName, string PEEmail, string hop_id, string hop_Name, string hop_Email, string buheadid, string buheadName, string buheadEmail)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into QualityUploads(Transaction_ID,BUName,ProjectName,ProjectCode,Reporting_On,Evidence_For,Category,Comments,Reporting_Date,Target_Date,ItemStatus,FQE_Submission_Date,Last_Update_Date,FQE_ID,FQE_Name,FQE_Email,RCM_ID,RCM_Name,RCM_Email,PM_ID,PM_Name,PM_Email,PE_ID,PE_Name,PE_Email,HOP_ID,HOP_Name,HOP_Email,BUHead_ID,BUHead_Name,BUHead_Email)");
            insertQuery.Append(" values ("); insertQuery.Append("" + Tid + ","); insertQuery.Append("'" + bu + "',"); insertQuery.Append("'" + projectname + "',"); insertQuery.Append("'" + projectcode + "',"); insertQuery.Append("'" + reportingon + "',");
            insertQuery.Append("'" + evidencefor + "',"); insertQuery.Append("'" + category + "',"); insertQuery.Append("'" + comments + "',"); insertQuery.Append("'" + rdate + "',"); insertQuery.Append("@targetdate,"); insertQuery.Append("'" + itemstatus + "',");
            insertQuery.Append("'" + submissiondate + "',"); insertQuery.Append("'" + lastupdatedate + "',"); insertQuery.Append("'" + SOID + "',"); insertQuery.Append("'" + SOName + "',"); insertQuery.Append("'" + SOEmail + "',");
            insertQuery.Append("'" + RCMID + "',"); insertQuery.Append("'" + RCMName + "',"); insertQuery.Append("'" + RCMEmail + "',"); insertQuery.Append("'" + PMID + "',"); insertQuery.Append("'" + PMName + "',");
            insertQuery.Append("'" + PMEmail + "',"); insertQuery.Append("'" + PEID + "',"); insertQuery.Append("'" + PEName + "',");
            insertQuery.Append("'" + PEEmail + "',"); insertQuery.Append("'" + hop_id + "',"); insertQuery.Append("'" + hop_Name + "',"); insertQuery.Append("'" + hop_Email + "',");
            insertQuery.Append("'" + buheadid + "',"); insertQuery.Append("'" + buheadName + "',"); insertQuery.Append("'" + buheadEmail + "')");
            SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
            cmd.Parameters.Add("@targetdate", SqlDbType.DateTime);
            if (reportingon == "Good Practice")
            {
                cmd.Parameters["@targetdate"].Value = DBNull.Value;
            }
            else
            {
                cmd.Parameters["@targetdate"].Value = DateTime.Parse(tdate);
            }
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {

            }
            return isSaved;
        }

        public string GetLatestTransactionIDSafety()
        {
            string transactionID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 Transaction_ID FROM [EQHSAutomation].[dbo].[SafetyUploads] ORDER BY Transaction_ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Transaction_ID"] != null)
                        {
                            transactionID = dt.Rows[0]["Transaction_ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {

            }
            return transactionID;
        }

        public string GetLatestTransactionIDQuality()
        {
            string transactionID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 Transaction_ID FROM [EQHSAutomation].[dbo].[QualityUploads] ORDER BY Transaction_ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Transaction_ID"] != null)
                        {
                            transactionID = dt.Rows[0]["Transaction_ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {

            }
            return transactionID;
        }

        public string GetOnlyEmployeeID(string loggedInName)
        {
            string empID = string.Empty;
            string loggedNameLowerCase = loggedInName.ToLower().ToString();
            if (loggedNameLowerCase.Contains("tpl"))
            {

                string replaced = loggedNameLowerCase.Replace('\\', '@');
                if (replaced.Contains("@"))
                {
                    string[] seperators = { "@" };
                    string[] nameArray = replaced.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
                    if (nameArray.Length == 2)
                    {
                        if (nameArray[1] != null)
                        {
                            empID = nameArray[1];
                        }
                    }
                }
                else
                {
                    string replacedTest = loggedNameLowerCase.Replace('\\', '@');
                }
            }
            return empID.ToUpper();
        }

        public DataTable GetSafetyUploadsDatabyTransactionID(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from SafetyUploads where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {

            }
            return dtChecklistData;
        }

        public DataTable GetQualityUploadsDatabyTransactionID(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from QualityUploads where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {

            }
            return dtChecklistData;
        }

        public DataTable GetAttachmentDatabyTransactionID(string transactionID, string listname)
        {
            DataTable dtData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from UploadAttachments where Transaction_ID='" + transactionID + "' and List='"+ listname +"' and DocumentType='Supporting'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtData);
            }
            catch (Exception exp)
            {

            }
            return dtData;
        }

        public DataTable GetEvidenceAttachmentDatabyTransactionID(string transactionID,string listname)
        {
            DataTable dtData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from UploadAttachments where Transaction_ID='" + transactionID + "' and List='"+ listname +"' and DocumentType='Evidence'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtData);
            }
            catch (Exception exp)
            {

            }
            return dtData;
        }

        public void UpdateSafetyStatus(string Tid,string status,string rcmsubdate)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update SafetyUploads set ItemStatus='" + status + "',RCM_Submission_Date='" + rcmsubdate + "',Last_Update_Date='"+ rcmsubdate +"' where Transaction_ID='" + Tid + "'";

            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {

            }
        }

        public void UpdateQualityStatus(string Tid, string status, string rcmsubdate)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update QualityUploads set ItemStatus='" + status + "',RCM_Submission_Date='" + rcmsubdate + "',Last_Update_Date='" + rcmsubdate + "' where Transaction_ID='" + Tid + "'";

            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {

            }
        }

        public void UpdateSafetyUploadsEditDetails(string Tid,string corraction,string rfclosure,string repdate,string targetdate,string socorrectiondate)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update SafetyUploads set Corrective_Action='" + corraction + "',ReadyForClosure='" + rfclosure + "',Reporting_Date='" + repdate + "',Target_Date='" + targetdate + "',SO_Correction_Date='" + socorrectiondate + "',Last_Update_Date='" + socorrectiondate + "' where Transaction_ID='" + Tid + "'";

            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {

            }
        }

        public void UpdateQualityUploadsEditDetails(string Tid, string corraction, string rfclosure, string repdate, string targetdate, string fqecorrectiondate)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            string sqlQuery = "update QualityUploads set Corrective_Action='" + corraction + "',ReadyForClosure='" + rfclosure + "',Reporting_Date='" + repdate + "',Target_Date='" + targetdate + "',FQE_Correction_Date='" + fqecorrectiondate + "',Last_Update_Date='" + fqecorrectiondate + "' where Transaction_ID='" + Tid + "'";

            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception exp)
            {

            }
        }

        public DataTable GetCategoryList(string Evidence)
        {
            DataTable dtData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select distinct Category from CategoryList where EvidenceFor='" + Evidence + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtData);
            }
            catch (Exception exp)
            {

            }
            return dtData;
        }
        public DataTable GetEvidenceData()
        {
            DataTable dtEvidence = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();
                string sqlQuery = "select distinct EvidenceFor from CategoryList";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtEvidence);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return dtEvidence;
        }

        public DataTable GetSafetyUploadsPendingItems(string pcode, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from SafetyUploads where ProjectCode in (" + pcode + ") and ItemStatus='" + stage + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {

            }
            return dtRCM;
        }

        public DataTable GetQualityUploadsPendingItems(string pcode, string stage)
        {
            DataTable dtRCM = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "  select * from QualityUploads where ProjectCode in (" + pcode + ") and ItemStatus='" + stage + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtRCM);
            }
            catch (Exception exp)
            {

            }
            return dtRCM;
        }

    }
}

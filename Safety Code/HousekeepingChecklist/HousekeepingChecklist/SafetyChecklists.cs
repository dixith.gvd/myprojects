﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
namespace HousekeepingChecklist
{
    class SafetyChecklists
    {
        public DataTable GetProjectCodeByLoggedInUserSafety()
        {
            //SPListItem projectCode = null;
            DataTable dt = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='Safety_x0020_Officer' /><Value Type='Integer'><UserID /></Value></Eq></Where>";
                            dt = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }

                }
            }
            return dt;
        }

        public DataTable GetProjectdetailsByProjectCode(string projectCode)
        {
            DataTable dtProjectDetails = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["TPL Projects List"] != null)
                    {
                        SPList list = web.Lists["TPL Projects List"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='Title' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                            dtProjectDetails = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }

                }
            }
            return dtProjectDetails;
        }

        public DataTable GetProjectCodeByProjectName(string projectCode)
        {
            DataTable dtProjectDetails = new DataTable();
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["TPL Projects List"] != null)
                    {
                        SPList list = web.Lists["TPL Projects List"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Contains><FieldRef Name='Final_x0020_Project_x0020_Name' /><Value Type='Text'>" + projectCode + "</Value></Contains></Where>";

                            dtProjectDetails = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }

                }
            }
            return dtProjectDetails;
        }

        public SPListItem GetProjectCodeByProjectNameForRCM(string projectCode)
        {
            SPListItem projectCodeForRCM = null;
            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='Project_x0020_Code' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                            SPListItemCollection items = list.GetItems(query);
                            foreach (SPListItem item in items)
                            {
                                projectCodeForRCM = item;
                                break;
                            }
                        }
                    }

                }
            }
            return projectCodeForRCM;
        }

        public string GetOnlyEmployeeID(string loggedInName)
        {
            string empID = string.Empty;
            string loggedNameLowerCase = loggedInName.ToLower().ToString();
            if (loggedNameLowerCase.Contains("tpl"))
            {

                string replaced = loggedNameLowerCase.Replace('\\', '@');
                if (replaced.Contains("@"))
                {
                    string[] seperators = { "@" };
                    string[] nameArray = replaced.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
                    if (nameArray.Length == 2)
                    {
                        if (nameArray[1] != null)
                        {
                            empID = nameArray[1];
                        }
                    }
                }
                else
                {
                    string replacedTest = loggedNameLowerCase.Replace('\\', '@');
                }
            }
            return empID.ToUpper();
        }

        public DataTable GetLocationsByProjectCodeSafety(string projectCode)
        {
            DataTable dtProjectTypes = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                //con.Open();
                string sqlQuery = "select distinct Location from [EHSLocations] where Project='" + projectCode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtProjectTypes);
                //con.Close();
            }
            catch (Exception exp)
            {

            }
            return dtProjectTypes;
        }

        public DataTable GetEmptyRows()
        {
            DataTable dtProjectTypes = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                //con.Open();
                string sqlQuery = "select top 5 Location from EHSLocations";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtProjectTypes);
                //con.Close();
            }
            catch (Exception exp)
            {

            }
            return dtProjectTypes;
        }

        public DataTable GetEmptyRowsPPE()
        {
            DataTable dtProjectTypes = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                //con.Open();
                string sqlQuery = "select top 15 Location from EHSLocations";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtProjectTypes);
                //con.Close();
            }
            catch (Exception exp)
            {

            }
            return dtProjectTypes;
        }

        public string GetLatestTransactionIDSafety()
        {
            string transactionID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 Transaction_ID FROM [EQHSAutomation].[dbo].[EHSChecklist] ORDER BY Transaction_ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Transaction_ID"] != null)
                        {
                            transactionID = dt.Rows[0]["Transaction_ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {

            }
            return transactionID;
        }

        public DataTable GetEmptyLocationEvidenceDetails(string TransID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist_Details where Transaction_ID='" + TransID + "' and PicEvidence_DocName!=''"; 
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
            }
            catch (Exception exp)
            {

            }
            return dt;
        }
        public DataTable GetEmptyCorrectiveEvidenceDetails(string TransID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist_Details where Transaction_ID='" + TransID + "' and (CorrEvidence_DocName is null or CorrEvidence_DocName='') and PicEvidence_DocName!=''";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
            }
            catch (Exception exp)
            {

            }
            return dt;
        }
        public bool SaveHousekeepingChecklist(int Tid, string frequency, string checklist, string bu, string project, string projectcode, string inspdate, string stage, string SOID, string SOName, string SOEmail, string RCMID, string RCMName, string RCMEmail, string PMID, string PMName, string PMEmail, string hop_id, string hop_Name, string hop_Email, string buheadid, string buheadName, string buheadEmail, string LastStageUpdate,string newsbu)
        {
            bool isSaved = false;
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

            StringBuilder insertQuery = new StringBuilder();
            insertQuery.Append("insert into EHSChecklist(Transaction_ID,Frequency,[Checklist Name],BUName,ProjectName,ProjectCode,[Inspection Date],Stage,SO_ID,SO_Name,SO_Email,RCM_ID,RCM_Name,RCM_Email,PM_ID,PM_Name,PM_Email,HOP_ID,HOP_Name,HOP_Email,BUHead_ID,BUHead_Name,BUHead_Email,[SO Submission Date],LastStageUpdate,SBUName)");
            insertQuery.Append(" values ("); insertQuery.Append("" + Tid + ","); insertQuery.Append("'" + frequency + "',"); insertQuery.Append("'" + checklist + "',"); insertQuery.Append("'" + bu + "',"); insertQuery.Append("'" + project + "',"); insertQuery.Append("'" + projectcode + "',"); insertQuery.Append("'" + inspdate + "',");
            insertQuery.Append("'" + stage + "',"); insertQuery.Append("'" + SOID + "',"); insertQuery.Append("'" + SOName + "',"); insertQuery.Append("'" + SOEmail + "',");
            insertQuery.Append("'" + RCMID + "',"); insertQuery.Append("'" + RCMName + "',"); insertQuery.Append("'" + RCMEmail + "',"); insertQuery.Append("'" + PMID + "',"); insertQuery.Append("'" + PMName + "',");
            insertQuery.Append("'" + PMEmail + "',"); insertQuery.Append("'" + hop_id + "',"); insertQuery.Append("'" + hop_Name + "',"); insertQuery.Append("'" + hop_Email + "',");
            insertQuery.Append("'" + buheadid + "',"); insertQuery.Append("'" + buheadName + "',"); insertQuery.Append("'" + buheadEmail + "',"); insertQuery.Append("'" + LastStageUpdate + "',"); insertQuery.Append("'" + LastStageUpdate + "',"); insertQuery.Append("'" + newsbu + "')");
            try
            {
                SqlCommand cmd = new SqlCommand(insertQuery.ToString(), con);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {

            }
            return isSaved;
        }

        public bool SendClosureMailHousekeeping(string rcmName, string EmailToSent, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {

            bool isSent = false;

            string toMail = EmailToSent;

            string CCMails = CoEEmail.Trim();
            //string CCMails2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";

            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
            MailMessage MyMessage = new MailMessage();

            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }

            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;

            MyMessage.Subject = "Checklist submitted";

            string Link = Constants.link + "SitePages/Housekeeping Test.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that meets compliance standards.";
            StringBuilder sbBody = new StringBuilder();

            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");

            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;

            try
            {
                SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {

            }
            return isSent;
        }

        public bool SendClosureMailPPE(string rcmName, string EmailToSent, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {

            bool isSent = false;

            string toMail = EmailToSent;

            string CCMails = CoEEmail.Trim();
            //string CCMails2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";

            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
            MailMessage MyMessage = new MailMessage();

            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }

            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;

            MyMessage.Subject = "Checklist submitted";

            string Link = Constants.link + "SitePages/PPEChecklist.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that meets compliance standards.";
            StringBuilder sbBody = new StringBuilder();

            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");

            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;

            try
            {
                SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {

            }
            return isSent;
        }

        public bool SendClosureMailHiraTalk(string rcmName, string EmailToSent, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {

            bool isSent = false;

            string toMail = EmailToSent;

            string CCMails = CoEEmail.Trim();
            //string CCMails2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";

            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
            MailMessage MyMessage = new MailMessage();

            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }

            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;

            MyMessage.Subject = "Checklist submitted";

            string Link = Constants.link + "SitePages/HiraTalkChecklist.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that meets compliance standards.";
            StringBuilder sbBody = new StringBuilder();

            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");

            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;

            try
            {
                SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {

            }
            return isSent;
        }

        public bool SendCompRCMForClosureMailHousekeeping(string rcmName, string EmailToSent, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {

            bool isSent = false;

            string toMail = EmailToSent;

            string CCMails = CoEEmail.Trim();
            //string CCMails2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";

            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
            MailMessage MyMessage = new MailMessage();

            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }

            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;

            MyMessage.Subject = "Checklist submitted";

            string Link = Constants.link + "SitePages/Housekeeping%20Test.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that does not meet compliance standards.";
            StringBuilder sbBody = new StringBuilder();

            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");

            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;

            try
            {
                SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {

            }
            return isSent;
        }

        public bool SendCompRCMForClosureMailPPE(string rcmName, string EmailToSent, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {

            bool isSent = false;

            string toMail = EmailToSent;

            string CCMails = CoEEmail.Trim();
            //string CCMails2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";

            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
            MailMessage MyMessage = new MailMessage();

            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }

            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;

            MyMessage.Subject = "Checklist submitted";

            string Link = Constants.link + "SitePages/PPEChecklist.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that does not meet compliance standards.";
            StringBuilder sbBody = new StringBuilder();

            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");

            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;

            try
            {
                SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {

            }
            return isSent;
        }

        public bool SendCompRCMForClosureMailHiratalk(string rcmName, string EmailToSent, string project, string checklist, string createdby, string pmEmail, string CoEEmail, string transactionId)
        {

            bool isSent = false;

            string toMail = EmailToSent;

            string CCMails = CoEEmail.Trim();
            //string CCMails2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string ccMail = string.Concat(pmEmail, ";", CCMails, ";", CCMails2);
            string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";

            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
            MailMessage MyMessage = new MailMessage();

            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }

            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;

            MyMessage.Subject = "Checklist submitted";

            string Link = Constants.link + "SitePages/HiraTalkChecklist.aspx?Tid=" + transactionId;
            string compli = "has uploaded a checklist that does not meet compliance standards.";
            StringBuilder sbBody = new StringBuilder();

            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append(createdby + " " + compli);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by - " + createdby);
            sbBody.Append("<br>");
            //sbBody.Append("Transaction ID  - " + transactionId);
            //sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");

            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;

            try
            {
                SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {

            }
            return isSent;
        }

        public DataTable GetChecklistDatabyTransactionIDSafety(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist where Transaction_ID='" + transactionID + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {

            }
            return dtChecklistData;
        }

        public DataTable GetLocationComplianceDatabyTransactionID(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist_Details where Transaction_ID='" + transactionID + "' and (CorrEvidence_DocName is null or CorrEvidence_DocName='')";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {

            }
            return dtChecklistData;
        }

        public DataTable GetLocationComplianceDataFilled(string transactionID)
        {
            DataTable dtChecklistData = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select * from EHSChecklist_Details where Transaction_ID='"+ transactionID +"' and CorrEvidence_DocName!=''";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dtChecklistData);
            }
            catch (Exception exp)
            {

            }
            return dtChecklistData;
        }

        public bool SendAssigneeMailSafety(string rcmname, string rcmEmail, string EmailToSent, string soname, string project, string checklist, string assigneename, string targetclosuredate, string remarks, string pmEmail, string CoEEmail, string transactionId)
        {

            bool isSent = false;

            string toMail = EmailToSent;

            string CCMails = CoEEmail;

            //string ccMail2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string ccMail = string.Concat(pmEmail, ";", rcmEmail, ";", ccMail2, ";", CCMails);
            string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
            MailMessage MyMessage = new MailMessage();

            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }

            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;

            MyMessage.Subject = "Checklist is assigned to you";

            string Link = Constants.link + "SitePages/Housekeeping%20Test.aspx?Tid=" + transactionId;

            StringBuilder sbBody = new StringBuilder();

            sbBody.Append("Dear " + assigneename);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The RCM " + rcmname + " has set a target date for the Housekeeping Checklist that you uploaded and has sent it back to you..");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Submitted by -" + soname);
            sbBody.Append("<br>");
            sbBody.Append("Target Date for closure - " + Convert.ToDateTime(targetclosuredate).ToShortDateString());
            sbBody.Append("<br>");
            sbBody.Append("RCM comments - " + remarks);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking link  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            sbBody.Append("<br>");
            //sbBody.Append("Non Complience");
            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;

            try
            {
                SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {

            }
            return isSent;
        }

        public bool SendRCMForClosureMail(string rcmName, string EmailToSent, string soName, string soEmail, string project, string checklist, string transactionId, string pmEmail, string CoEEmail)
        {

            bool isSent = false;

            string toMail = EmailToSent;
            string CCMails = CoEEmail.Trim();
            //string CCMails2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string ccMail = string.Concat(pmEmail, ";", EmailToSent, ";", CCMails2, ";", CCMails);
            string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";
            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
            MailMessage MyMessage = new MailMessage();

            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }

            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;

            MyMessage.Subject = "Safety Checklist - Verification & Close";

            string Link = Constants.link + "SitePages/Housekeeping%20Test.aspx?Tid=" + transactionId;

            StringBuilder sbBody = new StringBuilder();

            sbBody.Append("Dear " + rcmName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The assignee has taken necessary actions and closed the Non-compliance in Safety.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item and take the necessary actions by clicking  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");

            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;

            try
            {
                SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {

            }
            return isSent;
        }

        public bool SendFinalClosureMail(string rcmName, string EmailToSent, string soName, string soEmail, string project, string checklist, string transactionId, string pmEmail, string CoEEmail)
        {

            bool isSent = false;

            string toMail = EmailToSent;

            string CCMails = CoEEmail.Trim();
            //string CCMails2 = "rasheedsa@tataprojects.com;debasishkirtuniya@tataprojects.com;pradeepkumars@tataprojects.com;jaikishanhemant@tataprojects.com;rshankar@tataprojects.com;vinodbhaskar@tataprojects.com";
            //string ccMail = string.Concat(pmEmail, ";", EmailToSent, ";", CCMails2, ";", CCMails);
            string ccMail = "deekshithgvd-t@tataprojects.com;srinivasiyer-t@tataprojects.com";

            MailAddress SendFrom = new MailAddress("TPLIntranet@tataprojects.com");
            MailMessage MyMessage = new MailMessage();

            if (!string.IsNullOrEmpty(toMail))
            {
                string strTo = toMail.Trim();
                string[] toEmailids = strTo.Split(';');
                foreach (string toEmail in toEmailids)
                {
                    MyMessage.To.Add(new MailAddress(toEmail));
                }
            }

            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    MyMessage.CC.Add(new MailAddress(CCEmail));
                }
            }
            MyMessage.From = SendFrom;

            MyMessage.Subject = "Safety Closure Email ";

            string Link = Constants.link + "SitePages/Housekeeping%20Test.aspx?Tid=" + transactionId;

            StringBuilder sbBody = new StringBuilder();

            sbBody.Append("Dear " + soName);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("The Non-compliance in Safety for the below mentioned Checklist has been closed.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Project - " + project);
            sbBody.Append("<br>");
            sbBody.Append("Checklist - " + checklist);
            sbBody.Append("<br>");
            sbBody.Append("Transaction ID  - " + transactionId);
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Request you to view the item  <a href='" + Link + "'>here</a> ");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Regards,");
            sbBody.Append("<br>");

            MyMessage.Body = sbBody.ToString();
            MyMessage.IsBodyHtml = true;

            try
            {
                SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                emailClient.Send(MyMessage);
                isSent = true;
            }
            catch (Exception ex)
            {

            }
            return isSent;
        }

    }
}

﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO.Compression;

namespace QMDDashboard_TPL.ChecklistsModule.FQPChecklistsActions
{
    [ToolboxItemAttribute(false)]
    public partial class FQPChecklistsActions : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public FQPChecklistsActions()
        {

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
            if (this.Page.Request.QueryString["Tid"] != null)
            {
                lblTransactionID.Text = this.Page.Request.QueryString["Tid"].ToString();

                DataTable dtdata = objQuality.GetChecklistDatabyTransactionIDForFQP(lblTransactionID.Text);
                if (dtdata.Rows.Count > 0)
                {
                    if (dtdata.Rows[0]["FQE_Submitted_Date"] != null)
                    {
                        string dtAssSubDate = dtdata.Rows[0]["FQE_Submitted_Date"].ToString();
                        if (!string.IsNullOrEmpty(dtAssSubDate))
                        {
                            DateTime dtFqeDate = Convert.ToDateTime(dtAssSubDate);
                            FQESubmittedDate.Text = dtFqeDate.ToString("dd/MM/yyyy");
                            assigneeClosureDateDATE.MinDate = dtFqeDate;
                            dtTargetClosureDate.MinDate = dtFqeDate;

                        }

                        string dtClosureDate = dtdata.Rows[0]["Assignee_Action_Date"].ToString();
                        if (!string.IsNullOrEmpty(dtClosureDate))
                        {
                            DateTime dtAssigneDate = Convert.ToDateTime(dtClosureDate);
                            //  dtClosuredate.MinDate = dtAssigneDate;

                        }

                    }
                }
            }
            // assigneeClosureDateDATE.MaxDate = DateTime.Now.Date;

        }
        QualityChecklists objQuality = new QualityChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {

                if (this.Page.Request.QueryString["Tid"] != null)
                {
                    lblTransactionID.Text = this.Page.Request.QueryString["Tid"].ToString();
                    DataTable dtdata = objQuality.GetChecklistDatabyTransactionIDForFQP(lblTransactionID.Text);
                    DataTable dtRWCdata = objQuality.GetRWCbyTransactionID(lblTransactionID.Text);

                    if (dtdata.Rows.Count > 0 || dtRWCdata.Rows.Count > 0)
                    {

                        string rcmEmail = string.Empty;
                        string assigneeEmail = string.Empty;
                        bool mainFormLogic = true;
                        bool rwcFormLogic = true;
                        string fqeEmail = string.Empty;
                        string stage = string.Empty;
                        string peEmail = string.Empty;
                        dvFilledForm.Visible = true;

                        if (dtdata.Rows[0]["Stage"] != null)
                        {
                            stage = dtdata.Rows[0]["Stage"].ToString();
                        }

                        if (stage.Equals("Pending with PE"))
                        {
                            if (dtdata.Rows[0]["PE_Email"] != null)
                            {
                                peEmail = dtdata.Rows[0]["PE_Email"].ToString();
                                lblPERCMName.Text = dtdata.Rows[0]["RCM_Email"].ToString();
                                lblPEEmail.Text = dtdata.Rows[0]["PE_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(peEmail))
                                {
                                    dvFilledForm.Visible = true;
                                    mainFormLogic = true;
                                    dvAssignee.Visible = false;
                                    peSubmit.Visible = true;
                                    dvRCM.Visible = false;
                                    divPE.Visible = true;
                                    Div1.Visible = true;
                                    Div2.Visible = true;

                                }

                            }

                        }
                        else if (stage.Equals("Pending with RCM"))
                        {
                            tblSWI.Visible = false;
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                rcmEmail = dtdata.Rows[0]["RCM_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(rcmEmail))
                                {
                                    dvFilledForm.Visible = true;
                                    mainFormLogic = true;
                                    rwcFormLogic = true;
                                    dvAssignee.Visible = false;
                                    RCMSubmit.Visible = true;
                                    dvRCM.Visible = true;
                                    divPE.Visible = false;
                                    Div1.Visible = true;
                                    Div2.Visible = true;
                                    tblSWI.Visible = true;
                                    tblReworkBanner.Visible = true;

                                    if (dtRWCdata.Rows.Count > 0)
                                    {
                                        if (dtRWCdata.Rows[0]["Rework_Details"] != null)
                                        {
                                            txtRWCdetails.Text = dtRWCdata.Rows[0]["Rework_Details"].ToString();


                                        }
                                        if (dtRWCdata.Rows[0]["Rework_Approx_Cost"] != null)
                                        {
                                            txtINR.Text = dtRWCdata.Rows[0]["Rework_Approx_Cost"].ToString();

                                        }
                                        if (dtdata.Rows[0]["PE_Remarks"] != null)
                                        {
                                            lblPERemarks.Text = dtdata.Rows[0]["PE_Remarks"].ToString();
                                        }

                                    }
                                    else
                                    {
                                        tblSWI.Visible = false;
                                        Div1.Visible = false;
                                        Div2.Visible = false;
                                        Div3.Visible = false;
                                        petblRemarks.Visible = false;
                                        tblReworkBanner.Visible = false;
                                    }
                                }

                            }

                        }

                        else if (stage.Equals("Pending with Assignee"))
                        {
                            if (dtdata.Rows[0]["Assignee_Email"] != null)
                            {
                                assigneeEmail = dtdata.Rows[0]["Assignee_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(assigneeEmail))
                                {
                                    dvFilledForm.Visible = true;
                                    mainFormLogic = true;
                                    dvAssignee.Visible = true;
                                    divPE.Visible = false;
                                    dvRCM.Visible = false;
                                    RCMSubmit.Visible = false;
                                    AssigneeSubmit.Visible = true;
                                    Div1.Visible = true;
                                    Div2.Visible = true;
                                    Div3.Visible = true;
                                    tblReworkBanner.Visible = true;
                                    if (dtRWCdata.Rows.Count > 0)
                                    {
                                        if (dtRWCdata.Rows[0]["Rework_Details"] != null)
                                        {
                                            txtRWCdetails.Text = dtRWCdata.Rows[0]["Rework_Details"].ToString();

                                        }
                                        if (dtRWCdata.Rows[0]["Rework_Approx_Cost"] != null)
                                        {
                                            txtINR.Text = dtRWCdata.Rows[0]["Rework_Approx_Cost"].ToString();

                                        }
                                        txtRWCdetails.Enabled = false;
                                        txtINR.Enabled = false;
                                    }
                                    else
                                    {
                                        tblSWI.Visible = false;
                                        Div1.Visible = false;
                                        Div2.Visible = false;
                                        Div3.Visible = false;
                                        petblpeRemarksforAssign.Visible = false;
                                        tblReworkBanner.Visible = false;
                                    }
                                    if (dtdata.Rows[0]["PE_Remarks"] != null)
                                    {
                                        lblPERemarksinAssign.Text = dtdata.Rows[0]["PE_Remarks"].ToString();
                                    }
                                    if (dtdata.Rows[0]["RCM_Remarks"] != null)
                                    {
                                        lblAssigneeRCMRemarks.Text = dtdata.Rows[0]["RCM_Remarks"].ToString();
                                    }
                                    if (dtdata.Rows[0]["Target_Closure_Date"] != null)
                                    {
                                        string dtTCD = dtdata.Rows[0]["Target_Closure_Date"].ToString();
                                        if (!string.IsNullOrEmpty(dtTCD))
                                        {
                                            DateTime dtDate = Convert.ToDateTime(dtTCD);
                                            lblTargetClosureDateAssRCM.Text = dtDate.ToString("dd/MM/yyyy");

                                        }

                                    }

                                }

                            }


                        }
                        else if (stage.Trim().Equals("Awaiting Closure"))
                        {
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                rcmEmail = dtdata.Rows[0]["RCM_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(rcmEmail))
                                {
                                    dvFilledForm.Visible = true;
                                    mainFormLogic = true;
                                    
                                    if (Convert.ToInt32(lblTransactionID.Text) > 43561)
                                    {
                                        dvRCMClose.Visible = false;
                                        LinkButton1.Visible = false;
                                        dvfqeclose.Visible = true;
                                        LinkButton2.Visible = true;
                                    }
                                    else
                                    {
                                        dvRCMClose.Visible = true;
                                        LinkButton1.Visible = true;
                                        dvfqeclose.Visible = false;
                                        LinkButton2.Visible = false;
                                    }
                                    Div1.Visible = true;
                                    Div2.Visible = true;
                                    Div3.Visible = true;
                                    GetAssigneeEvidenceDocument();
                                    

                                    assigneeClosureDateDATE.SelectedDate = DateTime.Today;
                                    dtClosuredate.Enabled = false;
                                    tblReworkBanner.Visible = true;
                                    if (dtdata.Rows[0]["Assignee_Remarks"] != null)
                                    {
                                        lblRCMAssigneeRemarks.Text = dtdata.Rows[0]["Assignee_Remarks"].ToString();
                                        lblRCMAssigneeRemarksfqe.Text = dtdata.Rows[0]["Assignee_Remarks"].ToString();
                                    }
                                    if (dtRWCdata.Rows.Count > 0)
                                    {
                                        if (dtRWCdata.Rows[0]["Rework_Details"] != null)
                                        {
                                            txtRWCdetails.Text = dtRWCdata.Rows[0]["Rework_Details"].ToString();
                                        }
                                        if (dtRWCdata.Rows[0]["Rework_Approx_Cost"] != null)
                                        {
                                            txtINR.Text = dtRWCdata.Rows[0]["Rework_Approx_Cost"].ToString();
                                        }
                                        if (dtRWCdata.Rows[0]["Rework_Quality"] != null)
                                        {
                                            txtRCM.Text = dtRWCdata.Rows[0]["Rework_Quality"].ToString();
                                        }
                                        if (dtRWCdata.Rows[0]["Rework_Actual_Cost"] != null)
                                        {
                                            txtRWCRCMCost.Text = dtRWCdata.Rows[0]["Rework_Actual_Cost"].ToString();
                                        }
                                        if (dtRWCdata.Rows[0]["CostBorneBy"] != null)
                                        {
                                            ddlborne.SelectedItem.Text = dtRWCdata.Rows[0]["CostBorneBy"].ToString();
                                        }
                                        txtRWCdetails.Enabled = false;
                                        txtINR.Enabled = false;
                                        txtRCM.Enabled = false;
                                        txtRWCRCMCost.Enabled = false;
                                        tblReworkBanner.Visible = false;
                                        ddlborne.Enabled = false;
                                    }
                                    GetAssigneeEvidenceDocument();

                                }

                            }
                        }
                        else if (stage.Equals("Non Compliant and Closed"))
                        {
                            dvFilledForm.Visible = true;
                            mainFormLogic = true;

                        }

                        else if (dtdata.Rows[0]["FQE_Email"] != null)
                        {
                            fqeEmail = dtdata.Rows[0]["FQE_Email"].ToString();
                            if (IsLoggedInAuthorisedUser(fqeEmail))
                            {
                                dvFilledForm.Visible = true;
                                dvRCM.Visible = false;
                                mainFormLogic = true;
                                dvAssignee.Visible = false;
                                RCMSubmit.Visible = false;
                                tblSWI.Visible = false;

                            }

                        }
                        else
                        {
                            lblUnAuth.Visible = true;


                        }

                        if (mainFormLogic)
                        {
                            lblTransID.Text = lblTransactionID.Text;

                            if (dtdata.Rows[0]["BUName"] != null)
                            {
                                lblBU.Text = dtdata.Rows[0]["BUName"].ToString();
                            }
                            if (dtdata.Rows[0]["SBGName"] != null)
                            {
                                lblSBG.Text = dtdata.Rows[0]["SBGName"].ToString();
                            }
                            if (dtdata.Rows[0]["SBUName"] != null)
                            {
                                lblSBU.Text = dtdata.Rows[0]["SBUName"].ToString();
                                lblNewSBU.Text = dtdata.Rows[0]["SBUName"].ToString();
                                if (lblNewSBU.Text == "Industrial Infrastructure")
                                {
                                    lblInduCoE.Text = "coeqii@tataprojects.com";
                                }
                                else if (lblNewSBU.Text == "Urban Infrastructure")
                                {
                                    lblInduCoE.Text = "coeqii@tataprojects.com";
                                }
                                else
                                {
                                    lblInduCoE.Text = "coeqii@tataprojects.com";
                                }
                            }
                            if (dtdata.Rows[0]["ProjectName"] != null)
                            {
                                lblProject.Text = dtdata.Rows[0]["ProjectName"].ToString();
                                lblProjectLocation.Text = dtdata.Rows[0]["ProjectName"].ToString();
                            }
                            if (dtdata.Rows[0]["ProjectCode"] != null)
                            {
                                lblTplProjectCode.Text = dtdata.Rows[0]["ProjectCode"].ToString();
                            }

                            if (dtdata.Rows[0]["Location"] != null)
                            {
                                lblLocation.Text = dtdata.Rows[0]["Location"].ToString();
                            }
                            if (dtdata.Rows[0]["SubLocation"] != null)
                            {
                                lblSubLocation.Text = dtdata.Rows[0]["SubLocation"].ToString();
                            }
                            if (dtdata.Rows[0]["ProjectType"] != null)
                            {
                                lblProjectType.Text = dtdata.Rows[0]["ProjectType"].ToString();
                            }
                            if (dtdata.Rows[0]["Discipline"] != null)
                            {
                                lblDiscipline.Text = dtdata.Rows[0]["Discipline"].ToString();
                            }
                            if (dtdata.Rows[0]["MainActivity"] != null)
                            {
                                lblMainactivity.Text = dtdata.Rows[0]["MainActivity"].ToString();
                            }
                            if (dtdata.Rows[0]["Activity"] != null)
                            {
                                lblActivity.Text = dtdata.Rows[0]["Activity"].ToString();
                            }
                            if (dtdata.Rows[0]["Sub Activity"] != null)
                            {
                                lblSubActivity.Text = dtdata.Rows[0]["Sub Activity"].ToString();
                            }
                            if (dtdata.Rows[0]["MainDefectCategory"] != null)
                            {
                                lblDefectCategory.Text = dtdata.Rows[0]["MainDefectCategory"].ToString();
                            }
                            if (dtdata.Rows[0]["SubDefectCategory"] != null)
                            {
                                lblSubDefectCategory.Text = dtdata.Rows[0]["SubDefectCategory"].ToString();
                            }
                            if (dtdata.Rows[0]["RootCauseCategory"] != null)
                            {
                                lblRootCauseCategory.Text = dtdata.Rows[0]["RootCauseCategory"].ToString();
                            }
                            if (dtdata.Rows[0]["RootCause"] != null)
                            {
                                lblRootCause.Text = dtdata.Rows[0]["RootCause"].ToString();
                            }
                            if (dtdata.Rows[0]["Why1"] != null)
                            {
                                lblWhy1.Text = dtdata.Rows[0]["Why1"].ToString();
                            }
                            if (dtdata.Rows[0]["Why2"] != null)
                            {
                                lblWhy2.Text = dtdata.Rows[0]["Why2"].ToString();
                            }
                            if (dtdata.Rows[0]["Why3"] != null)
                            {
                                lblWhy3.Text = dtdata.Rows[0]["Why3"].ToString();
                            }
                            if (dtdata.Rows[0]["Why4"] != null)
                            {
                                lblWhy4.Text = dtdata.Rows[0]["Why4"].ToString();
                            }
                            if (dtdata.Rows[0]["Why5"] != null)
                            {
                                lblWhy5.Text = dtdata.Rows[0]["Why5"].ToString();
                            }
                            if (dtdata.Rows[0]["Identification"] != null)
                            {
                                lblidentify.Text = dtdata.Rows[0]["Identification"].ToString();
                            }
                            if (dtdata.Rows[0]["SWIIssuedBy"] != null)
                            {
                                lblissuedby.Text = dtdata.Rows[0]["SWIIssuedBy"].ToString();
                            }
                            if (dtdata.Rows[0]["FQE_ID"] != null)
                            {
                                lblFQEID.Text = dtdata.Rows[0]["FQE_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["FQE_Name"] != null)
                            {
                                lblFQEName.Text = dtdata.Rows[0]["FQE_Name"].ToString();
                            }

                            if (dtdata.Rows[0]["FQE_Email"] != null)
                            {
                                lblFQEEmail.Text = dtdata.Rows[0]["FQE_Email"].ToString();
                            }

                            if (dtdata.Rows[0]["RCM_ID"] != null)
                            {
                                lblRCMID.Text = dtdata.Rows[0]["RCM_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Name"] != null)
                            {
                                lblRCMName.Text = dtdata.Rows[0]["RCM_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["RCM_Email"] != null)
                            {
                                lblRCMEmail.Text = dtdata.Rows[0]["RCM_Email"].ToString();
                            }

                            if (dtdata.Rows[0]["PM_ID"] != null)
                            {
                                lblPMID.Text = dtdata.Rows[0]["PM_ID"].ToString();
                            }
                            if (dtdata.Rows[0]["PM_Name"] != null)
                            {
                                lblPMName.Text = dtdata.Rows[0]["PM_Name"].ToString();
                            }
                            if (dtdata.Rows[0]["PM_Email"] != null)
                            {
                                lblPMEmail.Text = dtdata.Rows[0]["PM_Email"].ToString();
                            }
                            if (dtdata.Rows[0]["Assignee_Submitted_Date"] != null)
                            {
                                string dtAssSubDate = dtdata.Rows[0]["Assignee_Submitted_Date"].ToString();
                                if (!string.IsNullOrEmpty(dtAssSubDate))
                                {
                                    DateTime dtDate = Convert.ToDateTime(dtAssSubDate);
                                    lblAssigneeActiondate.Text = dtDate.ToString("dd/MM/yyyy");
                                    lblAssigneeActiondatefqe.Text = dtDate.ToString("dd/MM/yyyy");
                                }

                            }


                            DateTime dtNow = DateTime.Now;
                            dtClosuredate.SelectedDate = dtNow;


                            if (dtdata.Rows[0]["Assignee_Email"] != null)
                            {
                                lblAssigneEmail.Text = dtdata.Rows[0]["Assignee_Email"].ToString();

                            }
                            if (dtdata.Rows[0]["Target_Closure_Date"] != null)
                            {
                                string dtTCD = dtdata.Rows[0]["Target_Closure_Date"].ToString();
                                if (!string.IsNullOrEmpty(dtTCD))
                                {
                                    DateTime dtDate = Convert.ToDateTime(dtTCD);
                                    lblRWCDate.Text = dtDate.ToString("dd/MM/yyyy");

                                }

                            }

                            //GeteEvidenceDocument();
                            DataTable dtChildData = objQuality.GetComplaincesDatabyTransactionIDforFQP(lblTransactionID.Text);
                            if (dtChildData != null)
                            {
                                grdCompliances.DataSource = dtChildData;
                                grdCompliances.DataBind();
                            }
                            if (dtdata.Rows[0]["isCompliant"] != null)
                            {
                                if (dtdata.Rows[0]["isCompliant"].ToString().Trim() == "No")
                                {
                                    dvuploadNCR.Visible = true;
                                    DataTable dtNCRData = objQuality.GetNCRUploadDatabyTransactionIDforFQP(lblTransactionID.Text);
                                    if (dtNCRData != null)
                                    {
                                        GridView1.DataSource = dtNCRData;
                                        GridView1.DataBind();
                                    }
                                }
                            }
                        }
                        if (rwcFormLogic)
                        {
                            if (dtRWCdata.Rows.Count > 0)
                            {
                                if (dtRWCdata.Rows[0]["RWCID"] != null)
                                {
                                    lblRWCNO.Text = dtRWCdata.Rows[0]["RWCID"].ToString();

                                }

                                if (dtRWCdata.Rows[0]["RWC_Date"] != null)
                                {
                                    string dtTCD = dtRWCdata.Rows[0]["RWC_Date"].ToString();
                                    if (!string.IsNullOrEmpty(dtTCD))
                                    {
                                        DateTime dtDate = Convert.ToDateTime(dtTCD);
                                        lblRWCDate.Text = dtDate.ToString("dd/MM/yyyy");

                                    }

                                }
                                if (dtRWCdata.Rows[0]["Subcontractor"] != null)
                                {
                                    lblSubContract.Text = dtRWCdata.Rows[0]["Subcontractor"].ToString();

                                }
                                if (dtRWCdata.Rows[0]["PO_No"] != null)
                                {
                                    lblPoWo.Text = dtRWCdata.Rows[0]["PO_No"].ToString();

                                }
                                if (dtRWCdata.Rows[0]["Package"] != null)
                                {
                                    lblPackage.Text = dtRWCdata.Rows[0]["Package"].ToString();

                                }
                                if (dtRWCdata.Rows[0]["NCRFINCCR"] != null)
                                {
                                    lblReworkDue.Text = dtRWCdata.Rows[0]["NCRFINCCR"].ToString();

                                }
                                if (dtRWCdata.Rows[0]["NC_RootCause"] != null)
                                {
                                    string s = dtRWCdata.Rows[0]["NC_RootCause"].ToString();
                                    lblNC_RootCause.Text = s.Replace(",", "</br>");

                                }
                                if (dtRWCdata.Rows[0]["NC_Details"] != null)
                                {
                                    txtNCDetails.Text = dtRWCdata.Rows[0]["NC_Details"].ToString();
                                    txtNCDetails.Enabled = false;

                                }

                            }
                            else
                            {
                                tblSWI.Visible = false;
                                Div1.Visible = false;
                                Div2.Visible = false;
                                Div3.Visible = false;
                                petblRemarks.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        dvUnAuth.Visible = true;
                        dvFilledForm.Visible = true;
                        dvRCM.Visible = false;
                        RCMSubmit.Visible = false;
                        dvAssignee.Visible = false;
                        dvRCMClose.Visible = false;
                        tblSWI.Visible = false;
                    }

                }

            }
        }


        private bool IsLoggedInAuthorisedUser(string email)
        {
            bool isValid = SPContext.Current.Web.CurrentUser.Email.Equals(email);
            return isValid;
        }

        protected void lnkView_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            LinkButton btn = sender as LinkButton;
            string docName = btn.Text;

            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select Supp_DocName,Supp_DocContentType,Supp_Document from QChecklist_TnxChild where Transaction_ID='" + lblTransactionID.Text + "'and Supp_DocName='" + docName + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["Supp_DocName"].ToString();
                    contentType = sdr["Supp_DocContentType"].ToString();
                    if (Convert.ToInt32(lblTransactionID.Text) > 31467)
                    {
                        bytes = Decompress((byte[])sdr["Supp_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Supp_Document"];
                    }
                    // lnkView.Text = fileName;


                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }


            catch (Exception ex)
            {

            }

        }

        protected void lnkView2_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            LinkButton btn = sender as LinkButton;
            string docName = btn.Text;

            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select Evi_DocName,Evi_DocContentType,Evi_Document from QChecklist_TnxChild where Transaction_ID='" + lblTransactionID.Text + "'and Evi_DocName='" + docName + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["Evi_DocName"].ToString();
                    contentType = sdr["Evi_DocContentType"].ToString();
                    if (Convert.ToInt32(lblTransactionID.Text) > 31467)
                    {
                        bytes = Decompress((byte[])sdr["Evi_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Evi_Document"];
                    }
                    // lnkView.Text = fileName;


                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }


            catch (Exception ex)
            {

            }

        }

        private bool UpdatePEActions()
        {
            bool isSaved = false;
            DataTable dtPE = new DataTable();

            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                DateTime today = DateTime.Now;
                string todayStr = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                String sqlQuery = "update QChecklist_TnxMain set Stage='Pending with RCM', PE_Remarks='" + txtPERemarks.Text.Replace("'", "''") + "',PE_Submitted_Date='" + todayStr + "' where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtPE);
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {

            }


            return isSaved;

        }

        private bool UpdatePEActionsRWC()
        {
            bool isSaved = false;
            //bool none = false;

            DataTable dtPE = new DataTable();
            //int value;
            //int.TryParse(txtINR.Text, out value);

            //string rwcDetails = txtRWCdetails.Text;
            //if ((!String.IsNullOrEmpty(rwcDetails)) && value !=)
            //{
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                String sqlQuery = "update FQP_Rework set Rework_Details='" + txtRWCdetails.Text.Replace("'", "''") + "',Rework_Approx_Cost='" + txtINR.Text + "' where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtPE);
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {

            }
            // }
            //else
            //{
            //    string message = "You must specify a value for Rework Details and Approximate cost.";
            //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //    sb.Append("<script type = 'text/javascript'>");
            //    sb.Append("window.onload=function(){");
            //    sb.Append("alert('");
            //    sb.Append(message);
            //    sb.Append("')};");
            //    sb.Append("</script>");
            //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
            //    return none;

            //}




            return isSaved;

        }


        protected void btnPEActions_Click(object sender, EventArgs e)
        {
            bool isSaved = UpdatePEActions();
            DataTable dtRWCdata = objQuality.GetRWCbyTransactionID(lblTransactionID.Text);
            if (dtRWCdata.Rows.Count > 0)
            {
                bool isSavedRWC = UpdatePEActionsRWC();

            }
            if (isSaved)
            {
                int value;
                int.TryParse(txtINR.Text, out value);
                if (value > 500000)
                {
                    DataTable dtdata = objQuality.GetChecklistDatabyTransactionIDForFQP(lblTransactionID.Text);
                    string hopEmail = string.Empty;
                    if (dtdata.Rows[0]["HOP_Email"] != null)
                    {
                        hopEmail = dtdata.Rows[0]["HOP_Email"].ToString();
                        if (IsLoggedInAuthorisedUser(hopEmail))
                        {
                            objQuality.SendPEtoRCMMailMoreThanFive(hopEmail, lblRCMName.Text, lblAssigneEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text, hopEmail);

                        }
                    }
                }
                else
                {
                    objQuality.SendPEtoRCMMail(lblRCMEmail.Text, lblPERCMName.Text, lblPEEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);

                }

                btnPEActions.Enabled = false;
                lblRCMClosureSubmitNotification.Text = "Submitted successfully";
                string navigateUrl = Constants.link + "Pages/FQPPendingItems.aspx";
                lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
            }



        }

        private bool UpdateRCMActions()
        {
            bool isSaved = false;
            DataTable dtDiscipline = new DataTable();
            DateTime dtSelected = dtTargetClosureDate.SelectedDate;
            try
            {
                PickerEntity pckEntity = (PickerEntity)ppAssigneeName.ResolvedEntities[0];
                if (pckEntity != null)
                {
                    try
                    {
                        string assignee = pckEntity.Key;
                        SPUser userAssignee = SPContext.Current.Web.EnsureUser(assignee);
                        if (userAssignee != null)
                        {
                            string assigneeID = objQuality.GetOnlyEmployeeID(userAssignee.LoginName);
                            string assigneeName = userAssignee.Name;
                            string assigneeEmail = userAssignee.Email;
                            lblAssigneEmail.Text = userAssignee.Email;
                            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                            DateTime today = DateTime.Now;
                            string todayStr = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                            String sqlQuery = "update QChecklist_TnxMain set Stage='Pending with Assignee', Assignee_ID='" + assigneeID + "',Assignee_Name='" + assigneeName + "',Assignee_Email='" + assigneeEmail + "',Target_Closure_Date='" + dtSelected.Date.ToString("yyyy-MM-dd HH:mm:ss") + "',RCM_Remarks='" + txtRCMRemarks.Text.Replace("'", "''") + "',[RCM_Submitted Date]='" + todayStr + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                            SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                            daAdap.Fill(dtDiscipline);
                            isSaved = true;
                            con.Close();
                        }
                    }
                    catch (Exception exp)
                    {

                    }

                }
            }
            catch (Exception ex)
            {

            }
            return isSaved;

        }

        private bool UpdateReworkDetails()
        {
            bool isSaved = false;
            bool none = false;

            int value;
            int.TryParse(txtINR.Text, out value);
            if (value != 0)
            {
                try
                {
                    DataTable dtDiscipline = new DataTable();
                    SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                    string sqlQuery = "update FQP_Rework set Rework_Details='" + txtRWCdetails.Text + "',Rework_Approx_Cost='" + txtINR.Text + "',Rework_Quality='" + txtRCM.Text.Replace("'", "''") + "',Rework_Actual_Cost='" + txtRWCRCMCost.Text + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                    SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                    daAdap.Fill(dtDiscipline);
                    isSaved = true;
                    con.Close();
                }
                catch (Exception exp)
                {
                    //lblError.Text = exp.Message;
                }
            }
            else
            {
                string message = "You must specify a value for Approximate cost of Rework ";
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<script type = 'text/javascript'>");
                sb.Append("window.onload=function(){");
                sb.Append("alert('");
                sb.Append(message);
                sb.Append("')};");
                sb.Append("</script>");
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                return none;

            }
            return isSaved;

        }

        protected void btnRCMActions_Click(object sender, EventArgs e)
        {
            bool isSaved = UpdateRCMActions();
            DataTable dtRWCdata = objQuality.GetRWCbyTransactionID(lblTransactionID.Text);

            if (dtRWCdata.Rows.Count > 0)
            {
                bool isReworkSaved = UpdateReworkDetails();

            }
            if (isSaved)
            {

                objQuality.SendAssigneeMailforFQP(lblRCMEmail.Text, lblRCMName.Text, lblAssigneEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);

                btnRCMActions.Enabled = false;

                lblRCMClosureSubmitNotification.Text = "Submitted successfully";
                string navigateUrl = Constants.link + "Pages/FQPPendingItems.aspx";
                lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
            }


        }

        private bool UpdateAssigeeActions()
        {
            bool isSaved = false;
            try
            {
                DataTable dtDiscipline = new DataTable();
                DateTime dt = assigneeClosureDateDATE.SelectedDate;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                DateTime today = DateTime.Now;
                string todayStr = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                string sqlQuery = "update QChecklist_TnxMain set Stage='Awaiting Closure',Assignee_Action_Date='" + dt.Date.ToString("yyyy-MM-dd HH:mm:ss") + "',Assignee_Submitted_Date='" + todayStr + "',Assignee_Remarks='" + assigneeRemarksTXT.Text.Replace("'", "''") + "',Assignee_SWI_Remarks='" + assigneeRemarksTXT.Text.Replace("'", "''") + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
                //lblError.Text = exp.Message;
            }
            return isSaved;

        }

        private bool InsertRWCAssigeeActions(string borneby)
        {
            bool isSaved = false;
            bool none = false;

            //int value;
            //int.TryParse(txtRWCRCMCost.Text, out value);

            //string rcmQuaRWCDetails = txtRCM.Text;

            //if ((!String.IsNullOrEmpty(rcmQuaRWCDetails)) && value!=0)
            //{

            try
            {
                DataTable dtDiscipline = new DataTable();
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "update FQP_Rework set Rework_Details='" + txtRWCdetails.Text.Replace("'", "''") + "', Rework_Approx_Cost='" + txtINR.Text + "',Rework_Quality='" + txtRCM.Text.Replace("'", "''") + "',Rework_Actual_Cost='" + txtRWCRCMCost.Text + "', CostBorneBy='" + borneby + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                isSaved = true;
                con.Close();
            }
            catch (Exception exp)
            {
                //lblError.Text = exp.Message;
            }
            //  }
            //else
            //{
            //    string message = "You must specify a value for Quality of Rework Details and Approximate cost.";
            //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //    sb.Append("<script type = 'text/javascript'>");
            //    sb.Append("window.onload=function(){");
            //    sb.Append("alert('");
            //    sb.Append(message);
            //    sb.Append("')};");
            //    sb.Append("</script>");
            //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
            //    return none;

            //}
            return isSaved;

        }

        protected void upLoadEvidenceDoc_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select FQE_Ev_DocName,FQE_Ev_DocContentType,FQE_Ev_Doc from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["FQE_Ev_DocName"].ToString();
                    if (Convert.ToInt32(lblTransactionID.Text) > 31467)
                    {
                        bytes = Decompress((byte[])sdr["FQE_Ev_Doc"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["FQE_Ev_Doc"];
                    }
                    contentType = sdr["FQE_Ev_DocContentType"].ToString();
                    upLoadEvidenceDoc.Text = fileName;

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select DocName,DocContentType,Evidence_Document from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["DocName"].ToString();
                    DateTime lastUpdateDate = LastDate();
                    DateTime dt2 = DateTime.Parse("2017-12-27 18:05:00.000");
                    if (lastUpdateDate > dt2)
                    {
                        bytes = Decompress((byte[])sdr["Evidence_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Evidence_Document"];
                    }
                    contentType = sdr["DocContentType"].ToString();
                    LinkButton1.Text = fileName;

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnAssigneeSubmit_Click(object sender, EventArgs e)
        {
            int cost = 0;
            DataTable dtRWCdata = objQuality.GetRWCbyTransactionID(lblTransactionID.Text);
            if (dtRWCdata.Rows.Count > 0)
            {
                cost = Convert.ToInt32(txtRWCRCMCost.Text);
            }
            if ((cost > 0 && ddlborne.SelectedIndex > 0) || (cost==0))
            {
                if (assigneeEvidenceFUP.HasFile)
                {
                    bool isSaved = UpdateAssigeeActions();
                    EvidenceDocument();
                    string costborne = string.Empty;
                    if (cost>0)
                    {
                        costborne = ddlborne.SelectedItem.Text; ;
                    }
                    if (dtRWCdata.Rows.Count > 0)
                    {
                        bool isReworkSaved = InsertRWCAssigeeActions(costborne);
                    }
                    if (isSaved)
                    {
                        int value;
                        int.TryParse(txtINR.Text, out value);
                        if (value > 500000)
                        {
                            DataTable dtdata = objQuality.GetChecklistDatabyTransactionIDForFQP(lblTransactionID.Text);
                            string hopEmail = string.Empty;
                            if (dtdata.Rows[0]["HOP_Email"] != null)
                            {
                                hopEmail = dtdata.Rows[0]["HOP_Email"].ToString();
                                if (IsLoggedInAuthorisedUser(hopEmail))
                                {
                                    objQuality.SendRCMForClosureMailforFQPMoreThanFive(hopEmail, lblAssigneEmail.Text, lblRCMName.Text, lblRCMEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);

                                }
                            }
                        }
                        else
                        {
                            if (dtRWCdata.Rows.Count > 0)
                            {
                                objQuality.SendRCMForClosureMailforFQP(lblAssigneEmail.Text, lblRCMName.Text, lblRCMEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                            }
                            else
                            {
                                objQuality.SendRCMForClosureMailforFQPNoRWC(lblAssigneEmail.Text, lblRCMName.Text, lblRCMEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                            }

                        }


                        btnAssigneeSubmit.Visible = false;
                        assigneeEvidenceFUP.Enabled = false;
                        lblRCMSubNotification.Text = "Submitted successfully";
                        string navigateUrl = Constants.link + "Pages/FQPPendingItems.aspx";
                        lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);

                    }
                }
                else
                {
                    lblError.Text = "Document upload is mandatory";
                }
            }
            else
            {
                lblError.Text = "Please Select Cost borne by";
            }
        }

        private void InsertRWCRCMClosureActions()
        {
            try
            {
                DataTable dtDiscipline = new DataTable();
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "update FQP_Rework set Rework_Quality='" + txtRCM.Text.Replace("'", "''") + "',Rework_Actual_Cost='" + txtRWCRCMCost.Text + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                con.Close();
            }
            catch (Exception exp)
            {
                //lblError.Text = exp.Message;
            }

        }

        protected void btnClosure_Click(object sender, EventArgs e)
        {
            try
            {
                InsertRWCRCMClosureActions();
                DataTable dtDiscipline = new DataTable();
                DateTime dt = dtClosuredate.SelectedDate;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                DateTime today = DateTime.Now;
                string todayStr = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                string sqlQuery = "update QChecklist_TnxMain set Stage='Non Compliant and Closed',Closure_Date='" + dt.Date.ToString("yyyy-MM-dd HH:mm:ss") + "',Actual_Closed_Date='" + todayStr + "',RCM_Closure_Remarks='" + txtverifi_Remarks.Text.Replace("'", "''") + "' where Transaction_ID='" + lblTransactionID.Text + "'";

                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtDiscipline);
                con.Close();
                btnClosure.Enabled = false;

                objQuality.SendFinalClosureMailforFQP(lblAssigneEmail.Text, lblRCMEmail.Text, lblRCMName.Text, lblFQEEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);

                lblAssigneeClosure.Text = "Submitted successfully";
                string navigateUrl = Constants.link + "Pages/FQPPendingItems.aspx";
                lblRCMClosureSubmitNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
            }
            catch (Exception exp)
            {
                //lblError.Text = exp.Message;
            }
        }

        protected void chkClosureYes_CheckedChanged(object sender, EventArgs e)
        {
            if (chkClosureYes.Checked)
            {
                btnClosure.Enabled = true;
            }
            else
            {
                btnClosure.Enabled = false;
            }

        }

        private void EvidenceDocument()
        {

            string filePath = assigneeEvidenceFUP.PostedFile.FileName;
            string filename = Path.GetFileName(filePath);
            string ext = Path.GetExtension(filename);
            string contenttype = String.Empty;

            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
                case ".PDF":
                    contenttype = "application/pdf";
                    break;
            }
            if (contenttype != String.Empty)
            {

                Stream fs = assigneeEvidenceFUP.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                byte[] compressedData = Compress(bytes);
                //Update the file into database
                string strQuery = "UPDATE QChecklist_TnxMain SET Evidence_Document = @Evidence_Document, DocContentType = @DocContentType, DocName = @DocName where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@DocName", SqlDbType.VarChar).Value = filename;
                cmd.Parameters.Add("@DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@Evidence_Document", SqlDbType.Binary).Value = compressedData;
                UpdateData(cmd);
                // lblMessage.ForeColor = System.Drawing.Color.Green;
                // lblMessage.Text = "File Uploaded Successfully";
            }
            else
            {
                // lblMessage.ForeColor = System.Drawing.Color.Red;
                // lblMessage.Text = "File format not recognised." + " Upload Image/Word/PDF/Excel formats";
            }
        }

        private Boolean UpdateData(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        private void GetAssigneeEvidenceDocument()
        {
            try
            {
                string fileName = string.Empty;
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);

                SqlCommand cmd = new SqlCommand("select DocName from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                cmd.Connection = con;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (dr["DocName"] != null)
                        {
                            LinkButton1.Text = dr["DocName"].ToString();
                        }

                    }
                }
                else
                {
                    LinkButton1.Text = "File not uploaded";
                }
                con.Close();
            }
            catch (Exception exp)
            {

            }

        }

        private void ShowBasicDialog(string Url, string Title)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"<script type=""text/ecmascript"">");
            sb.AppendLine(@"ExecuteOrDelayUntilScriptLoaded(openBasicServerDialog, ""sp.js"");");
            sb.AppendLine(@"    function openBasicServerDialog()");
            sb.AppendLine(@"    {");
            sb.AppendLine(@"        var options = {");
            sb.AppendLine(string.Format(@"            url: '{0}',", Url));
            sb.AppendLine(string.Format(@"            title: '{0}'", Title));
            sb.AppendLine(@"        };");
            sb.AppendLine(@"        SP.UI.ModalDialog.showModalDialog(options);");
            sb.AppendLine(@"    }");
            sb.AppendLine(@"</script>");
            ltScriptLoader.Text = sb.ToString();

        }
        protected void lnkViewNCR_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            LinkButton btn = sender as LinkButton;
            string docName = btn.Text;

            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select NCR_DocName,NCR_DocContentType,NCR_Document from Qchecklist_NCRdoc where Transaction_ID='" + lblTransactionID.Text + "' and NCR_DocName='" + docName + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["NCR_DocName"].ToString();
                    contentType = sdr["NCR_DocContentType"].ToString();
                    if (Convert.ToInt32(lblTransactionID.Text) > 31459)
                    {
                        bytes = Decompress((byte[])sdr["NCR_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["NCR_Document"];
                    }
                    // lnkView.Text = fileName;


                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }


            catch (Exception ex)
            {

            }

        }
        private byte[] Compress(byte[] data)
        {
            var output = new MemoryStream();
            using (var gzip = new GZipStream(output, CompressionMode.Compress, true))
            {
                gzip.Write(data, 0, data.Length);
                gzip.Close();
            }
            return output.ToArray();
        }
        private byte[] Decompress(byte[] data)
        {
            var output = new MemoryStream();
            var input = new MemoryStream();
            input.Write(data, 0, data.Length);
            input.Position = 0;
            using (var gzip = new GZipStream(input, CompressionMode.Decompress, true))
            {
                var buff = new byte[64];
                var read = gzip.Read(buff, 0, buff.Length);
                while (read > 0)
                {
                    output.Write(buff, 0, read);
                    read = gzip.Read(buff, 0, buff.Length);
                }
                gzip.Close();
            }
            return output.ToArray();
        }
        public DateTime LastDate()
        {
            string date = string.Empty;
            DataTable dt = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "select Assignee_Submitted_Date from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
                if (dt != null)
                {
                    date = dt.Rows[0]["Assignee_Submitted_Date"].ToString();
                }
            }
            catch (Exception exp)
            {
            }
            return Convert.ToDateTime(date);
        }

        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Text = e.Row.Cells[1].Text.Replace("&amp;", "&");
                //var txt = e.Row.FindControl("txtFQECompRemarks") as TextBox;
                //txt.Text.Replace("&amp;", "&");

            }
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] bytes;
                string fileName, contentType;

                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                con.Open();

                SqlCommand cmd = new SqlCommand("select DocName,DocContentType,Evidence_Document from QChecklist_TnxMain where Transaction_ID='" + lblTransactionID.Text + "'", con);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    fileName = sdr["DocName"].ToString();
                    DateTime lastUpdateDate = LastDate();
                    DateTime dt2 = DateTime.Parse("2017-12-27 18:05:00.000");
                    if (lastUpdateDate > dt2)
                    {
                        bytes = Decompress((byte[])sdr["Evidence_Document"]);
                    }
                    else
                    {
                        bytes = (byte[])sdr["Evidence_Document"];
                    }
                    contentType = sdr["DocContentType"].ToString();
                    LinkButton2.Text = fileName;

                }
                con.Close();
                Context.Response.Buffer = true;
                Context.Response.Charset = "";
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Context.Response.ContentType = "application/docx";
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnclosurefqe_Click(object sender, EventArgs e)
        {
            
            try
            {
                InsertRWCRCMClosureActions();
                DataTable dtfqeclosure = new DataTable();
                DateTime dt = dtClosuredatefqe.SelectedDate;
                SqlConnection con = null;
                DateTime today = DateTime.Now;
                string todayStr = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                string sqlQuery = string.Empty;
                if (ddlclosureverify.SelectedItem.Text == "Accept")
                {
                    using (con = new SqlConnection(Constants.ConnectionStringQMDChecklists))
                    {
                        sqlQuery = "update QChecklist_TnxMain set Stage='Non Compliant and Closed',Closure_Date='" + dt.Date.ToString("yyyy-MM-dd HH:mm:ss") + "',Actual_Closed_Date='" + todayStr + "',RCM_Closure_Remarks='" + txtverifi_Remarks.Text.Replace("'", "''") + "' where Transaction_ID='" + lblTransactionID.Text + "'";
                        SqlCommand cmd = new SqlCommand(sqlQuery, con);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                else
                {
                    using (con = new SqlConnection(Constants.ConnectionStringQMDChecklists))
                    {
                        sqlQuery = "update QChecklist_TnxMain set Stage='Pending with RCM',RCM_Remarks='',[RCM_Submitted Date]=@RCMSubmittedDate,Assignee_Action_Date=@AssigneeActionDate,Assignee_Remarks='',Assignee_SWI_Remarks='',Assignee_Submitted_Date=@AssigneeSubmittedDate,RCM_Closure_Remarks='" + txtverifi_Remarksfqe.Text.Replace("'", "''") + "' where Transaction_ID='" + lblTransactionID.Text + "'";
                        SqlCommand cmd = new SqlCommand(sqlQuery, con);
                        cmd.Parameters.Add("@RCMSubmittedDate", SqlDbType.DateTime).Value = DBNull.Value;
                        cmd.Parameters.Add("@AssigneeActionDate", SqlDbType.DateTime).Value = DBNull.Value;
                        cmd.Parameters.Add("@AssigneeSubmittedDate", SqlDbType.DateTime).Value = DBNull.Value;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                    }
                }
                btnclosurefqe.Enabled = false;
                //lblFQEEmail.Text
                if (ddlclosureverify.SelectedItem.Text == "Accept")
                {
                    objQuality.SendFinalClosureMailforFQP(lblAssigneEmail.Text, lblRCMEmail.Text, lblRCMName.Text, lblFQEEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                }
                else
                {
                    objQuality.SendBacktoRCM(lblRCMName.Text, lblRCMEmail.Text, lblFQEName.Text, lblFQEEmail.Text, lblProject.Text, lblActivity.Text, lblSubActivity.Text, lblLocation.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                }
                
                lblAssigneeClosure.Text = "Submitted successfully";
                string navigateUrl = Constants.link + "Pages/FQPPendingItems.aspx";
                lblRCMClosureSubmitNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to view other items in your list", navigateUrl);
            }
            catch (Exception exp)
            {
                //lblError.Text = exp.Message;
            }
        }
    }
}
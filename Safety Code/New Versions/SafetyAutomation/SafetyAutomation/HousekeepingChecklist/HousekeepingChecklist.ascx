﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HousekeepingChecklist.ascx.cs" Inherits="SafetyAutomation.HousekeepingChecklist.HousekeepingChecklist" %>

<style type="text/css">
    .table {
        padding: 0;
        border-spacing: 0;
        text-align: center;
        font-family:'Trebuchet MS';
        font-size: 10pt;
        vertical-align: middle;
        border-collapse: collapse;
    }

    .auto-style1 {
        height: 30px;
    }

    .required {
        color: Red;
    }

    .auto-style2 {
        width: 60%;
        height: 30px;
    }
</style>

<div id="dvMainForm" runat="server" visible="false">

     <asp:Panel ID="PanelMain" runat="server">
        <div id="dvMainFormChecklist" runat="server" visible="false">
            <br />
            <table border="1" cellpadding="1" cellspacing="1" width="100%">
                <tr>
                    <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                        <asp:Label ID="Label5" runat="server" ForeColor="White" Text="Upload Safety Checklists"  Font-Size="Large"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right" >
                        <asp:Button Font-Size="Medium" ID="btnhome" runat="server" BackColor="#507CD1" ForeColor="White" Text="Home"  OnClick="btnhome_Click" Width="61px" />
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                        <asp:Label ID="Label21" ForeColor="White" runat="server" Text="Frequency" ></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <asp:Label ID="lblfrequency"  runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                        <asp:Label ID="Label23" ForeColor="White" runat="server" Text="Checklist" ></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                        <asp:Label ID="lblchecklist" runat="server" Text="" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                        <asp:Label ID="Label1" ForeColor="White" runat="server" Text="BU" ></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <asp:Label ID="lblBU" runat="server" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                        <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Project" ></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                        <asp:DropDownList ID="ddlProjectCode" runat="server" OnSelectedIndexChanged="ddlProjectCode_SelectedIndexChanged" Width="100%" AutoPostBack="true">
                        </asp:DropDownList>
                        <%--<asp:RequiredFieldValidator ID="RFProject" runat="server" ErrorMessage="Please Select Project" InitialValue="--Select--" ControlToValidate="ddlProjectCode" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                        <%-- <asp:Label ID="lblProject" runat="server"></asp:Label>--%></td>
                </tr>
                <tr>
                    <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                        <asp:Label ID="Label22" ForeColor="White" runat="server" Text="Inspection Date" ></asp:Label>
                    </td>
                    <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                        <SharePoint:DateTimeControl ID="DateTimeControl1" DateOnly="true" runat="server" />
                    </td>
                </tr>
            </table>
            <asp:GridView ID="grdCompliances" runat="server" CellPadding="4" AutoGenerateColumns="false" Width="100%" ForeColor="#333333" GridLines="Both">
                    <Columns>
                        <asp:BoundField DataField="Location" HeaderText="Location" />
                        <asp:TemplateField HeaderText="Is Housekeeping Proper?">
                            <ItemTemplate>
                                <asp:RadioButton ID="rdbYes" Text="Yes" runat="server" GroupName="GC" ValidationGroup="GC"/><br />
                                <asp:RadioButton ID="rdbNo" Text="No" GroupName="GC" ValidationGroup="GC" runat="server" /><br />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Photo evidence(with date & time)">
                            <ItemTemplate>
                                <asp:FileUpload ID="uploadEvidence" runat="server" Width="95%" />
                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.png|.jpg|.gif|.xls|.xlsx)$"
                                    ControlToValidate="uploadEvidenFUP" runat="server" ForeColor="Red" ErrorMessage="Please select a valid word,excel,jpg,png,gif or pdf file format."
                                    Display="Dynamic" />--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks(If No selected)">
                            <ItemTemplate>
                                <asp:TextBox ID="txtSORemarks" TextMode="MultiLine" runat="server" Width="95%"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Corrective Action photo evidence(with date and time)">
                            <ItemTemplate>
                                <asp:FileUpload ID="uploadCorrectiveEvidence" runat="server" Width="95%" />
                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.png|.jpg|.gif|.xls|.xlsx)$"
                                    ControlToValidate="uploadEvidenFUP" runat="server" ForeColor="Red" ErrorMessage="Please select a valid word,excel,jpg,png,gif or pdf file format."
                                    Display="Dynamic" />--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Corrective Action Details">
                            <ItemTemplate>
                                <asp:TextBox ID="txtActionDetails" TextMode="MultiLine" runat="server" Width="95%"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
        </div>
        <br />
        <div id="dvUnAuth" runat="server" visible="false">
            <asp:Label ID="Label13" runat="server" ForeColor="Red" Font-Size="Large"  Text="You are not authorized to access"></asp:Label>
            <br />
            <br />
            <br />
            <asp:Label ID="Label114" runat="server" ForeColor="Red" Font-Size="Large"  Text="[Can be filled only by safety officer]"></asp:Label>
        </div>
        <table>
            <tr>
                <td >
                    <asp:Label ID="lblSOSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium"  runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblRCMSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium"  runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblAsigneeSubNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium"  runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td >&nbsp;</td>
            </tr>
            <tr>
                <td >&nbsp;</td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblLinkClosureNotification" runat="server"  ></asp:Label>
                </td>
            </tr>
        </table>
        <asp:Label ID="lblProjectCode" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblTransactionID" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblRCMID" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblRCMName" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblRCMEmail" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblPMID" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblPMName" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblPMEmail" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblAssigneID" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblAssigneName" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblAssigneEmail" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblbuheadid" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblbuheadName" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblbuheadEmail" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblHead" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblHeadID" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblHeadEmail" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblSOName" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblSOID" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblSOEmail" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblPE" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblPEID" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblPEEmail" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblNewSBU" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblError" ForeColor="Red" runat="server"  ></asp:Label>
        <asp:Label ID="lblInduCoE" runat="server" Visible="false" ></asp:Label>
    </asp:Panel>

    <br />
   

</div>
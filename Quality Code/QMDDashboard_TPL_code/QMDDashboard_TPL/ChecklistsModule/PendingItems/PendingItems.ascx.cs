﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using Microsoft.SharePoint;

namespace QMDDashboard_TPL.ChecklistsModule.PendingItems
{
    [ToolboxItemAttribute(false)]
    public partial class PendingItems : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public PendingItems()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                Tab1.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;
                DataTable dtData = objQuality.GetRCMPendingItems(SPContext.Current.Web.CurrentUser.Email, "Pending with RCM");
                if (dtData.Rows.Count > 0)
                {
                    grdRCMPendings.Visible = true;
                    grdRCMPendings.DataSource = dtData;
                    grdRCMPendings.DataBind();
                    dvRCM.Visible = false;
                    dvAssignee.Visible = false;
                    dvRCMClose.Visible = false;
                   
                }
                else
                {
                    dvRCM.Visible = true;
                    grdRCMPendings.Visible = false;
                    grdAssigneePendings.Visible = false;
                    grdRCMClosurePendings.Visible = false;
                }

           }
        }
        QualityChecklists objQuality = new QualityChecklists();
        protected void Tab1_Click(object sender, EventArgs e)
        {
                Tab1.CssClass = "Clicked";
                Tab2.CssClass = "Initial";
                Tab3.CssClass = "Initial";
                MainView.ActiveViewIndex = 0;
                DataTable dtData = objQuality.GetRCMPendingItems(SPContext.Current.Web.CurrentUser.Email, "Pending with RCM");
                if (dtData.Rows.Count > 0)
                {
                    grdRCMPendings.Visible = true;
                    grdRCMPendings.DataSource = dtData;
                    grdRCMPendings.DataBind();
                    dvRCM.Visible = false;
                    dvAssignee.Visible = false;
                    dvRCMClose.Visible = false;
                }
                else
                {
                    dvRCM.Visible = true;
                    dvAssignee.Visible = false;
                    dvRCMClose.Visible = false;
                    grdRCMPendings.Visible = false;
                    grdAssigneePendings.Visible = false;
                    grdRCMClosurePendings.Visible = false;
                }
            

        }

        protected void Tab2_Click(object sender, EventArgs e)
        {
           
                Tab1.CssClass = "Initial";
                Tab2.CssClass = "Clicked";
                Tab3.CssClass = "Initial";
                MainView.ActiveViewIndex = 1;
                DataTable dtData = objQuality.GetAssigneePendingItems(SPContext.Current.Web.CurrentUser.Email, "Pending with Assignee");
                
                if (dtData.Rows.Count > 0)
                {
                    grdAssigneePendings.Visible = true;
                    grdAssigneePendings.DataSource = dtData;
                    grdAssigneePendings.DataBind();
                    dvRCM.Visible = false;
                    dvAssignee.Visible = false;
                    dvRCMClose.Visible = false;
                }
                else
                {
                    dvAssignee.Visible = true;
                    grdRCMClosurePendings.Visible = false;
                    grdRCMPendings.Visible = false;
                    grdAssigneePendings.Visible = false;
                    dvRCM.Visible = false;
                    dvRCMClose.Visible = false;
                }
            
        }

        protected void Tab3_Click(object sender, EventArgs e)
        {
            
                Tab1.CssClass = "Initial";
                Tab2.CssClass = "Initial";
                Tab3.CssClass = "Clicked";
                MainView.ActiveViewIndex = 2;
                DataTable dtData = objQuality.GetRCMPendingItems(SPContext.Current.Web.CurrentUser.Email, "Awaiting Closure");
                if (dtData.Rows.Count > 0)
                {
                    grdRCMClosurePendings.Visible = true;
                    grdRCMClosurePendings.DataSource = dtData;
                    grdRCMClosurePendings.DataBind();
                    dvRCM.Visible = false;
                    dvAssignee.Visible = false;
                    dvRCMClose.Visible = false;
                }
                else
                {
                    dvRCMClose.Visible = true;
                    grdRCMClosurePendings.Visible = false;
                    grdRCMPendings.Visible = false;
                    grdAssigneePendings.Visible = false;
                    dvRCM.Visible = false;
                    dvAssignee.Visible = false;
                }
           
        }
    }
}

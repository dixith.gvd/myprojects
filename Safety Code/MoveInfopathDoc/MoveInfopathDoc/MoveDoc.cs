﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using System.Xml;
using System.IO;
using System.Xml.XPath;
using System.Data;
using System.Collections;
using Microsoft.Office;
using Microsoft.SharePoint.WorkflowActions;
using Microsoft.SharePoint.Workflow;
using Microsoft.Office.InfoPath;
using Microsoft.SharePoint.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using System.Text.RegularExpressions;
namespace MoveInfopathDoc
{
    class MoveDoc
    {
        public void StartProcess()
        {
            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            SPSite site = new SPSite(siteurl);
            SPWeb oWeb = site.OpenWeb();
            SPList _list = oWeb.Lists["Checklists_EHS"];
            SPQuery query = new SPQuery();
            string ids = "2583,2584,2585,2586,2587,982,681,682,985,1454,1455,1456,769,770,2417,2418,1860,1863,1864,1865,1866,1867,1558,1559,1560,1561,2850,2851,2852,3033,1184,1185,1186,1187,1188,1189,2217,3122,3123,3124,3125,3126,3127,3141,867,868,2489,2490,3338,3339,3423,3424,3425,3427,3428,3624,3625,2259,2261,2263,2264,3917,3918,4064,4065,4066,4067,4086,4213,953,954,956,1688,1689,2003,2005,4446,4448,4496,2009,4867,1354,1355,1356,1357";
            string[] id = ids.Split(',');
            //query.Query = "<Where><Gt><FieldRef Name='ID' /><Value Type='Counter'>26608</Value></Gt></Where>";
            //query.Query = "<Where><Gt><FieldRef Name='ID' /><Value Type='Counter'>100</Value></Gt></Where>";
            //SPListItemCollection items = _list.Items;
            //SPListItemCollection items = _list.GetItems(query);
            string rcmemail = string.Empty;
            string rcmname = string.Empty;
            string rcmid = string.Empty;
            string buheadid = string.Empty;
            string buheadname = string.Empty;
            string buheademail = string.Empty;
            string pmemail = string.Empty;
            string pmname = string.Empty;
            string pmid = string.Empty;
            string hopemail = string.Empty;
            string hopname = string.Empty;
            string hopid = string.Empty;
            string soemail = string.Empty;
            string soname = string.Empty;
            string soid = string.Empty;
            string assigneename = string.Empty;
            string assigneeemail = string.Empty;
            string assigneeid = string.Empty;
            string transID = string.Empty;
            string assigneesubdate = string.Empty;
            string rcmsubdate = string.Empty;
            string sbuname = string.Empty;

            foreach (string eachid in id)
            {
                SPListItem item = _list.GetItemById(Convert.ToInt32(eachid));
                //SPListItem item = _list.GetItemById(200);

                DateTime created2 = Convert.ToDateTime(item["Created"]);
                string sosubmissiondate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", created2);
                MemoryStream oMemoryStream = new MemoryStream(item.File.OpenBinary());
                XmlTextReader oReader = new XmlTextReader(oMemoryStream);
                XmlDocument oDoc = new XmlDocument();
                oDoc.Load(oReader);
                oReader.Close();
                oMemoryStream.Close();
                XPathNavigator ipFormNav = oDoc.DocumentElement.CreateNavigator();
                XmlNamespaceManager NamespaceManager = new XmlNamespaceManager(ipFormNav.NameTable);
                NamespaceManager.AddNamespace("my", "http://schemas.microsoft.com/office/infopath/2003/myXSD/2016-12-21T04:42:18");
                XPathNavigator frequency = ipFormNav.SelectSingleNode("my:Frequency", NamespaceManager);
                XPathNavigator checklist = ipFormNav.SelectSingleNode("my:CheckListName", NamespaceManager);
                XPathNavigator UploadSelfCert = ipFormNav.SelectSingleNode("my:AttachUploadSelfCert", NamespaceManager);
                XPathNavigator NonCompliance = ipFormNav.SelectSingleNode("my:AttachNonCompliance", NamespaceManager);
                XPathNavigator Evidencedoc = ipFormNav.SelectSingleNode("my:AssigneeSection/my:AssigneeEvidenceDoc", NamespaceManager);
                XPathNavigator buname = ipFormNav.SelectSingleNode("my:BUName", NamespaceManager);
                XPathNavigator projectname = ipFormNav.SelectSingleNode("my:ProjectName", NamespaceManager);
                string projectcode = projectname.InnerXml.Split('-')[1];
                projectcode = Regex.Replace(projectcode, "[^0-9]+", string.Empty);

                DataTable projectCodeFromList = GetProjectCodeByProjectName(projectcode);
                if (projectCodeFromList != null)
                {
                    sbuname = projectCodeFromList.Rows[0]["NewBUName"].ToString();
                    for (int i = 0; i < projectCodeFromList.Rows.Count; i++)
                    {
                        string projectCode = projectCodeFromList.Rows[i][0].ToString();

                        SPListItem projectCodeFromPMRCM = GetProjectCodeByProjectNameForRCM(projectCode);
                        if (projectCodeFromPMRCM != null)
                        {
                            if (projectCodeFromPMRCM["RCM"] != null)
                            {
                                string strRCM = projectCodeFromPMRCM["RCM"].ToString();
                                if (!string.IsNullOrEmpty(strRCM))
                                {
                                    try
                                    {
                                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strRCM);
                                        rcmemail = userValue.User.Email.ToString();
                                        rcmname = userValue.User.Name;
                                        rcmid = GetOnlyEmployeeID(userValue.User.LoginName);
                                    }
                                    catch (Exception ex)
                                    {
                                        rcmemail = null;
                                        rcmname = null;
                                        rcmid = null;
                                    }
                                }

                            }
                            if (projectCodeFromPMRCM["BU_x0020_Head"] != null)
                            {
                                string strbuHead = projectCodeFromPMRCM["BU_x0020_Head"].ToString();
                                if (!string.IsNullOrEmpty(strbuHead))
                                {
                                    try
                                    {
                                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strbuHead);
                                        buheadid = GetOnlyEmployeeID(userValue.User.LoginName);
                                        buheadname = userValue.User.Name;
                                        buheademail = userValue.User.Email.ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        buheadid = null;
                                        buheadname = null;
                                        buheademail = null;
                                    }
                                }
                            }
                            if (projectCodeFromPMRCM["PM"] != null)
                            {
                                string strPM = projectCodeFromPMRCM["PM"].ToString();
                                if (!string.IsNullOrEmpty(strPM))
                                {
                                    try
                                    {
                                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPM);
                                        pmemail = userValue.User.Email.ToString();
                                        pmname = userValue.User.Name;
                                        pmid = GetOnlyEmployeeID(userValue.User.LoginName);
                                    }
                                    catch (Exception ex)
                                    {
                                        pmemail = null;
                                        pmname = null;
                                        pmid = null;
                                    }
                                }

                            }

                            if (projectCodeFromPMRCM["Head_x0020_Operations"] != null)
                            {
                                string strHead = projectCodeFromPMRCM["Head_x0020_Operations"].ToString();
                                if (!string.IsNullOrEmpty(strHead))
                                {
                                    try
                                    {
                                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strHead);
                                        hopemail = userValue.User.Email.ToString();
                                        hopname = userValue.User.Name;
                                        hopid = GetOnlyEmployeeID(userValue.User.LoginName);
                                    }
                                    catch (Exception ex)
                                    {
                                        hopemail = null;
                                        hopname = null;
                                        hopid = null;
                                    }
                                }

                            }

                        }
                        else
                        {
                            rcmemail = null;
                            rcmname = null;
                            rcmid = null;
                            buheadid = null;
                            buheadname = null;
                            buheademail = null;
                            pmemail = null;
                            pmname = null;
                            pmid = null;
                            hopemail = null;
                            hopname = null;
                            hopid = null;
                            sbuname = null;
                        }
                    }

                }
                else
                {
                    rcmemail = null;
                    rcmname = null;
                    rcmid = null;
                    buheadid = null;
                    buheadname = null;
                    buheademail = null;
                    pmemail = null;
                    pmname = null;
                    pmid = null;
                    hopemail = null;
                    hopname = null;
                    hopid = null;
                    sbuname = null;
                }
                //string soname = item["Created By"].ToString().Split('#')[1];
                string strso = item["Created By"].ToString();
                if (!string.IsNullOrEmpty(strso))
                {
                    try
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(item.ParentList.ParentWeb, strso);
                        soemail = userValue.User.Email.ToString();
                        soname = userValue.User.Name;
                        soid = GetOnlyEmployeeID(userValue.User.LoginName);
                    }
                    catch (Exception ex)
                    {
                        soemail = null;
                        soname = null;
                        soid = null;
                    }
                }
                int TransactionID = Convert.ToInt32(eachid);

                XPathNavigator date = ipFormNav.SelectSingleNode("my:Date", NamespaceManager);
                //DateTime date2 = Convert.ToDateTime(ipFormNav.SelectSingleNode("my:Date", NamespaceManager));
                XPathNavigator location = ipFormNav.SelectSingleNode("my:Location", NamespaceManager);
                XPathNavigator compliance = ipFormNav.SelectSingleNode("my:Compliance", NamespaceManager);
                XPathNavigator stage = ipFormNav.SelectSingleNode("my:Stage", NamespaceManager);

                DateTime modified2 = Convert.ToDateTime(item["Modified"]);
                string modified = String.Format("{0:yyyy-MM-dd HH:mm:ss}", modified2);
                string lastupdate = modified;
                if (stage.InnerXml == "Closed")
                {
                    if (compliance.InnerXml == "Yes")
                    {
                        assigneesubdate = null;
                        rcmsubdate = null;
                    }
                    else
                    {
                        assigneesubdate = modified;
                        rcmsubdate = null;
                    }
                }
                else if (stage.InnerXml == "Pending with Assignee")
                {
                    rcmsubdate = modified;
                    assigneesubdate = null;
                }
                else if (stage.InnerXml == "Pending with RCM")
                {
                    rcmsubdate = null;
                    assigneesubdate = null;
                }
                XPathNavigator remarks = ipFormNav.SelectSingleNode("my:Remarks", NamespaceManager);
                XPathNavigator selfcert = ipFormNav.SelectSingleNode("my:SelfCertification", NamespaceManager);
                XPathNavigator assignee = ipFormNav.SelectSingleNode("my:RCMSection/my:L2Assignee", NamespaceManager);
                XPathNavigator observation = ipFormNav.SelectSingleNode("my:RCMObservations", NamespaceManager);
                XPathNavigator rcmdate = ipFormNav.SelectSingleNode("my:RCMSection/my:RCMdate", NamespaceManager);
                //DateTime date3 = Convert.ToDateTime(ipFormNav.SelectSingleNode("my:RCMdate", NamespaceManager));
                XPathNavigator actiontaken = ipFormNav.SelectSingleNode("my:AssigneeSection/my:AssigneeActionTaken", NamespaceManager);
                XPathNavigator closuredate = ipFormNav.SelectSingleNode("my:AssigneeSection/my:AssigneeClosureDate", NamespaceManager);
                //DateTime date4 = Convert.ToDateTime(ipFormNav.SelectSingleNode("my:AssigneeClosureDate", NamespaceManager));
                XPathNavigator assigneeselfcert = ipFormNav.SelectSingleNode("my:AssigneeSection/my:AssigneeSelfcertification", NamespaceManager);
                if (assignee.ToString() != "")
                {
                    string name = assignee.ToString().Split('\\')[1].Trim();

                    if (!string.IsNullOrEmpty(name))
                    {
                        try
                        {
                            name = name.Replace("User", "");
                            SPUser user = oWeb.EnsureUser(name);
                            SPFieldUserValue usercol = new SPFieldUserValue(oWeb, user.ID, user.Email.ToString());
                            //SPFieldUserValue userValue2 = new SPFieldUserValue(item.ParentList.ParentWeb, assigneename);
                            assigneeemail = usercol.User.Email.ToString();
                            assigneeid = GetOnlyEmployeeID(usercol.User.LoginName);
                            assigneename = usercol.User.Name.ToString();
                        }
                        catch (Exception ex)
                        {
                            assigneeemail = null;
                            assigneeid = null;
                            assigneename = null;
                        }
                    }
                }
                else
                {
                    assigneeemail = null;
                    assigneeid = null;
                    assigneename = null;
                }


                string attachmentNode = string.Empty;
                try
                {
                    StringBuilder insertQuery2 = new StringBuilder();
                    insertQuery2.Append("insert into EHSChecklist([Inspection Date],[RCM Submission Date],[Assignee Submission Date],[Assignee Actual Closed Date],Transaction_ID,Frequency,[Checklist Name],BUName,SBUName,ProjectName,ProjectCode,isCompliant,Stage,Location,[SO Remarks],Assignee_ID,Assignee_Name,Assignee_Email,[RCM Comments],[Target Closure Date],[Assignee Action Details],[Self Certification],[Assignee Self Certification],[SO Submission Date],SO_ID,SO_Name,SO_Email,RCM_ID,RCM_Name,RCM_Email,PM_ID,PM_Name,PM_Email,HOP_ID,HOP_Name,HOP_Email,BUHead_ID,BUHead_Name,BUHead_Email,LastStageUpdate)");
                    insertQuery2.Append(" values ("); insertQuery2.Append("@inspectiondate,"); insertQuery2.Append("@rcmsubdate,");
                    insertQuery2.Append("@assigneesubdate,"); insertQuery2.Append("@closuredate,"); insertQuery2.Append("" + TransactionID + ","); insertQuery2.Append("'" + frequency.InnerXml + "',");
                    insertQuery2.Append("'" + checklist.InnerXml + "',"); insertQuery2.Append("'" + buname.InnerXml + "',"); insertQuery2.Append("'" + sbuname + "',"); insertQuery2.Append("'" + projectname.InnerXml + "',");
                    insertQuery2.Append("'" + projectcode + "',"); insertQuery2.Append("'" + compliance.InnerXml + "',");
                    insertQuery2.Append("'" + stage.InnerXml + "',"); insertQuery2.Append("'" + location.InnerXml.Replace("\'", "") + "',"); insertQuery2.Append("'" + remarks.InnerXml.Replace("\'", "") + "',");
                    insertQuery2.Append("'" + assigneeid + "',"); insertQuery2.Append("'" + assigneename + "',"); insertQuery2.Append("'" + assigneeemail + "',"); insertQuery2.Append("'" + observation.InnerXml.Replace("\'", "") + "',"); insertQuery2.Append("'" + rcmdate.InnerXml + "',");
                    insertQuery2.Append("'" + actiontaken.InnerXml.Replace("\'", "") + "',"); insertQuery2.Append("'" + selfcert.InnerXml + "',"); insertQuery2.Append("'" + assigneeselfcert.InnerXml + "',");
                    insertQuery2.Append("'" + sosubmissiondate + "',");
                    insertQuery2.Append("'" + soid + "',"); insertQuery2.Append("'" + soname + "',"); insertQuery2.Append("'" + soemail + "',"); insertQuery2.Append("'" + rcmid + "',");
                    insertQuery2.Append("'" + rcmname + "',"); insertQuery2.Append("'" + rcmemail + "',"); insertQuery2.Append("'" + pmid + "',"); insertQuery2.Append("'" + pmname + "',");
                    insertQuery2.Append("'" + pmemail + "',"); insertQuery2.Append("'" + hopid + "',"); insertQuery2.Append("'" + hopname + "',");
                    insertQuery2.Append("'" + hopemail + "',"); insertQuery2.Append("'" + buheadid + "',"); insertQuery2.Append("'" + buheadname + "',");
                    insertQuery2.Append("'" + buheademail + "',"); insertQuery2.Append("'" + lastupdate + "')");
                    SqlCommand cmd2 = new SqlCommand(insertQuery2.ToString());
                    cmd2.Parameters.Add("@inspectiondate", SqlDbType.DateTime);
                    cmd2.Parameters.Add("@rcmsubdate", SqlDbType.DateTime);
                    cmd2.Parameters.Add("@assigneesubdate", SqlDbType.DateTime);
                    cmd2.Parameters.Add("@closuredate", SqlDbType.DateTime);
                    //cmd2.Parameters.Add("@transid", SqlDbType.Int).Value = transID;
                    if ((date.InnerXml == null) || (date.InnerXml == ""))
                    {
                        cmd2.Parameters["@inspectiondate"].Value = DBNull.Value;
                    }
                    else
                    {
                        cmd2.Parameters["@inspectiondate"].Value = DateTime.Parse(date.InnerXml);
                    }
                    if ((rcmsubdate == null) || (rcmsubdate == ""))
                    {
                        cmd2.Parameters["@rcmsubdate"].Value = DBNull.Value;
                    }
                    else
                    {
                        cmd2.Parameters["@rcmsubdate"].Value = DateTime.Parse(rcmsubdate);
                    }
                    if ((assigneesubdate == null) || (assigneesubdate == ""))
                    {
                        cmd2.Parameters["@assigneesubdate"].Value = DBNull.Value;
                    }
                    else
                    {
                        cmd2.Parameters["@assigneesubdate"].Value = DateTime.Parse(assigneesubdate);
                    }
                    if ((closuredate.InnerXml == null) || (closuredate.InnerXml == ""))
                    {
                        cmd2.Parameters["@closuredate"].Value = DBNull.Value;
                    }
                    else
                    {
                        cmd2.Parameters["@closuredate"].Value = DateTime.Parse(closuredate.InnerXml);
                    }
                    bool isSaved = InsertEvidenceDoc(cmd2);
                    if (isSaved)
                    {
                        //transID = GetLatestTransactionIDSafety();
                        transID = TransactionID.ToString();
                    }
                }
                catch (Exception ex)
                {
                }
                if (UploadSelfCert != null && !string.IsNullOrEmpty(UploadSelfCert.Value))
                {
                    InsertChecklistDocument(UploadSelfCert, transID);
                }
                if (NonCompliance != null && !string.IsNullOrEmpty(NonCompliance.Value))
                {
                    InsertNonCompliance(NonCompliance, transID);
                }
                if (Evidencedoc != null && !string.IsNullOrEmpty(Evidencedoc.Value))
                {
                    InsertEvidenceDocument(Evidencedoc, transID);
                }
            }
        }

        public string GetOnlyEmployeeID(string loggedInName)
        {
            string empID = string.Empty;
            string loggedNameLowerCase = loggedInName.ToLower().ToString();
            if (loggedNameLowerCase.Contains("tpl"))
            {

                string replaced = loggedNameLowerCase.Replace('\\', '@');
                if (replaced.Contains("@"))
                {
                    string[] seperators = { "@" };
                    string[] nameArray = replaced.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
                    if (nameArray.Length == 2)
                    {
                        if (nameArray[1] != null)
                        {
                            empID = nameArray[1];
                        }
                    }
                }
                else
                {
                    string replacedTest = loggedNameLowerCase.Replace('\\', '@');
                }
            }
            return empID.ToUpper();
        }


        public SPListItem GetProjectCodeByProjectNameForRCM(string projectCode)
        {
            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            SPListItem projectCodeForRCM = null;
            using (SPSite site = new SPSite(siteurl))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    if (web.Lists["Project PM RCM"] != null)
                    {
                        SPList list = web.Lists["Project PM RCM"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = "<Where><Eq><FieldRef Name='Project_x0020_Code' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                            //query.ViewFields = "<FieldRef Name='Final_x0020_Project_x0020_Name' /><FieldRef Name='SBUName' />";
                            SPListItemCollection items = list.GetItems(query);
                            foreach (SPListItem item in items)
                            {
                                projectCodeForRCM = item;
                                break;

                            }

                            //dtProjectDetails = list.GetItems(query).GetDataTable();
                            //web.AllowUnsafeUpdates = false;
                        }
                    }

                }
            }
            return projectCodeForRCM;
        }

        public DataTable GetProjectCodeByProjectName(string projectCode)
        {
            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            DataTable dtProjectDetails = new DataTable();
            // SPListItem projectCodeItem = null;
            using (SPSite site = new SPSite(siteurl))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    if (web.Lists["TPL Projects List"] != null)
                    {
                        SPList list = web.Lists["TPL Projects List"];
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            //query.Query = "<Where><Contains><FieldRef Name='Final_x0020_Project_x0020_Name' /><Value Type='Text'>" + projectCode + "</Value></Contains></Where>";
                            query.Query = "<Where><Eq><FieldRef Name='Title' /><Value Type='Text'>" + projectCode + "</Value></Eq></Where>";
                            //query.ViewFields = "<FieldRef Name='Final_x0020_Project_x0020_Name' /><FieldRef Name='SBUName' />";
                            // SPListItemCollection items = list.GetItems(query);
                            //foreach (SPListItem item in items)
                            //{
                            //    projectCodeItem = item;
                            //    break;

                            //}

                            dtProjectDetails = list.GetItems(query).GetDataTable();
                            web.AllowUnsafeUpdates = false;
                        }
                    }

                }
            }
            return dtProjectDetails;
        }

        private void InsertChecklistDocument(XPathNavigator UploadSelfCert, string Transactionid)
        {
            string attachmentNode = string.Empty;
            attachmentNode = UploadSelfCert.Value;
            byte[] attachmentNodeBytes = Convert.FromBase64String(attachmentNode);
            int fnLength = attachmentNodeBytes[20] * 2;
            byte[] fnBytes = new byte[fnLength];

            for (int i = 0; i < fnLength; i++)
            {
                fnBytes[i] = attachmentNodeBytes[24 + i];
            }
            char[] charFileName = UnicodeEncoding.Unicode.GetChars(fnBytes);
            string fileName = new string(charFileName);
            fileName = fileName.Substring(0, fileName.Length - 1);

            byte[] fileContents = new byte[attachmentNodeBytes.Length - (24 + fnLength)];
            for (int i = 0; i < fileContents.Length; i++)
            {
                fileContents[i] = attachmentNodeBytes[24 + fnLength + i];
            }
            string ext = Path.GetExtension(fileName).ToLower();
            if (ext.Contains("pdf") || string.IsNullOrEmpty(ext))
            {
                ext = ".pdf";
            }

            string contenttype = String.Empty;
            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
                case ".pptx":
                    contenttype = "application/pptx";
                    break;
                case ".jpeg":
                    contenttype = "image/jpeg";
                    break;
                case ".tif":
                    contenttype = "image/tif";
                    break;
                case ".tiff":
                    contenttype = "image/tiff";
                    break;
                case ".bmp":
                    contenttype = "image/bmp";
                    break;
                case ".rar":
                    contenttype = "application/rar";
                    break;
            }
            if (contenttype != String.Empty)
            {
                MemoryStream stream = new MemoryStream(fileContents);
                //Stream fs = stream
                //fileContents.InputStream;
                BinaryReader br = new BinaryReader(stream);
                Byte[] bytes = br.ReadBytes((Int32)stream.Length);
                string strQuery = "UPDATE EHSChecklist SET Checklist_Document = @Checklist_Document, Checklist_DocContentType = @Checklist_DocContentType, Checklist_DocName = @Checklist_DocName where Transaction_ID='" + Transactionid + "'";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@Checklist_DocName", SqlDbType.VarChar).Value = fileName;
                cmd.Parameters.Add("@Checklist_DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@Checklist_Document", SqlDbType.Binary).Value = bytes;
                InsertEvidenceDoc(cmd);
            }
        }

        private void InsertNonCompliance(XPathNavigator NonCompliance, string Transactionid)
        {
            string attachmentNode = string.Empty;
            attachmentNode = NonCompliance.Value;
            byte[] attachmentNodeBytes = Convert.FromBase64String(attachmentNode);
            int fnLength = attachmentNodeBytes[20] * 2;
            byte[] fnBytes = new byte[fnLength];

            for (int i = 0; i < fnLength; i++)
            {
                fnBytes[i] = attachmentNodeBytes[24 + i];
            }
            char[] charFileName = UnicodeEncoding.Unicode.GetChars(fnBytes);
            string fileName = new string(charFileName);
            fileName = fileName.Substring(0, fileName.Length - 1);

            byte[] fileContents = new byte[attachmentNodeBytes.Length - (24 + fnLength)];
            for (int i = 0; i < fileContents.Length; i++)
            {
                fileContents[i] = attachmentNodeBytes[24 + fnLength + i];
            }
            //string filePath = fileContents.ToString();
            ////uploadattach.PostedFile.FileName;
            //string filename = Path.GetFileName(filePath);
            //File.WriteAllBytes(string path, fileContents);
            string ext = Path.GetExtension(fileName).ToLower();
            if (ext.Contains("pdf") || string.IsNullOrEmpty(ext))
            {
                ext = ".pdf";
            }
            string contenttype = String.Empty;
            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
                case ".pptx":
                    contenttype = "application/pptx";
                    break;
                case ".jpeg":
                    contenttype = "image/jpeg";
                    break;
                case ".tif":
                    contenttype = "image/tif";
                    break;
                case ".tiff":
                    contenttype = "image/tiff";
                    break;
                case ".bmp":
                    contenttype = "image/bmp";
                    break;
                case ".rar":
                    contenttype = "application/rar";
                    break;
            }
            if (contenttype != String.Empty)
            {
                MemoryStream stream = new MemoryStream(fileContents);
                //Stream fs = stream
                //fileContents.InputStream;
                BinaryReader br = new BinaryReader(stream);
                Byte[] bytes = br.ReadBytes((Int32)stream.Length);
                String strQuery = "update EHSChecklist set NonComp_DocName=@NonComp_DocName,NonComp_DocContentType=@NonComp_DocContentType,NonComp_Document=@NonComp_Document where Transaction_ID='" + Transactionid + "'";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@NonComp_DocName", SqlDbType.VarChar).Value = fileName;
                cmd.Parameters.Add("@NonComp_DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@NonComp_Document", SqlDbType.Binary).Value = bytes;
                InsertEvidenceDoc(cmd);
            }
        }

        private void InsertEvidenceDocument(XPathNavigator Evidencedoc, string Transactionid)
        {
            string attachmentNode = string.Empty;
            attachmentNode = Evidencedoc.Value;
            byte[] attachmentNodeBytes = Convert.FromBase64String(attachmentNode);
            int fnLength = attachmentNodeBytes[20] * 2;
            byte[] fnBytes = new byte[fnLength];

            for (int i = 0; i < fnLength; i++)
            {
                fnBytes[i] = attachmentNodeBytes[24 + i];
            }
            char[] charFileName = UnicodeEncoding.Unicode.GetChars(fnBytes);
            string fileName = new string(charFileName);
            fileName = fileName.Substring(0, fileName.Length - 1);

            byte[] fileContents = new byte[attachmentNodeBytes.Length - (24 + fnLength)];
            for (int i = 0; i < fileContents.Length; i++)
            {
                fileContents[i] = attachmentNodeBytes[24 + fnLength + i];
            }
            //string filePath = fileContents.ToString();
            ////uploadattach.PostedFile.FileName;
            //string filename = Path.GetFileName(filePath);
            //File.WriteAllBytes(string path, fileContents);
            string ext = Path.GetExtension(fileName).ToLower();
            if (ext.Contains("pdf") || string.IsNullOrEmpty(ext))
            {
                ext = ".pdf";
            }
            string contenttype = String.Empty;
            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
                case ".pptx":
                    contenttype = "application/pptx";
                    break;
                case ".jpeg":
                    contenttype = "image/jpeg";
                    break;
                case ".tif":
                    contenttype = "image/tif";
                    break;
                case ".tiff":
                    contenttype = "image/tiff";
                    break;
                case ".bmp":
                    contenttype = "image/bmp";
                    break;
                case ".rar":
                    contenttype = "application/rar";
                    break;
            }
            if (contenttype != String.Empty)
            {
                MemoryStream stream = new MemoryStream(fileContents);
                //Stream fs = stream
                //fileContents.InputStream;
                BinaryReader br = new BinaryReader(stream);
                Byte[] bytes = br.ReadBytes((Int32)stream.Length);
                string strQuery = "UPDATE EHSChecklist SET Evidence_Document = @Evidence_Document, Evidence_DocContentType = @Evidence_DocContentType, Evidence_DocName = @Evidence_DocName where Transaction_ID='" + Transactionid + "'";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@Evidence_DocName", SqlDbType.VarChar).Value = fileName;
                cmd.Parameters.Add("@Evidence_DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@Evidence_Document", SqlDbType.Binary).Value = bytes;
                InsertEvidenceDoc(cmd);
            }
        }

        private Boolean InsertEvidenceDoc(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public string GetLatestTransactionIDSafety()
        {
            string transactionID = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                string sqlQuery = "SELECT TOP 1 Transaction_ID FROM [EQHSAutomation].[dbo].[EHSChecklist] ORDER BY Transaction_ID DESC";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Transaction_ID"] != null)
                        {
                            transactionID = dt.Rows[0]["Transaction_ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {

            }
            return transactionID;
        }
    }
}

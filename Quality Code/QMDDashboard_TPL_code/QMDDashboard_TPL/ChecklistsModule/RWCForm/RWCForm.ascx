﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RWCForm.ascx.cs" Inherits="QMDDashboard_TPL.ChecklistsModule.RWCForm.RWCForm" %>

<table border="1" style="width: 80%; font-family: 'Trebuchet MS'; font-size: 14px" id="tblSWI" runat="server">
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td>
                        <asp:Image ID="imgTataLogo" ImageUrl="../../_layouts/15/images/TataLogo.jpg" runat="server" />
                    </td>
                    <td style="width: 100%;">
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="Label12" runat="server" Text="TATA PROJECTS LTD" Font-Size="Large"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="Label13" runat="server" Text="REWORK CARD(RWC)" Font-Size="Large"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr></table>

    <div id="Div1" runat="server">
        <table border="1" style="width: 80%; font-family: 'Trebuchet MS'; font-size: 14px" id="RWC1">
            <tr>
                <td class="auto-style3">&nbsp;
            <asp:Label ID="Label4" runat="server" Text="RWC No:" Style="font-weight: 600"></asp:Label>
                    <asp:Label ID="lblRWCNO" runat="server"></asp:Label>
                </td>

                <td>
                    <table>
                        <tr>
                            <td>&nbsp;<asp:Label ID="lblDateTime" runat="server" Text="Date" Style="font-weight: 600"></asp:Label></td>
                            <td>
                                <SharePoint:DateTimeControl ID="dtSWIdate" DateOnly="true" runat="server" Enabled="false" />
                            </td>
                        </tr>
                    </table>


                </td>
            </tr>
            
            <tr style="height: 28px">
                <td>&nbsp;
            <asp:Label ID="Label14" runat="server" Style="font-weight: 600" Text="Project/Location :"></asp:Label>
                    <asp:Label ID="lblProjectLocation" runat="server" Text="Auto populated"></asp:Label>
                </td>
                <td>&nbsp;
            <asp:Label ID="lblTPLJobNo" runat="server" Style="font-weight: 600" Text="TPL Job No. :"></asp:Label>
                    <asp:Label ID="Label22" runat="server" Text="Auto populated"></asp:Label>
                </td>

            </tr>
            <tr style="height: 28px">
                <td>&nbsp;<asp:Label ID="Label29" runat="server" Style="font-weight: 600" Text="Subcontractor:"></asp:Label>
                    <asp:TextBox ID="txtSubcontractors" runat="server" Width="60%"></asp:TextBox>
                    &nbsp;</td>
                <td class="auto-style3">&nbsp;<asp:Label ID="Label28" runat="server" Style="font-weight: 600" Text="P.O./W.O. No.:"></asp:Label>
                    <asp:TextBox ID="txtPackage" runat="server" Width="50%"></asp:TextBox>
                </td>

            </tr>
            <tr style="height: 28px">
                <td colspan="2">&nbsp;<asp:Label ID="Label27" Font-Bold="true" runat="server" Text="Description of Package / Work :"></asp:Label>
                    <asp:TextBox ID="txtDescof" Width="300px" runat="server"></asp:TextBox>
                </td>

            </tr>
            <tr style="height: 28px">
                <td colspan="2">
                    <asp:Label ID="Label1" runat="server" Text="Rework due to :"></asp:Label>
                    <asp:RadioButton ID="rdCustomerComplaint" GroupName="RWC" Text="Customer Complaint" runat="server" />
                    <asp:RadioButton ID="rdNCR" GroupName="RWC" Text="NCR" runat="server" />
                    <asp:RadioButton ID="rdFIN" GroupName="RWC" Text="FIN" runat="server" />
                    <asp:RadioButton ID="rdNote" GroupName="RWC" Text="Note" runat="server" />
                    <asp:RadioButton ID="rdEmail" GroupName="RWC" Text="E-mail Reference" runat="server" />
                </td>
            </tr>
            <tr style="height: 28px">
                <td colspan="2">
                    <asp:Label ID="lbldesc" runat="server" Text="Details of Non-Conformity / Discrepancy :"></asp:Label>
                </td>

            </tr>

            <tr>
                <td colspan="2">
                    <asp:Label ID="lblRWCroot" runat="server" Text="Root Cause for Non-Conformity / Discrepancy :" style="font-weight: 700"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="CheckBox1" Text="Defect Material (Supply / Procured at Site)" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="CheckBox2" Text=" Modifications asked by Client" runat="server" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:CheckBox ID="CheckBox3" Text="Engineering Error" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="CheckBox4" Text=" Mis-handling of Materials / Equipments" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="CheckBox5" Text=" Defective workmanship of Sub-Contractor" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="CheckBox6" Text="TPL Scope of work increased" runat="server" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:CheckBox ID="CheckBox7" Text="Defective workmanship of TPL In-house" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="CheckBox8" Text="Drawing Revisions after completion of works" runat="server" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:CheckBox ID="CheckBox9" Text="Missmatch between Civil & Mech. Drawings" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="CheckBox10" Text="Inspection Error" runat="server" />
                </td>
            </tr>
           <%-- <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Button ID="Button1" runat="server" Font-Bold="true" OnClick="btnSWISubmit_Click" Text="Submit" />
                    <asp:Button ID="Button2" runat="server" Font-Bold="true" Text="Cancel" />
                </td>
            </tr>--%>
        </table>
    </div>
    <div id="Div2" runat="server">
        <table border="1" style="width: 80%; font-family: 'Trebuchet MS'; font-size: 14px" id="RWC2">
            <tr>
                <td colspan="2">
                    <asp:Label ID="RWCDetails" runat="server" Text="Details of Reworks : " style="font-weight: 700"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="txtRWCdetails" Width="95%" TextMode="MultiLine" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label15" runat="server" Text="Approximate cost of Rework :"></asp:Label>
                    INR:<asp:TextBox ID="txtINR" runat="server"></asp:TextBox>Number field:<asp:TextBox ID="txtNumberField" runat="server"></asp:TextBox>
                </td>
            </tr>
            <%--<tr>
                <td colspan="2" style="text-align: center">
                    <asp:Button ID="Button3" runat="server" Font-Bold="true" OnClick="btnSWISubmit_Click" Text="Submit" />
                    <asp:Button ID="Button4" runat="server" Font-Bold="true" Text="Cancel" />
                </td>
            </tr>--%>
        </table>
    </div>
    <div id="Div3" runat="server">
        <table border="1" style="width: 80%; font-family: 'Trebuchet MS'; font-size: 14px" id="RWC3">
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label16" runat="server" Text="Quality of Rework after completion :" style="font-weight: 700"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="txtRCM" runat="server" Width="95%" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label17" runat="server" Text="Approximate cost of Rework :"></asp:Label>
                    INR:<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>Number field:<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
            </tr>
            <%--<tr>
                <td colspan="2" style="text-align: center">
                    <asp:Button ID="btnSWISubmit" runat="server" Font-Bold="true" OnClick="btnSWISubmit_Click" Text="Submit" />
                    <asp:Button ID="btnSWIcancel" runat="server" Font-Bold="true" Text="Cancel" />
                </td>
            </tr>--%>
        </table>
    </div>

﻿using Microsoft.SharePoint;
using System;
using System.ComponentModel;
using System.Data;
using System.Web.UI.WebControls.WebParts;
namespace QMDDashboard_TPL.ChecklistsModule.FQPPendingItems
{
    [ToolboxItemAttribute(false)]
    public partial class FQPPendingItems : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public FQPPendingItems()
        {
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                Tab1.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;
                DataTable dtData = objQuality.GetPEPendingItemsforFQP(SPContext.Current.Web.CurrentUser.Email, "Pending with PE");
                if (dtData.Rows.Count > 0)
                {
                    dvPE.Visible = false;
                    dvRCM.Visible = false;
                    dvAssignee.Visible = false;
                    dvRCMClose.Visible = false;
                    grdPEPendings.Visible = true;
                    grdPEPendings.DataSource = dtData;
                    grdPEPendings.DataBind();
                    grdRCMPendings.Visible = false;
                    grdAssigneePendings.Visible = false;
                    grdRCMClosurePendings.Visible = false;
                }
                else
                {
                    dvPE.Visible = true;
                    dvRCM.Visible = false;
                    dvAssignee.Visible = false;
                    dvRCMClose.Visible = false;
                    grdPEPendings.Visible = false;
                    grdRCMPendings.Visible = false;
                    grdAssigneePendings.Visible = false;
                    grdRCMClosurePendings.Visible = false;
                }
            }
        }
        QualityChecklists objQuality = new QualityChecklists();
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
            DataTable dtData = objQuality.GetPEPendingItemsforFQP(SPContext.Current.Web.CurrentUser.Email, "Pending with PE");
            if (dtData.Rows.Count > 0)
            {
                dvPE.Visible = false;
                dvRCM.Visible = false;
                dvAssignee.Visible = false;
                dvRCMClose.Visible = false;
                grdPEPendings.Visible = true;
                grdPEPendings.DataSource = dtData;
                grdPEPendings.DataBind();
                grdRCMPendings.Visible = false;
                grdAssigneePendings.Visible = false;
                grdRCMClosurePendings.Visible = false;
            }
            else
            {
                dvPE.Visible = true;
                dvRCM.Visible = false;
                dvAssignee.Visible = false;
                dvRCMClose.Visible = false;
                grdPEPendings.Visible = false;
                grdRCMPendings.Visible = false;
                grdAssigneePendings.Visible = false;
                grdRCMClosurePendings.Visible = false;
            }
        }
        protected void Tab2_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Initial";
            MainView.ActiveViewIndex = 1;
            DataTable dtData = objQuality.GetRCMPendingItemsforFQP(SPContext.Current.Web.CurrentUser.Email, "Pending with RCM");
            if (dtData.Rows.Count > 0)
            {
                dvPE.Visible = false;
                dvRCM.Visible = false;
                dvAssignee.Visible = false;
                dvRCMClose.Visible = false;
                grdPEPendings.Visible = false;
                grdRCMPendings.Visible = true;
                grdRCMPendings.DataSource = dtData;
                grdRCMPendings.DataBind();
                grdAssigneePendings.Visible = false;
                grdRCMClosurePendings.Visible = false;
            }
            else
            {
                dvPE.Visible = false;
                dvRCM.Visible = true;
                dvAssignee.Visible = false;
                dvRCMClose.Visible = false;
                grdPEPendings.Visible = false;
                grdRCMPendings.Visible = false;
                grdAssigneePendings.Visible = false;
                grdRCMClosurePendings.Visible = false;
            }
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            Tab4.CssClass = "Initial";
            MainView.ActiveViewIndex = 2;
            DataTable dtData = objQuality.GetAssigneePendingItemsforFQP(SPContext.Current.Web.CurrentUser.Email, "Pending with Assignee");
            if (dtData.Rows.Count > 0)
            {
                dvPE.Visible = false;
                dvRCM.Visible = false;
                dvAssignee.Visible = false;
                dvRCMClose.Visible = false;
                grdPEPendings.Visible = false;
                grdRCMPendings.Visible = false;
                grdAssigneePendings.Visible = true;
                grdAssigneePendings.DataSource = dtData;
                grdAssigneePendings.DataBind();
                grdRCMClosurePendings.Visible = false;
            }
            else
            {
                dvPE.Visible = false;
                dvRCM.Visible = false;
                dvAssignee.Visible = true;
                dvRCMClose.Visible = false;
                grdPEPendings.Visible = false;
                grdRCMPendings.Visible = false;
                grdAssigneePendings.Visible = false;
                grdRCMClosurePendings.Visible = false;
            }
        }
        protected void Tab4_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Clicked";
            MainView.ActiveViewIndex = 3;
            DataTable dtData = objQuality.GetRCMPendingItemsforFQP(SPContext.Current.Web.CurrentUser.Email, "Awaiting Closure");
            if (dtData.Rows.Count > 0)
            {
                dvPE.Visible = false;
                dvRCM.Visible = false;
                dvAssignee.Visible = false;
                dvRCMClose.Visible = false;
                grdPEPendings.Visible = false;
                grdRCMPendings.Visible = false;
                grdAssigneePendings.Visible = false;
                grdRCMClosurePendings.Visible = true;
                grdRCMClosurePendings.DataSource = dtData;
                grdRCMClosurePendings.DataBind();
            }
            else
            {
                dvPE.Visible = false;
                dvRCM.Visible = false;
                dvAssignee.Visible = false;
                dvRCMClose.Visible = true;
                grdPEPendings.Visible = false;
                grdRCMPendings.Visible = false;
                grdAssigneePendings.Visible = false;
                grdRCMClosurePendings.Visible = false;
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using System.Security;
using Microsoft.SharePoint.Security;
using Microsoft.SharePoint.Utilities;
using Microsoft.Office.DocumentManagement.DocumentSets;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Windows.Forms;
namespace Finance_Communicator.FinanceComm
{
    [ToolboxItemAttribute(false)]
    public partial class FinanceComm : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public FinanceComm()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            using (SPSite site = new SPSite(SPContext.Current.Web.Url.ToString()))
            {
                using (SPWeb web = site.OpenWeb())
                {

                    SPFolder oFolder = web.GetFolder("TDS Files");
                    SPFileCollection collFile = oFolder.Files;

                    //Copy the files to a generic List of type SPFile
                    List<SPFile> listFiles = new List<SPFile>(collFile.Count);

                    foreach (SPFile oFile in collFile)
                    {
                        listFiles.Add(oFile);
                    }

                    // Enumerate the List and move the files into the Destination library.
                    foreach (SPFile moveFile in listFiles)
                    {
                        
                    }
                    // To get folder collection
                }
            }
            if (!string.IsNullOrEmpty(txtfolderpath.Text))
            {
                string logText = "Started on " + DateTime.Today.ToString("dd/MM/yyyy") + " at " + DateTime.Now.ToLongTimeString();
                ReminderBackupLog(logText);
                FileNotFoundLog(logText);
                if (!string.IsNullOrEmpty(ddlsentto.SelectedItem.Text))
                {
                    if (!string.IsNullOrEmpty(ddlassessyear.SelectedItem.Text))
                    {
                        if (string.IsNullOrEmpty(ddlquarter.SelectedItem.Text) && ddlsentto.Text == "Employee")
                        {
                            if (!string.IsNullOrEmpty(txtfilepath.Text))
                            {
                                if (uploadattach.HasFiles)
                                {
                                    string filePath = uploadattach.PostedFile.FileName;
                                    string filename = Path.GetFileName(filePath);
                                    DataTable dtEmps = Data(filename);
                                    string[] columnNames = (from dc in dtEmps.Columns.Cast<DataColumn>()
                                                            select dc.ColumnName).ToArray();
                                    string Name = columnNames[0];
                                    string EmailID = columnNames[1];
                                    string PanNumber = columnNames[2];
                                    if (Name != "Name" || EmailID != "EmailID" || PanNumber != "PAN Number")
                                    {
                                        MessageBox.Show("Please ensure that the column names given in the excel sheet are as per the note");
                                        return;
                                    }
                                    int Count = 0;
                                    Count = dtEmps.Rows.Count;
                                    //pgbfilesuploaded.Minimum = 0;
                                    //pgbfilesuploaded.Maximum = Count;
                                    //pgbfilesuploaded.Value = 0;
                                    for (int i = 0; i < dtEmps.Rows.Count; i++)
                                    {

                                        if (!string.IsNullOrEmpty(dtEmps.Rows[i]["Name"].ToString()) && !string.IsNullOrEmpty(dtEmps.Rows[i]["EmailID"].ToString()) && !string.IsNullOrEmpty(dtEmps.Rows[i]["PAN Number"].ToString()))
                                        {
                                            string userName = dtEmps.Rows[i]["Name"].ToString();
                                            string mailID = dtEmps.Rows[i]["EmailID"].ToString();
                                            string PAN = dtEmps.Rows[i]["PAN Number"].ToString();
                                            try
                                            {
                                                if (!string.IsNullOrEmpty(txtsenderemail.Text))
                                                {
                                                    MailAddress SendFrom = new MailAddress(txtsenderemail.Text.Trim());
                                                    MailAddress SendTo = new MailAddress(mailID);
                                                    MailMessage MyMessage = new MailMessage(SendFrom, SendTo);
                                                    MyMessage.Subject = "Employee’s TDS Certificate of AY " + ddlassessyear.SelectedItem.Text + "";
                                                    StringBuilder MailBody = TextFile(userName, PAN);
                                                    MyMessage.Body = MailBody.ToString();
                                                    if (!string.IsNullOrEmpty(txtccemail.Text))
                                                    {
                                                        string strCC = txtccemail.Text.Trim();
                                                        string[] CCEmailids = strCC.Split(';');
                                                        foreach (string CCEmail in CCEmailids)
                                                        {
                                                            MyMessage.CC.Add(new MailAddress(CCEmail)); //Adding Multiple CC email Id
                                                        }
                                                    }
                                                    string fPath = string.Empty;
                                                    if (ddlsentto.SelectedItem.Text == "Vendor")
                                                    {
                                                        fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                        if (System.IO.File.Exists(fPath))
                                                        {
                                                            fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                        }
                                                        else
                                                        {
                                                            fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".pdf";
                                                        }
                                                    }
                                                    if (ddlsentto.SelectedItem.Text == "Employee")
                                                    {
                                                        fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                        if (System.IO.File.Exists(fPath))
                                                        {
                                                            fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                        }
                                                        else
                                                        {
                                                            fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".pdf";
                                                        }
                                                    }

                                                    if (System.IO.File.Exists(fPath))
                                                    {
                                                        System.Net.Mail.Attachment attachFile = new System.Net.Mail.Attachment(fPath);
                                                        MyMessage.Attachments.Add(attachFile);
                                                        SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                                                        try
                                                        {
                                                            emailClient.Send(MyMessage);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            ReminderBackupLog("Mail not sent to :" + mailID + " with PAN No : " + PAN);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        FileNotFoundLog("Existing file path not found: " + fPath + ", Mail ID:" + mailID);
                                                    }

                                                    btnsubmit.Enabled = false;
                                                    //pgbfilesuploaded.Value = i;
                                                    //pgbfilesuploaded.Update();
                                                    System.Threading.Thread.Sleep(1000);
                                                    int j = i + 1;
                                                    lblRecordsProcessed.Text = "Record " + j + "/" + Count + " being processed";
                                                    //lblRecordsProcessed.Update();
                                                    System.Threading.Thread.Sleep(1000);
                                                }
                                                else
                                                {
                                                    MessageBox.Show("Please enter sender EmialID");

                                                }
                                            }

                                            catch (Exception ex)
                                            {
                                                lblmessage.Text = ex.Message;
                                            }
                                        }

                                    }
                                    MessageBox.Show("Done");

                                }
                                else
                                {
                                    MessageBox.Show("Please Upload excel file");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Please enter path of files");
                            }
                        }
                        if (ddlsentto.SelectedItem.Text == "Vendor")
                        {
                            if (!string.IsNullOrEmpty(ddlquarter.SelectedItem.Text))
                            {
                                if (!string.IsNullOrEmpty(txtfilepath.Text))
                                {
                                    string filePath = uploadattach.PostedFile.FileName;
                                    string filename = Path.GetFileName(filePath);
                                    if (!string.IsNullOrEmpty(filename))
                                    {
                                        DataTable dtEmps = Data(filename);
                                        string[] columnNames = (from dc in dtEmps.Columns.Cast<DataColumn>()
                                                                select dc.ColumnName).ToArray();
                                        string Name = columnNames[0];
                                        string EmailID = columnNames[1];
                                        string PanNumber = columnNames[2];
                                        if (Name != "Name" || EmailID != "EmailID" || PanNumber != "PAN Number")
                                        {
                                            MessageBox.Show("Please ensure that the column names given in the excel sheet are as per the note");
                                            return;
                                        }
                                        int Count = 0;
                                        Count = dtEmps.Rows.Count;
                                        //pgbfilesuploaded.Minimum = 0;
                                        //pgbfilesuploaded.Maximum = Count;
                                        //pgbfilesuploaded.Value = 0;
                                        for (int i = 0; i < dtEmps.Rows.Count; i++)
                                        {
                                            if (!string.IsNullOrEmpty(dtEmps.Rows[i]["Name"].ToString()) && !string.IsNullOrEmpty(dtEmps.Rows[i]["EmailID"].ToString()) && !string.IsNullOrEmpty(dtEmps.Rows[i]["PAN Number"].ToString()))
                                            {
                                                string userName = dtEmps.Rows[i]["Name"].ToString();
                                                string mailID = dtEmps.Rows[i]["EmailID"].ToString();
                                                string PAN = dtEmps.Rows[i]["PAN Number"].ToString();
                                                try
                                                {
                                                    if (!string.IsNullOrEmpty(txtsenderemail.Text))
                                                    {
                                                        MailAddress SendFrom = new MailAddress(txtsenderemail.Text.Trim());
                                                        MailAddress SendTo = new MailAddress(mailID);

                                                        MailMessage MyMessage = new MailMessage(SendFrom, SendTo);
                                                        MyMessage.Subject = "TDS Certificate for : " + ddlquarter.SelectedItem.Text + " of AY " + ddlassessyear.SelectedItem.Text + "";
                                                        StringBuilder MailBody = TextFile(userName, PAN);
                                                        MyMessage.Body = MailBody.ToString();
                                                        if (!string.IsNullOrEmpty(txtccemail.Text))
                                                        {
                                                            string strCC = txtccemail.Text.Trim();
                                                            string[] CCEmailids = strCC.Split(';');
                                                            foreach (string CCEmail in CCEmailids)
                                                            {
                                                                MyMessage.CC.Add(new MailAddress(CCEmail)); //Adding Multiple CC email Id
                                                            }
                                                        }
                                                        string fPath = string.Empty;
                                                        if (ddlsentto.SelectedItem.Text == "Vendor")
                                                        {
                                                            fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                            if (System.IO.File.Exists(fPath))
                                                            {
                                                                fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                            }
                                                            else
                                                            {
                                                                fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlquarter.SelectedItem.Text + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".pdf";
                                                            }
                                                        }
                                                        if (ddlsentto.SelectedItem.Text == "Employee")
                                                        {
                                                            fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.ToString() + ".PDF";
                                                            if (System.IO.File.Exists(fPath))
                                                            {
                                                                fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".PDF";
                                                            }
                                                            else
                                                            {
                                                                fPath = txtfilepath.Text + "\\" + PAN + "_" + ddlassessyear.SelectedItem.Text.ToString() + ".pdf";
                                                            }
                                                        }
                                                        if (System.IO.File.Exists(fPath))
                                                        {
                                                            System.Net.Mail.Attachment attachFile = new System.Net.Mail.Attachment(fPath);
                                                            MyMessage.Attachments.Add(attachFile);
                                                            SmtpClient emailClient = new SmtpClient("tplmail.tpl.local");
                                                            try
                                                            {
                                                                emailClient.Send(MyMessage);
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                //ReminderBackupLog("Exception Occurs :" + userName + " with msg " + ex.Message);
                                                                ReminderBackupLog("Mail not sent to :" + mailID + " with PAN No : " + PAN);
                                                            }

                                                        }
                                                        else
                                                        {
                                                            FileNotFoundLog("Existing file path not found: " + fPath + ", Mail ID:" + mailID);
                                                        }
                                                        //Attachment attachFile = new Attachment(fPath);                      
                                                        //MyMessage.Attachments.Add(attachFile);                                               
                                                        btnsubmit.Enabled = false;
                                                        //pgbfilesuploaded.Value = i;
                                                        //pgbfilesuploaded.Update();
                                                        System.Threading.Thread.Sleep(1000);
                                                        int j = i + 1;
                                                        lblRecordsProcessed.Text = "Record " + j + "/" + Count + " being processed";
                                                        //lblRecordsProcessed.Update();
                                                        System.Threading.Thread.Sleep(1000);
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show("Please enter sender EmailID");
                                                        return;
                                                    }

                                                }
                                                catch (Exception ex)
                                                {
                                                    lblmessage.Text = ex.Message;
                                                }
                                            }
                                        }
                                        MessageBox.Show("Done");
                                    }
                                    else
                                    {
                                        MessageBox.Show("Please Upload excel file");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please enter path of files");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Please select quarter");
                            }

                        }
                    }
                    else
                    {
                        MessageBox.Show("Please select assesement year");
                    }
                }
                else
                {
                    MessageBox.Show("Please select sent to");
                }
            }
            else
            {
                MessageBox.Show("Please enter logs folder path");
            }

            string logCompleteText = "Completed on " + DateTime.Today.ToString("dd/MM/yyyy") + " at " + DateTime.Now.ToLongTimeString();
            ReminderBackupLog(logCompleteText);
            FileNotFoundLog(logCompleteText);
        }
        public void ReminderBackupLog(string message)
        {
            try
            {
                string PathName = txtfolderpath.Text;
                PathName = PathName + @"\EmailNotSent_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                StreamWriter sw = new StreamWriter(PathName, true);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception exp)
            {
                lblmessage.Text = exp.Message;
                MessageBox.Show("Not valid path");
            }
        }
        public void FileNotFoundLog(string message)
        {
            try
            {

                string PathName = txtfolderpath.Text;
                PathName = PathName + @"\FileNotFound_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                StreamWriter sw = new StreamWriter(PathName, true);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception exp)
            {
                lblmessage.Text = exp.Message;
                MessageBox.Show("Not valid path");
            }
        }
        public DataTable Data(String filePath)
        {
            DataTable dt = new DataTable();
            try
            {
                string MyString = string.Empty;
                System.Data.OleDb.OleDbConnection MyConnection;
                System.Data.DataSet DtSet;
                System.Data.OleDb.OleDbDataAdapter MyCommand;
                string extension;
                extension = Path.GetExtension(filePath);
                if (extension.Trim() == ".xls")
                {
                    MyString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                }
                else if (extension.Trim() == ".xlsx")
                {
                    MyString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                }
                MyConnection = new System.Data.OleDb.OleDbConnection(MyString);//Extended Properties=Excel 8.0;
                //MyConnection = new System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0; Data Source='" + filePath + "';Extended Properties=Excel 8.0;");//Extended Properties=Excel 8.0;
                MyCommand = new System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection);
                MyCommand.TableMappings.Add("Table", "TestTable");
                DtSet = new System.Data.DataSet();
                MyCommand.Fill(DtSet);
                dt = DtSet.Tables[0];
                MyConnection.Close();
            }
            catch (Exception ex)
            {
                lblmessage.Text = ex.Message;
            }
            return dt;

        }
        public StringBuilder TextFile(string Name, string PanNumber)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                string[] lines = null;
                if (ddlsentto.SelectedItem.Text == "Employee")
                {
                    lines = System.IO.File.ReadAllLines(Environment.CurrentDirectory + @"\Employee Mail Body.txt");
                    //lines = System.IO.File.ReadAllLines(@"C:\TPL\Ujwal\SivaLaxmi\Finance Communicator\Employee Mail Body.txt");
                }
                if (ddlsentto.SelectedItem.Text == "Vendor")
                {
                    lines = System.IO.File.ReadAllLines(Environment.CurrentDirectory + @"\Vendor  Mail Body.txt");
                    //lines = System.IO.File.ReadAllLines(@"C:\TPL\Ujwal\SivaLaxmi\Finance Communicator\Vendor  Mail Body.txt");
                }
                string content = string.Empty;
                foreach (string line in lines)
                {
                    content = line;
                    if (content.Contains("<%EmpName%>"))
                    {
                        content = content.Replace("<%EmpName%>", Name);
                    }
                    if (content.Contains("<%AssesementYear%>"))
                    {
                        if (ddlassessyear.SelectedItem.Text != "---Select---")
                        {
                            content = content.Replace("<%AssesementYear%>", ddlassessyear.SelectedItem.Text);
                        }
                        else
                        {
                            MessageBox.Show("Please select  Assesement Year");
                        }
                    }
                    if (content.Contains("<%Quater%>"))
                    {
                        if (ddlsentto.SelectedItem.Text == "Vendor")
                        {
                            if (ddlquarter.SelectedItem.Text != "---Select---")
                            {
                                content = content.Replace("<%Quater%>", ddlquarter.SelectedItem.Text);
                            }
                            else
                            {
                                MessageBox.Show("Please select quarter");
                            }
                        }
                    }
                    if (content.Contains("<%PAN%>"))
                    {
                        content = content.Replace("<%PAN%>", PanNumber);
                    }
                    if (string.IsNullOrEmpty(line))
                    {
                        sb.Append(Environment.NewLine);
                        sb.Append(Environment.NewLine);
                        // sb.Append(Environment.NewLine);
                    }
                    else
                    {
                        sb.Append(content);
                    }
                }
            }
            catch (Exception ex)
            {
                lblmessage.Text = ex.Message;
            }
            return sb;
        }
    }
}

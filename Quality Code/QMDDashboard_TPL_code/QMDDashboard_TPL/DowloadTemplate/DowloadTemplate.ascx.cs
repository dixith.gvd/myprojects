﻿using Microsoft.SharePoint;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace QMDDashboard_TPL.DowloadTemplate
{
    [ToolboxItemAttribute(false)]
    public partial class DowloadTemplate : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public DowloadTemplate()
        {
        }




        DataTable dtTemp = new DataTable();
        QMDBAL objBal = new QMDBAL();
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ddlTemplate.Attributes.Add("onchange", "waitMessage();");
            ddlNewExsiting.Attributes.Add("onchange", "waitMessage();");
            ddlProjectName.Attributes.Add("onchange", "waitMessage();");
            ddlPeriod.Attributes.Add("onchange", "waitMessage();");
            //btnDownload.Attributes.Add("onclick", "waitMessage();");
            btnDownload.UseSubmitBehavior = false;
            btnDownload.OnClientClick = "_spFormOnSubmitCalled = false;_spSuppressFormOnSubmitWrapper=true";
            if (!Page.IsPostBack)
            {
                BindTemplateDataList();

            }

            //btnDownload.Visible = false;
            btnDownload.Attributes.Add("style", "display: none;");
            lblError.Text = string.Empty;
            //lblError.Visible = false;
            lblError.Attributes.Add("style", "display: none;");
        }
        private void BindTemplateDataList()
        {
            try
            {
                SPUserToken adminToken = SPContext.Current.Site.SystemAccount.UserToken;
                using (SPSite site = new SPSite(SPContext.Current.Site.Url, adminToken))
                {
                    using (SPWeb objSPWeb = site.OpenWeb())
                    {
                        SPList oList = SPContext.Current.Web.Lists[Constants.TemplateList];
                        SPQuery query = new SPQuery();//Template
                        query.Query = "<OrderBy><FieldRef Name='Template' /></OrderBy><Where><And><Eq><FieldRef Name='Status' /><Value Type='Choice'>Active</Value></Eq><IsNotNull><FieldRef Name='Template' /></IsNotNull></And></Where>";

                        DataTable dtcamltest = oList.GetItems(query).GetDataTable();
                        DataView dtview = new DataView(dtcamltest);
                        DataTable dtdistinct = dtview.ToTable(true, "Template", "TemplateNo");
                        if (dtdistinct != null && dtdistinct.Rows.Count > 0)
                        {
                            ddlTemplate.DataSource = dtdistinct;
                            ddlTemplate.DataTextField = "Template";
                            ddlTemplate.DataValueField = "TemplateNo";
                            ddlTemplate.DataBind();
                            ddlTemplate.Items.Insert(0, new ListItem("--Select--", "0"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

        }

        protected void ddlTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTemplate.SelectedIndex > 0)
                {
                    BindProjectsData(ddlTemplate.SelectedItem.Value.Trim());
                }
                else
                {
                    ddlForeNight.Attributes.Add("style", "display: none;");
                    lblForeNight.Attributes.Add("style", "display: none;");
                    lblDateofRating.Attributes.Add("style", "display: none;");
                    ddlNewExsiting.Attributes.Add("style", "display: none;");
                    lbNewExsiting.Attributes.Add("style", "display: none;");
                    ddlPeriodofRating.Attributes.Add("style", "display: none;");
                    lblPeriodofRating.Attributes.Add("style", "display: none;");
                    ddlProjectName.Attributes.Add("style", "display: none;");
                    lblProjectName.Attributes.Add("style", "display: none;");
                    lblPeriod.Attributes.Add("style", "display: none;");
                    ddlPeriod.Attributes.Add("style", "display: none;");
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "msg", "closeWaitMessage();", true);
            }
        }

        //protected void ddlNewExsiting_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlNewExsiting.SelectedIndex > 0)
        //        {
        //            ddlProjectName.Attributes.Add("style", "display: block;");
        //            lblProjectName.Attributes.Add("style", "display: block;");
        //            if (ddlNewExsiting.SelectedItem.Text == Constants.Exsiting)
        //            {
        //                string TemplateName = Constants.NCExisting;
        //                DataTable dtExstingData = objBal.GetExsitingData(TemplateName);
        //            }
        //            else
        //            {

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "msg", "closeWaitMessage();", true);
        //    }
        //}

        protected void ddlProjectName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlProjectName.SelectedIndex > 0)
                {
                    BindPeriodsData(ddlTemplate.SelectedItem.Value.Trim(), ddlProjectName.SelectedValue.Trim());
                    //if (ddlNewExsiting.SelectedIndex < 0)
                    //{
                    //ddlPeriod.Attributes.Add("style", "display: none;");
                    //lblPeriod.Attributes.Add("style", "display: none;");
                    //}
                }
                else
                {
                    ddlPeriod.Attributes.Add("style", "display: none;");
                    lblPeriod.Attributes.Add("style", "display: none;");
                    ddlNewExsiting.Attributes.Add("style", "display: none;");
                    lbNewExsiting.Attributes.Add("style", "display: none;");
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "msg", "closeWaitMessage();", true);
            }
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlPeriod.SelectedItem.Text != "--Select--")
                    btnDownload.Attributes.Add("style", "display: block;");
            }
            catch (Exception ex)
            {
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "msg", "closeWaitMessage();", true);
            }
        }

        private void BindProjectsData(string TemplateName)
        {
            try
            {
                string userName = SPContext.Current.Web.CurrentUser.Name;
                SPUserToken adminToken = SPContext.Current.Site.SystemAccount.UserToken;
                using (SPSite site = new SPSite(SPContext.Current.Site.Url, adminToken))
                {
                    using (SPWeb objSPWeb = site.OpenWeb())
                    {
                        SPList projectList = SPContext.Current.Web.Lists[Constants.ListProjectCodProjectName];
                        SPQuery queryProjectList = new SPQuery();
                        queryProjectList.Query = @"<Where><And><And><Eq><FieldRef Name='TemplateNo' /><Value Type='Number'>" + ddlTemplate.SelectedValue.ToString() + "</Value></Eq><Eq><FieldRef Name='EmployeeName' /><Value Type='User'>" + userName + "</Value></Eq></And><Eq><FieldRef Name='Active' /><Value Type='Boolean'>1</Value></Eq></And></Where>";
                        
                        DataTable dtProjectList = projectList.GetItems(queryProjectList).GetDataTable();

                        if (dtProjectList != null && dtProjectList.Rows.Count > 0)
                        {
                            ddlProjectName.DataSource = dtProjectList;
                            ddlProjectName.DataTextField = "ProjectName";
                            ddlProjectName.DataValueField = "ProjectCode";
                            ddlProjectName.DataBind();
                            ddlProjectName.Items.Insert(0, new ListItem("--Select--", "0"));

                            ddlProjectName.Attributes.Add("style", "display: block;");
                            lblProjectName.Attributes.Add("style", "display: block;");
                            ddlForeNight.Attributes.Add("style", "display: none;");
                            lblForeNight.Attributes.Add("style", "display: none;");
                            lblDateofRating.Attributes.Add("style", "display: none;");
                            ddlNewExsiting.Attributes.Add("style", "display: none;");
                            lbNewExsiting.Attributes.Add("style", "display: none;");
                            ddlPeriodofRating.Attributes.Add("style", "display: none;");
                            lblPeriodofRating.Attributes.Add("style", "display: none;");
                            lblPeriod.Attributes.Add("style", "display: none;");
                            ddlPeriod.Attributes.Add("style", "display: none;");

                        }
                        else
                        {
                            lblError.Text = "There are no projects assigned for the user!";
                            lblError.Attributes.Add("style", "display: block;");
                            lblError.ForeColor = Color.Red;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void BindPeriodsData(string TemplateNo, string ProjectName)
        {
            try
            {
                SPUserToken adminToken = SPContext.Current.Site.SystemAccount.UserToken;
                using (SPSite site = new SPSite(SPContext.Current.Site.Url, adminToken))
                {
                    using (SPWeb objSPWeb = site.OpenWeb())
                    {
                        DataTable dt = new DataTable();
                        string ConnectionString = Constants.ConnectionString;
                        SqlConnection con = new SqlConnection(ConnectionString);
                        //SqlDataAdapter da = new SqlDataAdapter("select [Template Name],[Template No],[Project Code],[Project Name],[Approved] from Transactions where [Approved] ='Y' and [Project Code]=' " + ProjectName + "' and [Template No]='" + TemplateNo + "'", con);
                        SqlDataAdapter da = new SqlDataAdapter("select [Template Name],[Period],[Template No],[Project Code],[Project Name],[Approved] from Transactions where [Approved] ='N' and [Project Code]='" + ProjectName + "' and [Template No]='" + TemplateNo + "' ", con);

                        DataSet ds = new DataSet();
                        con.Open();
                        da.Fill(ds);
                        if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        {
                            dt = ds.Tables[0];
                            ddlPeriod.DataSource = dt;
                            ddlPeriod.DataTextField = "Period";
                            ddlPeriod.DataValueField = "Period";
                            ddlPeriod.DataBind();
                            ddlPeriod.Items.Insert(0, new ListItem("--Select--", "0"));
                            ddlPeriod.Attributes.Add("style", "display: block;");
                            lblPeriod.Attributes.Add("style", "display: block;");
                            btnDownload.Attributes.Add("style", "display: none;");
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            DataTable dtItem = new DataTable();
            DataTable dtSBU = new DataTable();
            try
            {
                bool Status = false;
                string Template = string.Empty;

                if (ddlTemplate.SelectedIndex > 0)
                {
                    //Template = ddlTemplate.SelectedItem.Text.Trim().ToString();

                    ///Getting Template No
                    Template = ddlTemplate.SelectedItem.Value.Trim().ToString();


                    #region [ Get File from Doc Library]
                    string path = objBal.GetFilefromDocumetLib(Template);
                    #endregion

                    #region [Acessing and writing data to Excel File from Temp Location]

                    if (path != Constants.FilenotFound)
                    {
                        DataTable dtPrevFNDetails = new DataTable();
                        dtItem.Columns.Add("ProjectName", typeof(string));
                        dtItem.Columns.Add("Period", typeof(string));
                        dtItem.Columns.Add("SBU", typeof(string));
                        dtItem.Columns.Add("Project Code", typeof(string));
                        dtItem.Columns.Add("MRRs_IMIRs_Rectifiable_Backlog", typeof(int));
                        dtItem.Columns.Add("MRRs_IMIRs_NonRectifiable_Backlog", typeof(int));
                        dtItem.Columns.Add("MRRs_IMIRs_NonRectifiable-Rejected_Backlog", typeof(int));
                        dtItem.Columns.Add("FINs_Backlog", typeof(int));
                        dtItem.Columns.Add("NCRs_FORs_Backlog", typeof(int));
                        dtItem.Columns.Add("CostToTplForNCRs_FORs_Backlog", typeof(int));
                        dtItem.Columns.Add("NoCostToTPLForNCRs_FORs_Backlog", typeof(int));
                        dtItem.Columns.Add("Customer");
                        if (ddlProjectName.SelectedIndex > 0)
                        {

                        }
                        //Checking CC/NCR template condition
                        string ISExsiting = string.Empty;
                        if (ddlNewExsiting.SelectedIndex > 0)
                        {
                            ISExsiting = ddlNewExsiting.SelectedItem.Text;
                        }
                        if (dtItem != null && dtItem.Rows.Count > 0)
                        {
                            //Updating the document with auto populated fields
                            Status = objBal.UpdateTheDocument(path, dtItem, Template, ISExsiting, ddlProjectName.SelectedItem.Text.Trim().ToString(), ddlProjectName.SelectedItem.Value.Trim().ToString());
                            if (!Status)
                            {
                                lblError.Text = "There are no existing data";
                            }
                        }
                        else
                            Status = false;
                    }

                    #endregion

                    objBal.DownloadFinalTemplate(path);

                    #region [Deleting the File from Temporary Location ]

                    #endregion
                    ddlTemplate.SelectedIndex = 0;
                    ddlProjectName.SelectedIndex = 0;
                    ddlPeriod.SelectedIndex = 0;
                }
                else
                {
                    //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select Template!');", true);
                }

            }
            catch (Exception ex)
            {
                lblError.Attributes.Add("style", "display: block;");
                lblError.Text = ex.Message;
            }
            finally
            {
                dtItem.Dispose();
                dtSBU.Dispose();
            }
        }
    }
}

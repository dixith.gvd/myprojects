﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Collections;
using Microsoft.SharePoint.Client;
using System.Security;
using System.Net;
namespace QualityEscalationsNew
{
    class Escalations
    {
        public void StartProcess()
        {
            //ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            pendingWithRCM();
            pendingWithPE();
            pendingWithAssignee();
            pendingWithRCM7Days();
            pendingWithPE7days();
            pendingWithAssignee3days();
            pendingWithRCM15Days();
            pendingWithPE15days();
            pendingWithAssignee7days();
            pendingWithAssignee15days();
        }
        public void pendingWithRCM()
        {
            DataTable pendingrcm = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select PM_Email, PM_Name, count(*) from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with RCM' and DATEDIFF(d,LastStageUpdate,GetDate())=2 and PM_Email is not null group by PM_Email, PM_Name";
                string sqlQuery = "Select PM_Email, count(*), A.ProjectCode from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with RCM' and A.LastStageUpdate<GETDATE()-1 and DATEDIFF(d,A.LastStageUpdate,GetDate())=2 and B.[Quality Active]=1 and A.PM_Email is not null group by A.PM_Email,A.ProjectCode";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingrcm);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingrcm, "RCM", 2);
        }

        public void pendingWithRCM7Days()
        {
            DataTable pendingrcm = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select HOP_Email, HOP_Name, count(*) from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with RCM' and DATEDIFF(d,LastStageUpdate,GetDate())>7 and DATEDIFF(d,LastStageUpdate,GetDate())%3=0 group by HOP_Email, HOP_Name";
                string sqlQuery = "Select HOP_Email, count(*),A.ProjectCode from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with RCM' and A.LastStageUpdate<GETDATE()-1 and DATEDIFF(d,A.LastStageUpdate,GetDate())>7 and DATEDIFF(d,A.LastStageUpdate,GetDate())%3=0 and B.[Quality Active]=1 group by A.HOP_Email,A.ProjectCode, A.ProjectCode";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingrcm);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingrcm, "RCM", 7);
        }

        public void pendingWithRCM15Days()
        {
            DataTable pendingrcm = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select BUHead_Email, BUHead_Name, count(*) from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with RCM' and DATEDIFF(d,LastStageUpdate,GetDate())%15=0 group by BUHead_Email, BUHead_Name";
                string sqlQuery = "Select BUHead_Email, count(*),A.ProjectCode from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with RCM' and A.LastStageUpdate<GETDATE()-1 and DATEDIFF(d,A.LastStageUpdate,GetDate())%15=0  and B.[Quality Active]=1 group by A.BUHead_Email, A.ProjectCode";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingrcm);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingrcm, "RCM", 15);
        }

        public DataTable PendingRecordsRCM(string pmemail, string pcode)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select * from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with RCM' and LastStageUpdate<GETDATE()-1 and DATEDIFF(d,LastStageUpdate,GetDate())=2 and PM_Email='" + pmemail + "' and ProjectCode='" + pcode + "'";
                //string sqlQuery = "Select Transaction_ID,Stage,Compliance_Type,BUName,ProjectName,ProjectType,Discipline,Activity,[Sub Activity],FQE_Submitted_Date,isCompliant,RCM_Email,RCM_Name from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code]  where A.Stage = 'Pending with RCM' and DATEDIFF(d,A.LastStageUpdate,GetDate())=2 and B.[Quality Active]=1 and A.PM_Email='" + pmemail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        public DataTable PendingRecordsRCM7days(string hopemail, string pcode)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select * from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with RCM' and LastStageUpdate<GETDATE()-1 and DATEDIFF(d,LastStageUpdate,GetDate())>7 and DATEDIFF(d,LastStageUpdate,GetDate())%3=0 and HOP_Email='" + hopemail + "' and ProjectCode='" + pcode + "'";
                //string sqlQuery = "Select Transaction_ID,Stage,Compliance_Type,BUName,ProjectName,ProjectType,Discipline,Activity,[Sub Activity],FQE_Submitted_Date,isCompliant,RCM_Email,RCM_Name from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with RCM' and DATEDIFF(d,A.LastStageUpdate,GetDate())>7 and DATEDIFF(d,LastStageUpdate,GetDate())%3=0 and B.[Quality Active]=1 and HOP_Email='" + hopemail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        public DataTable PendingRecordsRCM15days(string buheademail, string pcode)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select * from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with RCM' and LastStageUpdate<GETDATE()-1 and DATEDIFF(d,LastStageUpdate,GetDate())%15=0 and BUHead_Email='" + buheademail + "' and ProjectCode='" + pcode + "'";
                //string sqlQuery = "Select Transaction_ID,Stage,Compliance_Type,BUName,ProjectName,ProjectType,Discipline,Activity,[Sub Activity],FQE_Submitted_Date,isCompliant,RCM_Email,RCM_Name from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with RCM' and DATEDIFF(d,A.LastStageUpdate,GetDate())%15=0 and B.[Quality Active]=1 and A.BUHead_Email='" + buheademail + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        public void pendingWithAssignee()
        {
            DataTable pendingassignee = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select RCM_Email, RCM_Name, count(*),A.ProjectCode from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with Assignee' and Target_Closure_Date < GETDATE() and DATEDIFF(d,LastStageUpdate,GetDate())=2 group by RCM_Email, RCM_Name";
                string sqlQuery = "Select RCM_Email, count(*),A.ProjectCode from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with Assignee' and A.Target_Closure_Date < GETDATE()-1 and DATEDIFF(d,A.Target_Closure_Date,GetDate())=2 and B.[Quality Active]=1 group by A.RCM_Email, A.ProjectCode";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingassignee);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingassignee, "Assignee", 2);
        }

        public void pendingWithAssignee3days()
        {
            DataTable pendingassignee = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select PM_Email, PM_Name, count(*),A.ProjectCode from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with Assignee' and Target_Closure_Date < GETDATE() and DATEDIFF(d,LastStageUpdate,GetDate())%3=0 group by PM_Email, PM_Name";
                string sqlQuery = "Select PM_Email, count(*),A.ProjectCode from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with Assignee' and A.Target_Closure_Date < GETDATE()-1 and B.[Quality Active]=1 and DATEDIFF(d,A.Target_Closure_Date,GetDate())%3=0 group by A.PM_Email, A.ProjectCode";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingassignee);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingassignee, "Assignee", 3);
        }

        public void pendingWithAssignee7days()
        {
            DataTable pendingassignee = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select HOP_Email, HOP_Name, count(*),A.ProjectCode from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with Assignee' and Target_Closure_Date < GETDATE() and DATEDIFF(d,LastStageUpdate,GetDate())%7=0 group by HOP_Email, HOP_Name";
                string sqlQuery = "Select HOP_Email, count(*),A.ProjectCode from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with Assignee' and A.Target_Closure_Date < GETDATE()-1 and B.[Quality Active]=1 and DATEDIFF(d,A.Target_Closure_Date,GetDate())%7=0 group by A.HOP_Email, A.ProjectCode";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingassignee);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingassignee, "Assignee", 7);
        }

        public void pendingWithAssignee15days()
        {
            DataTable pendingassignee = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select BUHead_Email, BUHead_Name, count(*),A.ProjectCode from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with Assignee' and Target_Closure_Date < GETDATE() and DATEDIFF(d,LastStageUpdate,GetDate())%15=0 group by BUHead_Email, BUHead_Name";
                string sqlQuery = "Select BUHead_Email, count(*),A.ProjectCode from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with Assignee' and A.Target_Closure_Date < GETDATE()-1 and B.[Quality Active]=1 and DATEDIFF(d,A.Target_Closure_Date,GetDate())%15=0 group by A.BUHead_Email, A.ProjectCode";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingassignee);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingassignee, "Assignee", 15);
        }

        public DataTable PendingRecordsAssignee(string rcmemail, string pcode)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select * from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with Assignee' and Target_Closure_Date < GETDATE()-1 and DATEDIFF(d,Target_Closure_Date,GetDate())=2 and RCM_Email='" + rcmemail + "' and ProjectCode='" + pcode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        public DataTable PendingRecordsAssignee3days(string pmemail, string pcode)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select * from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with Assignee' and Target_Closure_Date < GETDATE()-1 and DATEDIFF(d,Target_Closure_Date,GetDate())%3=0 and PM_Email='" + pmemail + "' and ProjectCode='" + pcode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        public DataTable PendingRecordsAssignee7days(string hopemail, string pcode)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select * from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with Assignee' and Target_Closure_Date < GETDATE()-1 and DATEDIFF(d,Target_Closure_Date,GetDate())%7=0 and HOP_Email='" + hopemail + "' and ProjectCode='" + pcode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        public DataTable PendingRecordsAssignee15days(string buheademail, string pcode)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select * from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with Assignee' and Target_Closure_Date < GETDATE()-1 and DATEDIFF(d,Target_Closure_Date,GetDate())%15=0 and BUHead_Email='" + buheademail + "' and ProjectCode='" + pcode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        public void pendingWithPE()
        {
            DataTable pendingpe = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select RCM_Email,RCM_Name,count(*),A.ProjectCode,PM_Email from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with PE' and DATEDIFF(d,LastStageUpdate,GetDate())=2 group by RCM_Email,RCM_Name,PM_Email";
                string sqlQuery = "Select RCM_Email,count(*),PM_Email, A.ProjectCode from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with PE' and A.LastStageUpdate<GETDATE()-1 and DATEDIFF(d,A.LastStageUpdate,GetDate())=2 and B.[Quality Active]=1 group by A.RCM_Email,A.PM_Email, A.ProjectCode";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingpe);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingpe, "PE", 2);
        }

        public void pendingWithPE7days()
        {
            DataTable pendingpe = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select HOP_Email, HOP_Name, count(*) from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with PE' and DATEDIFF(d,LastStageUpdate,GetDate())>7 and DATEDIFF(d,LastStageUpdate,GetDate())%3=0 group by HOP_Email, HOP_Name";
                string sqlQuery = "Select HOP_Email, count(*), A.ProjectCode from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with PE' and A.LastStageUpdate<GETDATE()-1 and DATEDIFF(d,A.LastStageUpdate,GetDate())>7 and DATEDIFF(d,A.LastStageUpdate,GetDate())%3=0 and B.[Quality Active]=1 group by A.HOP_Email, A.ProjectCode";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingpe);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingpe, "PE", 7);
        }

        public void pendingWithPE15days()
        {
            DataTable pendingpe = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                //string sqlQuery = "Select BUHead_Email, BUHead_Name, count(*) from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with PE' and DATEDIFF(d,LastStageUpdate,GetDate())%15=0 group by BUHead_Email, BUHead_Name";
                string sqlQuery = "Select BUHead_Email, count(*), A.ProjectCode from [dbo].[QChecklist_TnxMain] as A FULL JOIN [dbo].[Monitored_Projects] as B ON A.ProjectCode=B.[Project Code] where A.Stage = 'Pending with PE' and A.LastStageUpdate<GETDATE()-1 and DATEDIFF(d,A.LastStageUpdate,GetDate())%15=0 and B.[Quality Active]=1 group by A.BUHead_Email, A.ProjectCode";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingpe);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            SendMailList(pendingpe, "PE", 15);
        }

        public DataTable PendingRecordsPE(string rcmemail, string pcode)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select * from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with PE' and LastStageUpdate<GETDATE()-1 and DATEDIFF(d,LastStageUpdate,GetDate())=2 and RCM_Email='" + rcmemail + "' and ProjectCode='" + pcode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        public DataTable PendingRecordsPE7days(string hopmail, string pcode)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select * from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with PE' and LastStageUpdate<GETDATE()-1 and DATEDIFF(d,LastStageUpdate,GetDate())>7 and DATEDIFF(d,LastStageUpdate,GetDate())%3=0 and HOP_Email='" + hopmail + "' and ProjectCode='" + pcode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        public DataTable PendingRecordsPE15days(string buheademail, string pcode)
        {
            DataTable pendingRecords = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                con.Open();
                string sqlQuery = "Select * from [dbo].[QChecklist_TnxMain] where Stage = 'Pending with PE' and LastStageUpdate<GETDATE()-1 and DATEDIFF(d,LastStageUpdate,GetDate())%15=0 and BUHead_Email='" + buheademail + "' and ProjectCode='" + pcode + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(pendingRecords);
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return pendingRecords;
        }

        string CCMailQualityII()
        {
            string CCMailQuality = ConfigurationManager.AppSettings["CCQualityEmailListII"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(CCMailQuality))
            {
                string strTo = CCMailQuality.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    if (!MyMessage.CC.Contains(new MailAddress(ccEmail)))
                    {
                        MyMessage.CC.Add(ccEmail);
                    }
                }
            }
            return MyMessage.CC.ToString();
        }
        string CCMailQualityUI()
        {
            string CCMailQuality = ConfigurationManager.AppSettings["CCQualityEmailListUI"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(CCMailQuality))
            {
                string strTo = CCMailQuality.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    if (!MyMessage.CC.Contains(new MailAddress(ccEmail)))
                    {
                        MyMessage.CC.Add(ccEmail);
                    }
                }
            }
            return MyMessage.CC.ToString();
        }

        string DaysEscalationII()
        {
            string CCMailQuality = ConfigurationManager.AppSettings["7DaysEscalationII"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(CCMailQuality))
            {
                string strTo = CCMailQuality.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    if (!MyMessage.CC.Contains(new MailAddress(ccEmail)))
                    {
                        MyMessage.CC.Add(ccEmail);
                    }
                }
            }
            return MyMessage.CC.ToString();
        }

        string DaysEscalationUI()
        {
            string CCMailQuality = ConfigurationManager.AppSettings["7DaysEscalationUI"].ToString();
            MailMessage MyMessage = new MailMessage();
            if (!string.IsNullOrEmpty(CCMailQuality))
            {
                string strTo = CCMailQuality.Trim();
                string[] CCEmailids = strTo.Split(';');
                foreach (string ccEmail in CCEmailids)
                {
                    if (!MyMessage.CC.Contains(new MailAddress(ccEmail)))
                    {
                        MyMessage.CC.Add(ccEmail);
                    }
                }
            }
            return MyMessage.CC.ToString();
        }
        public string GetFQEMailIds(string pcode)
        {
            DataTable dt = new DataTable();
            string value = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                string sqlQuery = "select [Field Quality Engineer] from Monitored_Projects where [Project Code]='" + pcode + "' and [Field Quality Engineer] is not null";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Field Quality Engineer"] != null)
                        {
                            value = dt.Rows[0]["Field Quality Engineer"].ToString();
                        }
                        else
                        {
                        }
                    }
                }
            }
            catch (Exception exp)
            {

            }
            return value;
        }
        public string GetEmailIds(string key, string buname)
        {
            DataTable dt = new DataTable();
            string value = string.Empty;
            try
            {
                SqlConnection con = new SqlConnection(Constants.ConnectionStringQualityUploadDetails);
                string sqlQuery = "select Value_Escalation from EmailList_Quality where [key]='" + key + "' and BU_Name='" + buname + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
                sda.Fill(dt);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[0]["Value_Escalation"] != null)
                        {
                            value = dt.Rows[0]["Value_Escalation"].ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {

            }
            return value;
        }
        public void AddCC(string ccMail, MailMessage msg)
        {
            if (!string.IsNullOrEmpty(ccMail))
            {
                string strCC = ccMail.Trim();
                string[] CCEmailids = strCC.Split(';');
                foreach (string CCEmail in CCEmailids)
                {
                    if (!msg.CC.Contains(new MailAddress(CCEmail)))
                    {
                        msg.CC.Add(new MailAddress(CCEmail));
                    }
                }
            }
        }
        public void AddCCBUWise(DataTable ds, MailMessage msg)
        {
            if (ds.Rows[0]["BUName"].ToString() != "")
            {
                string mailids = GetEmailIds("Quality", ds.Rows[0]["BUName"].ToString());
                AddCC(mailids, msg);
            }

        }
        public void AddCCProjectWise(string pcode, MailMessage msg)
        {
            string fqeEmails = GetFQEMailIds(pcode);
            AddCC(fqeEmails, msg);
        }
        private string GetName(string emailID)
        {
            string name = string.Empty;

                
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var targetSite = new Uri("https://tataprojects4.sharepoint.com/sites/TPLHome/");
                var login = "defspuser@tataprojects4.onmicrosoft.com";
                var password = "D3faultTplU5er";
                var securePassword = new SecureString();
                foreach (char c in password)
                {
                    securePassword.AppendChar(c);
                }

                var onlineCredentials = new SharePointOnlineCredentials(login, securePassword);
                using (ClientContext clientContext = new ClientContext(targetSite))
                {
                    clientContext.Credentials = onlineCredentials;
                    User user = clientContext.Web.EnsureUser(emailID);
                    clientContext.Load(user);
                    clientContext.ExecuteQuery();
                    name = user.Title;
                }
            
            return name;
        }
        public void SendMailList(DataTable pending, string role, int days)
        {
            //string CCMailQuality = ConfigurationManager.AppSettings["CCQMDCQSH"].ToString();
            MailAddress SendFrom = new MailAddress("coeqii@tataprojects.com");
            SmtpClient emailClient = new SmtpClient();
            emailClient.UseDefaultCredentials = false;
            emailClient.Credentials = new System.Net.NetworkCredential("coeqii@tataprojects.com", "tata@1237");
            emailClient.Host = "smtp.office365.com";
            emailClient.Port = 25;
            emailClient.EnableSsl = true;
            emailClient.TargetName = "STARTTLS/smtp.office365.com";
            emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            DataTable dt = pending;

            MailMessage MyMessage = new MailMessage();

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    DataTable ds = null;
                    if (row[0].ToString() != "")
                    {
                        string empname = GetName(row[0].ToString());
                        if (role == "RCM")
                        {
                            if (days == 2)
                            {
                                ds = PendingRecordsRCM(row[0].ToString(), row[2].ToString());
                                //MyMessage.CC.Add("coeqii@tataprojects.com");
                                //MyMessage.CC.Add(ds.Rows[0]["FQE_Email"].ToString());
                                AddCCProjectWise(row[2].ToString(), MyMessage);
                                AddCCBUWise(ds, MyMessage);
                            }
                            if (days == 7)
                            {
                                ds = PendingRecordsRCM7days(row[0].ToString(), row[2].ToString());
                                //MyMessage.CC.Add(ds.Rows[0]["FQE_Email"].ToString());
                                AddCCProjectWise(row[2].ToString(), MyMessage);
                                AddCCBUWise(ds, MyMessage);
                                foreach (DataRow email in ds.Rows)
                                {
                                    if (email["PM_Email"] != "" || email["PM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["PM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["PM_Email"].ToString());
                                        }
                                    }
                                }
                            }
                            if (days == 15)
                            {
                                ds = PendingRecordsRCM15days(row[0].ToString(), row[2].ToString());
                                //MyMessage.CC.Add(ds.Rows[0]["FQE_Email"].ToString());
                                AddCCProjectWise(row[2].ToString(), MyMessage);
                                MyMessage.CC.Add("rrsankar@tataprojects.com");
                                MyMessage.CC.Add("gvacharyulu@tataprojects.com");
                                AddCCBUWise(ds, MyMessage);
                                foreach (DataRow email in ds.Rows)
                                {
                                    if (email["PM_Email"] != "" || email["PM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["PM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["PM_Email"].ToString());
                                        }
                                    }
                                    if (email["HOP_Email"] != "" || email["HOP_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["HOP_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["HOP_Email"].ToString());
                                        }
                                    }
                                }
                            }
                            foreach (DataRow email in ds.Rows)
                            {
                                if (email["RCM_Email"] != null)
                                {
                                    if (email["RCM_Email"] != "" || email["RCM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["RCM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["RCM_Email"].ToString());
                                        }
                                    }
                                }
                            }
                            MyMessage.To.Add(row[0].ToString());
                            MyMessage.From = SendFrom;
                            MyMessage.Subject = "Escalation on QPRC / FQPC";
                            MyMessage.Body = FormMailBodyQualityRCM(empname, row[1].ToString(), ds, role, days);
                            MyMessage.IsBodyHtml = true;
                            //emailClient.Send(MyMessage);
                            MyMessage.To.Clear();
                            MyMessage.CC.Clear();
                        }
                        else if (role == "PE")
                        {
                            if (days == 2)
                            {
                                ds = PendingRecordsPE(row[0].ToString(), row[3].ToString());
                                MyMessage.CC.Add(row[2].ToString());
                                //MyMessage.CC.Add("coeqii@tataprojects.com");
                                //MyMessage.CC.Add(ds.Rows[0]["FQE_Email"].ToString());
                                AddCCProjectWise(row[3].ToString(), MyMessage);
                                AddCCBUWise(ds, MyMessage);
                            }
                            if (days == 7)
                            {
                                ds = PendingRecordsPE7days(row[0].ToString(), row[2].ToString());
                                //MyMessage.CC.Add(ds.Rows[0]["FQE_Email"].ToString());
                                AddCCProjectWise(row[2].ToString(), MyMessage);
                                AddCCBUWise(ds, MyMessage);
                                foreach (DataRow email in ds.Rows)
                                {
                                    if (email["RCM_Email"] != "" || email["RCM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["RCM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["RCM_Email"].ToString());
                                        }
                                    }
                                    if (email["PM_Email"] != "" || email["PM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["PM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["PM_Email"].ToString());
                                        }
                                    }
                                }
                            }
                            if (days == 15)
                            {
                                ds = PendingRecordsPE15days(row[0].ToString(), row[2].ToString());
                                //MyMessage.CC.Add(ds.Rows[0]["FQE_Email"].ToString());
                                AddCCProjectWise(row[2].ToString(), MyMessage);
                                MyMessage.CC.Add("rrsankar@tataprojects.com");
                                MyMessage.CC.Add("gvacharyulu@tataprojects.com");
                                AddCCBUWise(ds, MyMessage);
                                foreach (DataRow email in ds.Rows)
                                {
                                    if (email["RCM_Email"] != "" || email["RCM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["RCM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["RCM_Email"].ToString());
                                        }
                                    }
                                    if (email["PM_Email"] != "" || email["PM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["PM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["PM_Email"].ToString());
                                        }
                                    }
                                    if (email["HOP_Email"] != "" || email["HOP_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["HOP_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["HOP_Email"].ToString());
                                        }
                                    }
                                }
                            }
                            foreach (DataRow email in ds.Rows)
                            {
                                if (email["PE_Email"] != "" || email["PE_Email"] != string.Empty)
                                {
                                    if (!MyMessage.CC.Contains(new MailAddress(email["PE_Email"].ToString())))
                                    {
                                        MyMessage.CC.Add(email["PE_Email"].ToString());
                                    }
                                }
                                else
                                {
                                    if (!MyMessage.CC.Contains(new MailAddress(email["RCM_Email"].ToString())))
                                    {
                                        MyMessage.CC.Add(email["RCM_Email"].ToString());
                                    }
                                }
                            }
                            MyMessage.To.Add(row[0].ToString());
                            MyMessage.From = SendFrom;
                            MyMessage.Subject = "Escalation on QPRC / FQPC";
                            MyMessage.Body = FormMailBodyQualityPE(empname, row[1].ToString(), ds, role, days);
                            MyMessage.IsBodyHtml = true;
                            //emailClient.Send(MyMessage);
                            MyMessage.To.Clear();
                            MyMessage.CC.Clear();
                        }
                        else
                        {
                            if (days == 2)
                            {
                                ds = PendingRecordsAssignee(row[0].ToString(), row[2].ToString());
                                //MyMessage.CC.Add(ds.Rows[0]["FQE_Email"].ToString());
                                AddCCProjectWise(row[2].ToString(), MyMessage);
                                AddCCBUWise(ds, MyMessage);
                                //MyMessage.CC.Add("coeqii@tataprojects.com");
                            }
                            if (days == 3)
                            {
                                ds = PendingRecordsAssignee3days(row[0].ToString(), row[2].ToString());
                                //MyMessage.CC.Add(ds.Rows[0]["FQE_Email"].ToString());
                                AddCCProjectWise(row[2].ToString(), MyMessage);
                                AddCCBUWise(ds, MyMessage);
                                foreach (DataRow email in ds.Rows)
                                {
                                    if (email["RCM_Email"] != "" || email["RCM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["RCM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["RCM_Email"].ToString());
                                        }
                                    }
                                }
                            }
                            if (days == 7)
                            {
                                ds = PendingRecordsAssignee7days(row[0].ToString(), row[2].ToString());
                                //MyMessage.CC.Add(ds.Rows[0]["FQE_Email"].ToString());
                                AddCCProjectWise(row[2].ToString(), MyMessage);
                                AddCCBUWise(ds, MyMessage);
                                foreach (DataRow email in ds.Rows)
                                {
                                    if (email["RCM_Email"] != "" || email["RCM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["RCM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["RCM_Email"].ToString());
                                        }
                                    }
                                    if (email["PM_Email"] != "" || email["PM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["PM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["PM_Email"].ToString());
                                        }
                                    }
                                }
                            }
                            if (days == 15)
                            {
                                ds = PendingRecordsAssignee15days(row[0].ToString(), row[2].ToString());
                                //MyMessage.CC.Add(ds.Rows[0]["FQE_Email"].ToString());
                                AddCCProjectWise(row[2].ToString(), MyMessage);
                                MyMessage.CC.Add("rrsankar@tataprojects.com");
                                MyMessage.CC.Add("gvacharyulu@tataprojects.com");
                                AddCCBUWise(ds, MyMessage);
                                foreach (DataRow email in ds.Rows)
                                {
                                    if (email["RCM_Email"] != "" || email["RCM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["RCM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["RCM_Email"].ToString());
                                        }
                                    }
                                    if (email["PM_Email"] != "" || email["PM_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["PM_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["PM_Email"].ToString());
                                        }
                                    }
                                    if (email["HOP_Email"] != "" || email["HOP_Email"] != string.Empty)
                                    {
                                        if (!MyMessage.CC.Contains(new MailAddress(email["HOP_Email"].ToString())))
                                        {
                                            MyMessage.CC.Add(email["HOP_Email"].ToString());
                                        }
                                    }
                                }
                            }
                            foreach (DataRow email in ds.Rows)
                            {
                                if (email["Assignee_Email"] != "" || email["Assignee_Email"] != string.Empty)
                                {
                                    if (!MyMessage.CC.Contains(new MailAddress(email["Assignee_Email"].ToString())))
                                    {
                                        MyMessage.CC.Add(email["Assignee_Email"].ToString());
                                    }
                                }
                            }
                            MyMessage.To.Add(row[0].ToString());
                            MyMessage.From = SendFrom;
                            MyMessage.Subject = "Escalation on QPRC / FQPC";
                            MyMessage.Body = FormMailBodyQualityAssignee(empname, row[1].ToString(), ds, role, days);
                            MyMessage.IsBodyHtml = true;
                            //emailClient.Send(MyMessage);
                            MyMessage.To.Clear();
                            MyMessage.CC.Clear();
                        }

                    }
                }
            }
        }

        string FormMailBodyQualityRCM(string user, string number, DataTable ds, string role, int pendingdays)
        {

            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + user + ",");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("There are <font color='red'>" + number + "</font> no of records pending with " + role + " for more than " + pendingdays + " days. We request you to take up with " + role + " on priority.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details of the pending records are as follows. Click on the Transaction ID to view the details of the checklist.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            //sbBody.Append("<html><head></head><title></title>");
            //sbBody.Append("<body style='font-size:12px;font-family:Trebuchet MS;'>");
            sbBody.Append("<table width='130%' border='1' cellpadding='1' cellspacing='1';'");
            sbBody.Append("<table><tr><td width='10%'>Transaction ID</td><td width='10%'>Stage</td><td width='10%'>Compliance Type</td><td width='10%'>BU Name</td><td width='10%'>Project Name</td><td width='10%'>Project Type</td><td width='10%'>Discipline</td><td width='10%'>Activity</td><td width='10%'>Sub Activity</td><td width='10%'>Date of Upload</td><td width='10%'>Is Compliant</td><td width='10%'>RCM Name</td><td width='10%'>RCM Email</td></tr>");
            for (int i = 0; i < ds.Rows.Count; i++)
            {
                sbBody.Append("<tr>");
                //sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Transaction_ID"] + "</td>"));
                sbBody.Append("<a href='https://tplnet.tataprojects.com/Pages/ChecklistDetails.aspx?Tid=" + ds.Rows[i]["Transaction_ID"] + "'><td width='10%'>" + Convert.ToString(ds.Rows[i]["Transaction_ID"] + "</td></a>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Stage"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Compliance_Type"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["BUName"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["ProjectName"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["ProjectType"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Discipline"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Activity"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Sub Activity"] + "</td>"));
                sbBody.Append("<td width='10%'>" + DateTime.Parse(ds.Rows[i]["FQE_Submitted_Date"].ToString()).ToShortDateString() + "</td>");
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["isCompliant"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["RCM_Name"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["RCM_Email"] + "</td>"));
                sbBody.Append("</tr>");
            }
            sbBody.Append("</table>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            return sbBody.ToString();
        }

        string FormMailBodyQualityPE(string user, string number, DataTable ds, string role, int pendingdays)
        {

            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + user + ",");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("There are <font color='red'>" + number + "</font> no of records pending with " + role + " for more than " + pendingdays + " days. We request you to take up with " + role + " on priority.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details of the pending records are as follows:");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            //sbBody.Append("<html><head></head><title></title>");
            //sbBody.Append("<body style='font-size:12px;font-family:Trebuchet MS;'>");
            sbBody.Append("<table width='130%' border='1' cellpadding='1' cellspacing='1';'");
            sbBody.Append("<table><tr><td width='10%'>Transaction ID</td><td width='10%'>Stage</td><td width='10%'>Compliance Type</td><td width='10%'>BU Name</td><td width='10%'>Project Name</td><td width='10%'>Project Type</td><td width='10%'>Discipline</td><td width='10%'>Activity</td><td width='10%'>Sub Activity</td><td width='10%'>Date of Upload</td><td width='10%'>Is Compliant</td><td width='10%'>PE Name</td><td width='10%'>PE Email</td></tr>");
            for (int i = 0; i < ds.Rows.Count; i++)
            {
                sbBody.Append("<tr>");
                //sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Transaction_ID"] + "</td>"));
                sbBody.Append("<a href='https://tplnet.tataprojects.com/Pages/ChecklistDetails.aspx?Tid=" + ds.Rows[i]["Transaction_ID"] + "'><td width='10%'>" + Convert.ToString(ds.Rows[i]["Transaction_ID"] + "</td></a>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Stage"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Compliance_Type"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["BUName"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["ProjectName"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["ProjectType"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Discipline"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Activity"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Sub Activity"] + "</td>"));
                sbBody.Append("<td width='10%'>" + DateTime.Parse(ds.Rows[i]["FQE_Submitted_Date"].ToString()).ToShortDateString() + "</td>");
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["isCompliant"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["PE_Name"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["PE_Email"] + "</td>"));
                sbBody.Append("</tr>");
            }
            sbBody.Append("</table>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            return sbBody.ToString();
        }

        string FormMailBodyQualityAssignee(string user, string number, DataTable ds, string role, int pendingdays)
        {

            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("Dear " + user + ",");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("There are <font color='red'>" + number + "</font> no of records pending with " + role + " for more than " + pendingdays + " days of target closure date. We request you to take up with " + role + " on priority.");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Details of the pending records are as follows:");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            //sbBody.Append("<html><head></head><title></title>");
            //sbBody.Append("<body style='font-size:12px;font-family:Trebuchet MS;'>");
            sbBody.Append("<table width='130%' border='1' cellpadding='1' cellspacing='1';'");
            sbBody.Append("<table><tr><td width='10%'>Transaction ID</td><td width='10%'>Stage</td><td width='10%'>Compliance Type</td><td width='10%'>BU Name</td><td width='10%'>Project Name</td><td width='10%'>Project Type</td><td width='10%'>Discipline</td><td width='10%'>Activity</td><td width='10%'>Sub Activity</td><td width='10%'>Date of Upload</td><td width='10%'>Is Compliant</td><td width='10%'>Assignee Name</td><td width='10%'>Assignee Email</td></tr>");
            for (int i = 0; i < ds.Rows.Count; i++)
            {
                sbBody.Append("<tr>");
                //sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Transaction_ID"] + "</td>"));
                sbBody.Append("<a href='https://tplnet.tataprojects.com/Pages/ChecklistDetails.aspx?Tid=" + ds.Rows[i]["Transaction_ID"] + "'><td width='10%'>" + Convert.ToString(ds.Rows[i]["Transaction_ID"] + "</td></a>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Stage"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Compliance_Type"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["BUName"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["ProjectName"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["ProjectType"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Discipline"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Activity"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Sub Activity"] + "</td>"));
                sbBody.Append("<td width='10%'>" + DateTime.Parse(ds.Rows[i]["FQE_Submitted_Date"].ToString()).ToShortDateString() + "</td>");
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["isCompliant"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Assignee_Name"] + "</td>"));
                sbBody.Append("<td width='10%'>" + Convert.ToString(ds.Rows[i]["Assignee_Email"] + "</td>"));
                sbBody.Append("</tr>");
            }
            sbBody.Append("</table>");
            sbBody.Append("<br>");
            sbBody.Append("<br>");
            sbBody.Append("Thanks & Regards,");
            return sbBody.ToString();
        }
        public static string GetEmpName(string email)
        {
            string name = string.Empty;
            DataTable dtname = new DataTable();
            try
            {
                SqlConnection con = new SqlConnection(Constants.constringemployee);
                con.Open();

                string sqlQuery = "select Name from TPL_EMP_MASTER where EmailID='" + email + "'";
                SqlDataAdapter daAdap = new SqlDataAdapter(sqlQuery, con);
                daAdap.Fill(dtname);
                foreach (DataRow row in dtname.Rows)
                {
                    name = row["Name"].ToString();
                }
                con.Close();

            }
            catch (Exception exp)
            {

            }
            return name;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Configuration;
using Microsoft.SharePoint.Client;
using System.Security;
namespace AssetInvScript
{
    class Program
    {
        static void Main(string[] args)
        {
            string siteurl = "https://tataprojects4.sharepoint.com/sites/TPLSampleMig/";
            string listName = "AssetScript";
            var targetSite = new Uri(siteurl);
            var login = "defspuser@tataprojects4.onmicrosoft.com";
            var password = "D3faultTplU5er";
            var securePassword = new SecureString();
            foreach (char c in password)
            {
                securePassword.AppendChar(c);
            }
            var onlineCredentials = new SharePointOnlineCredentials(login, securePassword);
            using (ClientContext clientContext = new ClientContext(targetSite))
            {
                try
                {
                    clientContext.Credentials = onlineCredentials;
                    Web web = clientContext.Web;
                    List oList = clientContext.Web.Lists.GetByTitle(listName);
                    clientContext.Load(oList);
                    clientContext.ExecuteQuery();
                    Folder folder = oList.RootFolder;
                    FileCollection files = folder.Files;
                    clientContext.Load(files);
                    clientContext.ExecuteQuery();


                    string tempLocation = @"C:\AssetInfo2\";
                    
                    foreach (Microsoft.SharePoint.Client.File file in files)
                    {
                        FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, file.ServerRelativeUrl);
                        clientContext.ExecuteQuery();

                        var filePath = tempLocation + file.Name;
                        using (var fileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Create))
                        {
                            fileInfo.Stream.CopyTo(fileStream);
                        }
                    }


                }
                catch (Exception ex)
                {
                    throw (ex);
                }

            }
        }
    }
}

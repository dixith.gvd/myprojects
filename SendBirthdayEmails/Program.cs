﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Drawing;
using System.IO;
namespace SendBirthdayEmails
{
    class Program
    {
        static void Main(string[] args)
        {
            ReminderBackupLog("Started");
            try
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = ConfigurationManager.AppSettings["ConnStringSrc"];
                string ccMail = ConfigurationManager.AppSettings["ccMails"].ToString();
                string bccMail = ConfigurationManager.AppSettings["bccMails"].ToString();
                string fPath = ConfigurationManager.AppSettings["fPath"].ToString();
                string sqlQuery = "Select TPLID,NAME,EmailID,Business_Name, Function_Name,DOB from [dbo].[TPL_EMP_MASTER] where IsActive=1 AND DAY(dob) = DAY(GETDATE()) AND MONTH(DOB)=MONTH(GETDATE()) AND Business_Name in ('Transportation','Tata-Aldesa JV') ORDER BY CAST(Usercode AS INT) DESC";
                string empname = string.Empty; string empemail = string.Empty;
                int i = 0;
                using (SqlCommand cmdRept = new SqlCommand(sqlQuery, con))
                {
                    SqlDataAdapter da = new SqlDataAdapter(sqlQuery, con);
                    DataTable dtttt = new DataTable();
                    da.Fill(dtttt);
                    con.Open();
                    using (SqlDataReader dr = cmdRept.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                i = i + 1;
                                empname = dr["NAME"].ToString();
                                empemail = dr["EmailID"].ToString();

                                using (var emailClient = new SmtpClient())
                                {
                                    MailMessage newMail = new MailMessage();
                                    newMail.From = new MailAddress("transportation@tataprojects.com");
                                    //newMail.To.Add(new MailAddress("deekshithgvd-t@tataprojects.com"));
                                    //newMail.CC.Add(new MailAddress("apoorvas@tataprojects.com"));
                                    newMail.To.Add(new MailAddress(empemail));
                                    if (!string.IsNullOrEmpty(ccMail))
                                    {
                                        string strCC = ccMail.Trim();
                                        string[] CCEmailids = strCC.Split(';');
                                        foreach (string CCEmail in CCEmailids)
                                        {
                                            newMail.CC.Add(new MailAddress(CCEmail));
                                        }
                                    }
                                    newMail.Bcc.Add(new MailAddress(bccMail));
                                    newMail.Subject = "Happy Birthday - " + empname;
                                    newMail.IsBodyHtml = true;
                                    string imageFilePath = fPath + i + ".png";
                                    SolidBrush brush1 = null;
                                    SolidBrush brush = null;
                                    PointF secondLocation;
                                    PointF firstLocation;
                                    string font = "";
                                    if (i == 1)
                                    {
                                        font = "Monotype Corsiva";
                                        firstLocation = new PointF(420f, 70f);
                                        Color color1 = System.Drawing.ColorTranslator.FromHtml("#31859C");
                                        brush1 = new SolidBrush(color1);
                                        secondLocation = new PointF(460f, 120f);
                                        Color color = System.Drawing.ColorTranslator.FromHtml("#31859C");
                                        brush = new SolidBrush(color);
                                    }
                                    else if (i == 2)
                                    {
                                        font = "Monotype Corsiva";
                                        firstLocation = new PointF(500f, 75f);
                                        Color color1 = System.Drawing.ColorTranslator.FromHtml("#BF0000");
                                        brush1 = new SolidBrush(color1);
                                        secondLocation = new PointF(500f, 130f);
                                        Color color = System.Drawing.ColorTranslator.FromHtml("#BF0000");
                                        brush = new SolidBrush(color);
                                    }
                                    else if (i == 3)
                                    {
                                        font = "Bradley Hand ITC";
                                        firstLocation = new PointF(420f, 85f);
                                        Color color1 = System.Drawing.ColorTranslator.FromHtml("#215968");
                                        brush1 = new SolidBrush(color1);
                                        secondLocation = new PointF(460f, 140f);
                                        Color color = System.Drawing.ColorTranslator.FromHtml("#215968");
                                        brush = new SolidBrush(color);
                                    }
                                    else if (i == 4)
                                    {
                                        font = "Bodoni MT";
                                        firstLocation = new PointF(490f, 85f);
                                        Color color1 = System.Drawing.ColorTranslator.FromHtml("#993366");
                                        brush1 = new SolidBrush(color1);
                                        secondLocation = new PointF(490f, 140f);
                                        Color color = System.Drawing.ColorTranslator.FromHtml("#993366");
                                        brush = new SolidBrush(color);
                                    }
                                    else if (i == 5)
                                    {
                                        font = "Comic Sans MS";
                                        firstLocation = new PointF(490f, 60f);
                                        Color color1 = System.Drawing.ColorTranslator.FromHtml("#990033");
                                        brush1 = new SolidBrush(color1);
                                        secondLocation = new PointF(500f, 120f);
                                        Color color = System.Drawing.ColorTranslator.FromHtml("#990033");
                                        brush = new SolidBrush(color);
                                    }
                                    else if (i == 6)
                                    {
                                        font = "Baskerville Old Face";
                                        firstLocation = new PointF(420f, 85f);
                                        Color color1 = System.Drawing.ColorTranslator.FromHtml("#215968");
                                        brush1 = new SolidBrush(color1);
                                        secondLocation = new PointF(460f, 140f);
                                        Color color = System.Drawing.ColorTranslator.FromHtml("#215968");
                                        brush = new SolidBrush(color);
                                    }
                                    else if (i == 7)
                                    {
                                        font = "Comic Sans MS";
                                        firstLocation = new PointF(500f, 75f);
                                        Color color1 = System.Drawing.ColorTranslator.FromHtml("#BF7300");
                                        brush1 = new SolidBrush(color1);
                                        secondLocation = new PointF(510f, 140f);
                                        Color color = System.Drawing.ColorTranslator.FromHtml("#BF7300");
                                        brush = new SolidBrush(color);
                                    }
                                    else if (i == 8)
                                    {
                                        font = "Bradley Hand ITC";
                                        firstLocation = new PointF(490f, 85f);
                                        Color color1 = System.Drawing.ColorTranslator.FromHtml("#993366");
                                        brush1 = new SolidBrush(color1);
                                        secondLocation = new PointF(500f, 140f);
                                        Color color = System.Drawing.ColorTranslator.FromHtml("#993366");
                                        brush = new SolidBrush(color);
                                    }
                                    else
                                    {
                                        font = "Monotype Corsiva";
                                        firstLocation = new PointF(500f, 75f);
                                        Color color1 = System.Drawing.ColorTranslator.FromHtml("#B65085");
                                        brush1 = new SolidBrush(color1);
                                        secondLocation = new PointF(520f, 130f);
                                        Color color = System.Drawing.ColorTranslator.FromHtml("#B65085");
                                        brush = new SolidBrush(color);
                                    }

                                    Bitmap newBitmap;
                                    using (var bitmap2 = (Bitmap)Image.FromFile(imageFilePath))//load the image file
                                    {
                                        using (Graphics graphics = Graphics.FromImage(bitmap2))
                                        {
                                            if (i == 3 || i == 8 || i == 9 || i == 6 || i == 4 || i == 2 || i == 1)
                                            {
                                                using (Font arialFont = new Font(font, 28, FontStyle.Bold))
                                                {
                                                    graphics.DrawString("Dear", arialFont, brush1, firstLocation);
                                                    graphics.DrawString(empname, arialFont, brush, secondLocation);
                                                }
                                            }
                                            else
                                            {
                                                using (Font arialFont = new Font(font, 28))
                                                {
                                                    graphics.DrawString("Dear", arialFont, brush1, firstLocation);
                                                    graphics.DrawString(empname, arialFont, brush, secondLocation);
                                                }
                                            }
                                        }
                                        newBitmap = new Bitmap(bitmap2);
                                    }

                                    newBitmap.Save(fPath + empname + ".jpg");//save the image file
                                    newBitmap.Dispose();
                                    var inlineLogo = new LinkedResource(fPath + empname + ".jpg", "image/png");
                                    inlineLogo.ContentId = Guid.NewGuid().ToString();
                                    string openHtml = @"<html>
                            <body>
                            <div style='position: relative;text-align: center;'>
                            <img src=""cid:{0}"" align=baseline />
                            </div>
                            </body></html>";
                                    //string htmlBody = openHtml.Replace("{", "{{").Replace("}", "}}");
                                    string body = string.Format(openHtml, inlineLogo.ContentId);
                                    //string mailBody = FormMailBody("GVD Deekshith", body);
                                    var view = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
                                    view.LinkedResources.Add(inlineLogo);
                                    newMail.AlternateViews.Add(view);

                                    emailClient.UseDefaultCredentials = false;
                                    emailClient.Credentials = new System.Net.NetworkCredential("transportation@tataprojects.com", "tata@123");
                                    emailClient.Host = "smtp.office365.com";
                                    emailClient.Port = 587;
                                    emailClient.EnableSsl = true;
                                    emailClient.TargetName = "STARTTLS/smtp.office365.com";
                                    emailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                                    emailClient.Send(newMail);
                                    view.Dispose();
                                    if (File.Exists(fPath + empname + ".jpg"))
                                    {
                                        File.Delete(fPath + empname + ".jpg");
                                    }
                                    if (i == 9)
                                    {
                                        i = 0;
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ReminderBackupLog(ex + "Mail not triggered");
            }
            ReminderBackupLog("Completed");
        }

        public static void ReminderBackupLog(string message)
        {
            try
            {
                string fPath = ConfigurationManager.AppSettings["fPathLog"].ToString();
                fPath = fPath + @"\Birthday_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                StreamWriter sw = new StreamWriter(fPath, true);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception exp)
            {

            }
        }

    }
}

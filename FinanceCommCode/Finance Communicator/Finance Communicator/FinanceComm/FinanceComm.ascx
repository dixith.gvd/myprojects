﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinanceComm.ascx.cs" Inherits="Finance_Communicator.FinanceComm.FinanceComm" %>

<div id="dvMainFormChecklist" runat="server">
    <br />
    <table border="1" cellpadding="1" cellspacing="1" width="60%">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label5" runat="server" ForeColor="White" Text="Send the TDS Certificates" CssClass="auto-style3" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width:30%; text-align: left; height:30px; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label21" ForeColor="White" runat="server" Text="Sent To" CssClass="auto-style3"></asp:Label>
                <strong><font color="#ff0000">*</font></strong>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:DropDownList ID="ddlsentto" runat="server" Width="100%">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>Employee</asp:ListItem>
                    <asp:ListItem>Vendor</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label23" ForeColor="White" runat="server" Text="Select Assessment Year" CssClass="auto-style3"></asp:Label>
                <strong><font color="#ff0000">*</font></strong>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlassessyear" runat="server" Width="100%">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>2013-14</asp:ListItem>
                    <asp:ListItem>2014-15</asp:ListItem>
                    <asp:ListItem>2015-16</asp:ListItem>
                    <asp:ListItem>2016-17</asp:ListItem>
                    <asp:ListItem>2017-18</asp:ListItem>
                    <asp:ListItem>2018-19</asp:ListItem>
                    <asp:ListItem>2019-20</asp:ListItem>
                    <asp:ListItem>2020-21</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; width: 30%;height: 30px; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label1" ForeColor="White" runat="server" Text="Select Quarter" CssClass="auto-style3"></asp:Label>
                <strong><font color="#ff0000">*</font></strong>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:DropDownList ID="ddlquarter" runat="server" Width="100%">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>Q1</asp:ListItem>
                    <asp:ListItem>Q2</asp:ListItem>
                    <asp:ListItem>Q3</asp:ListItem>
                    <asp:ListItem>Q4</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Select path of files" CssClass="auto-style3"></asp:Label>
                <strong><font color="#ff0000">*</font></strong>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <%--<asp:RequiredFieldValidator ID="RFProject" runat="server" ErrorMessage="Please Select Project" InitialValue="--Select--" ControlToValidate="ddlProjectCode" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                <%-- <asp:Label ID="lblProject" runat="server"></asp:Label>--%>
                <asp:TextBox ID="txtfilepath" runat="server" Width="96%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; width: 30%;height: 30px; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label22" ForeColor="White" runat="server" Text="Upload Excel File" CssClass="auto-style3"></asp:Label>
                <strong><font color="#ff0000">*</font></strong>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:FileUpload ID="uploadattach" runat="server" Width="96%" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx|.XLS|.XLSX)$"
                    ControlToValidate="uploadattach" runat="server" ForeColor="Red" CssClass="auto-style3" ErrorMessage="Please select a valid excel file format."
                    Display="Dynamic" />
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label6" ForeColor="White" runat="server" Text="Sender EmailID" CssClass="auto-style3"></asp:Label>
                <strong><font color="#ff0000">*</font></strong>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:TextBox ID="txtsenderemail" runat="server" Width="96%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; width: 30%;height: 30px; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label25" ForeColor="White" runat="server" Text="CC EmailID" CssClass="auto-style3"></asp:Label>
                &nbsp;</td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:TextBox ID="txtccemail" runat="server" Width="96%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; width: 30%;height: 30px; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label30" ForeColor="White" runat="server" Text="Enter Logs folder path" CssClass="auto-style3"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:TextBox ID="txtfolderpath" runat="server" Width="96%"></asp:TextBox>
            </td>
        </tr>
        </table>
    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label2" runat="server" Text="Notes:" style="text-decoration: underline"></asp:Label>
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="Please ensure that the Vendor Tax Declaration files conform to the following naming format"></asp:Label>
    <br />
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label31" runat="server" Text="PANNumber_Quarter_AssessmentYear.pdf"></asp:Label>
    <br />
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label32" runat="server" ForeColor="Red" Text="Please ensure that the Employee Tax Declaration files conform to the following naming format"></asp:Label>
    <br />
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label33" runat="server" Text="PANNumber_AssessmentYear.pdf"></asp:Label>
    <br />
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label34" runat="server" ForeColor="Red" Text="Please ensure that the excel sheet uploaded with the details has the following exact column names"></asp:Label>
    <br />
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label35" runat="server" Text="Column1              "></asp:Label>
    <asp:Label ID="Label7" runat="server" Text=":  Name"></asp:Label><br />
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label36" runat="server" Text="Column2              "></asp:Label>
    <asp:Label ID="Label8" runat="server" Text=":  EmailID"></asp:Label><br />
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label37" runat="server" Text="Column3              "></asp:Label>
    <asp:Label ID="Label9" runat="server" Text=":  PAN Number"></asp:Label>
    <br />
    <br />
    <table cellpadding="1" cellspacing="1" width="60%">
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Button Font-Size="Medium" ID="btnsubmit" runat="server" Text="Submit" CssClass="auto-style3" OnClick="Button1_Click" />
            </td>
        </tr>
    </table>
</div>
<asp:Label ID="lblRecordsProcessed" runat="server" Text=""></asp:Label>
<asp:Label ID="lblmessage" runat="server" Text="Label"></asp:Label>
<p>
    &nbsp;</p>


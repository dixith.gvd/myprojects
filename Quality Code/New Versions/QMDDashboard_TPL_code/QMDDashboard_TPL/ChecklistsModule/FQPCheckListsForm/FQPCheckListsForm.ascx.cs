﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.IO.Compression;
namespace QMDDashboard_TPL.ChecklistsModule.FQECheckListsForm
{
    [ToolboxItemAttribute(false)]
    public partial class FQECheckListsForm : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public FQECheckListsForm()
        {
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }
        QualityChecklists objQuality = new QualityChecklists();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                //  SPListItem projectCodeFromList = objQuality.GetProjectCodeFromList();
                DataTable projectCodeItem = objQuality.GetProjectCodeByLoggedInUser();
                if (projectCodeItem != null)
                {
                    dvUnAuth.Visible = false;
                    dvMainForm.Visible = true;
                    for (int i = 0; i < projectCodeItem.Rows.Count; i++)
                    {
                        string projectCode = projectCodeItem.Rows[i][0].ToString();
                        DataTable projectItem = objQuality.GetProjectdetailsByProjectCode(projectCode);
                        for (int j = 0; j < projectItem.Rows.Count; j++)
                        {
                            string projectName = projectItem.Rows[j]["Title"].ToString();
                            ddlProjectCode.Items.Add(new ListItem(projectName));
                        }
                        lblBU.Text = projectItem.Rows[0]["BU_x0020_Name"].ToString();
                        lblSBG.Text = projectItem.Rows[0]["SBGName"].ToString();
                        lblSBU.Text = projectItem.Rows[0]["SBU_x0020_Name"].ToString();
                        lblNewSBU.Text = projectItem.Rows[0]["SBU_x0020_Name"].ToString();
                        if (lblNewSBU.Text == "Industrial Infrastructure")
                        {
                            lblInduCoE.Text = "coeqii@tataprojects.com";
                        }
                        else if (lblNewSBU.Text == "Urban Infrastructure")
                        {
                            lblInduCoE.Text = "coeqii@tataprojects.com";
                        }
                        else
                        {
                            lblInduCoE.Text = "coeqii@tataprojects.com";
                        }
                    }
                    ddlProjectCode.Items.Insert(0, "--Select--");
                    if (ddlProjectCode.SelectedIndex > 0)
                    {
                        string projectNameForRCM = ddlProjectCode.SelectedItem.Text;
                        //DataTable projectCodeFromList = objQuality.GetProjectCodeByProjectName(projectNameForRCM);
                        //for (int i = 0; i < projectCodeFromList.Rows.Count; i++)
                        //{
                        //    string projectCode = projectCodeFromList.Rows[i][0].ToString();
                        string projectCode = ddlProjectCode.SelectedItem.Text.Split('-')[1];
                        SPListItem projectCodeFromPMRCM = objQuality.GetProjectCodeByProjectNameForRCM(projectCode);
                        if (projectCodeFromPMRCM["RCM"] != null)
                        {
                            string strRCM = projectCodeFromPMRCM["RCM"].ToString();
                            if (!string.IsNullOrEmpty(strRCM))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strRCM);
                                lblRCMEmail.Text = userValue.User.Email.ToString();
                                lblRCM.Text = userValue.User.Name;
                                lblRCMID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                            }
                        }
                        if (projectCodeFromPMRCM["BU_x0020_Head"] != null)
                        {
                            string strbuHead = projectCodeFromPMRCM["BU_x0020_Head"].ToString();
                            if (!string.IsNullOrEmpty(strbuHead))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strbuHead);
                                lblbuheadid.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                                lblbuheadName.Text = userValue.User.Name;
                                lblbuheadEmail.Text = userValue.User.Email.ToString();
                            }
                        }
                        if (projectCodeFromPMRCM["PM"] != null)
                        {
                            string strPM = projectCodeFromPMRCM["PM"].ToString();
                            if (!string.IsNullOrEmpty(strPM))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPM);
                                lblPMEmail.Text = userValue.User.Email.ToString();
                                lblPM.Text = userValue.User.Name;
                                lblPMID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                            }
                        }
                        if (projectCodeFromPMRCM["Head_x0020_Operations"] != null)
                        {
                            string strHead = projectCodeFromPMRCM["Head_x0020_Operations"].ToString();
                            if (!string.IsNullOrEmpty(strHead))
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strHead);
                                lblHeadEmail.Text = userValue.User.Email.ToString();
                                lblHead.Text = userValue.User.Name;
                                lblHeadID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                            }
                        }
                        //}
                    }
                    //Bind Project Types
                    DataTable dtprojectTypes = objQuality.GetProjectTypesByProcessFQP();
                    if (dtprojectTypes != null)
                    {
                        ddlProjectTypes.DataSource = dtprojectTypes;
                        ddlProjectTypes.DataTextField = "Project type";
                        ddlProjectTypes.DataValueField = "Project type";
                        ddlProjectTypes.DataBind();
                        ddlProjectTypes.Items.Insert(0, "--Select--");
                    }
                    //Bind Desciplines
                    DataTable dtDesciplines = objQuality.GetDesciplinesByProcessFQP();
                    if (dtDesciplines != null)
                    {
                        ddlDesciplines.DataSource = dtDesciplines;
                        ddlDesciplines.DataTextField = "Discipline";
                        ddlDesciplines.DataValueField = "Discipline";
                        ddlDesciplines.DataBind();
                        ddlDesciplines.Items.Insert(0, "--Select--");
                    }
                    //Bind locations
                    DataTable dtLocations = objQuality.GetLocationsByProjectCode(lblProjectCode.Text);
                    if (dtLocations != null)
                    {
                        ddlLocations.DataSource = dtLocations;
                        ddlLocations.DataTextField = "Location";
                        ddlLocations.DataValueField = "Location";
                        ddlLocations.DataBind();
                        ddlLocations.Items.Insert(0, "--Select--");
                    }
                    lblFQEEmail.Text = SPContext.Current.Web.CurrentUser.Email;
                    lblFQEName.Text = SPContext.Current.Web.CurrentUser.Name;
                    dtRWCdate.Enabled = false;
                }
                else
                {
                    dvUnAuth.Visible = true;
                    dvMainForm.Visible = false;
                }
            }
        }
        protected void ddlDesciplines_SelectedIndexChanged(object sender, EventArgs e)
        {
            string projectTypeID = ddlProjectTypes.SelectedItem.Value;
            string disciplineID = ddlDesciplines.SelectedItem.Value;
            DataTable dtMainactivitydata = objQuality.GetMainActivityforFQP(projectTypeID, disciplineID);
            if (dtMainactivitydata != null)
            {
                ddlMainactivity.DataSource = dtMainactivitydata;
                ddlMainactivity.DataTextField = "MainActivity";
                ddlMainactivity.DataValueField = "MainActivity";
                ddlMainactivity.DataBind();
                dvMainactivity.Visible = true;
                ddlMainactivity.Items.Insert(0, "--Select--");
            }
            else
            {
                lblError.Text = "There are no Activities for the selected filter";
            }
        }
        protected void ddlActivities_SelectedIndexChanged(object sender, EventArgs e)
        {
            string projectType = ddlProjectTypes.SelectedItem.Value;
            string discipline = ddlDesciplines.SelectedItem.Value;
            string mainactivity = ddlMainactivity.SelectedItem.Value;
            string activity = ddlActivities.SelectedItem.Text;
            DataTable dtActivitiesdata = objQuality.GetSubactivitiesforFQP(mainactivity, activity, projectType, discipline);
            if (dtActivitiesdata != null)
            {
                ddlSubActivities.DataSource = dtActivitiesdata;
                ddlSubActivities.DataTextField = "Sub Activity";
                ddlSubActivities.DataValueField = "Sub Activity";
                ddlSubActivities.DataBind();
                ddlSubActivities.Items.Insert(0, "--Select--");
                dvSubActivities.Visible = true;
            }
            else
            {
                lblError.Text = "There are no Sub-activities for the selected filter";
            }

        }
        protected void ddlSubActivities_SelectedIndexChanged(object sender, EventArgs e)
        {
            string projectType = ddlProjectTypes.SelectedItem.Value;
            string discipline = ddlDesciplines.SelectedItem.Value;
            string mainactivity = ddlMainactivity.SelectedItem.Value;
            string activity = ddlActivities.SelectedItem.Text;
            string subActivity = ddlSubActivities.SelectedItem.Text;
            DataTable dtCompliances = objQuality.GetCompliancesforFQP(mainactivity, activity, subActivity, projectType, discipline);
            if (dtCompliances != null)
            {
                lblGridHeading.Text = "FQP Compliances for : <b>'" + subActivity + "'</b>.";
                grdCompliances.DataSource = dtCompliances;
                grdCompliances.DataBind();
                dvCompliances.Visible = true;
            }
            else
            {
                lblError.Text = "There are no Complainces for the selected filter";
            }
        }
        protected void grdCompliances_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            int count = 0;
            int countNo = 0;
            int filecount = 0;
            int filecount2 = 0;
            foreach (GridViewRow gvr in grdCompliances.Rows)
            {
                RadioButton rdbYes = (RadioButton)gvr.FindControl("rdbYes");
                RadioButton rdbNo = (RadioButton)gvr.FindControl("rdbNo");
                RadioButton rdbNA = (RadioButton)gvr.FindControl("rdbNA");
                if (rdbYes.Checked || rdbNA.Checked)
                {
                    count += 1;
                }
                if (rdbNo.Checked)
                {
                    count += 1;
                    countNo += 1;
                }
                FileUpload fu = (FileUpload)gvr.FindControl("uploadNCR");
                FileUpload fu2 = (FileUpload)gvr.FindControl("uploadPhoto");
                //if (fu.HasFile)
                //{
                string filePath = fu.PostedFile.FileName;
                if (!string.IsNullOrEmpty(filePath))
                {
                    filecount += 1;
                }
                string filePath2 = fu2.PostedFile.FileName;
                if (!string.IsNullOrEmpty(filePath2))
                {
                    filecount2 += 1;
                }
            }
            if (filecount == 0 || filecount2 == 0)
            {
                lblError.Text = "file upload for atleast one Evidence & Photograph is mandatory";
                return;
            }
            if (countNo > 0)
            {
                string mainactivity = ddlMainactivity.SelectedItem.Value;
                string subactivity = ddlSubActivities.SelectedItem.Text;
                DataTable dtdefectdata = objQuality.GetDefectCategoryforFQP(mainactivity, subactivity);
                if (dtdefectdata != null)
                {
                    ddlDefectCategory.DataSource = dtdefectdata;
                    ddlDefectCategory.DataTextField = "Main_Defect_Category";
                    ddlDefectCategory.DataValueField = "Main_Defect_Category";
                    ddlDefectCategory.DataBind();
                    ddlDefectCategory.Items.Insert(0, "--Select--");

                }
                else
                {
                    lblError.Text = "There are no Main Defect Categories for the selected filter";
                }
                DataTable dtrootcausecategory = objQuality.GetRootCauseCategoryforFQP();
                if (dtrootcausecategory != null)
                {
                    ddlRootCauseCategory.DataSource = dtrootcausecategory;
                    ddlRootCauseCategory.DataTextField = "RootCauseCategory";
                    ddlRootCauseCategory.DataValueField = "RootCauseCategory";
                    ddlRootCauseCategory.DataBind();
                    ddlRootCauseCategory.Items.Insert(0, "--Select--");

                }
                else
                {
                    lblError.Text = "There are no Root Cause Categories for the selected filter";
                }
                chkRWCrequired.Checked = true;
                isNotSWI.Visible = false;
                rwcReq.Visible = false;
                tblRWC.Visible = true;
                Div1.Visible = true;
                Div2.Visible = false;
                Div3.Visible = false;
                trSave.Visible = false;
                dtRWCdate.SelectedDate = DateTime.Now;
                dvuploadNCR.Visible = true;
            }
            else
            {
                rwcReq.Visible = false;
                tblRWC.Visible = false;
                Div1.Visible = false;
                Div2.Visible = false;
                Div3.Visible = false;
                trSave.Visible = false;
            }
            if (grdCompliances.Rows.Count.Equals(count))
            {
                lblError.Text = string.Empty;
                string stage = string.Empty;
                //string ncissuedby = string.Empty;
                string isComplaint = "Yes";
                if (countNo > 0)
                {
                    isComplaint = "No";
                    stage = "Pending with PE";
                    //ncissuedby = ddlissuedby.SelectedItem.Text;
                }
                else
                {
                    stage = "Compliant and Closed";
                }
                #region Insert into main master
                string sbg = lblSBG.Text;
                string bu = lblBU.Text;
                string project = ddlProjectCode.SelectedItem.Text;
                string projectCode = lblProjectCode.Text;
                string location = ddlLocations.SelectedItem.Text;
                string subLocation = ddlSubLocations.SelectedItem.Text;
                string projectType = ddlProjectTypes.SelectedItem.Text;
                string discipline = ddlDesciplines.SelectedItem.Text;
                lblMainactivity.Text = ddlMainactivity.SelectedItem.Text;
                lblActivity.Text = ddlActivities.SelectedItem.Text;
                lblSubActivity.Text = ddlSubActivities.SelectedItem.Text;
                string loginUser = SPContext.Current.Web.CurrentUser.LoginName;
                string fqeID = objQuality.GetOnlyEmployeeID(loginUser);
                string fqeEmail = lblFQEEmail.Text;
                string fqeName = lblFQEName.Text;
                string newSBUName = lblNewSBU.Text;
                string hop_Name = lblHead.Text;
                string hop_Email = lblHeadEmail.Text;
                string pe_Id = lblPEID.Text;
                string pe_Name = lblPE.Text;
                string pe_Email = lblPEEmail.Text;
                DateTime today = DateTime.Now;
                string todayStr = String.Format("{0:yyyy-MM-dd HH:mm:ss}", today);
                string transID = string.Empty;
                bool isSaved = objQuality.SaveChecklistByMaster(stage, bu, project, projectCode, location, subLocation, "FQP", projectType, discipline, lblActivity.Text, lblSubActivity.Text, isComplaint, fqeID, fqeName, fqeEmail, todayStr, lblRCMID.Text, lblRCM.Text, lblRCMEmail.Text, lblPMID.Text, lblPM.Text, lblPMEmail.Text, newSBUName, hop_Name, hop_Email, pe_Id, pe_Name, pe_Email, lblbuheadid.Text, lblbuheadName.Text, lblbuheadEmail.Text, txtidentify.Text.Replace("'", "''"), sbg, lblMainactivity.Text);
                if (isSaved)
                {
                    transID = objQuality.GetLatestTransactionID();
                    lblTransactionID.Text = transID;
                    if (!string.IsNullOrEmpty(transID))
                    {
                        // InsertEvidenceDocument(transID);
                        foreach (GridViewRow gvr in grdCompliances.Rows)
                        {
                            string complainceText = grdCompliances.Rows[gvr.RowIndex].Cells[0].Text;
                            RadioButton rdbYes = (RadioButton)gvr.FindControl("rdbYes");
                            RadioButton rdbNo = (RadioButton)gvr.FindControl("rdbNo");
                            RadioButton rdbNA = (RadioButton)gvr.FindControl("rdbNA");
                            TextBox txtRemarks = (TextBox)gvr.FindControl("txtFQECompRemarks");
                            string isComplaintEach = string.Empty;
                            if (rdbYes.Checked)
                            {
                                isComplaintEach = rdbYes.Text;
                            }
                            if (rdbNo.Checked)
                            {
                                isComplaintEach = rdbNo.Text;
                            }
                            if (rdbNA.Checked)
                            {
                                isComplaintEach = rdbNA.Text;
                            }
                            FileUpload fu = (FileUpload)gvr.FindControl("uploadNCR");
                            FileUpload fu2 = (FileUpload)gvr.FindControl("uploadPhoto");
                            //if (fu.HasFile)
                            //{
                            string filePath = fu.PostedFile.FileName;
                            string filePath2 = fu2.PostedFile.FileName;
                            string filename = Path.GetFileName(filePath);
                            string filename2 = Path.GetFileName(filePath2);
                            string ext = Path.GetExtension(filename);
                            string ext2 = Path.GetExtension(filename2);
                            string contenttype = String.Empty;
                            string contenttype2 = String.Empty;
                            switch (ext)
                            {
                                case ".doc":
                                    contenttype = "application/vnd.ms-word";
                                    break;
                                case ".docx":
                                    contenttype = "application/vnd.ms-word";
                                    break;
                                case ".xls":
                                    contenttype = "application/vnd.ms-excel";
                                    break;
                                case ".xlsx":
                                    contenttype = "application/vnd.ms-excel";
                                    break;
                                case ".jpg":
                                    contenttype = "image/jpg";
                                    break;
                                case ".jpeg":
                                    contenttype = "image/jpeg";
                                    break;
                                case ".png":
                                    contenttype = "image/png";
                                    break;
                                case ".gif":
                                    contenttype = "image/gif";
                                    break;
                                case ".pdf":
                                    contenttype = "application/pdf";
                                    break;
                            }
                            switch (ext2)
                            {
                                case ".doc":
                                    contenttype2 = "application/vnd.ms-word";
                                    break;
                                case ".docx":
                                    contenttype2 = "application/vnd.ms-word";
                                    break;
                                case ".xls":
                                    contenttype2 = "application/vnd.ms-excel";
                                    break;
                                case ".xlsx":
                                    contenttype2 = "application/vnd.ms-excel";
                                    break;
                                case ".jpg":
                                    contenttype2 = "image/jpg";
                                    break;
                                case ".jpeg":
                                    contenttype2 = "image/jpeg";
                                    break;
                                case ".png":
                                    contenttype2 = "image/png";
                                    break;
                                case ".gif":
                                    contenttype2 = "image/gif";
                                    break;
                                case ".pdf":
                                    contenttype2 = "application/pdf";
                                    break;
                            }
                            //if (contenttype != String.Empty)
                            //{
                            Stream fs = fu.PostedFile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            byte[] compressedData = Compress(bytes);
                            Stream fs2 = fu2.PostedFile.InputStream;
                            BinaryReader br2 = new BinaryReader(fs2);
                            Byte[] bytes2 = br2.ReadBytes((Int32)fs2.Length);
                            byte[] compressedData2 = Compress(bytes2);
                            //Insert the file into database
                            String strQuery = "insert into QChecklist_TnxChild (Supp_DocName,Supp_DocContentType,Supp_Document,Evi_DocName,Evi_DocContentType,Evi_Document,Transaction_ID,Compliance_Text,isCompliant,SWIRemarks) values (@Supp_DocName, @Supp_DocContentType, @Supp_Document,@Evi_DocName, @Evi_DocContentType, @Evi_Document,@Transaction_ID, @Compliance_Text,@isCompliant,@SWIRemarks)";
                            SqlCommand cmd = new SqlCommand(strQuery);
                            cmd.Parameters.AddWithValue("@Supp_DocName", filename);
                            cmd.Parameters.AddWithValue("@Supp_DocContentType", contenttype);
                            cmd.Parameters.AddWithValue("@Supp_Document", compressedData);
                            cmd.Parameters.AddWithValue("@Evi_DocName", filename2);
                            cmd.Parameters.AddWithValue("@Evi_DocContentType", contenttype2);
                            cmd.Parameters.AddWithValue("@Evi_Document", compressedData2);
                            cmd.Parameters.AddWithValue("@Transaction_ID", lblTransactionID.Text);
                            cmd.Parameters.AddWithValue("@Compliance_Text", complainceText);
                            cmd.Parameters.AddWithValue("@isCompliant", isComplaintEach);
                            cmd.Parameters.AddWithValue("@SWIRemarks", txtRemarks.Text.Replace("'", "''"));
                            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                            //  }
                            // }
                            //else
                            //{
                            //    lblErrorNotification.Enabled = true;
                            //    lblErrorNotification.Text = "Please upload document.";
                            //}
                        }
                        btnSave.Visible = false;
                        isSWISubmit.Visible = true;
                    }
                }
                if (!string.IsNullOrEmpty(transID))
                {
                    if (countNo > 0)
                    {
                        rwcReq.Visible = false;
                        isSWISubmit.Visible = true;
                        grdCompliances.Visible = false;
                        Random rndNew = new Random();
                        int randNumber = rndNew.Next(1, 9);
                        string rwcNumber = string.Concat(transID, "/", randNumber.ToString());
                        lblRWCNO.Text = rwcNumber;
                        dtRWCdate.SelectedDate = DateTime.Now;
                        lblProjectLocation.Text = ddlProjectCode.SelectedItem.Text;
                        lblTPLJobNo.Text = lblProjectCode.Text;
                        DataTable dtChildData = objQuality.GetComplaincesDatabyTransactionID(transID);
                        if (dtChildData != null)
                        {
                        }
                    }
                    else
                    {
                        objQuality.SendCompRCMForClosureMailforFQP(lblRCM.Text, lblRCMEmail.Text, fqeName, fqeEmail, project, lblActivity.Text, lblSubActivity.Text, location, transID, lblPMEmail.Text, lblInduCoE.Text);
                        lblSWIsuccessNotification.Text = "You have submitted successfully";
                        fqesubmit.Enabled = false;
                        //fqeCancel.Enabled = false;
                        string navigateUrl = Constants.link + "/Pages/FQPChecklists.aspx";
                        lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to submit another FQP compliance", navigateUrl);
                    }
                }
                #endregion
            }
            else
            {
                lblError.Text = "All complied's to be selected";
            }
        }
        private void InsertNCRDocument(HttpPostedFile file, string Tid)
        {
            string filePath = file.FileName;
            string filename = Path.GetFileName(filePath);
            string ext = Path.GetExtension(filename).ToLower();
            string contenttype = String.Empty;
            switch (ext)
            {
                case ".doc":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".docx":
                    contenttype = "application/vnd.ms-word";
                    break;
                case ".xls":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contenttype = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                    contenttype = "image/jpg";
                    break;
                case ".jpeg":
                    contenttype = "image/jpeg";
                    break;
                case ".png":
                    contenttype = "image/png";
                    break;
                case ".gif":
                    contenttype = "image/gif";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    break;
                case ".PDF":
                    contenttype = "application/pdf";
                    break;
            }
            if (contenttype != String.Empty)
            {
                Stream fs = file.InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                byte[] compressedData = Compress(bytes);
                //Update the file into database
                string strQuery = "insert into Qchecklist_NCRdoc(NCR_DocName,NCR_DocContentType,NCR_Document,Transaction_ID) values(@NCR_DocName, @NCR_DocContentType,@NCR_Document, @Transaction_ID)";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.Add("@NCR_DocName", SqlDbType.VarChar).Value = filename;
                cmd.Parameters.Add("@NCR_DocContentType", SqlDbType.VarChar).Value = contenttype;
                cmd.Parameters.Add("@NCR_Document", SqlDbType.Binary).Value = compressedData;
                cmd.Parameters.AddWithValue("@Transaction_ID", Tid);
                UpdateData(cmd);
                // lblMessage.ForeColor = System.Drawing.Color.Green;
                // lblMessage.Text = "File Uploaded Successfully";
            }
            else
            {
                // lblMessage.ForeColor = System.Drawing.Color.Red;
                // lblMessage.Text = "File format not recognised." + " Upload Image/Word/PDF/Excel formats";
            }
        }
        private Boolean UpdateData(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        protected void chkSWIrequired_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRWCrequired.Checked)
            {
                tblRWC.Visible = true;
                Div1.Visible = true;
                Div2.Visible = false;
                Div3.Visible = false;
                trSave.Visible = false;
                dtRWCdate.SelectedDate = DateTime.Now;
            }
            else
            {
                tblRWC.Visible = false;
                Div1.Visible = false;
                Div2.Visible = false;
                Div3.Visible = false;
                trSave.Visible = false;
            }
        }
        protected void fqesubmit_Click(object sender, EventArgs e)
        {
            //Insert RWC info parent
            string rwcNumber = lblRWCNO.Text;
            string rwcdate = dtRWCdate.SelectedDate.Date.ToString("yyyy-MM-dd");
            string subContractor = txtSubcontractors.Text.Replace("'", "''");
            string powoNo = txtPoWoNo.Text.Replace("'", "''"); ;
            string NCRFINCC = txtNCRFINCC.Text.Replace("'", "''");
            string Pack = txtDescof.Text.Replace("'", "''");
            string Package = Pack.Replace("'", "''");
            string ncDetails = txtNCDetails.Text.Replace("'", "''");
            string NCDeatils = ncDetails.Replace("'", "''");
            string ncissuedby = ddlissuedby.SelectedItem.Text;
            string defectcategory = ddlDefectCategory.SelectedItem.Text;
            string subdefectcategory = ddlSubdefect.SelectedItem.Text;
            string rootcausecategory = ddlRootCauseCategory.SelectedItem.Text;
            string rootcause = ddlRootCause.SelectedItem.Text;
            string why1 = txtWhy1.Text;
            string why2 = txtWhy2.Text;
            string why3 = txtWhy3.Text;
            string why4 = txtWhy4.Text;
            string why5 = txtWhy5.Text;
            //string reworkDueTo = string.Empty;
            //if (rdCustomerComplaint.Checked)
            //{
            //    reworkDueTo = "Customer Complaint";
            //}
            //else if (rdNCR.Checked)
            //{
            //    reworkDueTo = "NCR";
            //}
            //else if (rdFIN.Checked)
            //{
            //    reworkDueTo = "FIN";
            //}
            //else if (rdNote.Checked)
            //{
            //    reworkDueTo = "Note";
            //}
            //else if (rdEmail.Checked)
            //{
            //    reworkDueTo = "E-mail Reference";
            //}
            string detailsofNon = lbldesc.Text;
            foreach (ListItem item in rootCauseForNon.Items)
            {
                if (item.Selected)
                {
                    lblRootCause.Text += item.Text + ",";
                }
            }
            string RWCrequired = string.Empty;
            if (chkRWCrequired.Checked)
            {
                string ErrMsg = string.Empty;
                try
                {
                    string stage = "Pending with PE";
                    RWCrequired = "Yes";
                    string projectName = ddlProjectCode.SelectedItem.Text;
                    //DataTable dtProjectCode = objQuality.GetProjectCodeByProjectName(projectName);
                    //for (int i = 0; i < dtProjectCode.Rows.Count; i++)
                    //{
                    string projectCode1 = projectName.Split('-')[1];
                    SPListItem projectCodeFromPMRCM = objQuality.GetProjectCodeByProjectNameForRCM(projectCode1);
                    if (projectCodeFromPMRCM["Planning_x0020_Engineer"] != null)
                    {
                        string strPE = projectCodeFromPMRCM["Planning_x0020_Engineer"].ToString();
                        if (!string.IsNullOrEmpty(strPE))
                        {
                            SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPE);
                            lblPEEmail.Text = userValue.User.Email.ToString();
                            lblPE.Text = userValue.User.Name;
                            lblPEID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                        }
                    }
                    else
                    {
                        if (ppPlanningEngineer.ResolvedEntities.Count > 0)
                        {
                            PickerEntity pckEntity = (PickerEntity)ppPlanningEngineer.ResolvedEntities[0];
                            if (pckEntity != null)
                            {
                                string assignee = pckEntity.Key;
                                SPUser userAssignee = SPContext.Current.Web.EnsureUser(assignee);
                                if (userAssignee != null)
                                {
                                    string assigneeID = objQuality.GetOnlyEmployeeID(userAssignee.LoginName);
                                    lblPE.Text = userAssignee.Name;
                                    lblPEEmail.Text = userAssignee.Email;
                                    lblPEID.Text = objQuality.GetOnlyEmployeeID(userAssignee.LoginName);
                                }
                            }
                        }
                        else
                        {
                            string message = "You must specify a value for Planning Engineer";
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            sb.Append("<script type = 'text/javascript'>");
                            sb.Append("window.onload=function(){");
                            sb.Append("alert('");
                            sb.Append(message);
                            sb.Append("')};");
                            sb.Append("</script>");
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                            return;
                        }
                    }
                    //}
                    if (uploadEvidenNCR.HasFile || uploadEvidenNCR.HasFiles)
                    {
                        IList<System.Web.HttpPostedFile> files = uploadEvidenNCR.PostedFiles;
                        foreach (HttpPostedFile file in files)
                        {
                            InsertNCRDocument(file, lblTransactionID.Text);
                        }
                    }
                    objQuality.InsertRWCInfoInChild(lblTransactionID.Text, rwcNumber, rwcdate, Package, subContractor, powoNo, NCRFINCC, NCDeatils, lblRootCause.Text);
                    objQuality.UpdateRWCInParentforPE(lblTransactionID.Text, stage, "NULL", RWCrequired, lblPEID.Text, lblPE.Text, lblPEEmail.Text, ncissuedby, defectcategory, subdefectcategory,rootcausecategory,rootcause,why1,why2,why3,why4,why5);
                    bool isMailSentPE = objQuality.SendPEtoRCMMailforFQP(lblRCMEmail.Text, lblPE.Text, lblPEEmail.Text, lblFQEName.Text, lblFQEEmail.Text, ddlProjectCode.SelectedItem.Text, lblActivity.Text, lblSubActivity.Text, ddlLocations.SelectedItem.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                }
                catch (Exception ex)
                {
                    ErrMsg = ex.Message;
                }
            }
            else
            {
                //string projectName = ddlProjectCode.SelectedItem.Text;
                //DataTable dtProjectCode = objQuality.GetProjectCodeByProjectName(projectName);
                //for (int i = 0; i < dtProjectCode.Rows.Count; i++)
                //{
                string projectCode1 = ddlProjectCode.SelectedItem.Text.Split('-')[1];
                SPListItem projectCodeFromPMRCM = objQuality.GetProjectCodeByProjectNameForRCM(projectCode1);
                if (projectCodeFromPMRCM["Planning_x0020_Engineer"] != null && chkRWCrequired.Checked)
                {
                    string RWCrequiredNo = "No";
                    string stage = "Pending with PE";
                    string strPE = projectCodeFromPMRCM["Planning_x0020_Engineer"].ToString();
                    if (!string.IsNullOrEmpty(strPE))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPE);
                        lblPEEmail.Text = userValue.User.Email.ToString();
                        lblPE.Text = userValue.User.Name;
                        lblPEID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                    }
                    objQuality.UpdateRWCInParent(lblTransactionID.Text, stage, "NULL", RWCrequiredNo, lblRCMID.Text, lblRCM.Text, lblRCMEmail.Text);
                    bool isMailSentPE = objQuality.SendPEtoRCMMailforFQP(lblRCMEmail.Text, lblPE.Text, lblPEEmail.Text, lblFQEName.Text, lblFQEEmail.Text, ddlProjectCode.SelectedItem.Text, lblActivity.Text, lblSubActivity.Text, ddlLocations.SelectedItem.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                }
                else
                {
                    string RWCrequiredNo = "No";
                    string stage = "Pending with RCM";
                    //string projectNameRCM = ddlProjectCode.SelectedItem.Text;
                    //DataTable dtProjectCodeRCM = objQuality.GetProjectCodeByProjectName(projectNameRCM);
                    //for (int j = 0; j < dtProjectCode.Rows.Count; j++)
                    //{
                    string projectCode1RCM = ddlProjectCode.SelectedItem.Text.Split('-')[1];
                    SPListItem projectCodeFromPMRCM1 = objQuality.GetProjectCodeByProjectNameForRCM(projectCode1RCM);
                    if (projectCodeFromPMRCM1["RCM"] != null)
                    {
                        string strRCM = projectCodeFromPMRCM1["RCM"].ToString();
                        if (!string.IsNullOrEmpty(strRCM))
                        {
                            SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM1.ParentList.ParentWeb, strRCM);
                            lblRCMEmail.Text = userValue.User.Email.ToString();
                            lblRCM.Text = userValue.User.Name;
                            lblRCMID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                        }
                    }
                    //}
                    objQuality.UpdateRWCInParent(lblTransactionID.Text, stage, "NULL", RWCrequiredNo, lblRCMID.Text, lblRCM.Text, lblRCMEmail.Text);
                    bool isMailSent = objQuality.SendRCMMailforFQP(lblRCM.Text, lblRCMEmail.Text, lblFQEName.Text, lblFQEEmail.Text, ddlProjectCode.SelectedItem.Text, lblActivity.Text, lblSubActivity.Text, ddlLocations.SelectedItem.Text, lblTransactionID.Text, lblPMEmail.Text, lblInduCoE.Text);
                }
                //}
            }
            fqesubmit.Enabled = false;
            //fqeCancel.Enabled = false;
            lblSWIsuccessNotification.Text = "Submitted successfully";
            string navigateUrl = Constants.link + "/Pages/FQPChecklists.aspx";
            lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to submit another FQP compliance", navigateUrl);
            btnSWISubmit.Enabled = false;
            btnSWIcancel.Enabled = false;
            // Page.Response.Redirect(Constants.FQEHomePage);
        }
        protected void ddlLocations_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlLocations.SelectedItem.Text) && ddlLocations.SelectedIndex > 0)
                {
                    string pcode = ddlProjectCode.SelectedItem.Text;
                    string[] code = pcode.Split('-');
                    string projcode = code[1];
                    DataTable dtSubLocations = objQuality.GetSubLocationsBylocations(ddlLocations.SelectedItem.Text, projcode);
                    if (dtSubLocations != null)
                    {
                        if (dtSubLocations.Rows.Count > 0)
                        {
                            ddlSubLocations.DataSource = dtSubLocations;
                            ddlSubLocations.DataTextField = "Sub Location";
                            ddlSubLocations.DataValueField = "Sub Location";
                            ddlSubLocations.DataBind();
                            ddlSubLocations.Items.Insert(0, "--Select--");
                            ddlSubLocations.Enabled = true;
                        }
                        else
                        {
                            lblError.Text = "No sub locations available";
                        }
                    }
                }
                else
                {
                    lblError.Text = "Please select valid location";
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int count = 0;
            int countNo = 0;
            foreach (GridViewRow gvr in grdCompliances.Rows)
            {
                RadioButton rdbYes = (RadioButton)gvr.FindControl("rdbYes");
                RadioButton rdbNo = (RadioButton)gvr.FindControl("rdbNo");
                RadioButton rdbNA = (RadioButton)gvr.FindControl("rdbNA");
                if (rdbYes.Checked || rdbNA.Checked)
                {
                    count += 1;
                }
                if (rdbNo.Checked)
                {
                    count += 1;
                    countNo += 1;
                }
            }
            lblSWIsuccessNotification.Text = "Submitted successfully";
            string navigateUrl = Constants.link + "/Pages/QMDCheklistForm.aspx";
            lblLinkClosureNotification.Text = string.Format("Click <a href=\'{0}'>here</a> to submit another QPR compliance", navigateUrl);
        }
        private Boolean InsertEvidenceDoc(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection(Constants.ConnectionStringQMDChecklists);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        protected void ddlProjectCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string projectName = ddlProjectCode.SelectedItem.Text;
            //DataTable dtProjectCode = objQuality.GetProjectCodeByProjectName(projectName);
            if (!string.IsNullOrEmpty(ddlProjectCode.SelectedItem.Text) && ddlProjectCode.SelectedIndex > 0)
            {
                //for (int i = 0; i < dtProjectCode.Rows.Count; i++)
                //{
                //    string projectCode = dtProjectCode.Rows[i][1].ToString();
                string projectCode = ddlProjectCode.SelectedItem.Text.Split('-')[1];
                lblProjectCode.Text = projectCode;
                DataTable dtLocationdata = objQuality.GetLocationsByProjectCode(projectCode);
                for (int j = 0; j < dtLocationdata.Rows.Count; j++)
                {
                    ddlLocations.DataSource = dtLocationdata;
                    ddlLocations.DataTextField = "Location";
                    ddlLocations.DataValueField = "Location";
                    ddlLocations.DataBind();
                    ddlLocations.Items.Insert(0, "--Select--");
                }
                SPListItem projectCodeFromPMRCM = objQuality.GetProjectCodeByProjectNameForRCM(projectCode);
                if (projectCodeFromPMRCM["RCM"] != null)
                {
                    string strRCM = projectCodeFromPMRCM["RCM"].ToString();
                    if (!string.IsNullOrEmpty(strRCM))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strRCM);
                        lblRCMEmail.Text = userValue.User.Email.ToString();
                        lblRCM.Text = userValue.User.Name;
                        lblRCMID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                    }
                }
                if (projectCodeFromPMRCM["PM"] != null)
                {
                    string strPM = projectCodeFromPMRCM["PM"].ToString();
                    if (!string.IsNullOrEmpty(strPM))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPM);
                        lblPMEmail.Text = userValue.User.Email.ToString();
                        lblPM.Text = userValue.User.Name;
                        lblPMID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                    }
                }
                if (projectCodeFromPMRCM["BU_x0020_Head"] != null)
                {
                    string strbuHead = projectCodeFromPMRCM["BU_x0020_Head"].ToString();
                    if (!string.IsNullOrEmpty(strbuHead))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strbuHead);
                        lblbuheadid.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                        lblbuheadName.Text = userValue.User.Name;
                        lblbuheadEmail.Text = userValue.User.Email.ToString();
                    }
                }
                if (projectCodeFromPMRCM["Head_x0020_Operations"] != null)
                {
                    string strHead = projectCodeFromPMRCM["Head_x0020_Operations"].ToString();
                    if (!string.IsNullOrEmpty(strHead))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strHead);
                        lblHeadEmail.Text = userValue.User.Email.ToString();
                        lblHead.Text = userValue.User.Name;
                        lblHeadID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                    }
                }
                if (projectCodeFromPMRCM["Planning_x0020_Engineer"] != null)
                {
                    string strPE = projectCodeFromPMRCM["Planning_x0020_Engineer"].ToString();
                    if (!string.IsNullOrEmpty(strPE))
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(projectCodeFromPMRCM.ParentList.ParentWeb, strPE);
                        lblPEEmail.Text = userValue.User.Email.ToString();
                        lblPE.Text = userValue.User.Name;
                        lblPEID.Text = objQuality.GetOnlyEmployeeID(userValue.User.LoginName);
                        lblPlaningEngi.Visible = true;
                        lblPlaningEngi.Text = lblPEEmail.Text;
                        ppPlanningEngineer.Visible = false;
                    }
                }
                else
                {
                    ppPlanningEngineer.Visible = true;
                    lblPlaningEngi.Visible = false;
                }
                //}
            }
            else
            {
            }
        }
        private byte[] Compress(byte[] data)
        {
            var output = new MemoryStream();
            using (var gzip = new GZipStream(output, CompressionMode.Compress, true))
            {
                gzip.Write(data, 0, data.Length);
                gzip.Close();
            }
            return output.ToArray();
        }

        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {


        }

        protected void ddlMainactivity_SelectedIndexChanged(object sender, EventArgs e)
        {
            string projectTypeID = ddlProjectTypes.SelectedItem.Value;
            string disciplineID = ddlDesciplines.SelectedItem.Value;
            string mainactivity = ddlMainactivity.SelectedItem.Value;
            DataTable dtActivitiesdata = objQuality.GetActivitiesforFQP(projectTypeID, disciplineID, mainactivity);
            if (dtActivitiesdata != null)
            {
                ddlActivities.DataSource = dtActivitiesdata;
                ddlActivities.DataTextField = "Activity";
                ddlActivities.DataValueField = "Activity";
                ddlActivities.DataBind();
                dvActivities.Visible = true;
                ddlActivities.Items.Insert(0, "--Select--");
            }
            else
            {
                lblError.Text = "There are no Activities for the selected filter";
            }
        }

        protected void ddlDefectCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string mainactivity = ddlMainactivity.SelectedItem.Value;
            string subactivity = ddlSubActivities.SelectedItem.Text;
            string defectcategory = ddlDefectCategory.SelectedItem.Text;
            DataTable dtsubdefectdata = objQuality.GetSubDefectCategoryforFQP(mainactivity, subactivity, defectcategory);
            if (dtsubdefectdata != null)
            {
                ddlSubdefect.DataSource = dtsubdefectdata;
                ddlSubdefect.DataTextField = "Sub_Defect_Category";
                ddlSubdefect.DataValueField = "Sub_Defect_Category";
                ddlSubdefect.DataBind();
                ddlSubdefect.Items.Insert(0, "--Select--");

            }
            else
            {
                lblError.Text = "There are no Main Defect Categories for the selected filter";
            }
        }

        protected void ddlSubdefect_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlRootCauseCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string rootcause = ddlRootCauseCategory.SelectedItem.Text;
            DataTable dtrootcause = objQuality.GetRootCauseforFQP(rootcause);
            if (dtrootcause != null)
            {
                ddlRootCause.DataSource = dtrootcause;
                ddlRootCause.DataTextField = "RootCause";
                ddlRootCause.DataValueField = "RootCause";
                ddlRootCause.DataBind();
                ddlRootCause.Items.Insert(0, "--Select--");

            }
            else
            {
                lblError.Text = "There are no Root Cause Categories for the selected filter";
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using System.Configuration;
using System.Data;
using System.IO;

namespace CloseSafetyUploads
{
    class CloseSafety
    {

        public void ProcessLogic()
        {
            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            string data = GetProjectCodes();
            string[] pcodes = data.Split(',');
            foreach (string code in pcodes)
            {
                UpdateForm(code);
            }

        }

        public string GetProjectCodes()
        {
            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            List<string> pcode = new List<string>();
            string pcodes = string.Empty;
            using (SPSite site = new SPSite(siteurl))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        SPList _list = web.Lists["Project PM RCM"];
                        SPQuery query = new SPQuery();
                        query.Query = "<Where><IsNull><FieldRef Name='Safety_x0020_Officer' /></IsNull></Where>";
                        SPListItemCollection items = _list.GetItems(query);
                        foreach (SPListItem item in items)
                        {
                            if (!pcode.Contains(item["Project_x0020_Code"].ToString()))
                            {
                                pcode.Add(item["Project_x0020_Code"].ToString());
                            }
                        }
                        if (pcode.Count > 0)
                        {
                            pcodes = string.Join(",", pcode);
                        }
                    });
                }
            }
            return pcodes;
        }

        public void UpdateForm(string pcode)
        {
            string siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
            string fpath = ConfigurationManager.AppSettings["fPath"].ToString();
            using (SPSite site = new SPSite(siteurl))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPList lst = null;
                    if (web.Lists["Site Safety Uploads"] != null)
                    {
                        lst = web.Lists["Site Safety Uploads"];
                        SPQuery query = new SPQuery();
                        //query.Query = "<Where><And><Eq><FieldRef Name='PCode' /><Value Type='Text'>" + pcode + "</Value></Eq><Neq><FieldRef Name='Stage' /><Value Type='Text'>Closed</Value></Neq></And></Where>";
                        query.Query = "<Where><And><Eq><FieldRef Name='Project_x0020_Code' /><Value Type='Calculated'>" + pcode + "</Value></Eq><Neq><FieldRef Name='ItemStatus' /><Value Type='Choice'>Closed</Value></Neq></And></Where>";
                        SPListItemCollection items = lst.GetItems(query);
                        foreach (SPListItem item in items)
                        {
                            //SPListItem lstitem = lst.GetItemById(660001);
                            DateTime todaysdate = DateTime.Today;
                            string todaydate = String.Format("{0:dd MMMM yyyy}", todaysdate);
                            item["Comments"] = "Closed on " + todaydate + " because the project was deactivated";
                            item["ItemStatus"] = "Closed";
                            item.Update();
                            StringBuilder sb = new StringBuilder();
                            sb.Append((int)item["ID"] + ",");
                            File.AppendAllText(fpath + "logclose.txt", sb.ToString());
                            sb.Clear();
                        }
                    }
                }
            }
        }

    }
}

﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChecklistsForm.ascx.cs" Inherits="QMDDashboard_TPL.ChecklistsModule.ChecklistsForm.ChecklistsForm" %>

<style type="text/css">
    .table {
        padding: 0;
        border-spacing: 0;
        text-align: center;
        font-family: 'Trebuchet MS';
        vertical-align: middle;
        border-collapse: collapse;
    }

    .auto-style1 {
        height: 30px;
    }

    .required {
        color: Red;
    }

    .auto-style2 {
        width: 60%;
        height: 30px;
    }

    .auto-style3 {
        width: 50%;
    }
</style>

<div id="dvMainForm" runat="server" visible="false">
    <table border="1" cellpadding="1" cellspacing="1" width="80%">
        <tr>
            <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label25" runat="server" ForeColor="White" Text="QPR Compliance" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label5" ForeColor="White" runat="server" Text="SBG"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:Label ID="lblSBG" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label23" ForeColor="White" runat="server" Text="SBU"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:Label ID="lblSBU" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>

        <tr>
            <td style="background-color: #507CD1; text-align: left; padding-left: 10px;" class="auto-style1">
                <asp:Label ID="Label1" ForeColor="White" runat="server" Text="BU"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; text-align: left;" class="auto-style2">
                <asp:Label ID="lblBU" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 30%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label3" ForeColor="White" runat="server" Text="Project"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlProjectCode" runat="server" OnSelectedIndexChanged="ddlProjectCode_SelectedIndexChanged" Width="100%" AutoPostBack="true"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFProject" runat="server" ErrorMessage="Please Select Project" InitialValue="--Select--" ControlToValidate="ddlProjectCode" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
                <%-- <asp:Label ID="lblProject" runat="server"></asp:Label>--%>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label6" ForeColor="White" runat="server" Text="Location"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlLocations" runat="server" OnSelectedIndexChanged="ddlLocations_SelectedIndexChanged" Width="100%" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ddlLocation" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--" ControlToValidate="ddlLocations" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label8" ForeColor="White" runat="server" Text="Sub Location"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlSubLocations" Enabled="false" runat="server" Width="100%" ValidationGroup="Req1">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlSubLocations" runat="server" ErrorMessage="Please Select Sub Location" InitialValue="--Select--" ControlToValidate="ddlSubLocations" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label7" ForeColor="White" runat="server" Text="Project Type"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlProjectTypes" runat="server" Width="100%"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlProjectTypes" runat="server" ErrorMessage="Please Select Project Type" InitialValue="--Select--" ControlToValidate="ddlProjectTypes" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label9" ForeColor="White" runat="server" Text="Discipline"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:DropDownList ID="ddlDesciplines" runat="server" OnSelectedIndexChanged="ddlDesciplines_SelectedIndexChanged" Width="100%" AutoPostBack="true"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlDesciplines" runat="server" ErrorMessage="Please Select Desciplines" InitialValue="--Select--" ControlToValidate="ddlDesciplines" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td colspan="2"></td>
        </tr>

    </table>
</div>
<div id="dvMainactivity" runat="server" visible="false">
    <table style="width: 80%" border="1">
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label22" ForeColor="White" runat="server" Text="Main Activity"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlMainactivity" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlMainactivity_SelectedIndexChanged"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlMainactivity" runat="server" ErrorMessage="Please Select Main Activity" InitialValue="--Select--" ControlToValidate="ddlMainactivity" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</div>
<div id="dvActivities" runat="server" visible="false">
    <table style="width: 80%" border="1">
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label10" ForeColor="White" runat="server" Text="Activity"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlActivities" runat="server" OnSelectedIndexChanged="ddlActivities_SelectedIndexChanged" AutoPostBack="true" Width="100%"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlActivities" runat="server" ErrorMessage="Please Select Activity" InitialValue="--Select--" ControlToValidate="ddlActivities" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</div>
<div id="dvSubActivities" runat="server" visible="false">
    <table style="width: 80%" border="1">
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label11" ForeColor="White" runat="server" Text="Sub Activity"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlSubActivities" runat="server" OnSelectedIndexChanged="ddlSubActivities_SelectedIndexChanged" AutoPostBack="true" Width="100%"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RFddlSubActivities" runat="server" ErrorMessage="Please Select Sub Activity" InitialValue="--Select--" ControlToValidate="ddlSubActivities" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <table style="width: 80%" border="1">
        <tr>
            <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="Label21" runat="server" ForeColor="White" Text="Identification"></asp:Label>
            </td>
            <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
                <asp:TextBox ID="txtidentify" Width="97%" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFIdentify" runat="server" ErrorMessage="Please enter text" InitialValue="" ControlToValidate="txtidentify" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
    </table>
</div>
<div id="dvCompliances" runat="server" visible="false">
    <table style="width: 80%" border="1">
        <tr>
            <td style="background-color: #507CD1; width: 20%; text-align: left; height: 30px; padding-left: 10px;">
                <asp:Label ID="lblGridHeading" ForeColor="White" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="grdCompliances" runat="server" CellPadding="4" AutoGenerateColumns="false" Width="100%" ForeColor="#333333" GridLines="Both" OnSelectedIndexChanged="grdCompliances_SelectedIndexChanged" OnRowDataBound="grdCompliances_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="Compliance" HeaderText="Prerequisite" />
                        <asp:TemplateField HeaderText="Complied">
                            <ItemTemplate>
                                <asp:RadioButton ID="rdbYes" Text="Yes" runat="server" GroupName="GC" ValidationGroup="GC" /><br />
                                <asp:RadioButton ID="rdbNo" Text="No" GroupName="GC" ValidationGroup="GC" runat="server" /><br />
                                <asp:RadioButton ID="rdbNA" Text="NA" GroupName="GC" ValidationGroup="GC" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:TextBox ID="txtFQECompRemarks" TextMode="MultiLine" runat="server" Width="95%"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Upload Evdence :
                <asp:FileUpload ID="uploadEvidenFUP" runat="server" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.PDF|.png|.jpg|.gif|.xls|.xlsx|.DOC|.DOCX|.XLS|.XLSX|.PNG|.JPG|.JPEG|.GIF)$"
                    ControlToValidate="uploadEvidenFUP" runat="server" ForeColor="Red" ErrorMessage="Please select a valid word,excel,jpg,png,gif or pdf file format."
                    Display="Dynamic" />
            </td>

            <br />
        </tr>
        <tr>
            <asp:Label ID="lblrb" runat="server"></asp:Label>
            <td style="text-align: center">
                <asp:Button ID="btnSave" Font-Bold="true" runat="server" OnClick="btnSave_Click" Text="Save" />
            </td>
        </tr>

    </table>
</div>

<asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>

<asp:Label ID="lblRCM" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblRCMID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblRCMEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPM" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPMID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPMEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblProjectCode" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblHead" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblHeadID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblHeadEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPE" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPEID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblPEEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblNewSBU" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblFQEName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblFQEEmail" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblTransactionID" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblInduCoE" runat="server" Visible="false"></asp:Label>

<asp:Label ID="lblbuheadid" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblbuheadName" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblbuheadEmail" runat="server" Visible="false"></asp:Label>
<table id="swiReq" runat="server" visible="false" style="width: 80%; text-align: center" border="1">
    <tr>
        <td colspan="2" style="background-color: #507CD1; width: 20%; text-align: center; height: 30px; padding-left: 10px;">
            <asp:Label ID="Label20" ForeColor="White" Font-Bold="true" runat="server" Text="Is SWI form required"></asp:Label>
            <asp:CheckBox ID="chkSWIrequired" OnCheckedChanged="chkSWIrequired_CheckedChanged" AutoPostBack="true" runat="server" />
        </td>

    </tr>

</table>
<table id="isNotSWI" runat="server" visible="false" style="width: 80%; text-align: center" border="1">
    <tr id="trSave" runat="server" style="text-align: center">
        <td>
            <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
            <%--<asp:Button ID="btnCancel"  runat="server" Text="Cancel" />--%>
        </td>

    </tr>
    <tr>
        <td>
            <asp:Label ID="lblMainSuccessNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" runat="server"></asp:Label>
        </td>
    </tr>
</table>

<table border="1" style="width: 80%; font-family: 'Trebuchet MS'; font-size: 14px" id="tblSWI" runat="server" visible="false">
    <tr>
        <td style="background-color: #507CD1; width: 40%; text-align: left; height: 30px; padding-left: 10px;">
            <asp:Label ID="lblissuedby" ForeColor="White" runat="server" Text="SWI Issued BY"></asp:Label>
        </td>
        <td style="background-color: #fff; padding-left: 10px; width: 60%; text-align: left; height: 30px;">
            <asp:DropDownList ID="ddlissuedby" runat="server" Width="100%" AutoPostBack="true">
                <asp:ListItem>--Select--</asp:ListItem>
                <asp:ListItem>FQE</asp:ListItem>
                <asp:ListItem>Customer</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvissuedby" runat="server" ErrorMessage="Please Select SWI Issued BY" InitialValue="--Select--" ControlToValidate="ddlissuedby" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table>

                <tr>
                    <td>
                        <asp:Image ID="imgTataLogo" ImageUrl="../../_layouts/15/images/TataLogo.jpg" runat="server" />
                    </td>
                    <td style="width: 100%;">
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="Label12" runat="server" Text="TATA PROJECTS LTD" Font-Size="Large"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="Label13" runat="server" Text="STOP WORK INSTRUCTION(SWI)" Font-Size="Large"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>

    </tr>
    <tr>
        <td class="auto-style3">&nbsp;
            <asp:Label ID="Label4" runat="server" Text="SWI No:" Style="font-weight: 600"></asp:Label>
            <asp:Label ID="lblSWINO" runat="server"></asp:Label>
        </td>

        <td>
            <table>
                <tr>
                    <td>&nbsp;<asp:Label ID="lblDateTime" runat="server" Text="Date & Time:" Style="font-weight: 600"></asp:Label></td>
                    <td>
                        <SharePoint:DateTimeControl ID="dtSWIdate" runat="server" Enabled="false" />
                    </td>
                </tr>
            </table>


        </td>
    </tr>
    <tr style="height: 28px">
        <td colspan="2">&nbsp;
            <asp:Label ID="Label14" runat="server" Style="font-weight: 600" Text="Project/Location :"></asp:Label>
            <asp:Label ID="lblProjectLocation" runat="server" Text="Auto populated"></asp:Label>
        </td>
    </tr>
    <tr style="height: 28px">
        <td class="auto-style3">&nbsp;<asp:Label ID="Label28" runat="server" Style="font-weight: 600" Text="Package / Equipment :"></asp:Label>
            <asp:TextBox ID="txtPackage" runat="server" Width="50%"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RFpackage" runat="server" ErrorMessage="Package / Equipment details are mandatory" InitialValue="" ControlToValidate="txtPackage" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>&nbsp;<asp:Label ID="Label29" runat="server" Style="font-weight: 600" Text="Subcontractor:"></asp:Label>
            <asp:TextBox ID="txtSubcontractors" runat="server" Width="60%"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RFsubcontractor" runat="server" ErrorMessage="Sub Contractor details are mandatory" InitialValue="" ControlToValidate="txtSubcontractors" CssClass="required" Display="Dynamic"></asp:RequiredFieldValidator>
            &nbsp;</td>
    </tr>
    <tr style="height: 28px">
        <td class="auto-style3">&nbsp;<asp:Label ID="Label27" Font-Bold="true" runat="server" Text="Project type :"></asp:Label>
            <asp:Label ID="lblProjectType" runat="server"></asp:Label>
        </td>
        <td>&nbsp;<asp:Label ID="Label24" Font-Bold="true" runat="server" Text="Discipline :"></asp:Label>
            <asp:Label ID="lblDiscipline" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 28px">
        <td class="auto-style3">&nbsp;<asp:Label ID="Label15" Font-Bold="true" runat="server" Width="30%" Text="Activity: "></asp:Label><asp:Label ID="lblActivity" runat="server"></asp:Label>
        </td>
        <td>&nbsp;<asp:Label ID="Label26" Font-Bold="true" runat="server" Width="30%" Text="Sub-Activity: "></asp:Label><asp:Label ID="lblSubActivity" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 28px">
        <td class="auto-style3">&nbsp;<asp:Label ID="Label16" Font-Bold="true" runat="server" Width="23%" Text="Issued by :"></asp:Label><asp:Label ID="lblFQENameSWI" runat="server"></asp:Label>
        </td>
        <td>&nbsp;<asp:Label ID="Label17" Font-Bold="true" runat="server" Width="20%" Text="Email ID:"></asp:Label>
            <asp:Label ID="lblFQEEmailSWI" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 28px">
        <td class="auto-style3">&nbsp;<asp:Label ID="Label18" Font-Bold="true" runat="server" Width="23%" Text="Issued to :"></asp:Label><asp:Label ID="lblRCMNameSWI" runat="server"></asp:Label>
        </td>
        <td>&nbsp;<asp:Label ID="Label19" Font-Bold="true" runat="server" Width="21%" Text="Email ID:"></asp:Label>
            <asp:Label ID="lblRCMEmailSWI" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 18px">
        <td colspan="2" style="height: 30px">You are requested to stop the work immediately due to the following deficiencies observed in QPR compliance.
        </td>
    </tr>
    <tr>
        <td colspan="2" style="width: 100%">
            <asp:GridView ID="grdSWIComplainces" AutoGenerateColumns="false" Width="100%" runat="server">
                <Columns>
                    <asp:BoundField DataField="SNo" HeaderText="S.No" />
                    <asp:BoundField DataField="Compliance_Text" HeaderText="Prerequisite" />
                    <asp:BoundField DataField="isCompliant" HeaderText="Complied" />

                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
<table id="isSWISubmit" runat="server" visible="false" style="width: 80%; text-align: center" border="1">
    <tr>
        <td colspan="2" style="text-align: center">
            <asp:Button ID="btnSWISubmit" runat="server" Font-Bold="true" OnClick="btnSWISubmit_Click" Text="Submit" />
            <asp:Button ID="btnSWIcancel" runat="server" Font-Bold="true" Text="Cancel" />
        </td>
    </tr>
</table>

<asp:HyperLink ID="hplNavigateUrl" runat="server" Target="_self" Visible="false">Here</asp:HyperLink>
<div id="dvUnAuth" runat="server" visible="false">
    <asp:Label ID="Label2" runat="server" ForeColor="Red" Font-Size="Large" Text="You are not authorized to access"></asp:Label>
</div>
<table>
    <tr>
        <td>
            <asp:Label ID="lblSWIsuccessNotification" Font-Bold="true" ForeColor="Blue" Font-Size="Medium" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblLinkClosureNotification" runat="server"></asp:Label>
        </td>
    </tr>
</table>


